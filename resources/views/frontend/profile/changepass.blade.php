@extends('frontend.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="">Profile</a>
      <span class="breadcrumb-item active">Đổi mật khẩu</span>
    </nav>
</div>
<div class="br-pagetitle" style="justify-content: space-between;">
	<h4>Thay đổi mật khẩu</h4>
</div>
<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<div>{{$msg}}</div>
			<div class="bd rounded table-responsive mt-3">
				<form action="{{route('user.profile.changepass')}}" method="post" role="form">
					@csrf
					<input name="action" type="hidden" value="changepass">
					<table class="table table_border_none">
						<tr>
							<td width="100">Old Password</td>
							<td><input type="password" class="form-control" style="width: 100%;" id="old_password" name="old_password" placeholder="Enter Old password"></td>
						</tr>
						<tr>
							<td>New Password</td>
							<td><input type="password" class="form-control" style="width: 100%;" id="password" name="password" placeholder="Password"></td>
						</tr>
						<tr>
							<td>Re-Password</td>
							<td><input type="password" class="form-control" style="width: 100%;" id="re_password" name="re_password" placeholder="Re Password"></td>
						</tr>
						<tr class="form-actions">
							<td></td>
							<td><button type="submit" class="btn btn-primary btn-block active">Change Password</button></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</section>
</div>
@endsection