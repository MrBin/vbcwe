<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>AdminCP</title>
    <link href="{{ asset('assets/admin/lib/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/admin/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/admin/lib/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/admin/css/bracket.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/admin/css/jquery.growl.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/admin/css/css_custom.css') }}" rel="stylesheet" type="text/css">
    @yield('CSS_HEADER_MORE')
    @yield('JS_HEADER_MORE')
</head>

<body class="">
    <div class="br-logo"><a href=""><span>[&nbsp;&nbsp;</span>THANG<i>DUNG</i><span>&nbsp;&nbsp;]</span></a></div>

    @include("frontend.sidebar")
    @include("frontend.header")
    @include("frontend.right")

    <div class="br-mainpanel">
    	@yield('CONTAINER')
    </div>

    @include("frontend.footer")

    <script type="text/javascript">
    	var urlUpload = '{{ Config("app.upload_url") }}';
    </script>

    <script src="{{ asset('assets/js/lib/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/jquery-ui/ui/widgets/datepicker.js') }}"></script>
    <script src="{{ asset('assets/js/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/bracket.js') }}"></script>
    <script src="{{ asset('assets/js/library.js?v=11122') }}"></script>
	<script src="{{ asset('assets/js/jquery.growl.js') }}"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    @yield('JS_FOOTER')
    @include("admin.flash-message")

</body>

</html>