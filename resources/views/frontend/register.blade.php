<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="{{ asset('assets/admin/lib/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('assets/admin/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('assets/admin/css/bracket.css') }}" rel="stylesheet" type="text/css">
</head>
<body class="hold-transition login-page">

	<div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v">
		<div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white rounded shadow-base">
			<div class="signin-logo tx-center tx-28 tx-bold tx-inverse"><span class="tx-normal"></span> Đăng ký <span class="tx-info">tài khoản</span> <span class="tx-normal"></span></div>
			<form action="{{route('authen.register')}}" method="post" class="mg-t-20">
        		@csrf
        		<div class="form-group">
					<input type="text" name="username"  class="form-control" placeholder="Họ và tên">
				</div><!-- form-group -->
				<div class="form-group">
					<input type="text" name="loginname"  class="form-control" placeholder="Tên đăng nhập">
				</div>
				<div class="form-group">
					<input type="text" name="phone"  class="form-control" placeholder="Số điện thoại">
				</div>
				<div class="form-group">
					<input type="password" name="password" class="form-control" placeholder="Password">
				</div><!-- form-group -->
				<div class="form-group">
					<input type="password" name="re_password" class="form-control" placeholder="Nhập lại Password">
					<a href="{{ route('authen.login')}}" class="tx-info tx-12 d-block mg-t-10" style="">Đã có tài khoản? Đăng nhập ngay!</a>
				</div>
				<input type="hidden" name="action" value="execute">
				<button type="submit" class="btn btn-info btn-block mg-t-20">Đăng ký</button>
			</form>
		</div><!-- login-wrapper -->
	</div><!-- d-flex -->

<!-- /.login-box -->
<script language="javascript" src="{{ asset('assets/admin/lib/jquery/jquery.min.js') }}"></script>

</body>
</html>
