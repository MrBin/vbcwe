@extends('frontend.layouts')
@section('CONTAINER')
@php
	use App\Helper\Upload;
	use App\Repositories\BillCodeRepository;
@endphp
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="">Vận đơn</a>
      <span class="breadcrumb-item active">Danh sách</span>
    </nav>
</div>
<div class="br-pagetitle" style="justify-content: space-between;">
	<h4>Danh sách vận đơn</h4>
	<div class=""><a href="{{ route('user.bills.create') }}" class="btn btn-info">Khai báo vận đơn</a></div>
</div>
<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
        	<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="">
  	  			<div class="row">
		            <div class="col-6 col-lg-2">
		              <input class="form-control" name="code" placeholder="Mã vận đơn" type="text" value="{{ isset($dataSearch['code']) ? $dataSearch['code'] : '' }}">
		            </div>
		            <div class="col-6 col-lg-2">
		            	<select class="form-control select_status" name="status" data-placeholder="Chọn trạng thái">
							<option label="Chọn trạng thái"></option>
							@foreach($arrStatus as $key => $value)
								<option value="{{ $key }}" @if($key == $dataSearch['status']) selected="selected" @endif>{{ $value }}</option>
							@endforeach
						</select>
		            </div>
		            <div class="col-6 col-lg-2">
		            	<input type="text" name="date_start" id="date_start" class="form-control fc-datepicker" placeholder="DD/MM/YYYY" value="{{ isset($dataSearch['date_start']) ? $dataSearch['date_start'] : '' }}">
		            </div>
		             <div class="col-6 col-lg-2">
		            	<input type="text" name="date_end" id="date_end" class="form-control fc-datepicker" placeholder="DD/MM/YYYY" value="{{ isset($dataSearch['date_end']) ? $dataSearch['date_end'] : '' }}">
		            </div>
		            <div class="col-6 col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>
  	    	<div class="bd rounded table-responsive mt-3">
		  	  	<table class="table table-bordered table-striped mg-b-0">
	              	<thead>
	                	<tr>
							<th class="wd-5p text-center">STT</th>
							<th class="wd-10p">Mã vận đơn</th>
							<th class="wd-25p">Sản phẩm</th>
							<th class="wd-10p text-center">Số lượng</th>
							<th class="wd-10p text-center">Đơn vị</th>
							<th class="wd-15p text-center">Trạng thái</th>
							<th class="wd-15p text-center">Ngày tạo</th>
							<th class="wd-10p text-center"></th>
	                	</tr>
	              	</thead>
	              	<tbody>
		              	@foreach ($bills as $bill)
							<tr id="record_id_{{ $bill->ubc_id }}">
					          	<td class="text-center">{{ $loop->index + 1 + ($pagination['current_page'] - 1) * $pagination['perpage'] }}</td>
					          	<td class="bill_code">{{ $bill->ubc_bill_code }}</td>
					          	<td>{{ $bill->ubc_product_name }}</td>
					          	<td class="text-center">{{ $bill->ubc_quantity }}</td>
					          	<td class="text-center">{{ $bill->ubc_unit }}</td>
					          	<td class="text-center">@isset ($arrStatus[$bill->ubc_status]) {{ $arrStatus[$bill->ubc_status] }} @endisset</td>
				          		<td class="text-center">{{ date("d/m/y H:i", $bill->ubc_created_at) }}</td>
				          		<td class="text-center">
				          			<a  href="javascript:;" onClick="viewDetail({{$bill->ubc_id}});" title="Xem chi tiết" class="text-info"><i class="icon ion-eye" style="font-size: 16px;"></i></a>&nbsp;
				          			@if(BillCodeRepository::userCanEdit($bill->ubc_status)) <a href="{{ route('user.bills.create', ['id' => $bill->ubc_id]) }}" title="Chỉnh sửa"><i class="icon ion-compose" style="font-size: 16px;"></i></a>&nbsp; @endif
				          			@if(BillCodeRepository::userCanDelete($bill->ubc_status)) <a href="javascript:;" onClick="deleteBill({{$bill->ubc_id}});" title="Xóa" class="text-danger"><i class="icon ion-trash-a" style="font-size: 16px;"></i></a> @endif
				          		</td>
				        	</tr>
			      		@endforeach
	              </tbody>
	            </table>
	        </div>
        </div>
    </section>
    @include("frontend.pagination")
</div>

<div class="modal fade" id="modalViewDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Chi tiết mã vận đơn: <span id="bill_code_detail"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="font-size: 13px;">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection


@section('JS_FOOTER')
<script type="text/javascript">
   $(document).ready(function(){

   		$('#date_start').datepicker({dateFormat: 'dd/mm/yy'});
   		$('#date_end').datepicker({dateFormat: 'dd/mm/yy'});

   		if($().select2) {
			$('.select_status').select2({
				minimumResultsForSearch: '',
				placeholder: 'Chọn trạng thái'
			});

			$('.select_level').select2({
				minimumResultsForSearch: Infinity,
				placeholder: 'Chọn độ khó'
			});
		}
   });

   function viewDetail(id) {
   		$.ajax({
   			url: "{{route('user.bills.get-detail')}}",
   			type: "POST",
   			data: {id: id},
   			success: function(data){
   				if(data.status == true){
   					$("#modalViewDetail #bill_code_detail").html(data.data.ubc_bill_code);
   					var htmlAdd = '<table class="table"><tbody>';
   					if(data.data.ubc_picture_url){
   						htmlAdd += '<tr><td class="text-center" style="border-top: none;" colspan="2"><img style="width: 100px;" src="' + data.data.ubc_picture_url + '"></td></tr>';
   					}
   					htmlAdd += '<tr><td style="border-top: none; white-space: nowrap;">Mã vận đơn</td><td style="border-top: none;">' + data.data.ubc_bill_code + '</td></tr>';
   					htmlAdd += '<tr><td style="white-space: nowrap;">Tên sản phẩm</td><td>' + data.data.ubc_product_name + '</td></tr>';
   					htmlAdd += '<tr><td>Số lượng</td><td>' + data.data.ubc_quantity + ' / ' + data.data.ubc_unit + '</td></tr>';
   					htmlAdd += '<tr><td>Trạng thái</td><td>' + data.data.ubc_status_text + '</td></tr>';
   					htmlAdd += '<tr><td>Ghi chú</td><td>' + data.data.ubc_user_note + '</td></tr>';
   					htmlAdd += '<tr><td>Ngày tạo</td><td>' + data.data.ubc_created_at_text + '</td></tr>';
   					htmlAdd += '<tr><td style="white-space: nowrap;">Cập nhật cuối</td><td>' + data.data.ubc_updated_at_text + '</td></tr>';
   					htmlAdd +='</tbody></table>';
   					$("#modalViewDetail .modal-body").html(htmlAdd);
   					$('#modalViewDetail').modal({});
   				}
   			},
   			dataType: 'json'
   		});
   }

   function deleteBill(id) {
   		var bill_code = $("#record_id_" + id + ' .bill_code').text();
   		if(!confirm('Bạn chắc chắn muốn xóa mã vận đơn: ' + bill_code)) return false;
   		$.ajax({
   			url: "{{route('user.bills.delete')}}",
   			type: "POST",
   			data: {id: id},
   			success: function(data){
   				if(data.status == true){
   					$("#record_id_" + id).hide(300).remove();
   				}
   			},
   			dataType: 'json'
   		});
   }
</script>
@endsection

