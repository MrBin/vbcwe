<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Đăng nhập hệ thống</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="{{ asset('assets/admin/lib/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('assets/admin/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('assets/admin/css/bracket.css') }}" rel="stylesheet" type="text/css">
</head>
<body class="hold-transition login-page">
	<div class="pd-y-50 mg-t-20">
        <div class="modal d-block pos-static">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0 rounded-0 wd-xs-350 wd-xl-auto mg-xl-x-25">
              <div class="modal-body pd-0">
                <div class="row flex-row-reverse no-gutters">
                  <div class="col-xl-6">
                    <div class="pd-30">
                      <div class="pd-xs-x-30 pd-y-10">
                        <h5 class="tx-xs-28 tx-inverse mg-b-5">Welcome back!</h5>
                        <p>Đăng nhập tài khoản để tiếp tục</p>
                        <br>
                        <form action="" method="post" class="mg-t-20">
        					@csrf
	                        <div class="form-group">
	                          <input type="text" name="username" class="form-control pd-y-12" placeholder="Tên đăng nhập">
	                        </div><!-- form-group -->
	                        <div class="form-group mg-b-20">
	                          <input type="password" name="password" class="form-control pd-y-12" placeholder="Mật khẩu">

	                        </div>
	                        <input type="hidden" name="action" value="login">
	                        <input type="submit" class="btn btn-primary pd-y-12 btn-block" value="Đăng nhập">
						</form>
                        <div class="mg-t-30 mg-b-20 tx-12">Bạn chưa có tài khoản? <a href="{{route('authen.register')}}">Đăng ký</a></div>
                      </div>
                    </div><!-- pd-20 -->
                  </div><!-- col-6 -->
                  <div class="col-xl-6 bg-primary">
                    <div class="pd-40">
                      <h4 class="tx-white mg-b-20"><span>[</span> THẮNG DUNG + <span>]</span></h4>
                      <p class="tx-white op-7 mg-b-60">Thắng Dung Oder Taobao, 1688, Tmall Logistics Chính Ngạch Tiểu Ngạch</p>
                      <p class="tx-white tx-13">
                        <span class="tx-uppercase tx-medium d-block mg-b-15">LIÊN HỆ:</span>
                        <span class="op-7">Số điện thoại: 0904338333-0985446838</span>
                      </p>
                    </div>
                  </div><!-- col-6 -->
                </div><!-- row -->
              </div><!-- modal-body -->
            </div><!-- modal-content -->
          </div><!-- modal-dialog -->
        </div><!-- modal -->
      </div><!-- pd-y-50 -->

<!-- /.login-box -->
<script language="javascript" src="{{ asset('assets/admin/lib/jquery/jquery.min.js') }}"></script>

</body>
</html>
