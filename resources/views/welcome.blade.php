<div class="pd-y-50 bg-black-1 mg-t-20">
            <div class="modal d-block pos-static">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content bd-0 rounded-0 wd-xs-350 wd-xl-auto mg-xl-x-25">
                  <div class="modal-body pd-0">
                    <div class="row flex-row-reverse no-gutters">
                      <div class="col-xl-6">
                        <div class="pd-30">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <div class="pd-xs-x-30 pd-y-10">
                            <h5 class="tx-xs-28 tx-inverse mg-b-5">Welcome back!</h5>
                            <p>Sign in to your account to continue</p>
                            <br>
                            <div class="form-group">
                              <input type="email" name="email" class="form-control pd-y-12" placeholder="Enter your email">
                            </div><!-- form-group -->
                            <div class="form-group mg-b-20">
                              <input type="email" name="password" class="form-control pd-y-12" placeholder="Enter your password">
                              <a href="" class="tx-12 d-block mg-t-10">Forgot password?</a>
                            </div><!-- form-group -->

                            <button class="btn btn-primary pd-y-12 btn-block">Sign In</button>

                            <div class="mg-t-30 mg-b-20 tx-12">Don't have an account yet? <a href="">Sign Up</a></div>
                          </div>
                        </div><!-- pd-20 -->
                      </div><!-- col-6 -->
                      <div class="col-xl-6 bg-primary">
                        <div class="pd-40">
                          <h4 class="tx-white mg-b-20"><span>[</span> bracket + <span>]</span></h4>
                          <p class="tx-white op-7 mg-b-60">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                          <p class="tx-white tx-13">
                            <span class="tx-uppercase tx-medium d-block mg-b-15">Our Address:</span>
                            <span class="op-7">Ayala Center, Cebu Business Park, Cebu City, Cebu, Philippines 6000</span>
                          </p>
                        </div>
                      </div><!-- col-6 -->
                    </div><!-- row -->
                  </div><!-- modal-body -->
                </div><!-- modal-content -->
              </div><!-- modal-dialog -->
            </div><!-- modal -->
          </div><!-- pd-y-50 -->