<header>
    <div class="container">
        <div class="row">
            <div class="col-4 col-md-4 col-sm-4 col-lg-2">
                <div class="logo">
                    <a href="/">
                        <img src="{{ asset('images/logo.png') }}" alt="Vbcwe">
                    </a>
                </div>
                <div class="logo logo-white">
                    <a href="/">
                        <img src="{{ asset('images/logo_white.png') }}" alt="Vbcwe">
                    </a>
                </div>
            </div>
            <div class="col-8 col-md-8 col-sm-12 col-lg-8 d-none d-xl-block">
                <ul class="vbc-menu">
                    {!! $htmlMenu['menuDesktop'] !!}
                </ul>
            </div>
            <div class="col-8 col-md-8 col-sm-8 col-lg-2">
                <div class="header-right header-right__desktop">
                    <div class="vbc-lang">
                        <a href="javascript:;" data-lang="vi">
                            <img src="{{ asset('images/icon-vn.png') }}" alt="">
                        </a>
                        <a href="javascript:;" data-lang="en">
                            <img src="{{ asset('images/icon-en.png') }}" alt="">
                        </a>
                    </div>
                    <div class="vbc-search">
                        <form action="{{ route('search') }}" class="search-form">
                            <img src="{{ asset('images/icon-search.png') }}" alt="">
                            <input type="text" class="form-control" name="name" value="">
                        </form>
                    </div>
                </div>

                <div class="nav_mobile drawer drawer--right drawer-close d-xl-none" id="menu_mobile">
                    <button type="button" class="drawer-toggle drawer-hamburger"><span class="sr-only">toggle navigation</span><span class="drawer-hamburger-icon"></span></button>
                    <nav class="drawer-nav" role="navigation">
                        <ul class="drawer-menu">
                            {!! $htmlMenu['menuMobile'] !!}
                        </ul>
                        <div class="d-xl-none">
                            <div class="header-right">
                                <div class="vbc-lang">
                                    <a href="javascript:;">
                                        <img src="{{ asset('images/icon-vn.png') }}" alt="">
                                    </a>
                                    <a href="javascript:;">
                                        <img src="{{ asset('images/icon-en.png') }}" alt="">
                                    </a>
                                </div>
                                <div class="vbc-search">
                                    <form action="" class="search-form">
                                        <img src="{{ asset('images/icon-search.png') }}" alt="">
                                        <input type="text" class="form-control">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </nav>
                    <div class="drawer-overlay drawer-toggle"></div>
                </div>
            </div>
        </div>
    </div>
</header>
