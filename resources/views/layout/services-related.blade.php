<?php
use App\Helper\Upload as UploadHelper;
?>
<div class="news-related">
    <div class="news-header--title">Dịch vụ liên quan</div>
    <div class="row">
        <div class="col-12 col-md-12 col-sm-12 col-lg-8">
            <div class="box-news-related">
                <div class="row">
                    @foreach($servicesRelated as $vServices)
                    <div class="col-12 col-md-6 col-sm-12 col-lg-6">
                        <div class="news-related--item">
                            <a href="{{ url('dich-vu', [$vServices->ser_name_rewrite, $vServices->ser_id]) }}" class="nri-img">
                                <img src="{{ UploadHelper::getUrlImage('service', $vServices->ser_picture) }}" alt="">
                            </a>
                            <div class="nri-content">
                                <div class="nri-title">
                                    <a href="{{ url('dich-vu', [$vServices->ser_name_rewrite, $vServices->ser_id]) }}" title="{{ $vServices->ser_title }}">{{ $vServices->ser_title }}</a>
                                </div>
                                <div class="nri-sub">
                                    <div class="nri-date">Ngày {{ date('d/m/Y', $vServices->ser_create_time) }}</div>
                                    <a href="{{ url('dich-vu', [$vServices->ser_name_rewrite, $vServices->ser_id]) }}" class="nri-link"> > Tìm hiểu thêm</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-sm-12 col-lg-4">
            <div class="box-category bg-white">
                <div class="category-title">Danh mục tin tức</div>
                <ul>
                    <li>
                        <a href="#">
                            <span>Tin doanh nghiệp</span>
                            <span class="arrow-white">
                                <img src="{{ asset('images/arrow-white.png') }}" alt="">
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span>Tin thời sự</span>
                            <span class="arrow-white">
                                <img src="{{ asset('images/arrow-white.png') }}" alt="">
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span>Tin gói dịch vụ</span>
                            <span class="arrow-white">
                                <img src="{{ asset('images/arrow-white.png') }}" alt="">
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span>Tin tức khác</span>
                            <span class="arrow-white">
                                <img src="{{ asset('images/arrow-white.png') }}" alt="">
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="box-follow bg-white">
                <div class="category-title">Theo dõi tin tức</div>
                <ul>
                    <li class="bf-icon">
                        <a href="#" class="bf-insta"></a>
                    </li>
                    <li class="bf-icon">
                        <a href="#" class="bf-facebook"></a>
                    </li>
                    <li class="bf-icon">
                        <a href="#" class="bf-youtube"></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
