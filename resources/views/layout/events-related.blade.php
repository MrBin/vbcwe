<?php
use App\Helper\Upload as UploadHelper;
?>
<div class="news-related">
    <div class="news-header--title">{{ __('Sự kiện liên quan') }}</div>
    <div class="row">
        <div class="col-12 col-md-12 col-sm-12 col-lg-8">
            <div class="box-news-related">
                <div class="row">
                    @foreach($eventsRelated as $events)
                    <div class="col-12 col-md-6 col-sm-12 col-lg-6">
                        <div class="news-related--item">
                            @if($events->eve_new == 0)
                                <a href="{{ url('hoat-dong-su-kien/su-kien-sap-dien-ra') }}" class="nri-img">
                                    <img src="{{ UploadHelper::getUrlImage('news', $events->eve_picture) }}" alt="">
                                </a>
                            @else
                                <a href="{{ url('hoat-dong-su-kien/su-kien-da-dien-ra') }}" class="nri-img">
                                    <img src="{{ UploadHelper::getUrlImage('news', $events->eve_picture) }}" alt="">
                                </a>
                            @endif

                            <div class="nri-content">
                                <div class="nri-title">
                                    @if($events->eve_new == 0)
                                        <a href="{{ url('hoat-dong-su-kien/su-kien-sap-dien-ra') }}" class="t_ov_3" title="{{ translateData(app()->getLocale(), $events->eve_title, $events->eve_title_en) }}">{{ translateData(app()->getLocale(), $events->eve_title, $events->eve_title_en) }}</a>
                                    @else
                                        <a href="{{ url('hoat-dong-su-kien/su-kien-da-dien-ra') }}" class="t_ov_3" title="{{ translateData(app()->getLocale(), $events->eve_title, $events->eve_title_en) }}">{{ translateData(app()->getLocale(), $events->eve_title, $events->eve_title_en) }}</a>
                                    @endif
                                </div>
                                <div class="nri-sub">
                                    <div class="nri-date">{{ __('Ngày') }} {{ date('d/m/Y', $events->eve_create_time) }}</div>
                                    @if($events->eve_new == 0)
                                        <a href="{{ url('hoat-dong-su-kien/su-kien-sap-dien-ra') }}" class="nri-link"> > {{ __('Tìm hiểu thêm') }}</a>
                                    @else
                                        <a href="{{ url('hoat-dong-su-kien/su-kien-da-dien-ra') }}" class="nri-link"> > {{ __('Tìm hiểu thêm') }}</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-sm-12 col-lg-4">
            <div class="box-category bg-white">
                <div class="category-title">{{ __('Danh mục tin tức') }}</div>
                <ul>
                    <li>
                        <a href="#">
                            <span>Tin doanh nghiệp</span>
                            <span class="arrow-white">
                                <img src="{{ asset('images/arrow-white.png') }}" alt="">
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span>Tin thời sự</span>
                            <span class="arrow-white">
                                <img src="{{ asset('images/arrow-white.png') }}" alt="">
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span>Tin gói dịch vụ</span>
                            <span class="arrow-white">
                                <img src="{{ asset('images/arrow-white.png') }}" alt="">
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span>Tin tức khác</span>
                            <span class="arrow-white">
                                <img src="{{ asset('images/arrow-white.png') }}" alt="">
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="box-follow bg-white">
                <div class="category-title">{{ __('Theo dõi tin tức') }}</div>
                <ul>
                    <li class="bf-icon">
                        <a href="#" class="bf-insta"></a>
                    </li>
                    <li class="bf-icon">
                        <a href="#" class="bf-facebook"></a>
                    </li>
                    <li class="bf-icon">
                        <a href="#" class="bf-youtube"></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
