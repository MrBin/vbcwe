<?php
use App\Helper\Upload as UploadHelper;
?>
<div class="news-related">
    <div class="news-header--title">{{ __('Kiến thức liên quan') }}</div>
    <div class="row">
        <div class="col-12 col-md-12 col-sm-12 col-lg-8">
            <div class="box-news-related">
                <div class="row">
                    @foreach($hubRelated as $static)
                    <div class="col-12 col-md-6 col-sm-12 col-lg-6">
                        <div class="news-related--item">
                            <a href="{{ url('kien-thuc-tham-khao', [$static->sta_name_rewrite, $static->sta_id]) }}" class="nri-img">
                                <img src="{{ UploadHelper::getUrlImage('other', $static->sta_picture) }}" alt="">
                            </a>
                            <div class="nri-content">
                                <div class="nri-title">
                                    <a href="{{ url('kien-thuc-tham-khao', [$static->sta_name_rewrite, $static->sta_id]) }}" class="t_ov_3" title="{{ translateData(app()->getLocale(), $static->sta_title, $static->sta_title_en) }}">{{ translateData(app()->getLocale(), $static->sta_title, $static->sta_title_en) }}</a>
                                </div>
                                <div class="nri-sub">
                                    <div class="nri-date">{{ __('Ngày') }} {{ date('d/m/Y', $static->sta_created_time) }}</div>
                                    <a href="{{ url('kien-thuc-tham-khao', [$static->sta_name_rewrite, $static->sta_id]) }}" class="nri-link"> > {{ __('Tìm hiểu thêm') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-sm-12 col-lg-4">
            <div class="box-category bg-white">
                <div class="category-title">{{ __('Danh mục tin tức') }}</div>
                <ul>
                    <li>
                        <a href="#">
                            <span>Tin doanh nghiệp</span>
                            <span class="arrow-white">
                                <img src="{{ asset('images/arrow-white.png') }}" alt="">
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span>Tin thời sự</span>
                            <span class="arrow-white">
                                <img src="{{ asset('images/arrow-white.png') }}" alt="">
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span>Tin gói dịch vụ</span>
                            <span class="arrow-white">
                                <img src="{{ asset('images/arrow-white.png') }}" alt="">
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span>Tin tức khác</span>
                            <span class="arrow-white">
                                <img src="{{ asset('images/arrow-white.png') }}" alt="">
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="box-follow bg-white">
                <div class="category-title">{{ __('Theo dõi tin tức') }}</div>
                <ul>
                    <li class="bf-icon">
                        <a href="#" class="bf-insta"></a>
                    </li>
                    <li class="bf-icon">
                        <a href="#" class="bf-facebook"></a>
                    </li>
                    <li class="bf-icon">
                        <a href="#" class="bf-youtube"></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
