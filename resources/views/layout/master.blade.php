<!DOCTYPE html>
<html lang="en">
<head>
    @include('layout.head')
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('drawer/css/drawer.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('swipers/swiper.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('fancybox_v3/jquery.fancybox.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/css_main.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/css_responsive.css') }}" type="text/css">
</head>
<body>

    <div id="wrapper">
        @include('layout.header')
        @yield('content')
        <script type="text/javascript" src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('drawer/js/iscroll.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('drawer/js/drawer.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('swipers/swiper.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('fancybox_v3/jquery.fancybox.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('sticky/jquery.sticky.js') }}"></script>
        <script type="text/javascript">
            $(function() {
                $("#menu_mobile").find("li.drawer-dropdown").click(function() {
                    $(this).toggleClass("open");
                });

                $('.drawer').drawer();

                $("header").sticky({topSpacing:0});
            });
        </script>
        @include('layout.footer')
        @stack('scripts')
    </div>
</body>
</html>
