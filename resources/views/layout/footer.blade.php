<footer>
    <div class="container">
        <div class="footer-top">
            <div class="row">
                <div class="col-12 col-md-12 col-sm-12 col-lg-6">
                    <div class="footer-images">
                        <img src="{{ asset('images/img-footer-top.jpg') }}" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-12 col-md-12 col-sm-12 col-lg-6">
                    <div class="footer-form">
                        <h2 class="vbcvm-title">{{ __('Đăng ký nhận bản tin') }}</h2>
                        <div id="errors-show" class="errors-show"></div>
                        <form action="{{ route('index') }}" id="frm-register" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputEmail1">{{ __('Email') }}:*</label>
                                <input type="email" class="form-control" name="email" id="inp-email">
                            </div>
                            <div class="form-group">
                                <label for="inp-fullname">{{ __('Họ tên') }}:*</label>
                                <input type="text" name="name" class="form-control" id="inp-fullname">
                            </div>
                            <div class="form-group">
                                <label for="inp-company">{{ __('Công ty/Tổ chức') }}:*</label>
                                <input type="text" class="form-control" id="inp-company" name="con_company_name">
                            </div>
                            <div class="form-group">
                                <label for="inp-position">{{ __('Chức vụ') }}:*</label>
                                <input type="text" class="form-control" id="inp-position" name="con_position">
                            </div>
                            <div class="text-right">
                                <button type="submit" class="btn" id="btn-submit">{{ __('ĐĂNG KÝ') }}</button>
                                <input type="hidden" id="data-action" name="action" value="action">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer-bottom">
            <div class="row">
                <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                    <div class="fb-item">
                        <div class="fb-title">{{ __('Về VBCWE') }}</div>
                        <div class="fb-text">{{ __('Mạng lưới Doanh nghiệp Việt Nam Hỗ trợ Phát triển Quyền năng Phụ nữ (Vietnam Business Coalition for Women\'s Empowerment - VBCWE) được thành lập bởi các doanh nghiệp có tầm ảnh hưởng với quy mô nhân sự lớn nhằm theo đuổi mục tiêu bình đẳng giới tại nơi làm việc và phát triển quyền kinh tế cho phụ nữ tại Việt Nam.') }}</div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                    <div class="fb-item">
                        <div class="fb-title">{{ __('Chia sẻ với chúng tôi') }}</div>
                        <div class="fb-main">
                            <form action="" id="frm-share" method="POST">
                                <div id="errors-show-story" class="errors-show"></div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="inp-name" placeholder="{{ __('Tên của bạn') }}*">
                                </div>
                                <div class="form-group">
                                    <textarea name="story" id="inp-story" cols="30" placeholder="{{ __('Câu chuyện của bạn') }}*" class="form-control"></textarea>
                                </div>
                                <button class="btn" id="btn-send" type="submit">
                                    <i class="icon-send"></i>
                                    <span>Inbox</span>
                                </button>
                                <input type="hidden" id="data-action-story" name="action" value="action">
                            </form>
                            <div class="fb-logo-brand">
                                <img src="{{ asset('images/logo-austra.png') }}" alt="">
                                <img src="{{ asset('images/logo-footer.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                    <div class="fb-item">
                        <div class="fb-title">{{ __('Liên hệ') }}</div>
                        <div class="fb-main">
                            <div class="fbm-item">
                                <i class="icon-location"></i>
                                <span>{{ translateData(app()->getLocale(), $dataConfigs->con_address, $dataConfigs->con_address_en) }}</span>
                            </div>
                            <div class="fbm-item">
                                <i class="icon-website"></i>
                                <span>{{ $dataConfigs->con_mail }}</span>
                            </div>
                            <div class="fbm-item">
                                <i class="icon-phone"></i>
                                <span>{{ $dataConfigs->con_hotline_1 }}</span>
                            </div>
                            <div class="fbm-item">
                                <a href="{{ $dataConfigs->con_linkedin }}" class="icon-insta"></a>
                                <a href="{{ $dataConfigs->con_facebook }}" class="icon-fb"></a>
                                <a href="{{ $dataConfigs->con_youtube }}" class="icon-youtube"></a>
                            </div>
                            <div class="fbm-copy">{{ $dataConfigs->con_copyright }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Modal -->
<div class="modal fade" id="modal-register" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center text-success" id="exampleModalLongTitle">{{ __('ĐĂNG KÝ THÀNH CÔNG') }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
            <h3>{{ __('Cảm ơn bạn đã đăng ký nhận bản tin!') }}</h3>
            <p>{{ __('Chúng tôi sẽ gửi bạn bản tin mới trong thời gian sớm nhất.') }}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-register__event" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center text-success" id="exampleModalLongTitle">{{ __('ĐĂNG KÝ THÀNH CÔNG') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <h3>{{ __('Cảm ơn bạn đã đăng ký!') }}</h3>
                <p>{{ __('Chúng tôi sẽ phản hồi lại bạn trong thời gian sớm nhất.') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-story" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center text-success">{{ __('GỬI THÀNH CÔNG') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <h3>{{ __('Cảm ơn bạn đã gửi câu chuyện của mình!') }}</h3>
                <p>{{ __('Chúng tôi sẽ phản hồi lại bạn trong thời gian sớm nhất.') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script type="text/javascript">
$(function(){
    $('#frm-register').submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ route('contact') }}",
            data: {
                '_token': '{{ csrf_token() }}',
                'email': $('#inp-email').val(),
                'name': $('#inp-fullname').val(),
                'con_company_name': $('#inp-company').val(),
                'con_position': $('#inp-position').val(),
                'action': $('#data-action').val()
            },
            success: function (data) {
                if(data['status']){
                    $('#frm-register')[0].reset();
                    $('#modal-register').modal();
                }
                else{
                    var arrData = Object.entries(data['errors']);
                    var strErrors = '';
                    arrData.forEach(function (item, index) {
                        strErrors += '<li>' + item[1] + '</li>';
                    });

                    if(strErrors !== ''){
                        strErrors = '<div class="alert alert-danger"><ul>' + strErrors + '</ul></div>';
                    }
                    $('#errors-show').html(strErrors);

                }
            },
            error: function (data) {
                console.log(data);
            },
        });
    });

    $('.vbc-lang a').each(function () {
        $(this).click(function () {
            var lang = $(this).attr('data-lang');
            var urlStr = "{{ route('lang', ['vi']) }}";
            if(lang === 'en'){
                var urlStr = "{{ route('lang', ['en']) }}";
            }

            $.ajax({
                type: "GET",
                cache: false,
                url: urlStr,
                data: {
                    '_token': '{{ csrf_token() }}',
                    'lang': lang,
                },
                success: function (data) {
                    location.reload();
                },
                error: function (data) {
                    console.log(data);
                },
            });
        });
    });

    $('#frm-share').submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ route('contact-story') }}",
            data: {
                '_token': '{{ csrf_token() }}',
                'name': $('#inp-name').val(),
                'story': $('#inp-story').val(),
                'action': $('#data-action-story').val()
            },
            success: function (data) {
                if(data['status']){
                    $('#errors-show-story .alert-danger').remove();
                    $('#frm-share')[0].reset();
                    $('#modal-story').modal();
                }
                else{
                    var arrData = Object.entries(data['errors']);
                    var strErrors = '';
                    arrData.forEach(function (item, index) {
                        strErrors += '<li>' + item[1] + '</li>';
                    });

                    if(strErrors !== ''){
                        strErrors = '<div class="alert alert-danger"><ul>' + strErrors + '</ul></div>';
                    }
                    $('#errors-show-story').html(strErrors);

                }
            },
            error: function (data) {
                console.log(data);
            },
        });
    });

    if($('#form-contact').length > 0){

        $('#form-contact').submit(function (e) {
            e.preventDefault();

            $.ajax({
                type: "POST",
                cache: false,
                url: "{{ route('contact-form') }}",
                data: {
                    '_token': '{{ csrf_token() }}',
                    'email': $('#ct-email').val(),
                    'name': $('#ct-name').val(),
                    'title': $('#ct-title').val(),
                    'description': $('#ct-content').val(),
                    'action': $('#data-action-contact').val()
                },
                success: function (data) {
                    if(data['status']){
                        $('#errors-show-contact .alert-danger').remove();
                        $('#form-contact')[0].reset();
                        $('#modal-story').modal();
                    }
                    else{
                        var arrData = Object.entries(data['errors']);
                        var strErrors = '';
                        arrData.forEach(function (item, index) {
                            strErrors += '<li>' + item[1] + '</li>';
                        });

                        if(strErrors !== ''){
                            strErrors = '<div class="alert alert-danger"><ul>' + strErrors + '</ul></div>';
                        }
                        $('#errors-show-contact').html(strErrors);

                    }
                },
                error: function (data) {
                    console.log(data);
                },
            });
        });
    }

    $('#fre-form').submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ route('contact-event') }}",
            data: {
                '_token': '{{ csrf_token() }}',
                'event-email': $('#event-email').val(),
                'event-phone': $('#event-phone').val(),
                'event-name': $('#event-name').val(),
                'event-company': $('#event-company').val(),
                'event-position': $('#event-position').val(),
                'event-sex': $('#event-sex').val(),
                'event-need': $('#event-need').val(),
                'event-action': $('#event-action').val(),
                'event-type': $('#event-type').val()
            },
            success: function (data) {
                if(data['status']){
                    $('#errors-show-contact .alert-danger').remove();
                    $('#fre-form')[0].reset();
                    $('#modal-register__event').modal();
                }
                else{
                    var arrData = Object.entries(data['errors']);
                    var strErrors = '';
                    arrData.forEach(function (item, index) {
                        strErrors += '<li>' + item[1] + '</li>';
                    });

                    if(strErrors !== ''){
                        strErrors = '<div class="alert alert-danger"><ul>' + strErrors + '</ul></div>';
                    }
                    $('#errors-show-contact').html(strErrors);

                }
            },
            error: function (data) {
                console.log(data);
            },
        });
    });

    $('#register-event__form').submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ route('register-event-form') }}",
            data: {
                '_token': '{{ csrf_token() }}',
                'event-email': $('#event-email').val(),
                'event-phone': $('#event-phone').val(),
                'event-name': $('#event-name').val(),
                'event-company': $('#event-company').val(),
                'event-position': $('#event-position').val(),
                'event-sex': $('#event-sex').val(),
                'event-action': $('#event-action').val(),
                'event-type': $('#event-type').val()
            },
            success: function (data) {
                if(data['status']){
                    $('#errors-show-contact .alert-danger').remove();
                    $('#register-event__form')[0].reset();
                    $('#modal-register__event').modal();
                }
                else{
                    var arrData = Object.entries(data['errors']);
                    var strErrors = '';
                    arrData.forEach(function (item, index) {
                        strErrors += '<li>' + item[1] + '</li>';
                    });

                    if(strErrors !== ''){
                        strErrors = '<div class="alert alert-danger"><ul>' + strErrors + '</ul></div>';
                    }
                    $('#errors-show-contact').html(strErrors);

                }
            },
            error: function (data) {
                console.log(data);
            },
        });
    });

    $('#jrf-form').submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            cache: false,
            url: "{{ route('contact-join') }}",
            data: {
                '_token': '{{ csrf_token() }}',
                'join-name': $('#join-name').val(),
                'join-area': $('#join-area').val(),
                'join-address': $('#join-address').val(),
                'join-scale': $('#join-scale').val(),
                'join-website': $('#join-website').val(),
                'join-who': $('#join-who').val(),
                'join-position': $('#join-position').val(),
                'join-email': $('#join-email').val(),
                'join-phone': $('#join-phone').val(),
                'join-need': $('#join-need').val(),
                'join-action': $('#join-action').val(),
                'event-type': $('#event-type').val()
            },
            success: function (data) {
                if(data['status']){
                    $('#jrf-form')[0].reset();
                    $('#errors-show-contact .alert-danger').remove();
                    $('#modal-register__event').modal();
                }
                else{
                    var arrData = Object.entries(data['errors']);
                    var strErrors = '';
                    arrData.forEach(function (item, index) {
                        strErrors += '<li>' + item[1] + '</li>';
                    });

                    if(strErrors !== ''){
                        strErrors = '<div class="alert alert-danger"><ul>' + strErrors + '</ul></div>';
                    }
                    $('#errors-show-contact').html(strErrors);

                }
            },
            error: function (data) {
                console.log(data);
            },
        });
    });
})
</script>
<script type="text/javascript">
    if ('loading' in HTMLImageElement.prototype) {
        const images = document.querySelectorAll('img[loading="lazy"]');
        images.forEach(img => {
            img.src = img.dataset.src;
        });
    } else {
        // Dynamically import the LazySizes library
        const script = document.createElement('script');
        script.src =
            'https://cdnjs.cloudflare.com/ajax/libs/lazysizes/5.1.2/lazysizes.min.js';
        document.body.appendChild(script);
    }
</script>
@endpush
