<?php
use App\Helper\Upload as UploadHelper;
?>
<div class="news-related">
    <div class="news-header--title">{{ __('Tin tức liên quan') }}</div>
    <div class="row">
        <div class="col-12 col-md-12 col-sm-12 col-lg-8">
            <div class="box-news-related">
                <div class="row">
                    @foreach($newsRelated as $vNews)
                    <div class="col-12 col-md-6 col-sm-12 col-lg-6">
                        <div class="news-related--item">
                            <a href="{{ url('tin-tuc', [$vNews->new_rewrite, $vNews->new_id]) }}" class="nri-img">
                                <img src="{{ UploadHelper::getUrlImage('news', $vNews->new_picture) }}" alt="">
                            </a>
                            <div class="nri-content">
                                <div class="nri-title t_ov_3">
                                    <a href="{{ url('tin-tuc', [$vNews->new_rewrite, $vNews->new_id]) }}" title="{{ translateData(app()->getLocale(), $vNews->new_title, $vNews->new_title_en) }}">{{ translateData(app()->getLocale(), $vNews->new_title, $vNews->new_title_en) }}</a>
                                </div>
                                <div class="nri-sub">
                                    <div class="nri-date">{{ __('Ngày') }} {{ date('d/m/Y', $vNews->new_create_time) }}</div>
                                    <a href="{{ url('tin-tuc', [$vNews->new_rewrite, $vNews->new_id]) }}" class="nri-link"> > {{ __('Tìm hiểu thêm') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-sm-12 col-lg-4">
            <div class="box-category bg-white">
                <div class="category-title">{{ __('Danh mục tin tức') }}</div>
                <ul>
                    @foreach($category as $cate)
                        @foreach($cate as $items)
                            <li>
                                <a href="{{ url('/', [$items->cat_name_rewrite, $items->cat_id]) }}">
                                    <span>{{ $items->cat_name }}</span>
                                    <span class="arrow-white">
                                        <img src="{{ asset('images/arrow-white.png') }}" alt="">
                                    </span>
                                </a>
                            </li>
                        @endforeach
                    @endforeach
                </ul>
            </div>
            <div class="box-follow bg-white">
                <div class="category-title">{{ __('Theo dõi tin tức') }}</div>
                <ul>
                    <li class="bf-icon">
                        <a href="{{ $dataConfigs->con_linkedin }}" class="bf-insta"></a>
                    </li>
                    <li class="bf-icon">
                        <a href="{{ $dataConfigs->con_facebook }}" class="bf-facebook"></a>
                    </li>
                    <li class="bf-icon">
                        <a href="{{ $dataConfigs->con_youtube }}" class="bf-youtube"></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
