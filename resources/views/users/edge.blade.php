<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', __('Chứng chỉ Edge'))

@section('content')
<main class="bg-gray">
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ __('Chứng chỉ Edge') }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li>
                    <a href="/">{{ __('Dịch vụ') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="#">{{ __('Chứng chỉ Edge') }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="services-detail__main vbcwe-inner">
        <div class="sdm-top">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-sm-12 col-lg-8">
                        <h2 class="about-title about-title-left">{{ translateData(app()->getLocale(), $dataEdge->coc_text_1, $dataEdge->coc_text_1_en) }}</h2>
                        <div class="sdmt-content">
                            {!! translateData(app()->getLocale(), $dataEdge->coc_text_6_4, $dataEdge->coc_text_6_4_en) !!}
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                        <div class="jn-image-center">
                            <div class="jn-image">
                                <img src="{{ UploadHelper::getUrlImage('other', $dataEdge->coc_picture_2_1) }}" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sdm-item">
            <div class="container">
                <h2 class="about-title">{{ translateData(app()->getLocale(), $dataEdge->coc_text_2_1, $dataEdge->coc_text_2_1_en) }}</h2>
                <div class="about-sub">{{ translateData(app()->getLocale(), $dataEdge->coc_text_2_2, $dataEdge->coc_text_2_2_en) }}</div>
                <div class="sdmi-list">
                    <div class="row">
                        @foreach($dataStepWhy as $why)
                        <div class="col-12 col-md-6 col-sm-12 col-lg-4">
                            <p>{{ translateData(app()->getLocale(), $why->its_teaser, $why->its_teaser_en) }}</p>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="sdm-item bg-violet">
            <div class="container">
                <h2 class="about-title text-white">{{ translateData(app()->getLocale(), $dataEdge->coc_text_4, $dataEdge->coc_text_4_en) }}</h2>
                <div class="edge-policy">
                    @foreach($dataStepProcess as $process)
                    <div class="edge-policy__item">
                        <div class="epi-icon">
                            <div class="epi-title">{{ translateData(app()->getLocale(), $process->its_title, $process->its_title_en) }}</div>
                            <div class="epi-number">0{{ $process->its_order }}</div>
                        </div>
                        <div class="epi-content">{{ translateData(app()->getLocale(), $process->its_teaser, $process->its_teaser_en) }}</div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="sdm-item">
            <div class="container">
                <h2 class="about-title">{{ translateData(app()->getLocale(), $dataEdge->coc_text_3_1, $dataEdge->coc_text_3_1_en) }}</h2>
                <div class="edge-review">
                    <div class="row">
                        @foreach($dataEvaluate as $evaluate)
                        <div class="col-12 col-md-6 col-sm-12 col-lg-4">
                            <div class="er-item">
                                <div class="eri-icon">
                                    <img src="{{ UploadHelper::getUrlImage('other', $evaluate->its_picture) }}" alt="">
                                </div>
                                <div class="er-title">{{ translateData(app()->getLocale(), $evaluate->its_title, $evaluate->its_title_en) }}</div>
                                <div class="er-sub">{{ translateData(app()->getLocale(), $evaluate->its_teaser, $evaluate->its_teaser_en) }}</div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="sdm-item">
            <div class="container">
                <h2 class="about-title">{{ translateData(app()->getLocale(), $dataEdge->coc_text_5_1, $dataEdge->coc_text_5_1_en) }}</h2>
                <div class="about-sub">{{ translateData(app()->getLocale(), $dataEdge->coc_text_5_2, $dataEdge->coc_text_5_2_en) }}</div>
                <div class="sdmi-list">
                    <div class="row">
                        @foreach($dataLevel as $level)
                        <div class="col-12 col-md-6 col-sm-12 col-lg-4">
                            <div class="sdmi-title">{{ translateData(app()->getLocale(), $level->its_title, $level->its_title_en) }}</div>
                            <p>{{ translateData(app()->getLocale(), $level->its_teaser, $level->its_teaser_en) }}</p>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="sdm-item">
            <div class="container">
                <h2 class="about-title">{{ translateData(app()->getLocale(), $dataEdge->coc_text_6_1, $dataEdge->coc_text_6_1_en) }}</h2>
                <div class="about-sub">{{ translateData(app()->getLocale(), $dataEdge->coc_text_6_2, $dataEdge->coc_text_6_2_en) }}</div>
            </div>
        </div>
        <div class="sdm-item">
            <div class="container">
                <h2 class="about-title">{{ translateData(app()->getLocale(), $dataEdge->coc_text_6_3, $dataEdge->coc_text_6_3_en) }}</h2>
                <div class="gears-images text-center">
                    <img src="{{ UploadHelper::getUrlImage('other', $dataEdge->coc_picture_2_1_en) }}" alt="" class="img-fluid">
                </div>
            </div>
        </div>
        <div class="vbc-register">
            <a href="{{ url('dang-ky-tham-gia') }}" class="vbc-register-link">
                <span>{{ __('ĐĂNG KÝ') }}</span>
                <span class="arrow-white">
                    <img src="{{ asset('images/arrow-white.png') }}" alt="">
                </span>
            </a>
        </div>
    </div>
</main>
@endsection
