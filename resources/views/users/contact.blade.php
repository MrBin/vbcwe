<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', 'VBCWE')

@section('content')
<main>
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ __('Liên hệ') }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="#">{{ __('Liên hệ') }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="contact-main">
        <div class="contact-top bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-6 col-md-6 col-sm-12">
                        <div class="ct-info">
                            <div class="ct-info-main">
                                <h3 class="contact-title">{{ __('Liên hệ') }}</h3>
                                <div class="contact-item">
                                    <span class="ct-item ct-location"></span>
                                    <div>
                                        <span class="ct-title"><b>{{ __('Trụ sở chính') }}:</b></span>
                                        <span>{{ translateData(app()->getLocale(), $dataConfigs->con_address, $dataConfigs->con_address_en) }}</span>
                                    </div>
                                </div>
                                <div class="contact-item">
                                    <span class="ct-item ct-email"></span>
                                    <div>
                                        <span>{{ $dataConfigs->con_mail }}</span>
                                    </div>
                                </div>
                                <div class="contact-item">
                                    <span class="ct-item ct-phone"></span>
                                    <a href="tel:02432022678" class="ct-number-phone">{{ $dataConfigs->con_hotline_1 }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-6 col-sm-12">
                        <div class="form-contact">
                            <h3 class="contact-title">{{ __('Chia sẻ với chúng tôi') }}</h3>
                            <form action="" id="form-contact" method="POST">
                                <div id="errors-show-contact" class="errors-show"></div>
                                <div class="form-row form-group">
                                    <div class="col">
                                        <input type="text" class="form-control" name="ct-name" id="ct-name" placeholder="{{ __('Tên của bạn') }}*">
                                    </div>
                                    <div class="col">
                                        <input type="email" class="form-control" name="ct-email" id="ct-email" placeholder="Email*">
                                    </div>
                                </div>
                                <div class="form-row form-group">
                                    <div class="col">
                                        <input type="text" class="form-control" name="ct-title" id="ct-title" placeholder="{{ __('Tiêu đề của bạn') }}*">
                                    </div>
                                </div>
                                <div class="form-row form-group">
                                    <div class="col">
                                        <textarea class="form-control" name="ct-content" id="ct-content" placeholder="{{ __('Câu chuyện của bạn') }}*"></textarea>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-send-contact">{{ __('Gửi tin nhắn') }}</button>
                                    <input type="hidden" id="data-action-contact" name="action" value="action">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="contact-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3724.519797356523!2d105.8091848653321!3d21.0118777937295!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1zNS03IE5nLiA5MiBMw6FuZyBI4bqhLCBMw6FuZyBI4bqhLCDEkOG7kW5nIMSQYSwgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2sus!4v1607326828095!5m2!1svi!2sus" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </div>
    </div>
</main>
@endsection

