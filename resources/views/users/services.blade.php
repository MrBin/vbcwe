<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', 'Dịch vụ')

@section('content')
<main>
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ __('Dịch vụ') }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="/">{{ __('Dịch vụ') }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="news-main service-main">
        <div class="container">
            @if(isset($servicesBig))
            <div class="news-big">
                <div class="row">
                    <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                        <div class="news-big__left">
                            <h2><a href="{{ url('dich-vu', [$servicesBig->ser_name_rewrite, $servicesBig->ser_id]) }}">{{ translateData(app()->getLocale(), $servicesBig->ser_title, $servicesBig->ser_title_en) }}</a></h2>
                            <div class="nbl-date">{{ date('d/m/Y', $servicesBig->ser_create_time) }}</div>
                            <div class="nbl-teaser t_ov_3">{{ translateData(app()->getLocale(), $servicesBig->ser_teaser, $servicesBig->ser_teaser_en) }}</div>
                            <a href="{{ url('dich-vu', [$servicesBig->ser_name_rewrite, $servicesBig->ser_id]) }}" class="link-more-big">{{ __('Tìm hiểu thêm') }}</a>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-sm-12 col-lg-8">
                        <div class="nbr-img">
                            <img src="{{ UploadHelper::getUrlImage('service', $servicesBig->ser_picture) }}" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="news-filter"></div>

            <div class="news-thumb">
                <div class="row">
                @if(!empty($dataServices))
                    @foreach($dataServices as $services)
                    <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                        <div class="news-item">
                            <div class="news-img">
                                <a href="{{ url('dich-vu', [$services->ser_name_rewrite, $services->ser_id]) }}">
                                    <img src="{{ UploadHelper::getUrlImage('service', $services->ser_picture) }}" alt="">
                                </a>
                            </div>
                            <div class="news-content">
                                <h3 class="news-title">
                                    <a href="{{ url('dich-vu', [$services->ser_name_rewrite, $services->ser_id]) }}" title="{{ translateData(app()->getLocale(), $services->ser_title, $services->ser_title_en) }}">{{ translateData(app()->getLocale(), $services->ser_title, $services->ser_title_en) }}</a>
                                </h3>
                                <div class="news-date">{{ __('Ngày') }} {{ date('d/m/Y', $services->ser_create_time) }}</div>
                                <div class="news-teaser t_ov_3">{{ translateData(app()->getLocale(), $services->ser_teaser, $services->ser_teaser_en) }}</div>
                                <div class="news-more">
                                    <a class="link-more-small" href="{{ url('dich-vu', [$services->ser_name_rewrite, $services->ser_id]) }}"> > {{ __('Tìm hiểu thêm') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
                </div>
            </div>
            @include("layout.pagination")

             @include("layout.news-related")
        </div>
    </div>
</main>
@endsection
