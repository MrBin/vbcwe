<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', __('Đối tác'))

@section('content')
<main class="bg-gray">
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ __('Giới thiệu') }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li>
                    <a href="{{ url('gioi-thieu') }}">{{ __('Về chúng tôi') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="#">{{ __('Đối tác') }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="partner-main">
        <div class="container">
            <div class="partner-thumb">
                <div class="vbcwe-title color-violet">{{ __('Đối tác tài trợ và hoạt động') }}</div>
                <div class="pt-list">
                    <div class="row">
                        @foreach($dataSponsor as $partner)
                        <div class="col-12 col-md-6 col-sm-12 col-lg-6">
                            <div class="partner-item">
                                <a class="pi-images" href="{{ $partner->par_link }}">
                                    <img src="{{ UploadHelper::getUrlImage('partner', $partner->par_picture) }}" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="partner-main bg-violet">
        <div class="container">
            <div class="partner-thumb partner-inter">
                <div class="vbcwe-title color-white">{{ __('Đối tác quốc tế') }}</div>
                <div class="partner-slide">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            @foreach($dataInternational as $partner)
                                <div class="swiper-slide">
                                    <a href="{{ $partner->par_link }}">
                                        <img src="{{ UploadHelper::getUrlImage('partner', $partner->par_picture) }}" alt="">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- navigation buttons -->
                    <div class="swiper-button-prev js-prev"></div>
                    <div class="swiper-button-next js-next"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="partner-main">
        <div class="container">
            <div class="partner-thumb">
                <div class="vbcwe-title color-violet">{{ __('Đối tác khu vực') }}</div>
                <div class="pt-list">
                    <div class="row">
                        <div class="col-12 col-md-0 col-sm-12 col-lg-3 text-center"></div>
                        @foreach($dataRegion as $partner)
                            <div class="col-12 col-md-4 col-sm-12 col-lg-2 text-center">
                                <a class="partner-images" href="{{ $partner->par_link }}">
                                    <img src="{{ UploadHelper::getUrlImage('partner', $partner->par_picture) }}" alt="" class="img-fluid">
                                </a>
                            </div>
                        @endforeach
                        <div class="col-12 col-md-0 col-sm-12 col-lg-3 text-center"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="partner-main bg-violet">
        <div class="container">
            <div class="partner-thumb bg-violet partner-domestic">
                <div class="vbcwe-title color-white">{{ __('Đối tác trong nước') }}</div>
                <div class="partner-slide">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            @foreach($dataDomestic as $partner)
                                <div class="swiper-slide">
                                    <a href="{{ $partner->par_link }}">
                                        <img src="{{ UploadHelper::getUrlImage('partner', $partner->par_picture) }}" alt="">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- navigation buttons -->
                    <div class="swiper-button-prev js-prev"></div>
                    <div class="swiper-button-next js-next"></div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
@push('scripts')
<script type="text/javascript">
    $(function () {
        var swiperPartner = new Swiper('.partner-inter .swiper-container', {
            slidesPerView: 1,
            spaceBetween: 20,
            autoplay: true,
            loop: true,
            navigation: {
                nextEl: '.partner-inter .swiper-button-next',
                prevEl: '.partner-inter .swiper-button-prev',
            },
            breakpoints: {
                640: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 10,
                },
                1024: {
                    slidesPerView: 5,
                    spaceBetween: 0,
                },
            }
        });

        var swiperPartner2 = new Swiper('.partner-domestic .swiper-container', {
            slidesPerView: 1,
            spaceBetween: 20,
            autoplay: true,
            loop: true,
            navigation: {
                nextEl: '.partner-domestic .swiper-button-next',
                prevEl: '.partner-domestic .swiper-button-prev',
            },
            breakpoints: {
                640: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 10,
                },
                1024: {
                    slidesPerView: 5,
                    spaceBetween: 0,
                },
            }
        });
    });
</script>
@endpush
