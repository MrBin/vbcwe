<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', 'VBCWE')

@section('content')
<main>
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ __('Mạng lưới thành viên') }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li>
                    <a href="{{ url('gioi-thieu') }}">{{ __('Về chúng tôi') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="#">{{ __('Mạng lưới thành viên') }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="member-network-main">
            <div class="container">
                <div class="member-network-top">
                    <div class="row">
                        @foreach($dataEnterprices as $enterprices)
                        <div class="col-6 col-md-4 col-sm-12 col-log-2">
                            <a href="{{ $enterprices->enn_link }}" class="mnt-item" target="_blank">
                                <img src="{{ UploadHelper::getUrlImage('other', $enterprices->enn_picture) }}" alt="">
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>

                <div class="member-network-bottom">
                    <div class="jn-title">
                        <h2>{{ translateData(app()->getLocale(), $dataMember->con_title_thamgia_mangluoi, $dataMember->con_title_thamgia_mangluoi_en) }}</h2>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                            <div class="jn-image">
                                <img src="{{ UploadHelper::getUrlImage('other', $dataMember->con_banner_thamgia_mangluoi) }}" alt="">
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-sm-12 col-lg-8">
                            <div class="jn-text">
                                {!! translateData(app()->getLocale(), $dataMember->con_description_thamgia_mangluoi, $dataMember->con_description_thamgia_mangluoi_en) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vbc-register">
                <a href="{{ url('dang-ky-tham-gia') }}" class="vbc-register-link">
                    <span>{{ __('ĐĂNG KÝ') }}</span>
                    <span class="arrow-white">
                        <img src="{{ asset('images/arrow-white.png') }}" alt="">
                    </span>
                </a>
            </div>
        </div>
</main>
@endsection

