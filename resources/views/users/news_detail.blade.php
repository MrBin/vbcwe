<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', translateData(app()->getLocale(), $newsDetail[0]->new_title, $newsDetail[0]->new_title_en))
@section('og')
<meta property="og:site_name" content="VBCWE">
<meta property="og:url" content="{{ url('tin-tuc', [$newsDetail[0]->new_rewrite, $newsDetail[0]->new_id]) }}">
<meta property="og:type" content="article">
<meta property="og:title" content="{{ translateData(app()->getLocale(), $newsDetail[0]->new_title, $newsDetail[0]->new_title_en) }}">
<meta property="og:image" content="{{ UploadHelper::getUrlImage('news', $newsDetail[0]->new_picture) }}">
<meta property="og:locale" content="vi_vn">
<link rel="canonical" href="{{ url('tin-tuc', [$newsDetail[0]->new_rewrite, $newsDetail[0]->new_id]) }}">
@endsection

@section('content')
<main>
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ translateData(app()->getLocale(), $newsDetail[0]->new_title, $newsDetail[0]->new_title_en) }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li>
                    <a href="#">{{ translateData(app()->getLocale(), $newsDetail[0]->category->cat_name, $newsDetail[0]->category->cat_name_en) }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="#">{{ cut_string2(translateData(app()->getLocale(), $newsDetail[0]->new_title, $newsDetail[0]->new_title_en), 15) }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="news-detail-main">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 col-sm-12 col-lg-9">
                    <div class="ndm-content">
                        <h2>{{ translateData(app()->getLocale(), $newsDetail[0]->new_title, $newsDetail[0]->new_title_en) }}</h2>
                        <div class="nbl-date">{{ date('d/m/Y', $newsDetail[0]->new_create_time) }}</div>
                        <div class="news-description">
                            {!! str_replace('uploads/', '/storage/uploads/', translateData(app()->getLocale(), $newsDetail[0]->new_description, $newsDetail[0]->new_description_en)) !!}
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-sm-12 col-lg-3">
                    @if(!empty($dataBanners))
                    <a href="" class="ndm-banner">
                        <img src="{{ UploadHelper::getUrlImage('banner', $dataBanners->ban_picture) }}" alt="" class="img-fluid">
                    </a>
                    @endif
                </div>
            </div>

            @include("layout.news-related")
        </div>
    </div>
</main>
@endsection
