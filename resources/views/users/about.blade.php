<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', __('Giới thiệu'))

@section('content')
<main>
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ __('Giới thiệu') }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li>
                    <a href="{{ url('gioi-thieu') }}">{{ __('Về chúng tôi') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="#">{{ __('Giới thiệu') }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="about-main">
        <div class="container">
            <div class="about-top">
                <div class="row">
                    <div class="col-12 col-md-12 col-sm-12 col-lg-6">
                        <h2 class="about-title about-title-left">{{ translateData(app()->getLocale(), $dataAbout->coa_text_2_1, $dataAbout->coa_text_2_1_en) }}</h2>
                        <div class="about-top-main">
                            {!! translateData(app()->getLocale(), $dataAbout->coa_text_2_2, $dataAbout->coa_text_2_2_en) !!}
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-sm-12 col-lg-6">
                        <div class="about-img">
                            <img src="{{ UploadHelper::getUrlImage('config', $dataAbout->coa_picture_2_1) }}" alt="" class="img-fluid">
                        </div>

                    </div>
                </div>
            </div>
            <div class="about-center">
                <div class="row">
                    <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                        <div class="about-img-2">
                            <img src="{{ UploadHelper::getUrlImage('config', $dataAbout->coa_picture_3_1) }}" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-sm-12 col-lg-8">
                        {!! translateData(app()->getLocale(), $dataAbout->coa_text_3_2, $dataAbout->coa_text_3_2_en) !!}
                    </div>
                </div>
            </div>
            <div class="about-bottom">
                <div class="row">
                    <div class="col-12 col-md-12 col-sm-12 col-lg-6">
                        <div class="jn-image">
                            <img src="{{ UploadHelper::getUrlImage('config', $dataAbout->coa_picture_4_1) }}" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-sm-12 col-lg-6">
                        <div class="jn-text">
                            {!! translateData(app()->getLocale(), $dataAbout->coa_text_4_2, $dataAbout->coa_text_4_2_en) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

