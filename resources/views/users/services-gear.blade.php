<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', translateData(app()->getLocale(), $dataGears->cog_text_1, $dataGears->cog_text_1_en))

@section('content')
<main class="bg-gray">
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ translateData(app()->getLocale(), $dataGears->cog_text_1, $dataGears->cog_text_1_en) }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li>
                    <a href="/">{{ __('Dịch vụ') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="#">{{ translateData(app()->getLocale(), $dataGears->cog_text_1, $dataGears->cog_text_1_en) }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="services-detail__main vbcwe-inner">
        <div class="sdm-top">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-sm-12 col-lg-8">
                        <h2 class="about-title about-title-left">{{ translateData(app()->getLocale(), $dataGears->cog_text_1, $dataGears->cog_text_1_en) }}</h2>
                        <div class="sdmt-content">
                            {!! translateData(app()->getLocale(), $dataGears->cog_text_2_2, $dataGears->cog_text_2_2_en) !!}
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                        <div class="jn-image-center">
                            <div class="jn-image">
                                <img src="{{ UploadHelper::getUrlImage('other', $dataGears->cog_picture_2) }}" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sdm-item">
            <div class="container">
                <h2 class="about-title">{{ translateData(app()->getLocale(), $dataGears->cog_text_2_1, $dataGears->cog_text_2_1_en) }}</h2>
                <div class="sdmi-list">
                    <div class="row">
                        <div class="col-12 col-md-6 col-sm-12 col-lg-8">
                            <div class="row sdmi-row">
                                @foreach($dataStepField as $field)
                                <div class="col-12 col-md-6 col-sm-12 col-lg-6">
                                    <div class="sdmi-item">
                                        <div class="sdmi-icon">0{{ $field->its_order }}</div>
                                        <div class="sdmi-text">{{ translateData(app()->getLocale(), $field->its_title, $field->its_title_en) }}</div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-sm-12 col-lg-4">
                            <div class="row">
                                @foreach($dataStepField2 as $fields)
                                <div class="col-12 col-md-6 col-sm-12 col-lg-12">
                                    <div class="sdmi-item">
                                        @if($fields->its_order < 10)
                                        <div class="sdmi-icon">0{{ $fields->its_order }}</div>
                                        @else
                                        <div class="sdmi-icon">{{ $fields->its_order }}</div>
                                        @endif
                                        <div class="sdmi-text">{{ translateData(app()->getLocale(), $fields->its_title, $fields->its_title_en) }}</div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="sdm-item">
            <div class="container">
                <h2 class="about-title">{{ translateData(app()->getLocale(), $dataGears->cog_text_3, $dataGears->cog_text_3_en) }}</h2>
                <div class="sdmi-list">
                    <div class="row">
                        @foreach($dataStepTable as $table)
                        <div class="col-12 col-md-6 col-sm-12 col-lg-3">
                            <div class="sdmi-item">
                                <div class="sdmi-icon__vuong">0{{ $table->its_order }}</div>
                                <div class="sdmi-text">{{ translateData(app()->getLocale(), $table->its_title, $table->its_title_en) }}</div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="sdm-item bg-violet">
            <div class="container">
                <h2 class="about-title text-white">{!! translateData(app()->getLocale(), $dataGears->cog_text_4, $dataGears->cog_text_4_en) !!}</h2>
                <div class="sdm-item__step">
                    <div class="row">
                        @foreach($dataProcess as $key => $process)
                            @if($key == 1)
                            <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                                <div class="sdmi-content__center">
                                    <div class="sdmi-content">
                                        <div class="sdmi-teaser">{!! translateData(app()->getLocale(), $process->its_teaser, $process->its_teaser_en) !!}</div>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                                <div class="sdmi-content">
                                    <div class="sdmi-teaser">{{ translateData(app()->getLocale(), $process->its_title, $process->its_title_en) }}</div>
                                    <div class="sdmi-teaser">{!! translateData(app()->getLocale(), $process->its_teaser, $process->its_teaser_en) !!}</div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="sdm-item">
            <div class="container">
                <h2 class="about-title">{{ translateData(app()->getLocale(), $dataGears->cog_text_5, $dataGears->cog_text_5_en) }}</h2>
                <div class="eight-list">
                    @foreach($dataEvaluate as $keyEva => $evaluate)
                    <div class="eight-item">
                        <div class="ei-icon">
                            @if($keyEva == 1)
                            <div class="eii-title">{{ __('Tuần') . $evaluate->its_order . '+' . ($evaluate->its_order + 1) }}</div>
                            @else
                            <div class="eii-title">{{ __('Tuần') . $evaluate->its_order }}</div>
                            @endif
                            <div class="eii-teaser">{{ translateData(app()->getLocale(), $evaluate->its_title, $evaluate->its_title_en) }}</div>
                        </div>
                        <div class="ei-content">{{ translateData(app()->getLocale(), $evaluate->its_teaser, $evaluate->its_teaser_en) }}</div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
{{--        <div class="sdm-item">--}}
{{--            <div class="container">--}}
{{--                <h2 class="about-title">Doanh nghiệp được chứng nhận gears</h2>--}}
{{--                <div class="gears-images text-center">--}}
{{--                    <img src="{{ asset('images/bg-gears.jpg') }}" alt="" class="img-fluid">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="vbc-register">
            <a href="{{ url('dang-ky-tham-gia') }}" class="vbc-register-link vbc-tool">
                <span>{{ __('TRẢI NGHIỆM CÔNG CỤ') }}</span>
                <span class="arrow-white">
                    <img src="{{ asset('images/arrow-white.png') }}" alt="">
                </span>
            </a>
            <a href="{{ url('dang-ky-tham-gia') }}" class="vbc-register-link">
                <span>{{ __('ĐĂNG KÝ') }}</span>
                <span class="arrow-white">
                    <img src="{{ asset('images/arrow-white.png') }}" alt="">
                </span>
            </a>
        </div>
    </div>
</main>
@endsection
