<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', 'VBCWE')
@section('og')
<meta property="og:site_name" content="VBCWE">
<meta property="og:url" content="https://vbcwe.com/">
<meta property="og:type" content="article">
<meta property="og:title" content="VBCWE">
<meta property="og:image" content="https://vbcwe.com/images/favicon.png">
<meta property="og:locale" content="vi_vn">
<link rel="canonical" href="https://vbcwe.com/">
@endsection

@section('content')
<main>
    <div class="vbc-slide">
        <div class="vbcwe-slide">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                @foreach($dataBanners as $banners)
                    <div class="swiper-slide">
                        <div class="slide-img">
                            <a href="{{ $banners->ban_link }}" target="_blank">
                                <img data-src="{{ UploadHelper::getUrlImage('banner', $banners->ban_picture) }}" alt="" loading="lazy" class="img-fluid lazyload">
                            </a>
                        </div>
                        @if($banners->ban_name != "")
                        <div class="slide-caption">
                            <h1>{{ translateData(app()->getLocale(), $banners->ban_name, $banners->ban_name_en) }}</h1>
                            <a href="{{ $banners->ban_link }}">{{ __('Tìm hiểu thêm') }}</a>
                        </div>
                        @endif
                    </div>
                @endforeach
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
        <div class="vbc-group">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                        <div class="vbcg-img">
                            <img data-src="images/group-logo.png" alt="" loading="lazy" class="lazyload">
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-sm-12 col-lg-8">
                        <div class="vbcg-main">
                            @foreach($dataAchievements as $key => $achievements)
                            <div class="vbcg-item">
                                <div class="vbcg-icon">
                                    @if($key == 0)
                                    <i class="icon-group"></i>
                                    @elseif($key == 1)
                                    <i class="icon-traffic"></i>
                                    @else
                                    <i class="icon-hand"></i>
                                    @endif
                                    <div class="vbcg-text">{{ $achievements->tht_index }}</div>
                                </div>
                                <div class="vbcg-title">{{ translateData(app()->getLocale(), $achievements->tht_title, $achievements->tht_title_en) }}</div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="vbc-product">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 col-sm-12 text-center col-lg-4">
                    <h2 class="vbc-title vbc-title__style-1">{{ __('Sản phẩm dịch vụ') }}</h2>
                </div>
                <div class="col-12 col-md-12 col-sm-12 col-lg-8">
                    <div class="vbcp-main">
                        <div class="vbcp-sub">{!! __('<strong>VBCWE là tổ chức đầu tiên tại Việt Nam</strong> cung cấp giải pháp mang tính hệ thống và toàn diện về bình đẳng giới ở nơi làm việc hỗ trợ doanh nghiệp cải thiện chính sách và môi trường làm việc, nâng cao hiệu quả kinh doanh hướng đến phát triển bền vững.') !!}</div>
                        <div class="row">
                            @foreach($dataServices as $keySer => $services)
                            <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                                <div class="vbcp-item">
                                    <div class="vbcp-item__icon">
                                        <div>
                                            <img data-src="{{ UploadHelper::getUrlImage('partners', $services->ser_icon) }}" alt="" loading="lazy" class="lazyload">
                                        </div>
                                        <h3>{{ translateData(app()->getLocale(), $services->ser_title, $services->ser_title_en) }}</h3>
                                    </div>
                                    <div class="vbcp-text">{!! translateData(app()->getLocale(), $services->ser_description, $services->ser_description_en) !!}</div>
                                    @switch($keySer)
                                        @case(0)
                                            <a href="{{ url('danh-gia-binh-dang-gioi') }}">{{ __('Tìm hiểu thêm') }}</a>
                                            @break

                                        @case(1)
                                            <a href="{{ url('khoa-dao-tao-doanh-nghiep') }}">{{ __('Tìm hiểu thêm') }}</a>
                                            @break

                                        @case(2)
                                            <a href="{{ url('tu-van-doanh-nghiep') }}">{{ __('Tìm hiểu thêm') }}</a>
                                            @break
                                    @endswitch
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-product-shape"></div>
        <div class="bg-product-shape-r"></div>
        <div class="elementor_element gt3_moved_element triangle_x_black">
            <svg xmlns="http://www.w3.org/2000/svg" width="21px" height="21px"><path fill="rgb(59, 54, 99)" d="M20.459,16.250 C19.769,17.446 18.240,17.855 17.044,17.165 L11.415,13.915 L8.165,19.544 C7.475,20.740 5.946,21.150 4.750,20.459 C3.554,19.769 3.145,18.240 3.835,17.044 L7.085,11.415 L1.456,8.165 C0.260,7.475 -0.150,5.946 0.541,4.750 C1.231,3.554 2.760,3.144 3.956,3.835 L9.585,7.085 L12.835,1.456 C13.525,0.260 15.054,-0.150 16.250,0.541 C17.446,1.231 17.855,2.760 17.165,3.956 L13.915,9.585 L19.544,12.835 C20.740,13.525 21.150,15.054 20.459,16.250 Z"></path></svg>
        </div>
        <div class="elementor_element gt3_flash_element triangle_x_blue">
            <svg xmlns="http://www.w3.org/2000/svg" width="13px" height="13px"><path fill="rgb(87, 230, 193)" d="M11.927,6.550 L8.279,7.527 L9.256,11.175 C9.464,11.950 9.004,12.746 8.229,12.954 C7.454,13.162 6.658,12.702 6.450,11.927 L5.473,8.279 L1.825,9.257 C1.050,9.464 0.254,9.004 0.046,8.229 C-0.162,7.454 0.298,6.658 1.073,6.450 L4.721,5.473 L3.744,1.825 C3.536,1.050 3.996,0.254 4.771,0.046 C5.546,-0.161 6.342,0.298 6.550,1.073 L7.527,4.721 L11.175,3.744 C11.950,3.536 12.746,3.996 12.954,4.771 C13.162,5.545 12.702,6.342 11.927,6.550 Z"></path></svg>
        </div>
        <div class="elementor_element gt3_rotated_element triangle_orange">
            <img src="images/triangle_orange.png" alt="">
        </div>
        <div class="elementor_element gt3_rotated_element triangle_blue">
            <img src="images/triangle_blue.png" alt="">
        </div>
        <div class="elementor_element gt3_moved_element triangle_x_blue_2">
            <svg xmlns="http://www.w3.org/2000/svg" width="13px" height="13px"><path fill="rgb(87, 230, 193)" d="M11.927,6.550 L8.279,7.527 L9.256,11.175 C9.464,11.950 9.004,12.746 8.229,12.954 C7.454,13.162 6.658,12.702 6.450,11.927 L5.473,8.279 L1.825,9.257 C1.050,9.464 0.254,9.004 0.046,8.229 C-0.162,7.454 0.298,6.658 1.073,6.450 L4.721,5.473 L3.744,1.825 C3.536,1.050 3.996,0.254 4.771,0.046 C5.546,-0.161 6.342,0.298 6.550,1.073 L7.527,4.721 L11.175,3.744 C11.950,3.536 12.746,3.996 12.954,4.771 C13.162,5.545 12.702,6.342 11.927,6.550 Z"></path></svg>
        </div>
    </div>

    <div class="vbc-join">
        <div class="container">
            <div class="vbcj-main">
                <h2 class="vbcj-title">{{ __('Tham gia cùng chúng tôi') }}</h2>
                <div class="vbcj-thumb">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            @foreach($dataEnterprices as $partners)
                            <div class="swiper-slide">
                                <a href="{{ $partners->enn_link }}" title="{{ $partners->enn_name }}" target="_blank" class="vbcj-image">
                                    <img data-src="{{ UploadHelper::getUrlImage('partners', $partners->enn_picture) }}" alt="{{ $partners->enn_name }}" class="img-fluid lazyload" loading="lazy">
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="vbc-video">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 col-sm-12 col-lg-6">
                    <div class="vbcv-video">
                        <a data-fancybox="" href="{{ $dataConfigs->con_add_footer }}">
                            <div class="vbcv-title">Investing in Women Workplace Gender Equality</div>
                            <img data-src="images/play.png" alt="" class="lazyload" loading="lazy">
                        </a>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-sm-12 col-lg-6">
                    <div class="vbcv-main">
                        <h2 class="vbcvm-title">{{ __('Ý kiến đối tác') }}</h2>
                        <div class="vbcv-content">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    @foreach($dataOpinions as $opinions)
                                    <div class="swiper-slide">
                                        <div class="vbcvm-text t_ov_3">{!! translateData(app()->getLocale(), $opinions->opi_description, $opinions->opi_description_en) !!}</div>
                                        <div class="vbcv-author">{{ translateData(app()->getLocale(), $opinions->opi_name, $opinions->opi_name_en) }}</div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-join"></div>
        <div class="elementor_element gt3_moved_element triangle_x_black">
            <svg xmlns="http://www.w3.org/2000/svg" width="21px" height="21px"><path fill="rgb(59, 54, 99)" d="M20.459,16.250 C19.769,17.446 18.240,17.855 17.044,17.165 L11.415,13.915 L8.165,19.544 C7.475,20.740 5.946,21.150 4.750,20.459 C3.554,19.769 3.145,18.240 3.835,17.044 L7.085,11.415 L1.456,8.165 C0.260,7.475 -0.150,5.946 0.541,4.750 C1.231,3.554 2.760,3.144 3.956,3.835 L9.585,7.085 L12.835,1.456 C13.525,0.260 15.054,-0.150 16.250,0.541 C17.446,1.231 17.855,2.760 17.165,3.956 L13.915,9.585 L19.544,12.835 C20.740,13.525 21.150,15.054 20.459,16.250 Z"></path></svg>
        </div>
        <div class="elementor_element gt3_flash_element triangle_x_blue">
            <svg xmlns="http://www.w3.org/2000/svg" width="13px" height="13px"><path fill="rgb(87, 230, 193)" d="M11.927,6.550 L8.279,7.527 L9.256,11.175 C9.464,11.950 9.004,12.746 8.229,12.954 C7.454,13.162 6.658,12.702 6.450,11.927 L5.473,8.279 L1.825,9.257 C1.050,9.464 0.254,9.004 0.046,8.229 C-0.162,7.454 0.298,6.658 1.073,6.450 L4.721,5.473 L3.744,1.825 C3.536,1.050 3.996,0.254 4.771,0.046 C5.546,-0.161 6.342,0.298 6.550,1.073 L7.527,4.721 L11.175,3.744 C11.950,3.536 12.746,3.996 12.954,4.771 C13.162,5.545 12.702,6.342 11.927,6.550 Z"></path></svg>
        </div>
        <div class="elementor_element gt3_rotated_element triangle_orange">
            <img src="images/triangle_orange.png" alt="">
        </div>
        <div class="elementor_element gt3_rotated_element triangle_blue">
            <img src="images/triangle_blue.png" alt="">
        </div>
    </div>

    <div class="vbc-news">
        <div class="container">
            <h2 class="vbcj-title"><a href="/tin-tuc" target="_blank">{{ __('Tin tức - sự kiện') }}</a></h2>
            <div class="vbcn-thumb">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                    @foreach($newsHome as $news)
                        <div class="vbcn-item swiper-slide">
                            <div class="vbcn-image">
                                <img data-src="{{ UploadHelper::getUrlImage('news', $news->new_picture) }}" alt="" loading="lazy" class="img-fluid lazyload">
                            </div>
                            <div class="vbcn-main">
                                <div class="vbcn-top">
                                    <div class="vbcn-top__cate">
                                        <i class="icon-arrow"></i>
                                        <span>{{ translateData(app()->getLocale(), $news->category->cat_name, $news->category->cat_name_en) }}</span>
                                    </div>
                                    <div class="vbcn-top__date">{{ date('d/m/Y', $news->new_create_time) }}</div>
                                </div>
                                <h3><a href="{{ url('tin-tuc', [$news->new_rewrite, $news->new_id]) }}">{{ translateData(app()->getLocale(), $news->new_title, $news->new_title_en) }}</a></h3>
                                <div class="vbcn-teaser t_ov_3">{{ translateData(app()->getLocale(), $news->new_teaser, $news->new_teaser_en) }}</div>
                                <a href="{{ url('tin-tuc', [$news->new_rewrite, $news->new_id]) }}" class="read-more">{{ __('Đọc thêm') }}</a>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </div>
</main>
@endsection
@push('scripts')
<script type="text/javascript">
    $(function () {
        var swiper = new Swiper('.vbcj-thumb .swiper-container', {
            slidesPerView: 1,
            spaceBetween: 20,
            autoplay: true,
            breakpoints: {
                640: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 40,
                },
                1024: {
                    slidesPerView: 5,
                    spaceBetween: 50,
                },
            }
        });

        var swiperNews = new Swiper('.vbcn-thumb .swiper-container', {
            slidesPerView: 1,
            spaceBetween: 20,
            navigation: {
                nextEl: '.vbcn-thumb .swiper-button-next',
                prevEl: '.vbcn-thumb .swiper-button-prev',
            },
            // autoplay: true,
            breakpoints: {
                640: {
                    slidesPerView: 1,
                    spaceBetween: 20
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 40,
                },
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 50,
                },
            }
        });

        var swiperReview = new Swiper('.vbcv-content .swiper-container', {
            slidesPerView: 1,
            spaceBetween: 20,
            autoplay: true,
            pagination: {
                el: '.vbcv-content .swiper-pagination',
                type: 'bullets',
                clickable: true
            },
        });

        $('[data-fancybox]').fancybox({
            youtube : {
                controls : 0,
                showinfo : 0
            }
        });

        var swiperSlide = new Swiper('.vbcwe-slide .swiper-container', {
            slidesPerView: 1,
            spaceBetween: 20,
            // autoplay: true,
            navigation: {
                nextEl: '.vbcwe-slide .swiper-button-next',
                prevEl: '.vbcwe-slide .swiper-button-prev',
            },
        });
    })
</script>
@endpush
