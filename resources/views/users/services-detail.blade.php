<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', 'VBCWE')

@section('content')
<main>
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ $servicesDetail->ser_title }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li>
                    <a href="/">{{ __('Dịch vụ') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="#">{{ cut_string2(translateData(app()->getLocale(), $servicesDetail->ser_title, $servicesDetail->ser_title_en), 15) }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="news-detail-main">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 col-sm-12 col-lg-12">
                    <div class="ndm-content">
                        <h2>{{ translateData(app()->getLocale(), $servicesDetail->ser_title, $servicesDetail->ser_title_en) }}</h2>
                        <div class="nbl-date">{{ __('Ngày') }} {{ date('d/m/Y', $servicesDetail->ser_create_time) }}</div>
                        <div class="news-description">
                            {!! translateData(app()->getLocale(), $servicesDetail->ser_description, $servicesDetail->ser_description_en) !!}
                        </div>
                    </div>
                </div>
            </div>

            @include("layout.news-related")
        </div>
    </div>
</main>
@endsection
