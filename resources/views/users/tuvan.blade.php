<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', __('Tư vấn doanh nghiệp'))

@section('content')
<main class="bg-gray">
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ __('Tư vấn doanh nghiệp') }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li>
                    <a href="/">{{ __('Dịch vụ') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="#">{{ __('Tư vấn doanh nghiệp') }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="services-detail__main">

        @foreach($dataServices as $kSer => $services)
            @if($kSer%2 == 0)
            <div class="sdm-top vbcwe-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-12 col-sm-12 col-lg-8">
                            <h2 class="about-title about-title-left">{{ translateData(app()->getLocale(), $services->ser_title, $services->ser_title_en) }}</h2>
                            <div class="sdmt-content">
                            {!! translateData(app()->getLocale(), $services->ser_description, $services->ser_description_en) !!}
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                            <div class="jn-image-center">
                                <div class="jn-image">
                                    <img src="{{ UploadHelper::getUrlImage('partners', $services->ser_picture) }}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @else
            <div class="sdm-top bg-violet vbcwe-inner">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                        <div class="jn-image-center">
                            <div class="jn-image">
                                <img src="{{ UploadHelper::getUrlImage('partners', $services->ser_picture) }}" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-sm-12 col-lg-8">
                        <h2 class="about-title about-title-left text-white">{{ translateData(app()->getLocale(), $services->ser_title, $services->ser_title_en) }}</h2>
                        <div class="sdmt-content sdmt-support">
                            {!! translateData(app()->getLocale(), $services->ser_description, $services->ser_description_en) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endif
        @endforeach
        <div class="vbc-register">
            <a href="#frm-register" class="vbc-register-link">
                <span>{{ __('ĐĂNG KÝ') }}</span>
                <span class="arrow-white">
                    <img src="{{ asset('images/arrow-white.png') }}" alt="">
                </span>
            </a>
        </div>
    </div>
</main>
@endsection
