<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', __('Đội ngũ'))

@section('content')
<main class="bg-gray">
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ __('Giới thiệu') }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li>
                    <a href="{{ url('gioi-thieu') }}">{{ __('Về chúng tôi') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="#">{{ __('Đội ngũ') }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="team-main">
        <div class="container">
            <div class="team-top">
                <div class="row">
                    <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                        <div class="tt-images">
                            <img src="{{ UploadHelper::getUrlImage('other', $teamBig->fou_picture) }}" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-sm-12 col-lg-8">
                        <div class="tt-content">
                            <div class="tt-content__main">
                                <h3>{{ translateData(app()->getLocale(), $teamBig->fou_name, $teamBig->fou_name_en) }}</h3>
                                <div class="tt-position">{{ translateData(app()->getLocale(), $teamBig->fou_chucvu, $teamBig->fou_chucvu_en) }}</div>
                                <div class="tt-info t_ov_4">
                                    {{ translateData(app()->getLocale(), $teamBig->fou_description, $teamBig->fou_description_en) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="team-list">
                <div class="row">
                    @foreach($teamList as $team)
                    <div class="col-12 col-md-12 col-sm-12 col-lg-3">
                        <div class="tl-item">
                            <div class="tl-images">
                                <img src="{{ UploadHelper::getUrlImage('other', $team->fou_picture) }}" alt="" class="img-fluid">
                            </div>
                            <div class="tl-name">{{ translateData(app()->getLocale(), $team->fou_name, $team->fou_name_en) }}</div>
                            <div class="tl-position">{{ translateData(app()->getLocale(), $team->fou_chucvu, $team->fou_chucvu_en) }}</div>
                            <div class="tl-teaser">{{ translateData(app()->getLocale(), $team->fou_description, $team->fou_description_en) }}</div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
