<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', 'Kết quả tìm kiếm')

@section('content')
    <main>
        <div class="box-breadcrumb">
            <div class="container">
                <h1>{{ __('kết quả tìm kiếm') }}</h1>
                <ul class="breadcrumb-sub">
                    <li>
                        <a href="/">{{ __('Trang chủ') }}</a>
                    </li>
                    <li>|</li>
                    <li class="active">
                        <a href="#">{{ __('Có') }} <b>{{ $searchResults->count() }}</b> {{ __('kết quả tìm kiếm') }} "{{ $keyword }}"</a>
                    </li>
                </ul>
            </div>
        </div>
        @if ($searchResults-> isEmpty())

        @else
            @foreach($searchResults->groupByType() as $type => $modelSearchResults)
            <div class="news-main search-main">
                <div class="container">
                    <div class="news-header--title">{{ __($type) }}</div>
                    <div class="news-thumb">
                        <div class="row">
                            @foreach($modelSearchResults as $searchResult)
                                @if($searchResult->searchable->searchableType == "Tin tức")
                                <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                                    <div class="news-item">
                                        <div class="news-img">
                                            <a href="{{ url('tin-tuc', [$searchResult->searchable->new_rewrite, $searchResult->searchable->new_id]) }}">
                                                <img src="{{ UploadHelper::getUrlImage('news', $searchResult->searchable->new_picture) }}" alt="">
                                            </a>
                                        </div>
                                        <div class="news-content">
                                            <h3 class="news-title">
                                                <a href="{{ url('tin-tuc', [$searchResult->searchable->new_rewrite, $searchResult->searchable->new_id]) }}" title="{{ translateData(app()->getLocale(), $searchResult->searchable->new_title, $searchResult->searchable->new_title_en) }}">{{ translateData(app()->getLocale(), $searchResult->searchable->new_title, $searchResult->searchable->new_title_en) }}</a>
                                            </h3>
                                            <div class="news-date">{{ __('Ngày') }} {{ date('d/m/Y', $searchResult->searchable->new_create_time) }}</div>
                                            <div class="news-teaser t_ov_3">{{ $searchResult->searchable->new_teaser }}</div>
                                            <div class="news-more">
                                                <a class="link-more-small" href="{{ url('tin-tuc', [$searchResult->searchable->new_rewrite, $searchResult->searchable->new_id]) }}"> > {{ __('Tìm hiểu thêm') }}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @else
{{--                                <div class="col-12 col-md-12 col-sm-12 col-lg-4">--}}
{{--                                    <div class="news-item">--}}
{{--                                        <div class="news-img">--}}
{{--                                            <a href="{{ url('dich-vu', [$searchResult->searchable->ser_name_rewrite, $searchResult->searchable->ser_id]) }}">--}}
{{--                                                <img src="{{ UploadHelper::getUrlImage('service', $searchResult->searchable->ser_picture) }}" alt="">--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                        <div class="news-content">--}}
{{--                                            <h3 class="news-title">--}}
{{--                                                <a href="{{ url('dich-vu', [$searchResult->searchable->ser_name_rewrite, $searchResult->searchable->ser_id]) }}" title="{{ translateData(app()->getLocale(), $searchResult->searchable->ser_title, $searchResult->searchable->ser_title_en) }}">{{ translateData(app()->getLocale(), $searchResult->searchable->ser_title, $searchResult->searchable->ser_title_en) }}</a>--}}
{{--                                            </h3>--}}
{{--                                            <div class="news-date">{{ __('Ngày') }} {{ date('d/m/Y', $searchResult->searchable->ser_create_time) }}</div>--}}
{{--                                            <div class="news-teaser t_ov_3">{{ $searchResult->searchable->ser_teaser }}</div>--}}
{{--                                            <div class="news-more">--}}
{{--                                                <a class="link-more-small" href="{{ url('dich-vu', [$searchResult->searchable->ser_name_rewrite, $searchResult->searchable->ser_id]) }}"> > {{ __('Tìm hiểu thêm') }}</a>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                @endif
                            @endforeach
                        </div>
                    </div>
                    @include("layout.pagination")
                </div>
            </div>
            @endforeach
        @endif
    </main>
@endsection
