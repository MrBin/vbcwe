<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', translateData(app()->getLocale(), $dataHub->sta_title, $dataHub->sta_title_en))

@section('content')
    <main>
        <div class="box-breadcrumb">
            <div class="container">
                <h1>{{ translateData(app()->getLocale(), $dataHub->sta_title, $dataHub->sta_title_en) }}</h1>
                <ul class="breadcrumb-sub">
                    <li>
                        <a href="/">{{ __('Trang chủ') }}</a>
                    </li>
                    <li>|</li>
                    <li>
                        <a href="javascript:;">{{ __('Kiến thức tham khảo') }}</a>
                    </li>
                    <li>|</li>
                    <li>
                        <a href="{{ url('kien-thuc-tham-khao', [$dataHub->category->cat_name_rewrite, $dataHub->category->cat_id]) }}">{{ translateData(app()->getLocale(), $dataHub->category->cat_name, $dataHub->category->cat_name_en) }}</a>
                    </li>
                    <li>|</li>
                    <li class="active">
                        <a href="javascript:;">{{ translateData(app()->getLocale(), $dataHub->sta_title, $dataHub->sta_title_en) }}</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="news-detail-main">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-sm-12 col-lg-12">
                        <div class="ndm-content">
                            <h2>{{ translateData(app()->getLocale(), $dataHub->sta_title, $dataHub->sta_title_en) }}</h2>
                            <div class="nbl-date">{{ __('Ngày') }} {{ date('d/m/Y', $dataHub->sta_created_time) }}</div>
                            <div class="events-images">
                                <img src="{{ UploadHelper::getUrlImage('other', $dataHub->sta_picture) }}" alt="" class="img-fluid">
                            </div>
                            <div class="news-description">
                                {!! translateData(app()->getLocale(), $dataHub->sta_description, $dataHub->sta_description_en) !!}
                            </div>
                            @if($dataHub->category->cat_name == 'Ấn phẩm - Báo cáo')
                            <a href="{{ url('dang-ky-tai-bao-cao') }}" class="vbc-register-link">
                                <span>{{ __('Tải tài liệu') }}</span>
                                <span class="arrow-white">
                                    <img src="{{ asset('images/arrow-white.png') }}" alt="">
                                </span>
                            </a>
                            @endif
                        </div>
                    </div>
{{--                    <div class="col-12 col-md-12 col-sm-12 col-lg-3">--}}
{{--                        <a href="" class="ndm-banner">--}}
{{--                            <img src="{{ asset('images/banner.jpg') }}" alt="" class="img-fluid">--}}
{{--                        </a>--}}
{{--                    </div>--}}
                </div>

{{--                @include("layout.static-related")--}}
            </div>
        </div>
    </main>
@endsection

