<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', 'VBCWE')

@section('content')
<main>
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ __('Tham gia mạng lưới') }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li>
                    <a href="{{ url('tham-gia-mang-luoi') }}">{{ __('Tham gia mạng lưới') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="#">{{ __('Các gói thành viên') }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="join-network-main">
        <div class="container">
            <div class="jn-title">
                <h2>{{ translateData(app()->getLocale(), $dataJoin->com_text_2_1, $dataJoin->com_text_2_1_en) }}</h2>
            </div>

            <div class="join-network-top">
                <div class="row">
                    <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                        <div class="jn-image">
                            <img src="{{ UploadHelper::getUrlImage('other', $dataJoin->com_picture_2) }}" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-sm-12 col-lg-8">
                        <div class="jn-text">
                            {!! translateData(app()->getLocale(), $dataJoin->com_text_2_2, $dataJoin->com_text_2_2_en) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="join-network-top">
                <div class="row">
                    <div class="col-12 col-md-12 col-sm-12 col-lg-7">
                        <div class="jn-title jn-title-left">
                            <h2>{!! translateData(app()->getLocale(), $dataJoin->com_text_3_1, $dataJoin->com_text_3_1_en) !!}</h2>
                        </div>
                        <div class="jn-text">
                            {!! translateData(app()->getLocale(), $dataJoin->com_text_3_2, $dataJoin->com_text_3_2_en) !!}
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-sm-12 col-lg-5">
                        <div class="jn-image">
                            <img src="{{ UploadHelper::getUrlImage('other', $dataJoin->com_picture_3) }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="join-network-bottom">
            <div class="container">
                <div class="jn-title">
                    <h2>{{ __('Các gói thành viên') }}</h2>
                </div>
                <div class="jnb-main text-center">
                    <img src="{{ UploadHelper::getUrlImage('other', $dataJoin->com_picture_1) }}" alt="" class="img-fluid">
                </div>
            </div>
        </div>
        <div class="vbc-register">
            <a href="{{ url('dang-ky-tham-gia') }}" class="vbc-register-link">
                <span>{{ __('ĐĂNG KÝ') }}</span>
                <span class="arrow-white">
                    <img src="images/arrow-white.png" alt="">
                </span>
            </a>
        </div>
    </div>
</main>
@endsection

