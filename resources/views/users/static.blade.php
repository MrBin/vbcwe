<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', translateData(app()->getLocale(), $staticBig->category->cat_name, $staticBig->category->cat_name_en))

@section('content')
<main>
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ translateData(app()->getLocale(), $staticBig->category->cat_name, $staticBig->category->cat_name_en) }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="javascript:;">{{ translateData(app()->getLocale(), $staticBig->category->cat_name, $staticBig->category->cat_name_en) }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="news-main">
        <div class="container">
            @if(isset($staticBig))
            <div class="news-big">
                <div class="row">
                    <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                        <div class="news-big__left">
                            <h2><a href="{{ url('kien-thuc-tham-khao', [$staticBig->category->cat_name_rewrite, $staticBig->sta_name_rewrite, $staticBig->sta_id]) }}">{{ translateData(app()->getLocale(), $staticBig->sta_title, $staticBig->sta_title_en) }}</a></h2>
                            <div class="nbl-date">{{ date('d/m/Y', $staticBig->sta_create_time) }}</div>
                            <div class="nbl-teaser">{{ translateData(app()->getLocale(), $staticBig->sta_teaser, $staticBig->sta_teaser_en) }}</div>
                            <a href="{{ url('kien-thuc-tham-khao', [$staticBig->category->cat_name_rewrite, $staticBig->sta_name_rewrite, $staticBig->sta_id]) }}" class="link-more-big">{{ __('Tìm hiểu thêm') }}</a>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-sm-12 col-lg-8">
                        <div class="nbr-img">
                            <img src="{{ UploadHelper::getUrlImage('other', $staticBig->sta_picture) }}" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            @endif

            <div class="news-thumb">
                <div class="row">
                @if(isset($dataStatic) && count($dataStatic) > 0)
                    @foreach($dataStatic as $static)
                    <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                        <div class="news-item">
                            <div class="news-img">
                                <a href="{{ url('kien-thuc-tham-khao', [$static->category->cat_name_rewrite, $static->sta_name_rewrite, $static->sta_id]) }}">
                                    <img src="{{ UploadHelper::getUrlImage('other', $static->sta_picture) }}" alt="">
                                </a>
                            </div>
                            <div class="news-content">
                                <h3 class="news-title">
                                    <a href="{{ url('kien-thuc-tham-khao', [$static->category->cat_name_rewrite, $static->sta_name_rewrite, $static->sta_id]) }}" title="{{ translateData(app()->getLocale(), $static->sta_title, $static->sta_title_en) }}">{{ translateData(app()->getLocale(), $static->sta_title, $static->sta_title_en) }}</a>
                                </h3>
                                <div class="news-date">{{ __('Ngày') . ' ' . date('d/m/Y', $static->sta_create_time) }}</div>
                                <div class="news-teaser">{{ translateData(app()->getLocale(), $static->sta_teaser, $static->sta_teaser_en) }}</div>
                                <div class="news-more">
                                    <a class="link-more-small" href="{{ url('kien-thuc-tham-khao', [$static->category->cat_name_rewrite, $static->sta_name_rewrite, $static->sta_id]) }}"> > {{ __('Tìm hiểu thêm') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
                </div>
            </div>
            @include("layout.pagination")
        </div>
    </div>
</main>
@endsection
