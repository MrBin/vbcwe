<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', 'VBCWE')

@section('content')
<main>
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ __('Thành viên Hội đồng quản trị') }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li>
                    <a href="{{ url('gioi-thieu') }}">{{ __('Về chúng tôi') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="#">{{ cut_string2(__('Thành viên Hội đồng quản trị'), 15) }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="member-main">
        <div class="member-top">
            <div class="member-top-main">
                <div class="container">
                    <div class="row">
                        @foreach($dataFoundersTop as $founderTop)
                        <div class="col-12 col-md-12 col-sm-12 col-lg-6">
                            <div class="member-top-item">
                                <img src="{{ UploadHelper::getUrlImage('other', $founderTop->fou_picture) }}" alt="" class="img-fluid">
                                <div class="mti-content">
                                    {!! translateData(app()->getLocale(), $founderTop->fou_description, $founderTop->fou_description_en) !!}
                                </div>
                                <div class="mti-g-name">
                                    <div class="mti-name-big">{!! translateData(app()->getLocale(), $founderTop->fou_name, $founderTop->fou_name_en) !!}</div>
                                    <div class="mti-note">{{ translateData(app()->getLocale(), $founderTop->fou_chucvu, $founderTop->fou_chucvu_en) }}</div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="member-bottom">
            <div class="container">
                <div class="member-bottom-thumb">
                    <div class="row">
                        @foreach($dataFoundersBottom as $founderBottom)
                        <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                            <div class="member-bottom-item">
                                <img src="{{ UploadHelper::getUrlImage('other', $founderBottom->fou_picture) }}" alt="">
                                <div class="mb-name">{{ translateData(app()->getLocale(), $founderBottom->fou_name, $founderBottom->fou_name_en) }}</div>
                                <div class="mb-teaser">{{ translateData(app()->getLocale(), $founderBottom->fou_chucvu, $founderBottom->fou_chucvu_en) }}</div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="member-bottom-thumb">
                    <div class="row">
                        <div class="col-2 col-md-2 col-sm-12"></div>
                        @foreach($dataFoundersBottom2 as $founderBottom2)
                        <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                            <div class="member-bottom-item">
                                <img src="{{ UploadHelper::getUrlImage('other', $founderBottom2->fou_picture) }}" alt="">
                                <div class="mb-name">{{ translateData(app()->getLocale(), $founderBottom2->fou_name, $founderBottom2->fou_name_en) }}</div>
                                <div class="mb-teaser">{{ translateData(app()->getLocale(), $founderBottom2->fou_chucvu, $founderBottom2->fou_chucvu_en) }}</div>
                            </div>
                        </div>
                        @endforeach
                        <div class="col-2 col-md-2 col-sm-12"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

