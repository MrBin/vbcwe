<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', __('Chiến dịch'))

@section('content')
    <main>
        <div class="box-breadcrumb">
            <div class="container">
                <h1>{{ __('Chiến dịch') }}</h1>
                <ul class="breadcrumb-sub">
                    <li>
                        <a href="/">{{ __('Trang chủ') }}</a>
                    </li>
                    <li>|</li>
                    <li>
                        <a href="javascript:;">{{ __('Hoạt động') }}</a>
                    </li>
                    <li>|</li>
                    <li class="active">
                        <a href="javascript:;">{{ __('Chiến dịch') }}</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="news-detail-main">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-sm-12 col-lg-12">
                        <div class="ndm-content">
                            <h2>{{ translateData(app()->getLocale(), $dataCampaignEvent->eve_title, $dataCampaignEvent->eve_title_en) }}</h2>
                            <div class="nbl-date">{{ __('Ngày') }} {{ date('d/m/Y', $dataCampaignEvent->eve_create_time) }}</div>
                            <div class="news-description">
                                {!! translateData(app()->getLocale(), $dataCampaignEvent->eve_description, $dataCampaignEvent->eve_description_en) !!}
                            </div>
                        </div>
                    </div>
{{--                    <div class="col-12 col-md-12 col-sm-12 col-lg-3">--}}
{{--                        <a href="" class="ndm-banner">--}}
{{--                            <img src="{{ asset('images/banner.jpg') }}" alt="" class="img-fluid">--}}
{{--                        </a>--}}
{{--                    </div>--}}
                </div>

                @include("layout.events-related")
            </div>
        </div>
    </main>
@endsection

