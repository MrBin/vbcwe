<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', __('Ghi nhận của Khách hàng - Đối tác'))

@section('content')
<main class="bg-gray">
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ __('Ghi nhận của Khách hàng - Đối tác') }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li>
                    <a href="{{ url('gioi-thieu') }}">{{ __('Về chúng tôi') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="#">{{ __('Ghi nhận của Khách hàng - Đối tác') }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="comments-main">
        <div class="container">
            <div class="comments-big">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        @foreach($dataOpinionsBig as $commentBig)
                        <div class="swiper-slide">
                            <div class="row">
                                <div class="col-12 col-md-12 col-sm-12 col-lg-6">
                                    <div class="comments-left">
                                        <div>
                                            <p>{!! translateData(app()->getLocale(), $commentBig->opi_description, $commentBig->opi_description_en) !!}</p>
                                            <div class="comment-author">{{ translateData(app()->getLocale(), $commentBig->opi_name, $commentBig->opi_name_en) }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-12 col-sm-12 col-lg-6">
                                    <div class="vbcv-video" style="background: url('https://img.youtube.com/vi/{{ get_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $commentBig->opi_video) }}/mqdefault.jpg') no-repeat center center; background-size: cover;">
                                        <a data-fancybox="" href="{{ $commentBig->opi_video }}">
{{--                                            <div class="vbcv-title">Investing in Women Workplace Gender Equality</div>--}}
                                            <img src="{{ asset('images/play.png') }}" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
            <div class="comments-list">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        @foreach($dataOpinions as $comment)
                        <div class="swiper-slide">
                            <div class="cl-images">
                                <img src="{{ UploadHelper::getUrlImage('other', $comment->opi_picture) }}" alt="{{ $comment->opi_name }}">
                            </div>
                            <div class="cl-comment">{!! translateData(app()->getLocale(), $comment->opi_description, $comment->opi_description_en) !!}</div>
                            <div class="cl-name">{{ translateData(app()->getLocale(), $comment->opi_name, $comment->opi_name_en) }}</div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </div>
</main>
@endsection
@push('scripts')
<script type="text/javascript">
    $(function () {

        var swiperCommentsBig = new Swiper('.comments-big .swiper-container', {
            slidesPerView: 1,
            spaceBetween: 20,
            navigation: {
                nextEl: '.comments-big .swiper-button-next',
                prevEl: '.comments-big .swiper-button-prev',
            },
            // autoplay: true,
            breakpoints: {
                640: {
                    slidesPerView: 1,
                    spaceBetween: 20
                },
                768: {
                    slidesPerView: 1,
                    spaceBetween: 40,
                },
                1024: {
                    slidesPerView: 1,
                    spaceBetween: 50,
                },
            }
        });

        var swiperComments = new Swiper('.comments-list .swiper-container', {
            slidesPerView: 1,
            spaceBetween: 20,
            navigation: {
                nextEl: '.comments-list .swiper-button-next',
                prevEl: '.comments-list .swiper-button-prev',
            },
            // autoplay: true,
            breakpoints: {
                640: {
                    slidesPerView: 1,
                    spaceBetween: 20
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 40,
                },
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 50,
                },
            }
        });
    })
</script>
@endpush

