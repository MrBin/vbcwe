<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', __('Tin tức'))

@section('content')
<main>
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ __('Tin tức') }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="javascript:;">{{ __('Tin tức') }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="news-main">
        <div class="container">
            @if(isset($newsBig))
            <div class="news-big">
                <div class="row">
                    <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                        <div class="news-big__left">
                            <h2><a href="{{ url('tin-tuc', [$newsBig->new_rewrite, $newsBig->new_id]) }}">{{ translateData(app()->getLocale(), $newsBig->new_title, $newsBig->new_title_en) }}</a></h2>
                            <div class="nbl-date">{{ date('d/m/Y', $newsBig->new_create_time) }}</div>
                            <div class="nbl-teaser t_ov_3">{{ translateData(app()->getLocale(), $newsBig->new_teaser, $newsBig->new_teaser_en) }}</div>
                            <a href="{{ url('tin-tuc', [$newsBig->new_rewrite, $newsBig->new_id]) }}" class="link-more-big">{{ __('Tìm hiểu thêm') }}</a>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-sm-12 col-lg-8">
                        <div class="nbr-img">
                            <img src="{{ UploadHelper::getUrlImage('news', $newsBig->new_picture) }}" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="news-filter">
                <div class="filter-item">
                    <select name="" id="filter-month" class="slb-filter">
                        <option value="0" selected>{{ __('THÁNG') }}</option>
                        <option value="1">1</option>
                        <option value="1">2</option>
                        <option value="1">3</option>
                        <option value="1">4</option>
                        <option value="1">5</option>
                        <option value="1">6</option>
                        <option value="1">7</option>
                        <option value="1">8</option>
                        <option value="1">9</option>
                        <option value="1">10</option>
                        <option value="1">11</option>
                        <option value="1">12</option>
                    </select>
                    <select name="" id="filter-year" class="slb-filter">
                        <option value="0" selected>{{ __('NĂM') }}</option>
                        @for($year=2010;$year<=date('Y', time());$year++)
                        <option value="{{ $year }}">{{ $year }}</option>
                        @endfor
                    </select>
                </div>
            </div>

            <div class="news-thumb">
                <div class="row">
                @if(isset($dataNews) && count($dataNews) > 0)
                    @foreach($dataNews as $news)
                    <div class="col-12 col-md-12 col-sm-12 col-lg-4">
                        <div class="news-item">
                            <div class="news-img">
                                <a href="{{ url('tin-tuc', [$news->new_rewrite, $news->new_id]) }}">
                                    <img src="{{ UploadHelper::getUrlImage('news', $news->new_picture) }}" alt="">
                                </a>
                            </div>
                            <div class="news-content">
                                <h3 class="news-title t_ov_3">
                                    <a class="t_ov_3" href="{{ url('tin-tuc', [$news->new_rewrite, $news->new_id]) }}" title="{{ translateData(app()->getLocale(), $news->new_title, $news->new_title_en) }}">{{ translateData(app()->getLocale(), $news->new_title, $news->new_title_en) }}</a>
                                </h3>
                                <div class="news-date">Ngày {{ date('d/m/Y', $news->new_create_time) }}</div>
                                <div class="news-teaser t_ov_3">{{ translateData(app()->getLocale(), $news->new_teaser, $news->new_teaser_en) }}</div>
                                <div class="news-more">
                                    <a class="link-more-small" href="{{ url('tin-tuc', [$news->new_rewrite, $news->new_id]) }}"> > {{ __('Tìm hiểu thêm') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
                </div>
            </div>
            @include("layout.pagination")

             @include("layout.news-related")
        </div>
    </div>
</main>
@endsection
