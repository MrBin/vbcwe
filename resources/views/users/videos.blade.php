<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', __('Videos - Thư viện - Vbcwe'))

@section('content')
<main>
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ __('Videos - Thư viện - Vbcwe') }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="#">{{ __('Videos - Thư viện - Vbcwe') }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="news-main">
        <div class="container">
            <div class="album-big bg-white">
                <h2>{{ translateData(app()->getLocale(), $videoBig->ban_name, $videoBig->ban_name_en) }}</h2>
                <div class="ab-main-slide abms-videos">
                    <div class="ab-main">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide" href="{{ $videoBig->ban_link }}" data-fancybox>
                                    <img src="{{ UploadHelper::getUrlImage('banner', $videoBig->ban_picture) }}" alt="" class="img-fluid">
                                    <div class="abms-play">
                                        <img src="{{ asset('images/icon-play.png') }}" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ab-control">
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
            </div>

            <div class="news-thumb">
                <div class="row">
                    @foreach($albumRelated as $albums)
                    <div class="col-12 col-md-6 col-sm-12 col-lg-4">
                        <div class="news-item">
                            <div class="news-img">
                                <a href="{{ url('thu-vien', [$albums->ban_name_rewrite, $albums->ban_id]) }}">
                                    <img src="{{ UploadHelper::getUrlImage('banner', $albums->ban_picture) }}" alt="">
                                </a>
                            </div>
                            <div class="news-content">
                                <h3 class="news-title album-title">
                                    <a href="{{ url('thu-vien', [$albums->ban_name_rewrite, $albums->ban_id]) }}" title="{{ translateData(app()->getLocale(), $albums->ban_name, $albums->ban_name_en) }}">{{ translateData(app()->getLocale(), $albums->ban_name, $albums->ban_name_en) }}</a>
                                </h3>
                                <div class="news-date">{{ __('Ngày') }} {{ date('d/m/Y', $albums->ban_create_time) }}</div>
                                <div class="news-more">
                                    <a class="link-more-small" href="{{ url('thu-vien', [$albums->ban_name_rewrite, $albums->ban_id]) }}"> > {{ __('Tìm hiểu thêm') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @include("layout.pagination")

            @include("layout.news-related")
        </div>
    </div>
</main>
@endsection
@push('scripts')
<script type="text/javascript">
    $(function () {
        var swiper = new Swiper('.ab-main .swiper-container', {
            slidesPerView: 1,
            spaceBetween: 20,
            autoplay: false,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            }
        });
    })
</script>
@endpush
