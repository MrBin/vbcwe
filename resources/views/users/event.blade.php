<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', __('Sự kiện sắp diễn ra'))

@section('content')
    <main class="bg-gray">
        <div class="box-breadcrumb">
            <div class="container">
                <h1>{{ __('Sự kiện sắp diễn ra') }}</h1>
                <ul class="breadcrumb-sub">
                    <li>
                        <a href="/">{{ __('Trang chủ') }}</a>
                    </li>
                    <li>|</li>
                    <li>
                        <a href="javascript:;">{{ __('Hoạt động') }}</a>
                    </li>
                    <li>|</li>
                    <li class="active">
                        <a href="javascript:;">{{ __('Sự kiện sắp diễn ra') }}</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="news-detail-main">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-sm-12 col-lg-12">
                        <div class="ndm-content">
                            <h2>{{ translateData(app()->getLocale(), $dataUpcomingEvent->eve_title, $dataUpcomingEvent->eve_title_en) }}</h2>
                            <div class="nbl-date">{{ __('Ngày') }} {{ date('d/m/Y', $dataUpcomingEvent->eve_create_time) }}</div>
                            <div class="events-images">
                                <img src="{{ UploadHelper::getUrlImage('news', $dataUpcomingEvent->eve_picture) }}" alt="" class="img-fluid">
                            </div>
                            <div class="news-description">
                                {!! translateData(app()->getLocale(), $dataUpcomingEvent->eve_description, $dataUpcomingEvent->eve_description_en) !!}
                            </div>
                            <div class="form-register__event">
                                <div class="fre-title">{{ __('Form đăng ký') }}</div>
                                <form action="" id="fre-form" method="POST">
                                    <div id="errors-show-contact" class="errors-show"></div>
                                    <div class="form-row form-group">
                                        <div class="col">
                                            <input type="text" class="form-control" name="event-name" id="event-name" placeholder="{{ __('Họ tên') }}*">
                                        </div>
                                        <div class="col">
                                            <input type="text" class="form-control" name="event-sex" id="event-sex" placeholder="{{ __('Giới tính') }}*">
                                        </div>
                                    </div>
                                    <div class="form-row form-group">
                                        <div class="col">
                                            <input type="text" class="form-control" name="event-company" id="event-company" placeholder="{{ __('Công ty/Tổ chức') }}*">
                                        </div>
                                        <div class="col">
                                            <input type="text" class="form-control" name="event-position" id="event-position" placeholder="{{ __('Chức vụ') }}">
                                        </div>
                                    </div>
                                    <div class="form-row form-group">
                                        <div class="col">
                                            <input type="email" class="form-control" name="event-email" id="event-email" placeholder="Email*">
                                        </div>
                                        <div class="col">
                                            <input type="text" class="form-control" name="event-phone" id="event-phone" placeholder="{{ __('Số điện thoại') }}*">
                                        </div>
                                    </div>
                                    <input type="hidden" id="event-action" name="event-action" value="action">
                                    <div class="vbc-register">
                                        <button class="vbc-register-link" type="submit">
                                            <span>{{ __('ĐĂNG KÝ') }}</span>
                                            <span class="arrow-white">
                                            <img src="{{ asset('images/arrow-white.png') }}" alt="">
                                        </span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
{{--                    <div class="col-12 col-md-12 col-sm-12 col-lg-3">--}}
{{--                        <a href="" class="ndm-banner">--}}
{{--                            <img src="{{ asset('images/banner.jpg') }}" alt="" class="img-fluid">--}}
{{--                        </a>--}}
{{--                    </div>--}}
                </div>

                @include("layout.events-related")
            </div>
        </div>
    </main>
@endsection

