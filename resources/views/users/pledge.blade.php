<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', translateData(app()->getLocale(), $dataCampaignEvent->eve_title, $dataCampaignEvent->eve_title_en))

@section('content')
<main  class="bg-gray">
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ translateData(app()->getLocale(), $dataCampaignEvent->eve_title, $dataCampaignEvent->eve_title_en) }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li>
                    <a href="#">{{ __('Hoạt động') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="#">{{ translateData(app()->getLocale(), $dataCampaignEvent->eve_title, $dataCampaignEvent->eve_title_en) }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="pledge-wrapper">
        <div class="container">
            <div class="pledge-main bg-white">
                <h2 class="about-title about-title-left">{{ translateData(app()->getLocale(), $dataCampaignEvent->eve_title, $dataCampaignEvent->eve_title_en) }}</h2>
                <div class="pm_main">
                    {!! translateData(app()->getLocale(), $dataCampaignEvent->eve_description, $dataCampaignEvent->eve_description_en) !!}
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

