<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', __('Đăng ký tham gia'))

@section('content')
<main>
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ __('Đăng ký tham gia') }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li>
                    <a href="{{ url('tham-gia-mang-luoi') }}">{{ __('Tham gia mạng lưới') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="#">{{ __('Đăng ký tham gia') }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="contact-main">
        <div class="contact-top bg-white">
            <div class="container">
                <div class="join-register__form">
                    <form method="POST" action="" id="jrf-form">
                        <div id="errors-show-contact" class="errors-show"></div>
                        <div class="form-row form-group">
                            <div class="col-sm-12 col-md-6">
                                <input type="text" class="form-control" name="join-name" id="join-name" placeholder="{{ __('Tên công ty') }}*">
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" class="form-control" name="join-area" id="join-area" placeholder="{{ __('Lĩnh vực hoạt động') }}*">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-12 col-md-12">
                                <input type="text" class="form-control" name="join-address" id="join-address" placeholder="{{ __('Địa chỉ giao dịch') }}*">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-12 col-md-4">
                                <select name="join-scale" id="join-scale" class="form-control">
                                    <option value="0">Quy mô công ty</option>
                                    <option value="1">0 - 50 nhân viên</option>
                                    <option value="2">51 - 200 nhân viên</option>
                                    <option value="3">201 - 500 nhân viên</option>
                                    <option value="4">500 - 1000 nhân viên</option>
                                    <option value="5">Hơn 1000 nhân viên</option>
                                </select>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <input type="text" class="form-control" name="join-website" id="join-website" placeholder="{{ __('Website') }}*">
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <input type="text" class="form-control" name="join-who" id="join-who" placeholder="{{ __('Người liên hệ') }}*">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-12 col-md-4">
                                <input type="text" class="form-control" name="join-position" id="join-position" placeholder="{{ __('Chức vụ') }}*">
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <input type="email" class="form-control" name="join-email" id="join-email" placeholder="{{ __('Email') }}*">
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <input type="text" class="form-control" name="join-phone" id="join-phone" placeholder="{{ __('Số điện thoại') }}*">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">{{ __('Nhu cầu') }} *</div>
                                </div>
                                <select name="join-need" id="join-need" class="form-control">
                                    <option value="0">Tìm hiểu về VBCWE</option>
                                    <option value="1">Gia nhập mạng lưới</option>
                                    <option value="2">Tư vấn về các công cụ đánh giá BĐG trong doanh nghiệp</option>
                                    <option value="3">Tìm hiểu về các thông tin khác</option>
                                </select>
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-send-contact text-uppercase">{{ __('Gửi thông tin') }}</button>
                            <input type="hidden" id="join-action" name="action" value="action">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

