<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('layout.master')
@section('title', __('Tải tài liệu'))

@section('content')
<main>
    <div class="box-breadcrumb">
        <div class="container">
            <h1>{{ __('Đăng ký tải tài liệu') }}</h1>
            <ul class="breadcrumb-sub">
                <li>
                    <a href="/">{{ __('Trang chủ') }}</a>
                </li>
                <li>|</li>
                <li>
                    <a href="javascript:;">{{ __('Kiến thức tham khảo') }}</a>
                </li>
                <li>|</li>
                <li class="active">
                    <a href="#">{{ __('Tải tài liệu') }}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="contact-main">
        <div class="contact-top bg-white">
            <div class="container">
                <div class="join-register__form">
                    <form method="POST" action="" id="fre-form">
                        <div id="errors-show-contact" class="errors-show"></div>
                        <div class="form-row form-group">
                            <div class="col-sm-12 col-md-6">
                                <input type="text" class="form-control" name="event-name" id="event-name" placeholder="{{ __('Họ tên') }}*">
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" class="form-control" name="event-sex" id="event-sex" placeholder="{{ __('Giới tính') }}*">
                            </div>
                        </div>
                        <div class="form-row form-group">
                            <div class="col-sm-12 col-md-12">
                                <input type="text" class="form-control" name="event-company" id="event-company" placeholder="{{ __('Công ty/Tổ chức') }}*">
                            </div>
                        </div>
                        <div class="form-row form-group">
                            <div class="col-sm-12 col-md-4">
                                <input type="text" class="form-control" name="event-position" id="event-position" placeholder="{{ __('Chức vụ') }}*">
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <input type="email" class="form-control" name="event-email" id="event-email" placeholder="{{ __('Email') }}*">
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <input type="text" class="form-control" name="event-phone" id="event-phone" placeholder="{{ __('Số điện thoại') }}*">
                            </div>
                        </div>
                        <div class="form-row form-group">
                            <div class="col-sm-12 col-md-12 input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">{{ __('Tên tài liệu') }} *</div>
                                </div>
                                <input type="text" placeholder="Tên tài liệu bạn muốn download" class="form-control" name="event-need" id="event-need">
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-send-contact btn-no-top text-uppercase">{{ __('Tải tài liệu') }}</button>
                             <input type="hidden" id="event-action" name="event-action" value="action">
                             <input type="hidden" id="event-type" name="event-type" value="3">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

