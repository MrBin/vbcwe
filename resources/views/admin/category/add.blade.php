@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.category.index')}}">Category</a>
      <span class="breadcrumb-item active">Thêm mới</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Thêm mới Category</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
	    <div class="br-section-wrapper container-fluid">
            @include("admin.error")
            <form role="form" method="post" action="{{route('admin.category.store')}}" enctype="multipart/form-data">
            	@csrf
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputCatname">Loại danh mục (*)</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                    <select class="form-control" name="cat_type" onchange="loadParent($(this).val())">
	                        <option value="0">-- Chọn --</option>
	                        @foreach ($arrType as $keys => $value)
	                            <?
	                            $checked = "";
	                            ?>
	                            <option value="{{ $keys }}" {{$checked}}>{{ $value }}</option>
	                        @endforeach
	                    </select>
	                </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputParent">Cấp cha</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <select class="form-control" name="cat_parent_id">
                            <option value="0">-- Chọn --</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Tên danh mục (*)</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                    {!! Form::text('cat_name', old('cat_name') ? old('cat_name') : null, ['class' => 'form-control','placeholder' => 'Nhập tên danh mục']) !!}
	                </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Từ khóa</label>
                     <div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                    {!! Form::textarea('cat_meta_keyword', old('cat_meta_keyword') ? old('cat_meta_keyword') : null, ['class' => 'form-control','placeholder' => 'Nhập từ khóa','style="height:100px;"']) !!}
	                </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Mô tả</label>
                     <div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                    {!! Form::textarea('cat_description', old('cat_description') ? old('cat_description') : null, ['class' => 'form-control','placeholder' => 'Nhập từ mô tả','style="height:100px;"']) !!}
	                </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="exampleInputFile">Ảnh đại diện</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                      	<input type="hidden" name="cat_picture" id="cat_picture" value="{{ old('cat_picture') }}">
						@component('admin.upload', ['id_upload' => 'gallery_upload_file', 'type_upload' => 'category', 'max_upload' => 1, 'item_upload' => 'cat_picture', 'name_data' => 'picture_data'])
						@endcomponent
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Order</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                    {!! Form::text('cat_order', old('cat_order') ? old('cat_order') : null, ['class' => 'form-control','placeholder' => 'Vị trí']) !!}
	                </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Kích hoạt</label>
                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                    <div class="custom-control custom-checkbox">
	                        {!! Form::checkbox('cat_active', '1', old('cat_active') ? true : false, ['class' => 'custom-control-input', 'id' => 'cat_active']) !!}
	                        <label class="col-sm-4 form-control-label custom-control-label" for="cat_active"></label>
	                    </div>
	                </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Nội dung</label>
                    <div class="col-sm-10 mg-b-10 mg-sm-b-10"><textarea class="form-control" rows="3" placeholder="Enter ..." id="editor1"></textarea></div>
                </div>
                <!-- /.card-body -->
                <div class="row mg-t-20">
                	<div class="col-sm-2 mg-b-10"></div>
                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                	<button type="reset" class="btn btn-warning mg-r-20">Làm lại</button>
	                    <button type="submit" class="btn btn-primary">Thêm mới</button>
	                </div>
                </div>
            </form>
        </div>
    </section>
</div>

@endsection

@section('JS_FOOTER')
    <script src="{{ asset('/assets/ckeditor/ckeditor.js') }}"></script>
    <script> CKEDITOR.replace('editor1'); </script>
    <script type="text/javascript">
        function loadParent($val){
            $.ajax({
                url: '/admin/category/ajax-load-parent',
                method: 'POST',
                data: {
                    typeID: $val
                },
                success: function (data) {
                    $('#parentBox').html(data);
                }
            });
        }

    </script>
@endsection