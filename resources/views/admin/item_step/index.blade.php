<?php
use App\Helper\Upload as UploadHelper;
use App\Repositories\ItemStepRepository as ItemStep;
?>
@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.configs.step')}}">Cấu hình</a>
      <span class="breadcrumb-item active">Danh sách cấu hình Step</span>
    </nav>
</div>
<div class="br-pagetitle" style="justify-content: space-between;">
	<h4>Danh sách cấu hình Step</h4>
	<div class=""><a href="{{ route('admin.configs.step_add') }}" class="btn btn-info">Thêm mới</a></div>
</div>
<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="">
  	  			<div class="row">
		            <div class="col-lg-3">
		              <input class="form-control" name="title" placeholder="Tên" type="text" value="{{ isset($dataSearch['title']) ? $dataSearch['title'] : '' }}">
		            </div>
		            <div class="col-lg-3">
		            	<select class="form-control" name="type">
                            <option value="0">-- Chọn --</option>
                            @foreach($allType as $key => $value)
                            	<option value="{{ $key }}" @if($key == $dataSearch['type']) selected="selected" @endif>
									{{ $value }}
                            	</option>
                            @endforeach
                        </select>
		            </div>
		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>

		  	<div class="bd rounded table-responsive">
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		                <tr class="thead-colored thead-light">
		                	<td class="wd-5p">STT</td>
							<td class="wd-10p">Tên</td>
							<td class="wd-10p">Tên En</td>
							<td class="wd-10p">Loại</td>
							<td class="wd-10p">Sắp xếp</td>
							<td class="wd-10p text-center">Kích hoạt</td>
							<td class="wd-10p text-center">Ngày tạo</td>
							<td class="wd-10p text-center">Sửa</td>
							<td class="wd-10p text-center">Xóa</td>
		                </tr>
				      	@foreach ($statics as $item)
							<tr id="record_{{ $item->its_id }}">
					          	<td class="text-center">{{ $loop->index + 1 }}</td>
					          	<td>{{ $item->its_title }}</td>
					          	<td>{{ $item->its_title_en }}</td>
					          	<td>{{ ItemStep::getTypeLabel($item->its_type) }}</td>
					          	<td>{{ $item->its_order }}</td>
					          	<td class="text-center">
									<a href="javascript:;" class="box_active" onclick="changeActive('{{ route('admin.configs.step_status')}}', '{{ $item->its_id }}', 'active')">
										@if($item->its_status == 1)
											<i class="far fa-check-square" style="font-size: 14px;"></i>
										@else
											<i class="far fa-square" style="font-size: 14px;"></i>
										@endif
									</a>
								</td>
					          	<td class="text-center">{{ date("d/m/y H:i",$item->its_created_time) }}</td>
					          	<td class="text-center">
					          		<a href="{{ route('admin.configs.step_add', ['id' => $item->its_id])}}" class=""><i class="fas fa-edit"></i> Sửa</a>
					          	</td>
					          	<td class="text-center">
									<a class="buttonDelteRecord" href="{{ route('admin.configs.step_delete', ['id' => $item->its_id]) }}" ><i class="fa fa-times-circle"></i> Xóa</a>
					          	</td>
					        </tr>
				      	@endforeach

		      		</tbody>
		  	  	</table>
		  	</div>

        </div>
    </section>
    @include("admin.pagination")
</div>
@endsection

