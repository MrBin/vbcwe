@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.configs.user_translate')}}">Ngôn ngữ</a>
      <span class="breadcrumb-item active">Danh sách</span>
    </nav>
</div>
<div class="br-pagetitle" style="justify-content: space-between;">
	<h4>Danh sách ngôn ngữ</h4>
</div>
<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="">
  	  			<div class="row">
		            <div class="col-lg-2">
		              <input class="form-control" name="keyword" placeholder="Từ khóa" type="text" value="{{ isset($dataSearch['keyword']) ? $dataSearch['keyword'] : '' }}">
		            </div>
		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>

		  	<div class="bd rounded table-responsive">
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		                <tr class="thead-colored thead-light">
		                	<td class="wd-5p">STT</td>
							<td class="wd-10p">Từ khóa</td>
							<td class="wd-10p">Tiếng Việt</td>
							<td class="wd-10p">Tiếng Anh</td>
							<td class="wd-10p text-center">Cập nhật</td>
							<td class="wd-10p text-center">Sửa</td>
		                </tr>
				      	@foreach ($data as $item)
							<tr id="record_{{ $item->ust_keyword_md5}}">
					          	<td class="text-center">{{ $loop->index + 1 + ($pagination['current_page'] - 1) * $pagination['perpage'] }}</td>
					          	<td class="keyword">{{ $item->ust_keyword }}</td>
					          	<td class="keyword_vi">{{ $item->ust_vi }}</td>
					          	<td class="keyword_en">{{ $item->ust_en }}</td>
					          	<td class="text-center">{{ date("d/m/y H:i",$item->ust_updated_at) }}</td>
					          	<td class="text-center">
					          		<a href="javascript:;" onclick="showEditKeyword('{{ $item->ust_keyword_md5 }}');" class=""><i class="fas fa-edit"></i> Sửa</a>
					          	</td>
					        </tr>
				      	@endforeach

		      		</tbody>
		  	  	</table>
		  	</div>

        </div>
    </section>
    @include("admin.pagination")
</div>
<div class="modal fade" id="editKeywordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Sửa từ khóa: <span id="keyword_edit"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        	<form action="">
                <div class="row">
                    <label class="col-3 form-control-label" for="InputUsername">Tiếng việt</label>
                    <div class="col-9 mg-b-10 mg-sm-b-10">
	                    <input type="text" name="keyword_vi" id="keyword_vi" value="" class="form-control">
	                    <input type="hidden" name="record_id" id="record_id" value="" class="form-control">
	                </div>
                </div>
                <div class="row">
                    <label class="col-3 form-control-label" for="InputUsername">Tiếng Anh</label>
                    <div class="col-9 mg-b-10 mg-sm-b-10">
	                    <input type="text" name="keyword_en" id="keyword_en" value="" class="form-control">
	                </div>
                </div>
        	</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        <button type="button" class="btn btn-primary" onclick="editKeyword();">Cập nhật</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section('JS_FOOTER')
	<script>
		function showEditKeyword(id){
			var keyword		= $("#record_" + id + ' .keyword').text();
			var keyword_vi	= $("#record_" + id + ' .keyword_vi').text();
			var keyword_en	= $("#record_" + id + ' .keyword_en').text();
			$('#editKeywordModal #keyword_edit').html(keyword);
			$('#editKeywordModal #record_id').val(id);
			$('#editKeywordModal #keyword_vi').val(keyword_vi);
			$('#editKeywordModal #keyword_en').val(keyword_en);
			$('#editKeywordModal').modal({});
		}

		function editKeyword(){
			var record_id	= $('#editKeywordModal #record_id').val();
			var keyword_vi	= $('#editKeywordModal #keyword_vi').val();
			var keyword_en	= $('#editKeywordModal #keyword_en').val();

			$.ajax({
				type: 'POST',
				url: '{{ route("admin.configs.user_translate_add") }}',
				data: {record_id: record_id, keyword_vi: keyword_vi, keyword_en: keyword_en},
				success: function(data){
					if(data.status == true){
						$("#record_" + record_id + ' .keyword_vi').text(keyword_vi);
						$("#record_" + record_id + ' .keyword_en').text(keyword_en);
						$('#editKeywordModal').modal('hide');
					}else{
						alert(data.message);
					}
				},
				dataType: 'json'
			});
		}
	</script>
@endsection
