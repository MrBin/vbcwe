@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.configs.gears')}}">Cấu hình trang GEARS</a>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Cấu hình trang GEARS</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
	    <div class="br-section-wrapper container-fluid">
            @include("admin.error")
            <form role="form" method="post" action="" enctype="multipart/form-data">
            	@csrf
				<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 1 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_1" id="text_1" value="{{ $data['text_1'] }}" class="form-control">
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 1 (EN)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_1_en" id="text_1_en" value="{{ $data['text_1_en'] }}" class="form-control">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Ảnh 2 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="hidden" name="picture_2" id="picture_2" value="{{ $data['picture_2'] }}">
							@component('admin.upload', ['class_add' => 'd-flex', 'arrGallery' => $data['picture_data_2'], 'id_upload' => 'gallery_upload_file_2', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'picture_2', 'name_data' => 'picture_data_2'])
							@endcomponent
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Ảnh 2 (EN)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="hidden" name="picture_2_en" id="picture_2_en" value="{{ $data['picture_2_en'] }}">
							@component('admin.upload', ['class_add' => 'd-flex', 'arrGallery' => $data['picture_data_2_en'], 'id_upload' => 'gallery_upload_file_2_en', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'picture_2_en', 'name_data' => 'picture_data_2_en'])
							@endcomponent
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 2.1 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_2_1" id="text_2_1" value="{{ $data['text_2_1'] }}" class="form-control">
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 2.1 (EN)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_2_1_en" id="text_2_1_en" value="{{ $data['text_2_1_en'] }}" class="form-control">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 2.2 (VI)</label>
	                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
		                    <textarea name="text_2_2" class="form-control " id="text_2_2">{{ $data['text_2_2'] }}</textarea>
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 2.2 (EN)</label>
	                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
		                    <textarea name="text_2_2_en" class="form-control " id="text_2_2_en">{{ $data['text_2_2_en'] }}</textarea>
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 3 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_3" id="text_3" value="{{ $data['text_3'] }}" class="form-control">
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 3 (EN)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_3_en" id="text_3_en" value="{{ $data['text_3_en'] }}" class="form-control">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 4 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_4" id="text_4" value="{{ $data['text_4'] }}" class="form-control">
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 4 (EN)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_4_en" id="text_4_en" value="{{ $data['text_4_en'] }}" class="form-control">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 5 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_5" id="text_5" value="{{ $data['text_5'] }}" class="form-control">
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 5 (EN)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_5_en" id="text_5_en" value="{{ $data['text_5_en'] }}" class="form-control">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 6 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_6" id="text_6" value="{{ $data['text_6'] }}" class="form-control">
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 6 (EN)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_6_en" id="text_6_en" value="{{ $data['text_6_en'] }}" class="form-control">
		                </div>
	                </div>

	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 7 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_7" id="text_7" value="{{ $data['text_7'] }}" class="form-control">
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 7 (EN)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_7_en" id="text_7_en" value="{{ $data['text_7_en'] }}" class="form-control">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Ảnh 7 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="hidden" name="picture_7" id="picture_7" value="{{ $data['picture_7'] }}">
							@component('admin.upload', ['class_add' => 'd-flex', 'arrGallery' => $data['picture_data_7'], 'id_upload' => 'gallery_upload_file_7', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'picture_7', 'name_data' => 'picture_data_7'])
							@endcomponent
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-7 form-control-label" for="InputUsername">Ảnh 7 (EN)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="hidden" name="picture_7_en" id="picture_7_en" value="{{ $data['picture_7_en'] }}">
							@component('admin.upload', ['class_add' => 'd-flex', 'arrGallery' => $data['picture_data_7_en'], 'id_upload' => 'gallery_upload_file_7_en', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'picture_7_en', 'name_data' => 'picture_data_7_en'])
							@endcomponent
		                </div>
	                </div>
	            </div>

                <!-- /.card-body -->
                <div class="row mg-t-20">
                	<div class="col-sm-2 mg-b-10"></div>
                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
                		<input type="hidden" name="action" value="execute">
	                	<button type="reset" class="btn btn-warning mg-r-20">Làm lại</button>
	                    <button type="submit" class="btn btn-primary">Cập nhật</button>
	                </div>
                </div>
            </form>
        </div>
    </section>
</div>

@endsection

@section('JS_FOOTER')
	<script src="{{ asset('/ckeditor/ckeditor.js') }}"></script>
	<script> CKEDITOR.replace('text_2_2'); </script>
	<script> CKEDITOR.replace('text_2_2_en'); </script>
@endsection