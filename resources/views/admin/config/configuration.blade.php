@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.configs.configuration')}}">Cấu hình chung</a>
      <span class="breadcrumb-item active">Thêm mới</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Thêm mới Banner</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
	    <div class="br-section-wrapper container-fluid">
            @include("admin.error")
            <form role="form" method="post" action="" enctype="multipart/form-data">
            	@csrf
            	<ul class="nav nav-tabs mg-b-20" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Thông tin</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="orther-tab" data-toggle="tab" href="#orther" role="tab" aria-controls="orther" aria-selected="false">Cấu hình hiển thị</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Cấu hình SEO</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="email-tab" data-toggle="tab" href="#email" role="tab" aria-controls="email" aria-selected="false">Cấu hình Email</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="social-tab" data-toggle="tab" href="#social" role="tab" aria-controls="social" aria-selected="false">Mạng xã hội</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
						<div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Website (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="website" value="{{ $data['website'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Website (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="website_en" value="{{ $data['website_en'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Địa chỉ (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="address" value="{{ $data['address'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Địa chỉ (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="address_en" value="{{ $data['address_en'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Web master tool (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="web_master_tool" value="{{ $data['web_master_tool'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Web master tool (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="web_master_tool_en" value="{{ $data['web_master_tool_en'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Favicon (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="favicon" id="con_favicon" value="{{ $data['favicon'] }}">
								@component('admin.upload', ['arrGallery' => $data['favicon_data'], 'id_upload' => 'gallery_upload_file_favicon', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_favicon', 'name_data' => 'favicon_data'])
								@endcomponent
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Favicon (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="favicon_en" id="con_favicon_en" value="{{ $data['favicon_en'] }}">
								@component('admin.upload', ['arrGallery' => $data['favicon_en_data'], 'id_upload' => 'gallery_upload_file_favicon_en', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_favicon_en', 'name_data' => 'favicon_en_data'])
								@endcomponent
			                </div>
		                </div>

		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Analytic (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="analytics" value="{{ $data['analytics'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Analytic (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="analytics_en" value="{{ $data['analytics_en'] }}">
			                </div>
		                </div>

		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Mail to công ty( (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="mail" value="{{ $data['mail'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Mail to công ty( (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="mail_en" value="{{ $data['mail_en'] }}">
			                </div>
		                </div>

		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Hotline 1( (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="hotline_1" value="{{ $data['hotline_1'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Hotline 1( (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="hotline_1_en" value="{{ $data['hotline_1_en'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Hotline 2( (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="hotline_2" value="{{ $data['hotline_2'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Hotline 2( (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="hotline_2_en" value="{{ $data['hotline_2_en'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Logo (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="logo" id="con_logo" value="{{ $data['logo'] }}">
								@component('admin.upload', ['arrGallery' => $data['logo_data'], 'id_upload' => 'gallery_upload_file_logo', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_logo', 'name_data' => 'logo_data'])
								@endcomponent
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Logo (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="logo_en" id="con_logo_en" value="{{ $data['logo_en'] }}">
								@component('admin.upload', ['arrGallery' => $data['logo_en_data'], 'id_upload' => 'gallery_upload_file_logo_en', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_logo_en', 'name_data' => 'logo_en_data'])
								@endcomponent
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Copyright ( (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="copyright" value="{{ $data['copyright'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Copyright ( (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="copyright_en" value="{{ $data['copyright_en'] }}">
			                </div>
		                </div>
		            </div>
		            <div class="tab-pane fade" id="orther" role="tabpanel" aria-labelledby="orther-tab">
		            	<div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Banner trang search (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="banner_search" id="con_banner_search" value="{{ $data['banner_search'] }}">
								@component('admin.upload', ['arrGallery' => $data['banner_search_data'], 'id_upload' => 'gallery_upload_file_banner_search', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_banner_search', 'name_data' => 'banner_search_data'])
								@endcomponent
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Banner trang search (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="banner_search_en" id="con_banner_search_en" value="{{ $data['banner_search_en'] }}">
								@component('admin.upload', ['arrGallery' => $data['banner_search_en_data'], 'id_upload' => 'gallery_upload_file_banner_search_en', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_banner_search_en', 'name_data' => 'banner_search_en_data'])
								@endcomponent
			                </div>
		                </div>


		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Banner thư viện (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="banner_thuvien" id="con_banner_thuvien" value="{{ $data['banner_thuvien'] }}">
								@component('admin.upload', ['arrGallery' => $data['banner_thuvien_data'], 'id_upload' => 'gallery_upload_file_banner_thuvien', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_banner_thuvien', 'name_data' => 'banner_thuvien_data'])
								@endcomponent
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Banner thư viện (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="banner_thuvien_en" id="con_banner_thuvien_en" value="{{ $data['banner_thuvien_en'] }}">
								@component('admin.upload', ['arrGallery' => $data['banner_thuvien_en_data'], 'id_upload' => 'gallery_upload_file_banner_thuvien_en', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_banner_thuvien_en', 'name_data' => 'banner_thuvien_en_data'])
								@endcomponent
			                </div>
		                </div>


		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Banner mặc định (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="banner_default" id="con_banner_default" value="{{ $data['banner_default'] }}">
								@component('admin.upload', ['arrGallery' => $data['banner_default_data'], 'id_upload' => 'gallery_upload_file_banner_default', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_banner_default', 'name_data' => 'banner_default_data'])
								@endcomponent
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Banner mặc định (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="banner_default_en" id="con_banner_default_en" value="{{ $data['banner_default_en'] }}">
								@component('admin.upload', ['arrGallery' => $data['banner_default_en_data'], 'id_upload' => 'gallery_upload_file_banner_default_en', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_banner_default_en', 'name_data' => 'banner_default_en_data'])
								@endcomponent
			                </div>
		                </div>


		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Banner trang sự kiện (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="banner_event" id="con_banner_event" value="{{ $data['banner_event'] }}">
								@component('admin.upload', ['arrGallery' => $data['banner_event_data'], 'id_upload' => 'gallery_upload_file_banner_event', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_banner_event', 'name_data' => 'banner_event_data'])
								@endcomponent
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Banner trang sự kiện (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="banner_event_en" id="con_banner_event_en" value="{{ $data['banner_event_en'] }}">
								@component('admin.upload', ['arrGallery' => $data['banner_event_en_data'], 'id_upload' => 'gallery_upload_file_banner_event_en', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_banner_event_en', 'name_data' => 'banner_event_en_data'])
								@endcomponent
			                </div>
		                </div>


		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Banner trang liên hệ (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="banner_contact" id="con_banner_contact" value="{{ $data['banner_contact'] }}">
								@component('admin.upload', ['arrGallery' => $data['banner_contact_data'], 'id_upload' => 'gallery_upload_file_banner_contact', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_banner_contact', 'name_data' => 'banner_contact_data'])
								@endcomponent
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Banner trang liên hệ (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="banner_contact_en" id="con_banner_contact_en" value="{{ $data['banner_contact_en'] }}">
								@component('admin.upload', ['arrGallery' => $data['banner_contact_en_data'], 'id_upload' => 'gallery_upload_file_banner_contact_en', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_banner_contact_en', 'name_data' => 'banner_contact_en_data'])
								@endcomponent
			                </div>
		                </div>

		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Nội dung trang liên hệ (VI)</label>
		                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="content_contact" class="form-control " id="content_contact">{{ $data['content_contact'] }}</textarea>
			                </div>
		                </div>
						<div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Nội dung trang liên hệ (EN)</label>
		                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="content_contact_en" class="form-control " id="content_contact_en">{{ $data['content_contact_en'] }}</textarea>
			                </div>
		                </div>

		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Tiêu đề trang các mạng lưới doanh nghiệp ( (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="title_mangluoi" value="{{ $data['title_mangluoi'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Tiêu đề trang các mạng lưới doanh nghiệp ( (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="title_mangluoi_en" value="{{ $data['title_mangluoi_en'] }}">
			                </div>
		                </div>

		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Banner trang các mạng lưới doanh nghiệp (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="banner_mangluoi" id="con_banner_mangluoi" value="{{ $data['banner_mangluoi'] }}">
								@component('admin.upload', ['arrGallery' => $data['banner_mangluoi_data'], 'id_upload' => 'gallery_upload_file_banner_mangluoi', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_banner_mangluoi', 'name_data' => 'banner_mangluoi_data'])
								@endcomponent
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Banner trang các mạng lưới doanh nghiệp (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="banner_mangluoi_en" id="con_banner_mangluoi_en" value="{{ $data['banner_mangluoi_en'] }}">
								@component('admin.upload', ['arrGallery' => $data['banner_mangluoi_en_data'], 'id_upload' => 'gallery_upload_file_banner_mangluoi_en', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_banner_mangluoi_en', 'name_data' => 'banner_mangluoi_en_data'])
								@endcomponent
			                </div>
		                </div>

		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Tiêu đề trang các thành viên sáng lập (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="title_member" value="{{ $data['title_member'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Tiêu đề trang các thành viên sáng lập (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="title_member_en" value="{{ $data['title_member_en'] }}">
			                </div>
		                </div>

		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Banner trang các thành viên sáng lập (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="banner_member" id="con_banner_member" value="{{ $data['banner_member'] }}">
								@component('admin.upload', ['arrGallery' => $data['banner_member_data'], 'id_upload' => 'gallery_upload_file_banner_member', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_banner_member', 'name_data' => 'banner_member_data'])
								@endcomponent
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Banner trang các thành viên sáng lập (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="banner_member_en" id="con_banner_member_en" value="{{ $data['banner_member_en'] }}">
								@component('admin.upload', ['arrGallery' => $data['banner_member_en_data'], 'id_upload' => 'gallery_upload_file_banner_member_en', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_banner_member_en', 'name_data' => 'banner_member_en_data'])
								@endcomponent
			                </div>
		                </div>



		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Tiêu đề trang tham gia mạng lưới (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="title_thamgia_mangluoi" value="{{ $data['title_thamgia_mangluoi'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Tiêu đề trang tham gia mạng lưới (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="title_thamgia_mangluoi_en" value="{{ $data['title_thamgia_mangluoi_en'] }}">
			                </div>
		                </div>

		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Banner trang tham gia mạng lưới (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="banner_thamgia_mangluoi" id="con_banner_thamgia_mangluoi" value="{{ $data['banner_thamgia_mangluoi'] }}">
								@component('admin.upload', ['arrGallery' => $data['banner_thamgia_mangluoi_data'], 'id_upload' => 'gallery_upload_file_banner_thamgia_mangluoi', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_banner_thamgia_mangluoi', 'name_data' => 'banner_thamgia_mangluoi_data'])
								@endcomponent
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Banner trang tham gia mạng lưới (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="banner_thamgia_mangluoi_en" id="con_banner_thamgia_mangluoi_en" value="{{ $data['banner_thamgia_mangluoi_en'] }}">
								@component('admin.upload', ['arrGallery' => $data['banner_thamgia_mangluoi_en_data'], 'id_upload' => 'gallery_upload_file_banner_thamgia_mangluoi_en', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_banner_thamgia_mangluoi_en', 'name_data' => 'banner_thamgia_mangluoi_en_data'])
								@endcomponent
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Nội dung trang tham gia mạng lưới (VI)</label>
		                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="description_thamgia_mangluoi" class="form-control " id="description_thamgia_mangluoi">{{ $data['description_thamgia_mangluoi'] }}</textarea>
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Nội dung trang tham gia mạng lưới (VI)</label>
		                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="description_thamgia_mangluoi_en" class="form-control " id="description_thamgia_mangluoi_en">{{ $data['description_thamgia_mangluoi_en'] }}</textarea>
			                </div>
		                </div>


		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Tiêu đề trang kiến thức tham khảo (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="title_kienthuc_thamkhao" value="{{ $data['title_kienthuc_thamkhao'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Tiêu đề trang kiến thức tham khảo (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="title_kienthuc_thamkhao_en" value="{{ $data['title_kienthuc_thamkhao_en'] }}">
			                </div>
		                </div>

		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Banner trang kiến thức tham khảo (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="banner_kienthuc_thamkhao" id="con_banner_kienthuc_thamkhao" value="{{ $data['banner_kienthuc_thamkhao'] }}">
								@component('admin.upload', ['arrGallery' => $data['banner_kienthuc_thamkhao_data'], 'id_upload' => 'gallery_upload_file_banner_kienthuc_thamkhao', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_banner_kienthuc_thamkhao', 'name_data' => 'banner_kienthuc_thamkhao_data'])
								@endcomponent
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Banner trang kiến thức tham khảo (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
			                    <input type="hidden" name="banner_kienthuc_thamkhao_en" id="con_banner_kienthuc_thamkhao_en" value="{{ $data['banner_kienthuc_thamkhao_en'] }}">
								@component('admin.upload', ['arrGallery' => $data['banner_kienthuc_thamkhao_en_data'], 'id_upload' => 'gallery_upload_file_banner_kienthuc_thamkhao_en', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'con_banner_kienthuc_thamkhao_en', 'name_data' => 'banner_kienthuc_thamkhao_en_data'])
								@endcomponent
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Nội dung trang kiến thức tham khảo (VI)</label>
		                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="description_kienthuc_thamkhao" class="form-control " id="description_kienthuc_thamkhao">{{ $data['description_kienthuc_thamkhao'] }}</textarea>
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Nội dung trang kiến thức tham khảo (VI)</label>
		                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="description_kienthuc_thamkhao_en" class="form-control " id="description_kienthuc_thamkhao_en">{{ $data['description_kienthuc_thamkhao_en'] }}</textarea>
			                </div>
		                </div>

		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Nội dung Footer (VI)</label>
		                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="footer" class="form-control " id="footer">{{ $data['footer'] }}</textarea>
			                </div>
		                </div>
						<div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Nội dung Footer (EN)</label>
		                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="footer_en" class="form-control " id="footer_en">{{ $data['footer_en'] }}</textarea>
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Bản đồ trang liên hệ (VI)</label>
		                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="map_contact" class="form-control " id="map_contact">{{ $data['map_contact'] }}</textarea>
			                </div>
		                </div>
						<div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Bản đồ trang liên hệ (EN)</label>
		                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="map_contact_en" class="form-control " id="map_contact_en">{{ $data['map_contact_en'] }}</textarea>
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Link video ý kiến đối tác</label>
		                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="add_footer" class="form-control " id="add_footer">{{ $data['add_footer'] }}</textarea>
			                </div>
		                </div>
{{--						<div class="row">--}}
{{--		                    <label class="col-sm-2 form-control-label" for="InputUsername">Chèn nội dung Footer (EN)</label>--}}
{{--		                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">--}}
{{--			                    <textarea name="add_footer_en" class="form-control " id="add_footer_en">{{ $data['add_footer_en'] }}</textarea>--}}
{{--			                </div>--}}
{{--		                </div>--}}
{{--		                <div class="row">--}}
{{--		                    <label class="col-sm-2 form-control-label" for="InputUsername">Chèn nội dung Header (VI)</label>--}}
{{--		                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">--}}
{{--			                    <textarea name="add_header" class="form-control " id="add_header">{{ $data['add_header'] }}</textarea>--}}
{{--			                </div>--}}
{{--		                </div>--}}
{{--						<div class="row">--}}
{{--		                    <label class="col-sm-2 form-control-label" for="InputUsername">Chèn nội dung Header (EN)</label>--}}
{{--		                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">--}}
{{--			                    <textarea name="add_header_en" class="form-control " id="add_header_en">{{ $data['add_header_en'] }}</textarea>--}}
{{--			                </div>--}}
{{--		                </div>--}}
					</div>
		            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
						<div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Từ khóa SEO (VI)</label>
			                <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="seo_keyword" class="form-control " id="seo_keyword">{{ $data['seo_keyword'] }}</textarea>
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Từ khóa SEO (EN)</label>
		                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="seo_keyword_en" class="form-control " id="seo_keyword_en">{{ $data['seo_keyword_en'] }}</textarea>
			                </div>
		                </div>

		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Mô tả SEO (VI)</label>
		                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="seo_description" class="form-control " id="seo_description">{{ $data['seo_description'] }}</textarea>
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Mô tả SEO (EN)</label>
		                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="seo_description_en" class="form-control " id="seo_description_en">{{ $data['seo_description_en'] }}</textarea>
			                </div>
		                </div>

		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Title SEO (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="seo_title" value="{{ $data['seo_title'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Title SEO (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="seo_title_en" value="{{ $data['seo_title_en'] }}">
			                </div>
		                </div>
		            </div>
		            <div class="tab-pane fade" id="email" role="tabpanel" aria-labelledby="email-tab">
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Tiêu đề mail (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="title_email" value="{{ $data['title_email'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Tiêu đề mail (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="title_email_en" value="{{ $data['title_email_en'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Tên email (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="name_email" value="{{ $data['name_email'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Tên email (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="name_email_en" value="{{ $data['name_email_en'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Email nhận thông tin đơn hàng (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="mail_receive_order" value="{{ $data['mail_receive_order'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Email nhận thông tin đơn hàng (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="mail_receive_order_en" value="{{ $data['mail_receive_order_en'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Email gửi( (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="mail_send_order" value="{{ $data['mail_send_order'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Email gửi( (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="mail_send_order_en" value="{{ $data['mail_send_order_en'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Pass mail gửi (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="mail_pass_send_order" value="{{ $data['mail_pass_send_order'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Pass mail gửi (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="mail_pass_send_order_en" value="{{ $data['mail_pass_send_order_en'] }}">
			                </div>
		                </div>
		            </div>
		            <div class="tab-pane fade" id="social" role="tabpanel" aria-labelledby="social-tab">
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Linkedin (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="linkedin" value="{{ $data['linkedin'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Linkedin (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="linkedin_en" value="{{ $data['linkedin_en'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Youtube (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="youtube" value="{{ $data['youtube'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Youtube (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="youtube_en" value="{{ $data['youtube_en'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Facebook (VI)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="facebook" value="{{ $data['facebook'] }}">
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Facebook (EN)</label>
		                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
								<input type="text" class="form-control" name="facebook_en" value="{{ $data['facebook_en'] }}">
			                </div>
		                </div>
		            </div>
	            </div>

                <!-- /.card-body -->
                <div class="row mg-t-20">
                	<div class="col-sm-2 mg-b-10"></div>
                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
                		<input type="hidden" name="action" value="execute">
	                	<button type="reset" class="btn btn-warning mg-r-20">Làm lại</button>
	                    <button type="submit" class="btn btn-primary">Cập nhật</button>
	                </div>
                </div>
            </form>
        </div>
    </section>
</div>

@endsection

@section('JS_FOOTER')
	<script src="{{ asset('/ckeditor/ckeditor.js') }}"></script>
	<script> CKEDITOR.replace('content_contact'); </script>
	<script> CKEDITOR.replace('content_contact_en'); </script>
	<script> CKEDITOR.replace('footer'); </script>
	<script> CKEDITOR.replace('footer_en'); </script>
	<script> CKEDITOR.replace('description_thamgia_mangluoi'); </script>
	<script> CKEDITOR.replace('description_thamgia_mangluoi_en'); </script>
	<script> CKEDITOR.replace('description_kienthuc_thamkhao'); </script>
	<script> CKEDITOR.replace('description_kienthuc_thamkhao_en'); </script>
@endsection
