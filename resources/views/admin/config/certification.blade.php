@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.configs.certification')}}">Cấu hình trang EDGE</a>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Cấu hình trang EDGE</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
	    <div class="br-section-wrapper container-fluid">
            @include("admin.error")
            <form role="form" method="post" action="" enctype="multipart/form-data">
            	@csrf
				<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
	                <div class="row" style="display: none;">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Ảnh 1 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="hidden" name="picture_1" id="picture_1" value="{{ $data['picture_1'] }}">
							@component('admin.upload', ['class_add' => 'd-flex', 'arrGallery' => $data['picture_data_1'], 'id_upload' => 'gallery_upload_file_1', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'picture_1', 'name_data' => 'picture_data_1'])
							@endcomponent
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 1 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_1" id="text_1" value="{{ $data['text_1'] }}" class="form-control">
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 1 (EN)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_1_en" id="text_1_en" value="{{ $data['text_1_en'] }}" class="form-control">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Ảnh 1.1 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="hidden" name="picture_2_1" id="picture_2_1" value="{{ $data['picture_2_1'] }}">
							@component('admin.upload', ['class_add' => 'd-flex', 'arrGallery' => $data['picture_data_2_1'], 'id_upload' => 'gallery_upload_file_2_1', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'picture_2_1', 'name_data' => 'picture_data_2_1'])
							@endcomponent
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 1.2 (VI)</label>
	                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
		                    <textarea name="text_6_4" class="form-control " id="text_6_4">{{ $data['text_6_4'] }}</textarea>
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 1.2 (EN)</label>
	                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
		                    <textarea name="text_6_4_en" class="form-control " id="text_6_4_en">{{ $data['text_6_4_en'] }}</textarea>
		                </div>
	                </div>
	                <div class="row" style="display: none;">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Ảnh 2.2 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="hidden" name="picture_2_2" id="picture_2_2" value="{{ $data['picture_2_2'] }}">
							@component('admin.upload', ['class_add' => 'd-flex', 'arrGallery' => $data['picture_data_2_2'], 'id_upload' => 'gallery_upload_file_2_2', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'picture_2_2', 'name_data' => 'picture_data_2_2'])
							@endcomponent
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 2.1 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_2_1" id="text_2_1" value="{{ $data['text_2_1'] }}" class="form-control">
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 2.1 (EN)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_2_1_en" id="text_2_1_en" value="{{ $data['text_2_1_en'] }}" class="form-control">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 2.2 (VI)</label>
	                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
		                    <textarea name="text_2_2" class="form-control " id="text_2_2">{{ $data['text_2_2'] }}</textarea>
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 2.2 (EN)</label>
	                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
		                    <textarea name="text_2_2_en" class="form-control " id="text_2_2_en">{{ $data['text_2_2_en'] }}</textarea>
		                </div>
	                </div>
	                <div class="row" style="display: none;">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Ảnh 3.1 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="hidden" name="picture_3_1" id="picture_3_1" value="{{ $data['picture_3_1'] }}">
							@component('admin.upload', ['class_add' => 'd-flex', 'arrGallery' => $data['picture_data_3_1'], 'id_upload' => 'gallery_upload_file_3_1', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'picture_3_1', 'name_data' => 'picture_data_3_1'])
							@endcomponent
		                </div>
	                </div>
	                <div class="row" style="display: none;">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Ảnh 3.1 (EN)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="hidden" name="picture_3_1_en" id="picture_3_1_en" value="{{ $data['picture_3_1_en'] }}">
							@component('admin.upload', ['class_add' => 'd-flex', 'arrGallery' => $data['picture_data_3_1_en'], 'id_upload' => 'gallery_upload_file_3_1_en', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'picture_3_1_en', 'name_data' => 'picture_data_3_1_en'])
							@endcomponent
		                </div>
	                </div>
	                 <div class="row" style="display: none;">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Ảnh 3.2 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="hidden" name="picture_3_2" id="picture_3_2" value="{{ $data['picture_3_2'] }}">
							@component('admin.upload', ['class_add' => 'd-flex', 'arrGallery' => $data['picture_data_3_2'], 'id_upload' => 'gallery_upload_file_3_2', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'picture_3_2', 'name_data' => 'picture_data_3_2'])
							@endcomponent
		                </div>
	                </div>
	                <div class="row" style="display: none;">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Ảnh 3.2 (EN)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="hidden" name="picture_3_2_en" id="picture_3_2_en" value="{{ $data['picture_3_2_en'] }}">
							@component('admin.upload', ['class_add' => 'd-flex', 'arrGallery' => $data['picture_data_3_2_en'], 'id_upload' => 'gallery_upload_file_3_2_en', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'picture_3_2_en', 'name_data' => 'picture_data_3_2_en'])
							@endcomponent
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 3 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_4" id="text_4" value="{{ $data['text_4'] }}" class="form-control">
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 3 (EN)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_4_en" id="text_4_en" value="{{ $data['text_4_en'] }}" class="form-control">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 4.1 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_3_1" id="text_3_1" value="{{ $data['text_3_1'] }}" class="form-control">
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 4.1 (EN)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_3_1_en" id="text_3_1_en" value="{{ $data['text_3_1_en'] }}" class="form-control">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 4.2 (VI)</label>
	                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
		                    <textarea name="text_3_2" class="form-control " id="text_3_2">{{ $data['text_3_2'] }}</textarea>
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 4.2 (EN)</label>
	                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
		                    <textarea name="text_3_2_en" class="form-control " id="text_3_2_en">{{ $data['text_3_2_en'] }}</textarea>
		                </div>
	                </div>

	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 5.1 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_5_1" id="text_5_1" value="{{ $data['text_5_1'] }}" class="form-control">
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 5.1 (EN)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_5_1_en" id="text_5_1_en" value="{{ $data['text_5_1_en'] }}" class="form-control">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 5.2 (VI)</label>
	                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
		                    <textarea type="text" name="text_5_2" class="form-control " id="text_5_2">{{ $data['text_5_2'] }}</textarea>
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 5.2 (EN)</label>
	                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
		                    <textarea type="text" name="text_5_2_en" class="form-control " id="text_5_2_en">{{ $data['text_5_2_en'] }}</textarea>
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 6.1 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_6_1" id="text_6_1" value="{{ $data['text_6_1'] }}" class="form-control">
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 6.1 (EN)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_6_1_en" id="text_6_1_en" value="{{ $data['text_6_1_en'] }}" class="form-control">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 6.2 (VI)</label>
	                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
                            <textarea type="text" name="text_6_2" id="text_6_2" class="form-control">{{ $data['text_6_2'] }}</textarea>
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 6.2 (EN)</label>
	                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
                            <textarea type="text" name="text_6_2_en" id="text_6_2_en" class="form-control">{{ $data['text_6_2_en'] }}</textarea>
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 6.3 (VI)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_6_3" id="text_6_3" value="{{ $data['text_6_3'] }}" class="form-control">
		                </div>
	                </div>
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Text 6.3 (EN)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="text_6_3_en" id="text_6_3_en" value="{{ $data['text_6_3_en'] }}" class="form-control">
		                </div>
	                </div>
	                <div class="row" style="display: none;">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Ảnh 6.3</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="hidden" name="picture_2_2_en" id="picture_2_2_en" value="{{ $data['picture_2_2_en'] }}">
							@component('admin.upload', ['class_add' => 'd-flex', 'arrGallery' => $data['picture_data_2_2_en'], 'id_upload' => 'gallery_upload_file_2_2_en', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'picture_2_2_en', 'name_data' => 'picture_data_2_2_en'])
							@endcomponent
		                </div>
	                </div>
	                <div class="row" style="display: none;">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Ảnh 6</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="hidden" name="picture_1_en" id="picture_1_en" value="{{ $data['picture_1_en'] }}">
							@component('admin.upload', ['class_add' => 'd-flex', 'arrGallery' => $data['picture_data_1_en'], 'id_upload' => 'gallery_upload_file_1_en', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'picture_1_en', 'name_data' => 'picture_data_1_en'])
							@endcomponent
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Ảnh 6</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="hidden" name="picture_2_1_en" id="picture_2_1_en" value="{{ $data['picture_2_1_en'] }}">
							@component('admin.upload', ['class_add' => 'd-flex', 'arrGallery' => $data['picture_data_2_1_en'], 'id_upload' => 'gallery_upload_file_2_1_en', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'picture_2_1_en', 'name_data' => 'picture_data_2_1_en'])
							@endcomponent
		                </div>
	                </div>

	            </div>

                <!-- /.card-body -->
                <div class="row mg-t-20">
                	<div class="col-sm-2 mg-b-10"></div>
                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
                		<input type="hidden" name="action" value="execute">
	                	<button type="reset" class="btn btn-warning mg-r-20">Làm lại</button>
	                    <button type="submit" class="btn btn-primary">Cập nhật</button>
	                </div>
                </div>
            </form>
        </div>
    </section>
</div>

@endsection

@section('JS_FOOTER')
	<script src="{{ asset('/ckeditor/ckeditor.js') }}"></script>
	<script> CKEDITOR.replace('text_6_4'); </script>
	<script> CKEDITOR.replace('text_6_4_en'); </script>
@endsection
