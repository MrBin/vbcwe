@extends('admin.layouts')

@section('CONTAINER')
	@switch($view)
		@case('config')
			@include("admin.configuration.config")
			@break
	@endswitch
@endsection
