<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.other.contact')}}">liên hệ</a>
      <span class="breadcrumb-item active">Danh sách liên hệ</span>
    </nav>
</div>
<div class="br-pagetitle" style="justify-content: space-between;">
	<h4>Danh sách liên hệ</h4>
</div>
<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="">
  	  			<div class="row">
		            <div class="col-lg-2">
		              <input class="form-control" name="name" placeholder="Tên" type="text" value="{{ isset($dataSearch['name']) ? $dataSearch['name'] : '' }}">
		            </div>
		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
		        <div class="row mb-2">
		        	@foreach ($allType as $key => $value)
		        		<div class="col-lg-2">
			              <a class="btn btn-sm {{ $dataSearch['type'] == $key ? 'btn-danger' : 'btn-normal'}}" href="{{ route('admin.other.contact', ['type' => $key] + $dataSearch)}}">{{ $value }}</a>
			            </div>
		        	@endforeach
		        </div>
  	    	</form>

		  	<div class="bd rounded table-responsive">
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		                <tr class="thead-colored thead-light">
		                	<td class="wd-5p">STT</td>
							<td class="wd-10p">Tên</td>
							<td class="wd-10p">Email</td>
							<td class="wd-10p">Tiêu đề</td>
							<td class="wd-10p text-center">Kích hoạt</td>
							<td class="wd-10p text-center">Ngày tạo</td>
							<td class="wd-10p text-center">Sửa</td>
							<td class="wd-10p text-center">Xóa</td>
		                </tr>
				      	@foreach ($contacts as $item)
							<tr id="record_{{ $item->con_id }}">
					          	<td class="text-center">{{ $loop->index + 1 }}</td>
					          	<td>{{ ($item->con_name == "" ? $item->con_who : $item->con_name) }}</td>
					          	<td>{{ $item->con_email }}</td>
					          	<td>{{ $item->con_title }}</td>
					          	<td class="text-center">
									<a href="javascript:;" class="box_active" onclick="changeActive('{{ route('admin.other.contact_status')}}', '{{ $item->con_id }}', 'active')">
										@if($item->con_status == 1)
											<i class="far fa-check-square" style="font-size: 14px;"></i>
										@else
											<i class="far fa-square" style="font-size: 14px;"></i>
										@endif
									</a>
								</td>
					          	<td class="text-center">{{ date("d/m/y H:i",$item->con_created_time) }}</td>
					          	<td class="text-center">
					          		<a href="{{ route('admin.other.detail', ['id' => $item->con_id, 'type' => $item->con_type])}}" class=""><i class="fas fa-edit"></i> Xem</a>
					          	</td>
					          	<td class="text-center">
									<a class="buttonDelteRecord" href="{{ route('admin.other.contact_delete', ['id' => $item->con_id]) }}" ><i class="fa fa-times-circle"></i> Xóa</a>
					          	</td>
					        </tr>
				      	@endforeach

		      		</tbody>
		  	  	</table>
		  	</div>

        </div>
    </section>
</div>
@endsection

