@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.other.enterprice_network')}}">liên hệ</a>
      <span class="breadcrumb-item active">Chỉnh sửa</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Chi tiết liên hệ</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
	    <div class="br-section-wrapper container-fluid">
            @include("admin.error")
            <form role="form" method="post" action="" enctype="multipart/form-data">
            	@csrf
				<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername"><b>Tên:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->ct_name }}
                        </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername"><b>Email:</b></label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    {{ $data->ct_email }}
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername"><b>Tiêu đề:</b></label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    {{ $data->ct_title }}
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername"><b>Tên công ty:</b></label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    {{ $data->ct_company_name }}
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername"><b>Lĩnh vực hoạt động:</b></label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    {{ $data->ct_area }}
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername"><b>Mô tả:</b></label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    {{ $data->ct_description }}
		                </div>
	                </div>

	            </div>

            </form>
        </div>
    </section>
</div>

@endsection
