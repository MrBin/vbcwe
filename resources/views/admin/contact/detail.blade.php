@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.other.contact')}}">Liên hệ</a>
      <span class="breadcrumb-item active">Chi tiết</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Chi tiết liên hệ</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
	    <div class="br-section-wrapper container-fluid">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                @switch($data->con_type)
                    @case(1)
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Tên:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_name }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Email:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_email }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Tên công ty:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_company_name }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Chức vụ:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_position }}
                        </div>
                    </div>
                    @break

                    @case(2)
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Tên công ty:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_company_name }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Lĩnh vực hoạt động:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_area }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Địa chỉ giao dịch:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_address }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Quy mô công ty:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $arrScale[$data->con_scale] }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Website:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_website }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Người liên hệ:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_name }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Chức vụ:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_position }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Email:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_email }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Số điện thoại:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_phone }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Nhu cầu:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $arrNeed[$data->con_need] }}
                        </div>
                    </div>
                    @break

                    @case(3)
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Họ tên:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_name }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Giới tính:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_sex }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Tên công ty:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_company_name }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Chức vụ:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_position }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Email:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_email }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Số điện thoại:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_phone }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Tên tài liệu:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_need }}
                        </div>
                    </div>
                    @break

                    @case(4)
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Họ tên:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_name }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Giới tính:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_sex }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Tên công ty:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_company_name }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Chức vụ:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_position }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Email:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_email }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Số điện thoại:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_phone }}
                        </div>
                    </div>
                    @break
                    @case(5)
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Tên:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_name }}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 form-control-label" for="InputUsername"><b>Câu chuyện:</b></label>
                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                            {{ $data->con_description }}
                        </div>
                    </div>
                    @break
                @endswitch

            </div>
        </div>
    </section>
</div>

@endsection
