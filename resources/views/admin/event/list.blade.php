<?php
use App\Repositories\EventsRepository as Events;
use App\Helper\Upload as UploadHelper;
?>
@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.event.index')}}">Sự kiện</a>
      <span class="breadcrumb-item active">Danh sách</span>
    </nav>
</div>
<div class="br-pagetitle" style="justify-content: space-between;">
	<h4>Danh sách Sự kiện</h4>
	<div class=""><a href="{{ route('admin.event.add') }}" class="btn btn-info">Thêm mới</a></div>
</div>
<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="">
  	  			<div class="row">
		            <div class="col-lg-2">
		              <input class="form-control" name="title" placeholder="Tiêu đề" type="text" value="{{ isset($dataSearch['title']) ? $dataSearch['title'] : '' }}">
		            </div>
		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>

		  	<div class="bd rounded table-responsive">
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		                <tr class="thead-colored thead-light">
		                	<td class="wd-5p">STT</td>
		                	<td class="wd-5p">Ảnh</td>
							<td class="wd-10p">Tiêu đề</td>
							<td class="wd-10p">Tiêu đề en</td>
							<td class="wd-10p text-center">Loại sự kiện</td>
							<td class="wd-10p text-center">Ngày tạo</td>
							<td class="wd-10p text-center">Sửa</td>
							<td class="wd-10p text-center">Xóa</td>
		                </tr>
				      	@foreach ($events as $item)
							<tr id="record_{{ $item->eve_id}}">
					          	<td class="text-center">{{ $loop->index + 1 + ($pagination['current_page'] - 1) * $pagination['perpage'] }}</td>
					          	<td>
                                    <img style="max-width: 100px; height: auto" src="{{ UploadHelper::getUrlImage('news', $item->eve_picture) }}" alt="">
                                </td>
					          	<td>{{ $item->eve_title }}</td>
					          	<td>{{ $item->eve_title_en }}</td>
					          	<td class="text-center">
									{{ Events::getStatusLabel($item->eve_new) }}
								</td>
					          	<td class="text-center">{{ date("d/m/y H:i",$item->eve_create_time) }}</td>
					          	<td class="text-center">
					          		<a href="{{ route('admin.event.add', ['id' => $item->eve_id])}}" class=""><i class="fas fa-edit"></i> Sửa</a>
					          	</td>
					          	<td class="text-center">
									<a class="buttonDelteRecord text-danger" href="{{ route('admin.event.delete', ['id' => $item->eve_id]) }}" ><i class="fa fa-times-circle"></i> Xóa</a>
					          	</td>
					        </tr>
				      	@endforeach

		      		</tbody>
		  	  	</table>
		  	</div>

        </div>
    </section>
    @include("admin.pagination")
</div>
@endsection

