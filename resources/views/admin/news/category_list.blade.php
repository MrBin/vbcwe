@extends('admin.layouts')
@section('CONTAINER')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.news.index')}}">Tin tức</a>
      <a class="breadcrumb-item" href="{{route('admin.news.category')}}">Danh mục</a>
      <span class="breadcrumb-item active">Danh sách</span>
    </nav>
</div>
<div class="br-pagetitle" style="justify-content: space-between;">
	<h4>Danh sách danh mục</h4>
	<div class=""><a href="{{ route('admin.news.category_add') }}" class="btn btn-info">Thêm mới</a></div>
</div>

<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="">
  	  			<div class="row">
		            <div class="col-lg-3">
		              <input class="form-control" name="name" placeholder="Tên danh mục" type="text" value="{{ isset($dataSearch['name']) ? $dataSearch['name'] : '' }}">
		            </div>
		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>


		  	<div class="bd rounded table-responsive">
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		                <tr class="thead-colored thead-light">
				          <td>Tiêu đề</td>
				          <td class="text-center">Thứ tự</td>
				          <td class="text-center">Kích hoạt</td>
				          <td class="text-center">Ngày tạo</td>
				          <td class="text-center">Sửa</td>
				        </tr>

				      	@foreach ($category as $keys => $cate)
					      	<tr>
								<td colspan="8" align="center" style="font-weight: bold; color: #ca1111; font-size: 15px;">{{ $arrType[$keys] }}</td>
							</tr>
							@foreach ($cate as $item)
								<tr id="record_{{ $item->cat_id}}">
						          	<td>
						          		@for ($i = 0; $i < $item['level']; $i++)
										---|
										@endfor
						          		{{ $item->cat_name }}
						          	</td>
						          	<td class="text-center">{{ $item->cat_order }}</td>
						          	<td class="text-center">
										<a href="javascript:;" class="box_active" onclick="changeActive('{{ route('admin.news.category_status')}}', '{{ $item->cat_id }}', 'active')">
											@if($item->cat_active == 1)
												<i class="far fa-check-square" style="font-size: 14px;"></i>
											@else
												<i class="far fa-square" style="font-size: 14px;"></i>
											@endif
										</a>
									</td>
						          	<td class="text-center">{{ date("d/m/y H:i",$item->cat_create_time) }}</td>
						          	<td class="text-center">
						          		<a href="{{ route('admin.news.category_add', ['id' => $item->cat_id])}}" class=""><i class="fas fa-edit"></i> Sửa</a>
						          	</td>
						        </tr>
							@endforeach
				      	@endforeach

		      		</tbody>
		  	  	</table>
            </div>
        </div>
    </section>

</div>
@endsection

