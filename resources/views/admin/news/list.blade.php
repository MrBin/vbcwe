<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.news.index')}}">Tin tức</a>
      <span class="breadcrumb-item active">Danh sách</span>
    </nav>
</div>
<div class="br-pagetitle" style="justify-content: space-between;">
	<h4>Danh sách tin tức</h4>
	<div class=""><a href="{{ route('admin.news.add') }}" class="btn btn-info">Thêm mới</a></div>
</div>
<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="">
  	  			<div class="row">
		            <div class="col-lg-2">
		              <input class="form-control" name="title" placeholder="Tiêu đề" type="text" value="{{ isset($dataSearch['title']) ? $dataSearch['title'] : '' }}">
		            </div>
		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>

		  	<div class="bd rounded table-responsive">
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		                <tr class="thead-colored thead-light">
		                	<td class="wd-5p">STT</td>
		                	<td class="wd-5p">Ảnh</td>
							<td class="wd-10p">Tiêu đề</td>
							<td class="wd-10p">Tiêu đề en</td>
							<td class="wd-10p">Danh mục</td>
							<td class="wd-10p text-center">Kích hoạt</td>
							<td class="wd-10p text-center">Trang chủ</td>
							<td class="wd-10p text-center">Ngày tạo</td>
							<td class="wd-10p text-center">Sửa</td>
							<td class="wd-10p text-center">Xóa</td>
		                </tr>
				      	@foreach ($news as $item)
							<tr id="record_{{ $item->new_id}}">
					          	<td class="text-center">{{ $loop->index + 1 + ($pagination['current_page'] - 1) * $pagination['perpage'] }}</td>
					          	<td><img src="{{ UploadHelper::getUrlImage('news', $item->new_picture) }}" alt="" style="max-width: 100px"></td>
					          	<td style="line-height: 140%">{{ $item->new_title }}</td>
					          	<td style="line-height: 140%">{{ $item->new_title_en }}</td>
					          	<td>@if($item->category) {{ $item->category->cat_name }} @endif</td>
					          	<td class="text-center">
									<a href="javascript:;" class="box_active" onclick="changeActive('{{ route('admin.news.status')}}', '{{ $item->new_id }}', 'active')">
										@if($item->new_active == 1)
											<i class="far fa-check-square" style="font-size: 14px;"></i>
										@else
											<i class="far fa-square" style="font-size: 14px;"></i>
										@endif
									</a>
								</td>
								<td class="text-center">
									<a href="javascript:;" class="box_vip" onclick="changeActive('{{ route('admin.news.status')}}', '{{ $item->new_id }}', 'vip')">
										@if($item->new_vip == 1)
											<i class="far fa-check-square" style="font-size: 14px;"></i>
										@else
											<i class="far fa-square" style="font-size: 14px;"></i>
										@endif
									</a>
								</td>
					          	<td class="text-center">{{ date("d/m/y H:i",$item->new_create_time) }}</td>
					          	<td class="text-center">
					          		<a href="{{ route('admin.news.add', ['id' => $item->new_id])}}" class=""><i class="fas fa-edit"></i> Sửa</a>
					          	</td>
					          	<td class="text-center">
									<a class="buttonDelteRecord text-danger" href="{{ route('admin.news.delete', ['id' => $item->new_id]) }}" ><i class="fa fa-times-circle"></i> Xóa</a>
					          	</td>
					        </tr>
				      	@endforeach

		      		</tbody>
		  	  	</table>
		  	</div>

        </div>
    </section>
    @include("admin.pagination")
</div>
@endsection

