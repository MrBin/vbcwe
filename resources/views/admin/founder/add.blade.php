@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.other.founder')}}">Nhà sáng lập</a>
      <span class="breadcrumb-item active">Thêm mới</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Thêm mới Nhà sáng lập</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
	    <div class="br-section-wrapper container-fluid">
            @include("admin.error")
            <form role="form" method="post" action="" enctype="multipart/form-data">
            	@csrf
				<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Chọn nhóm (*)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <select class="form-control" name="type">
	                            @foreach($allType as $key => $value)
	                            	<option value="{{ $key }}" @if($key == $data['type']) selected="selected" @endif>
										{{ $value }}
	                            	</option>
	                            @endforeach
	                        </select>
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Tên (*)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="name" value="{{ $data['name'] }}" class="form-control" placeholder="Nhập tên">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Tên en (*)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="name_en" value="{{ $data['name_en'] }}" class="form-control" placeholder="Nhập tên">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Chức vụ (*)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="chucvu" value="{{ $data['chucvu'] }}" class="form-control" placeholder="Nhập chức vụ">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Chức vụ en (*)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="chucvu_en" value="{{ $data['chucvu_en'] }}" class="form-control" placeholder="Nhập chức vụ en">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Mô tả</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <textarea name="description" class="form-control" placeholder="Nhập mô tả">{{ $data['description'] }}</textarea>
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Mô tả en</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <textarea name="description_en" class="form-control" placeholder="Nhập mô tả en">{{ $data['description_en'] }}</textarea>
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Link</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="link" value="{{ $data['link'] }}" class="form-control" placeholder="Nhập link">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Order</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
							<input type="text" class="form-control" name="order" value="{{ $data['order'] }}">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label">Kích hoạt</label>
	                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <div class="custom-control custom-checkbox">
		                        <input class="custom-control-input" type="checkbox" id="fou_active" name="active" @if($data['active'] == 1) checked="checked" @endif value="1">
		                        <label class="col-sm-4 form-control-label custom-control-label" for="fou_active"></label>
		                    </div>
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="exampleInputFile">Ảnh</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                      	<input type="hidden" name="picture" id="fou_picture" value="{{ $data['picture'] }}">
							@component('admin.upload', ['arrGallery' => $data['picture_data'], 'id_upload' => 'gallery_upload_file', 'type_upload' => 'other', 'max_upload' => 1, 'item_upload' => 'fou_picture', 'name_data' => 'picture_data'])
							@endcomponent
	                    </div>
	                </div>
	            </div>

                <!-- /.card-body -->
                <div class="row mg-t-20">
                	<div class="col-sm-2 mg-b-10"></div>
                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
                		<input type="hidden" name="action" value="execute">
	                	<button type="reset" class="btn btn-warning mg-r-20">Làm lại</button>
	                    <button type="submit" class="btn btn-primary">@if($data['id'] > 0) Cập nhật @else Thêm mới @endif</button>
	                </div>
                </div>
            </form>
        </div>
    </section>
</div>

@endsection