@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.view.service')}}">Sản phẩm và dịch vụ</a>
      <span class="breadcrumb-item active">Thêm mới</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Thêm mới sản phẩm và dịch vụ</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
	    <div class="br-section-wrapper container-fluid">
            @include("admin.error")
            <form role="form" method="post" action="" enctype="multipart/form-data">
            	@csrf
				<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
					<div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputParent">Loại</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                        <select class="form-control" name="type">
	                            <option value="0">-- Chọn --</option>
	                            @foreach($allType as $key => $value)
	                            	<option value="{{ $key }}" @if($key == $data['type']) selected="selected" @endif>
										{{ $value }}
	                            	</option>
	                            @endforeach
	                        </select>
	                    </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Tên (*)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="title" value="{{ $data['title'] }}" class="form-control" placeholder="Nhập tên">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Tên En (*)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="title_en" value="{{ $data['title_en'] }}" class="form-control" placeholder="Nhập tên en">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Order</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
							<input type="text" class="form-control" name="order" value="{{ $data['order'] }}">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label">Kích hoạt</label>
	                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <div class="custom-control custom-checkbox">
		                        <input class="custom-control-input" type="checkbox" id="ser_status" name="status" @if($data['status'] == 1) checked="checked" @endif value="1">
		                        <label class="col-sm-4 form-control-label custom-control-label" for="ser_status"></label>
		                    </div>
		                </div>
	                </div>

	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="exampleInputFile">Icon đại diện</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                      	<input type="hidden" name="icon" id="ser_icon" value="{{ $data['icon'] }}">
							@component('admin.upload', ['arrGallery' => $data['icon_data'], 'id_upload' => 'gallery_upload_file_icon', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'ser_icon', 'name_data' => 'icon_data'])
							@endcomponent
	                    </div>
	                </div>

	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="exampleInputFile">Ảnh đại diện</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                      	<input type="hidden" name="picture" id="ser_picture" value="{{ $data['picture'] }}">
							@component('admin.upload', ['arrGallery' => $data['picture_data'], 'id_upload' => 'gallery_upload_file_picture', 'type_upload' => 'config', 'max_upload' => 1, 'item_upload' => 'ser_picture', 'name_data' => 'picture_data'])
							@endcomponent
	                    </div>
	                </div>


		            	<div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Mô tả ngắn</label>
		                     <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea class="form-control" name="teaser">{{ $data['teaser'] }}</textarea>
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Mô tả ngắn En</label>
		                     <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea class="form-control" name="teaser_en">{{ $data['teaser_en'] }}</textarea>
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Mô tả</label>
		                     <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="description" class="form-control " id="editor1">{{ $data['description'] }}</textarea>
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Mô tả en</label>
		                     <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="description_en" class="form-control " id="editor2">{{ $data['description_en'] }}</textarea>
			                </div>
		                </div>
	            </div>

                <!-- /.card-body -->
                <div class="row mg-t-20">
                	<div class="col-sm-2 mg-b-10"></div>
                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
                		<input type="hidden" name="action" value="execute">
	                	<button type="reset" class="btn btn-warning mg-r-20">Làm lại</button>
	                    <button type="submit" class="btn btn-primary">@if($data['id'] > 0) Cập nhật @else Thêm mới @endif</button>
	                </div>
                </div>
            </form>
        </div>
    </section>
</div>

@endsection

@section('JS_FOOTER')
	<script src="{{ asset('/ckeditor/ckeditor.js') }}"></script>
	<script> CKEDITOR.replace('editor1'); </script>
	<script> CKEDITOR.replace('editor2'); </script>
@endsection