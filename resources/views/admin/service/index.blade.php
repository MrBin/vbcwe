<?php
use App\Helper\Upload as UploadHelper;
use App\Repositories\ServiceRepository as Service;
?>
@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.view.service')}}">Sản phẩm và dịch vụ</a>
      <span class="breadcrumb-item active">Danh sách sản phẩm và dịch vụ</span>
    </nav>
</div>
<div class="br-pagetitle" style="justify-content: space-between;">
	<h4>Danh sách sản phẩm và dịch vụ</h4>
	<div class=""><a href="{{ route('admin.view.service_add') }}" class="btn btn-info">Thêm mới</a></div>
</div>
<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="">
  	  			<div class="row">
		            <div class="col-lg-2">
		              <input class="form-control" name="title" placeholder="Tên" type="text" value="{{ isset($dataSearch['title']) ? $dataSearch['title'] : '' }}">
		            </div>
		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>

		  	<div class="bd rounded table-responsive">
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		                <tr class="thead-colored thead-light">
		                	<td class="wd-5p">STT</td>
		                	<td class="wd-10p">Icon</td>
		                	<td class="wd-10p">Ảnh</td>
							<td class="wd-10p">Tên</td>
							<td class="wd-10p">Tên En</td>
							<td class="wd-10p">Loại</td>
							<td class="wd-10p text-center">Kích hoạt</td>
							<td class="wd-10p text-center">Trang chủ</td>
							<td class="wd-10p text-center">Ngày tạo</td>
							<td class="wd-10p text-center">Sửa</td>
							<td class="wd-10p text-center">Xóa</td>
		                </tr>
				      	@foreach ($services as $item)
							<tr id="record_{{ $item->ser_id }}">
					          	<td class="text-center">{{ $loop->index + 1 }}</td>
					          	<td align="center"><img src="{{ UploadHelper::getUrlImage('config', $item->ser_icon) }}"></td>
					          	<td><img width="100%" src="{{ UploadHelper::getUrlImage('config', $item->ser_picture) }}"></td>
					          	<td>{{ $item->ser_title }}</td>
					          	<td>{{ $item->ser_title_en }}</td>
					          	<td>{{ Service::getTypeLabel($item->ser_type) }}</td>
					          	<td class="text-center">
									<a href="javascript:;" class="box_active" onclick="changeActive('{{ route('admin.view.service_status')}}', '{{ $item->ser_id }}', 'active')">
										@if($item->ser_status == 1)
											<i class="far fa-check-square" style="font-size: 14px;"></i>
										@else
											<i class="far fa-square" style="font-size: 14px;"></i>
										@endif
									</a>
								</td>
								<td class="text-center">
									<a href="javascript:;" class="box_show_home" onclick="changeActive('{{ route('admin.view.service_status')}}', '{{ $item->ser_id }}', 'show_home')">
										@if($item->ser_show_home == 1)
											<i class="far fa-check-square" style="font-size: 14px;"></i>
										@else
											<i class="far fa-square" style="font-size: 14px;"></i>
										@endif
									</a>
								</td>
					          	<td class="text-center">{{ date("d/m/y H:i",$item->ser_create_time) }}</td>
					          	<td class="text-center">
					          		<a href="{{ route('admin.view.service_add', ['id' => $item->ser_id])}}" class=""><i class="fas fa-edit"></i> Sửa</a>
					          	</td>
					          	<td class="text-center">
									<a class="buttonDelteRecord" href="{{ route('admin.view.service_delete', ['id' => $item->ser_id]) }}" ><i class="fa fa-times-circle"></i> Xóa</a>
					          	</td>
					        </tr>
				      	@endforeach

		      		</tbody>
		  	  	</table>
		  	</div>

        </div>
    </section>
    @include("admin.pagination")
</div>
@endsection

