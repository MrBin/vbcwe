<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="{{ asset('assets/admin/lib/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('assets/admin/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('assets/admin/css/bracket.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('assets/admin/css/jquery.growl.css') }}" rel="stylesheet" type="text/css">
    <style type="text/css">
        .login_error{
            list-style: none;
        }
    </style>
</head>
<body class="hold-transition login-page">

	<div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v">
		<div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white rounded shadow-base">
			<div class="signin-logo tx-center tx-28 tx-bold tx-inverse"><span class="tx-normal"></span> ADMIN <span class="tx-info">VBCWE</span> <span class="tx-normal"></span></div>
            @if ($errors->any())
                <ul class="alert alert-danger login_error">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            <form action="{{route('auth_admin.login')}}" method="post" class="mg-t-20">
        		@csrf
				<div class="form-group">
					<input type="text" name="username"  class="form-control" placeholder="Enter your username">
				</div><!-- form-group -->
				<div class="form-group">
					<input type="password" name="password" class="form-control" placeholder="Enter your Password">
					<a href="" class="tx-info tx-12 d-block mg-t-10" style="display: none !important;">Quên mật khẩu?</a>
				</div><!-- form-group -->
				<input type="hidden" name="action" value="login">
				<button type="submit" class="btn btn-info btn-block mg-t-20">Sign In</button>
			</form>
		</div><!-- login-wrapper -->
	</div><!-- d-flex -->

<!-- /.login-box -->
<script language="javascript" src="{{ asset('assets/admin/lib/jquery/jquery.min.js') }}"></script>
<script language="javascript" src="{{ asset('assets/admin/lib/jquery-ui/ui/widgets/datepicker.js') }}"></script>
<script language="javascript" src="{{ asset('assets/admin/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script language="javascript" src="{{ asset('assets/admin/js/jquery.growl.js') }}"></script>


@include("admin.flash-message")

</body>
</html>
