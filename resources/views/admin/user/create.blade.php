@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="">Vận đơn</a>
      <span class="breadcrumb-item active">Khai báo vận đơn</span>
    </nav>
</div>
<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
        	<div class="row">
        		<div class="col-12 col-xl-8 mg-t-20 mg-xl-t-0">
				    <div class="form-layout form-layout-5" style="border: none;">
				        <h6 class="br-section-label">Khai báo các thông tin mã vận đơn của khách</h6>
				        <p class="mt-3 mg-b-20">Những trường có đánh dấu * là bắt buộc.</p>
				        @include("frontend.error")
				        <form action="@if ($data['id'] > 0) {{route('admin.bills.create', ['id' => $data['id']])}} @else {{route('admin.bills.create')}} @ @endif" method="post" class="mg-t-20">
        					@csrf
					        <div class="row">
					            <label class="col-sm-3 form-control-label"><span class="tx-danger">*</span> Mã vận đơn:</label>
					            <div class="col-sm-9 mg-t-10 mg-sm-t-0">
					                <input type="text" disabled name="bill_code" class="form-control" placeholder="Mã vận đơn" value="{{ $data['bill_code'] }}">
					            </div>
					        </div>
					        <!-- row -->
					        <div class="row mg-t-20">
					            <label class="col-sm-3 form-control-label" style="white-space: nowrap;"><span class="tx-danger">*</span> Trạng thái:</label>
					            <div class="col-sm-9 mg-t-10 mg-sm-t-0">
					                <select class="form-control select_status" name="status" data-placeholder="Chọn trạng thái">
										<option label="Chọn trạng thái"></option>
										@foreach($arrStatus as $key => $value)
											<option value="{{ $key }}" @if($key == $data['status']) selected="selected" @endif>{{ $value }}</option>
										@endforeach
									</select>
					            </div>
					        </div>
					        <div class="row mg-t-20">
					            <label class="col-sm-3 form-control-label" style="white-space: nowrap;"><span class="tx-danger">*</span> Tên sản phẩm:</label>
					            <div class="col-sm-9 mg-t-10 mg-sm-t-0">
					                <input type="text" name="product_name" class="form-control" placeholder="Tên sản phẩm" value="{{ $data['product_name'] }}">
					            </div>
					        </div>
					        <div class="row mg-t-20">
					            <label class="col-sm-3 form-control-label"><span class="tx-danger">*</span> Số lượng:</label>
					            <div class="col-sm-9 mg-t-10 mg-sm-t-0">
					                <input type="text" name="product_quantity" class="form-control" placeholder="Số lượng" value="{{ $data['quantity'] > 0 ? $data['quantity'] : '' }}">
					            </div>
					        </div>
					        <div class="row mg-t-20">
					            <label class="col-sm-3 form-control-label"><span class="tx-danger">*</span> Đơn vị:</label>
					            <div class="col-sm-9 mg-t-10 mg-sm-t-0">
					                <input type="text" name="product_unit" class="form-control" placeholder="Đơn vị bao, kiện, cái, thùng..." value="{{ $data['unit'] }}">
					            </div>
					        </div>
					        <div class="row mg-t-20">
			                    <label class="col-sm-3 form-control-label" for="exampleInputFile">Ảnh sản phẩm</label>
			                    <div class="col-sm-9 mg-t-10 mg-sm-t-0">
			                      	<input type="hidden" name="product_picture" id="product_picture" value="{{ $data['picture'] }}">
									@component('frontend.upload', ['id_upload' => 'gallery_upload_file', 'type_upload' => 'product', 'max_upload' => 1, 'item_upload' => 'product_picture', 'name_data' => 'picture_data', 'arrGallery' => $data['picture_all']])
									@endcomponent
			                    </div>
			                </div>
			                <div class="row mg-t-20">
					            <label class="col-sm-3 form-control-label">Ghi chú của khách:</label>
					            <div class="col-sm-9 mg-t-10 mg-sm-t-0">
					               {{ $data['user_note'] }}
					            </div>
					        </div>
					        <div class="row mg-t-20">
					            <label class="col-sm-3 form-control-label">Ghi chú của bạn:</label>
					            <div class="col-sm-9 mg-t-10 mg-sm-t-0">
					                <textarea name="note" rows="2" class="form-control" placeholder="Ghi chú">{{ $data['staff_note'] }}</textarea>
					            </div>
					        </div>
					        <!-- row -->
					        <div class="row mg-t-30">
					            <div class="col-sm-9 mg-l-auto">
					                <div class="form-layout-footer">
					                	<input type="hidden" name="action" value="execute">
					                    <input type="submit" class="btn btn-info" value="@if ($data['id'] > 0) Cập nhật @else Thêm mới @endif" />
					                    <input type="reset" class="btn btn-secondary" value="Làm lại">
					                </div>
					                <!-- form-layout-footer -->
					            </div>
					            <!-- col-8 -->
					        </div>
					    </form>
				    </div>
				    <!-- form-layout -->
				</div>
        	</div>
        </div>
    </section>
</div>
@endsection


@section('JS_FOOTER')
@endsection

