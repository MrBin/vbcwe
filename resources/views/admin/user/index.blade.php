@extends('admin.layouts')
@section('CONTAINER')
@php
	use App\Helper\Upload;
	use App\Repositories\BillCodeRepository;
@endphp
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="">Người dùng</a>
      <span class="breadcrumb-item active">Danh sách</span>
    </nav>
</div>
<div class="br-pagetitle" style="justify-content: space-between;">
	<h4>Danh sách người dùng</h4>
</div>
<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
        	<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="">
  	  			<div class="row">
		            <div class="col-6 col-lg-2">
		              <input class="form-control" name="username" placeholder="Tên đăng nhập" type="text" value="{{ isset($dataSearch['username']) ? $dataSearch['username'] : '' }}">
		            </div>
		            <div class="col-6 col-lg-2">
		              <input class="form-control" name="phone" placeholder="Số điện thoại" type="text" value="{{ isset($dataSearch['phone']) ? $dataSearch['phone'] : '' }}">
		            </div>
		            <div class="col-6 col-lg-2">
		            	<input type="text" name="date_start" id="date_start" class="form-control fc-datepicker" placeholder="DD/MM/YYYY" value="{{ isset($dataSearch['date_start']) ? $dataSearch['date_start'] : '' }}">
		            </div>
		            <div class="col-6 col-lg-2">
		            	<input type="text" name="date_end" id="date_end" class="form-control fc-datepicker" placeholder="DD/MM/YYYY" value="{{ isset($dataSearch['date_end']) ? $dataSearch['date_end'] : '' }}">
		            </div>
		            <div class="col-6 col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>
  	    	<div class="bd rounded table-responsive mt-3">
		  	  	<table class="table table-bordered table-striped mg-b-0">
	              	<thead>
	                	<tr>
							<th class="wd-5p text-center">STT</th>
							<th class="wd-10p">Họ tên</th>
							<th class="wd-10p">Tên đăng nhập</th>
							<th class="wd-10p">Số điện thoại</th>
							<th class="wd-15p text-center">Trạng thái</th>
							<th class="wd-15p text-center">Ngày tạo</th>
	                	</tr>
	              	</thead>
	              	<tbody>
		              	@foreach ($users as $user)
							<tr id="record_{{ $user->use_id }}">
					          	<td class="text-center">{{ $loop->index + 1 + ($pagination['current_page'] - 1) * $pagination['perpage'] }}</td>
					          	<td class="bill_code">{{ $user->use_name }}</td>
					          	<td class="">{{ $user->use_loginname }}</td>
					          	<td>{{ $user->use_phone }}</td>
					          	<td class="text-center">
									<a href="javascript:;" class="box_active" onclick="changeActive('{{ route('admin.user.status')}}', '{{ $user['use_id'] }}', 'active')">
										@if($user['use_active'] == 0)
											<i class="far fa-square"></i>
										@else
											<i class="far fa-check-square"></i>
										@endif
									</a>
								</td>
				          		<td class="text-center">{{ date("d/m/y H:i", $user->use_created_time) }}</td>
				        	</tr>
			      		@endforeach
	              </tbody>
	            </table>
	        </div>
        </div>
    </section>
    @include("frontend.pagination")
</div>

<div class="modal fade" id="modalViewDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Chi tiết mã vận đơn: <span id="bill_code_detail"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="font-size: 13px;">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection


@section('JS_FOOTER')
<script type="text/javascript">

   function deleteBill(id) {
   		var bill_code = $("#record_id_" + id + ' .bill_code').text();
   		if(!confirm('Bạn chắc chắn muốn xóa mã vận đơn: ' + bill_code)) return false;
   		$.ajax({
   			url: "}",
   			type: "POST",
   			data: {id: id},
   			success: function(data){
   				if(data.status == true){
   					$("#record_id_" + id).hide(300).remove();
   				}
   			},
   			dataType: 'json'
   		});
   }
</script>
@endsection

