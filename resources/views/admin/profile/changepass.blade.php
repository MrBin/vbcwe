@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="">Profile</a>
      <span class="breadcrumb-item active">Đổi mật khẩu</span>
    </nav>
</div>
<div class="br-pagetitle" style="justify-content: space-between;">
	<h4>Thay đổi mật khẩu</h4>
</div>
<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			@include("admin.error")
			<form action="{{route('admin.profile.changepass')}}" method="post" role="form">
				@csrf
				<input name="action" type="hidden" value="changepass">
				<div class="row">
                    <label class="col-sm-2 form-control-label" for="InputPassword">Old Password</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
						<input type="password" class="form-control" style="width: 100%;" id="old_password" name="old_password" placeholder="Enter Old password">
                    </div>
                </div>
				<div class="row">
                    <label class="col-sm-2 form-control-label" for="InputPassword">Password</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
						<input type="password" class="form-control" style="width: 100%;" id="password" name="password" placeholder="Password">
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputPassword">Re-Password</label>
					<div class="col-sm-5 mg-b-10 mg-sm-b-10">
						<input type="password" class="form-control" style="width: 100%;" id="re_password" name="re_password" placeholder="Re Password">
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputPassword"></label>
					<div class="col-sm-5 mg-b-10 mg-sm-b-10">
						<button type="submit" class="btn btn-info active">Change Password</button>
                    </div>
                </div>
			</form>
		</div>
	</section>
</div>
@endsection