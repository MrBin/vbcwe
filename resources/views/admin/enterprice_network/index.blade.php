<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.other.enterprice_network')}}">Các mạnh lưới doanh nghiệp vùng</a>
      <span class="breadcrumb-item active">Danh sách Các mạnh lưới doanh nghiệp vùng</span>
    </nav>
</div>
<div class="br-pagetitle" style="justify-content: space-between;">
	<h4>Danh sách Các mạnh lưới doanh nghiệp vùng</h4>
	<div class=""><a href="{{ route('admin.other.enterprice_network_add') }}" class="btn btn-info">Thêm mới</a></div>
</div>
<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="">
  	  			<div class="row">
		            <div class="col-lg-2">
		              <input class="form-control" name="name" placeholder="Tên" type="text" value="{{ isset($dataSearch['name']) ? $dataSearch['name'] : '' }}">
		            </div>
		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>

		  	<div class="bd rounded table-responsive">
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		                <tr class="thead-colored thead-light">
		                	<td class="wd-5p">STT</td>
		                	<td class="wd-10p">Ảnh</td>
							<td class="wd-10p">Tên</td>
							<td class="wd-10p">Link</td>
							<td class="wd-10p text-center">Kích hoạt</td>
							<td class="wd-10p text-center">Ngày tạo</td>
							<td class="wd-10p text-center">Sửa</td>
							<td class="wd-10p text-center">Xóa</td>
		                </tr>
				      	@foreach ($enterprices as $item)
							<tr id="record_{{ $item->enn_id }}">
					          	<td class="text-center">{{ $loop->index + 1 }}</td>
					          	<td align="center"><img style="max-width: 100px;" src="{{ UploadHelper::getUrlImage('other', $item->enn_picture) }}"></td>
					          	<td>{{ $item->enn_name }}</td>
					          	<td>{{ $item->enn_link }}</td>
					          	<td class="text-center">
									<a href="javascript:;" class="box_active" onclick="changeActive('{{ route('admin.other.enterprice_network_status')}}', '{{ $item->enn_id }}', 'active')">
										@if($item->enn_active == 1)
											<i class="far fa-check-square" style="font-size: 14px;"></i>
										@else
											<i class="far fa-square" style="font-size: 14px;"></i>
										@endif
									</a>
								</td>
					          	<td class="text-center">{{ date("d/m/y H:i",$item->enn_create_time) }}</td>
					          	<td class="text-center">
					          		<a href="{{ route('admin.other.enterprice_network_add', ['id' => $item->enn_id])}}" class=""><i class="fas fa-edit"></i> Sửa</a>
					          	</td>
					          	<td class="text-center">
									<a class="buttonDelteRecord" href="{{ route('admin.other.enterprice_network_delete', ['id' => $item->enn_id]) }}" ><i class="fa fa-times-circle"></i> Xóa</a>
					          	</td>
					        </tr>
				      	@endforeach

		      		</tbody>
		  	  	</table>
		  	</div>

        </div>
    </section>
</div>
@endsection

