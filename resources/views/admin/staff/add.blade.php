@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.staff.index')}}">Staff</a>
      <span class="breadcrumb-item active">Thêm mới</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Thêm mới Staff</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			@include("admin.error")
            <form role="form" method="post" action="{{ $data['id'] > 0 ? route('admin.staff.add', ['id' => $data['id']]) : route('admin.staff.add')}}" enctype="multipart/form-data">
            	@csrf
            	<div class="row">
                    <label class="col-sm-2 form-control-label">Full name</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
						<input class="form-control" type="text" name="name" @if($data['id'] > 0) disabled="disabled" @endif value="{{$data['name']}}">
                    </div>
                </div>
            	<div class="row">
                    <label class="col-sm-2 form-control-label">Username</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
						<input class="form-control" type="text" name="loginname" @if($data['id'] > 0) disabled="disabled" @endif value="{{$data['loginname']}}">
                    </div>
                </div>
                @if($data['id'] <= 0)
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputPassword">Password</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
						<input class="form-control" type="password" name="password">
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputPassword">Repassword</label>
					<div class="col-sm-5 mg-b-10 mg-sm-b-10">
						<input class="form-control" type="password" name="repassword">
                    </div>
                </div>
                @endif
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputEmail">Email</label>
					<div class="col-sm-5 mg-b-10 mg-sm-b-10">
						<input class="form-control" type="text" name="email" value="{{$data['email']}}">
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Số điện thoại</label>
					<div class="col-sm-5 mg-b-10 mg-sm-b-10">
						<input type="text" class="form-control" name="phone" value="{{$data['phone']}}">
                    </div>
                </div>
                <div class="row d-none">
                  	<label class="col-sm-2 form-control-label">Bộ phận</label>
                  	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                  	<select class="form-control" style="width: 100%;" name="adm_department">
	                  		<option value="0">- Chọn -</option>
	                  	  	<option value="1">IT</option>
	                  	  	<option value="2">Kinh Doanh</option>
	                  	  	<option value="3">Nhân sự</option>
	                  	</select>
	                </div>
                </div>
                <div class="row">
                	<label class="col-sm-2 form-control-label">Kích hoạt</label>
                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                    <div class="custom-control custom-checkbox">
	                        <input class="custom-control-input" type="checkbox" name="active" @if($data['active'] == 1) checked="checked" @endif value="1" id="adm_active">
	                        <label class="col-sm-4 form-control-label custom-control-label" for="adm_active"></label>
	                    </div>
	                </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 form-control-label">Phân quyền</label>
                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
	                    <table class="table table-striped">
	                                @foreach($allRole as $key => $value)
	                                    <tr>
	                                        <td width="135" nowrap="nowrap"><b>{{ $value['info']['label'] }}: </b></td>
	                                        <td valign="center">
	                                            @foreach($value['sub'] as $key_sub => $value_sub)
	                                                <div class="custom-control custom-checkbox" style="display: inline-block;vertical-align: middle; margin-right: 10px;">
	                                                    @if(isset($data['json_role'][$key]) && isset($data['json_role'][$key][$key_sub]) && $data['json_role'][$key][$key_sub] == 1)
	                                                        <input class="custom-control-input" type="checkbox" checked="checked" name="{{$key}}_{{$key_sub}}" value="1" id="{{$key}}_{{$key_sub}}">
	                                                    @else
	                                                        <input class="custom-control-input" type="checkbox" name="{{$key}}_{{$key_sub}}" value="1" id="{{$key}}_{{$key_sub}}">
	                                                    @endif
	                                                    <label for="{{$key}}_{{$key_sub}}" class="custom-control-label" style=" font-weight: 400 !important; font-size: 14px;">{{ $value_sub['label'] }}</label>
	                                                </div>
	                                            @endforeach
	                                        </td>
	                                    </tr>
	                                @endforeach
	                    </table>
                	</div>
                </div>
                <div class="row mg-t-20">
                	<div class="col-sm-2 mg-b-10"></div>
                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
                		<input type="hidden" name="action" value="execute">
	                	<button type="reset" class="btn btn-warning mg-r-20">Làm lại</button>
	                    <button type="submit" class="btn btn-primary">@if($data['id'] > 0) Cập nhật @else Thêm mới @endif</button>
	                </div>
                </div>
            </form>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
