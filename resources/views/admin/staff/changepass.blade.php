<div style="width: 90%; max-width: 400px; margin: 50px auto;">
	<h2>Thay đổi mật khẩu</h2>
	<div>{{$msg}}</div>
	<form action="{{route('admin.profile.changepass')}}" method="post" role="form">
		@csrf
		<input name="action" type="hidden" value="changepass">
		<table class="table table_border_none">
			<tr>
				<td width="100">Old Password</td>
				<td><input type="password" class="form-control" style="width: 100%;" id="old_password" name="old_password" placeholder="Enter Old password"></td>
			</tr>
			<tr>
				<td>New Password</td>
				<td><input type="password" class="form-control" style="width: 100%;" id="password" name="password" placeholder="Password"></td>
			</tr>
			<tr>
				<td>Re-Password</td>
				<td><input type="password" class="form-control" style="width: 100%;" id="re_password" name="re_password" placeholder="Re Password"></td>
			</tr>
			<tr class="form-actions">
				<td></td>
				<td><button type="submit" class="btn btn-primary btn-block active">Change Password</button></td>
			</tr>
		</table>
	</form>
</div>