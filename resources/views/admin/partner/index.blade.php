<?php
use App\Helper\Upload as UploadHelper;
use App\Repositories\PartnerRepository as Partner;
?>
@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.view.banner')}}">Partner</a>
      <span class="breadcrumb-item active">Danh sách Partner</span>
    </nav>
</div>
<div class="br-pagetitle" style="justify-content: space-between;">
	<h4>Danh sách Partner</h4>
	<div class=""><a href="{{ route('admin.view.partner_add') }}" class="btn btn-info">Thêm mới</a></div>
</div>
<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="">
  	  			<div class="row">
		            <div class="col-lg-2">
		              <input class="form-control" name="title" placeholder="Tên" type="text" value="{{ isset($dataSearch['title']) ? $dataSearch['title'] : '' }}">
		            </div>
		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>

		  	<div class="bd rounded table-responsive">
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		                <tr class="thead-colored thead-light">
		                	<td class="wd-5p">STT</td>
		                	<td class="wd-10p">Ảnh</td>
							<td class="wd-10p">Tên</td>
							<td class="wd-10p">Tên En</td>
							<td class="wd-10p">Nhóm</td>
							<td class="wd-10p">Link</td>
							<td class="wd-10p text-center">Kích hoạt</td>
							<td class="wd-10p text-center">Ngày tạo</td>
							<td class="wd-10p text-center">Sửa</td>
							<td class="wd-10p text-center">Xóa</td>
		                </tr>
				      	@foreach ($partners as $item)
							<tr id="record_{{ $item->par_id }}">
					          	<td class="text-center">{{ $loop->index + 1 }}</td>
					          	<td><img width="100%" src="{{ UploadHelper::getUrlImage('partner', $item->par_picture) }}"></td>
					          	<td>{{ $item->par_name }}</td>
					          	<td>{{ $item->par_name_en }}</td>
					          	<td>{{ Partner::getTypeLabel($item->par_type) }}</td>
					          	<td>{{ $item->par_link }}</td>
					          	<td class="text-center">
									<a href="javascript:;" class="box_active" onclick="changeActive('{{ route('admin.view.partner_status')}}', '{{ $item->par_id }}', 'active')">
										@if($item->par_active == 1)
											<i class="far fa-check-square" style="font-size: 14px;"></i>
										@else
											<i class="far fa-square" style="font-size: 14px;"></i>
										@endif
									</a>
								</td>
					          	<td class="text-center">{{ date("d/m/y H:i",$item->par_create_time) }}</td>
					          	<td class="text-center">
					          		<a href="{{ route('admin.view.partner_add', ['id' => $item->par_id])}}" class=""><i class="fas fa-edit"></i> Sửa</a>
					          	</td>
					          	<td class="text-center">
									<a class="buttonDelteRecord text-danger" href="{{ route('admin.view.partner_delete', ['id' => $item->par_id]) }}" ><i class="fa fa-times-circle"></i> Xóa</a>
					          	</td>
					        </tr>
				      	@endforeach

		      		</tbody>
		  	  	</table>
		  	</div>

        </div>
    </section>
    @include("admin.pagination")
</div>
@endsection

