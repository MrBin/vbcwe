<?php $nameRouteCurrent = Route::currentRouteName();?>
<div class="br-sideleft sideleft-scrollbar ps ps--active-y">
      <ul class="br-sideleft-menu mg-t-20">
      	<li class="br-menu-item">
          <a href="{{ route('admin.dashboard.index') }}" class="br-menu-link {{ (request()->is('admin/dashboard*')) ? 'active' : '' }}">
            <i class="menu-item-icon icon ion-ios-home-outline tx-24"></i>
            <span class="menu-item-label">Dashboard</span>
          </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
		@isset($menuAdmin)
			@foreach ($menuAdmin as $key => $menu)
				<li class="br-menu-item">
	          		<a href="#" class="br-menu-link @if($menu['sub'])  with-sub @endif {{ (request()->is('admin/' . $menu['info']['prefix'] . '*')) ? 'active show-sub' : '' }}">
		          		<i class="menu-item-icon icon {{ $menu['info']['icon'] }} tx-20"></i>
		            	<span class="menu-item-label">{{ $menu['info']['label'] }}</span>
	            	</a>
	            	@if($menu['sub'])
						<ul class="br-menu-sub" style="{{ (request()->is('user/' . $menu['info']['prefix'] . '*')) ? 'display: block;' : '' }}" >
							@foreach ($menu['sub'] as $sub)
								@if($sub['is_menu'] == 1)
									<li class="sub-item">
						            	<a href="{{ route($sub['route']) }}" class="sub-link {{ ($nameRouteCurrent == $sub['route']) ? 'active' : '' }} ">{{ $sub['label'] }}</a>
						        	</li>
					        	@endif
							@endforeach
						</ul>
	            	@endif
	            </li>
			@endforeach
		@endisset
      </ul><!-- br-sideleft-menu -->
      <br>
    <div class="ps__rail-x" style="left: 0px; top: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 575px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 369px;"></div></div></div>
<!-- Main Sidebar Container -->
