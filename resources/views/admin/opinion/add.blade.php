@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.view.partner')}}">Ý kiến đối tác</a>
      <span class="breadcrumb-item active">Thêm mới</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Thêm mới Ý kiến đối tác</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
	    <div class="br-section-wrapper container-fluid">
            @include("admin.error")
            <form role="form" method="post" action="" enctype="multipart/form-data">
            	@csrf
				<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Tên đối tác (*)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="name" value="{{ $data['name'] }}" class="form-control" placeholder="Nhập tên đối tác">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Tên đối tác EN (*)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="name_en" value="{{ $data['name_en'] }}" class="form-control" placeholder="Nhập tên đối tác">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Video (*)</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <input type="text" name="video" value="{{ $data['video'] }}" class="form-control" placeholder="Link video">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="exampleInputFile">Ảnh đại diện</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                      	<input type="hidden" name="picture" id="opi_picture" value="{{ $data['picture'] }}">
							@component('admin.upload', ['arrGallery' => $data['picture_data'], 'id_upload' => 'gallery_upload_file', 'type_upload' => 'other', 'max_upload' => 1, 'item_upload' => 'opi_picture', 'name_data' => 'picture_data'])
							@endcomponent
	                    </div>
	                </div>
	                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Ý kiến</label>
		                     <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="description" class="form-control " id="editor1">{{ $data['description'] }}</textarea>
			                </div>
		                </div>
		                <div class="row">
		                    <label class="col-sm-2 form-control-label" for="InputUsername">Ý kiến en</label>
		                     <div class="col-sm-10 mg-b-10 mg-sm-b-10">
			                    <textarea name="description_en" class="form-control " id="editor2">{{ $data['description_en'] }}</textarea>
			                </div>
		                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label" for="InputUsername">Order</label>
	                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
							<input type="text" class="form-control" name="order" value="{{ $data['order'] }}">
		                </div>
	                </div>
	                <div class="row">
	                    <label class="col-sm-2 form-control-label">Kích hoạt</label>
	                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
		                    <div class="custom-control custom-checkbox">
		                        <input class="custom-control-input" type="checkbox" id="opi_status" name="status" @if($data['status'] == 1) checked="checked" @endif value="1">
		                        <label class="col-sm-4 form-control-label custom-control-label" for="opi_status"></label>
		                    </div>
		                </div>
	                </div>
	            </div>

                <!-- /.card-body -->
                <div class="row mg-t-20">
                	<div class="col-sm-2 mg-b-10"></div>
                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
                		<input type="hidden" name="action" value="execute">
	                	<button type="reset" class="btn btn-warning mg-r-20">Làm lại</button>
	                    <button type="submit" class="btn btn-primary">@if($data['id'] > 0) Cập nhật @else Thêm mới @endif</button>
	                </div>
                </div>
            </form>
        </div>
    </section>
</div>

@endsection

@section('JS_FOOTER')
	<script src="{{ asset('/ckeditor/ckeditor.js') }}"></script>
	<script> CKEDITOR.replace('editor1'); </script>
	<script> CKEDITOR.replace('editor2'); </script>
@endsection
