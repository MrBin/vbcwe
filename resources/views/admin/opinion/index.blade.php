<?php
use App\Helper\Upload as UploadHelper;
?>
@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.view.opinion')}}">Ý kiến đối tác</a>
      <span class="breadcrumb-item active">Danh sách Ý kiến đối tác</span>
    </nav>
</div>
<div class="br-pagetitle" style="justify-content: space-between;">
	<h4>Danh sách Ý kiến đối tác</h4>
	<div class=""><a href="{{ route('admin.view.opinion_add') }}" class="btn btn-info">Thêm mới</a></div>
</div>
<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="">
  	  			<div class="row">
		            <div class="col-lg-2">
		              <input class="form-control" name="name" placeholder="Tên đối tác" type="text" value="{{ isset($dataSearch['name']) ? $dataSearch['name'] : '' }}">
		            </div>
		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>

		  	<div class="bd rounded table-responsive">
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		                <tr class="thead-colored thead-light">
		                	<td class="wd-5p">STT</td>
							<td class="wd-10p">Ảnh đại diện</td>
							<td class="wd-10p">Tên đối tác</td>
							<td class="wd-10p">Tên đối tác En</td>
							<td class="wd-10p text-center">Kích hoạt</td>
							<td class="wd-10p text-center">Ngày tạo</td>
							<td class="wd-10p text-center">Sửa</td>
							<td class="wd-10p text-center">Xóa</td>
		                </tr>
				      	@foreach ($opinions as $item)
							<tr id="record_{{ $item->opi_id }}">
					          	<td class="text-center">{{ $loop->index + 1 }}</td>
					          	<td><img src="{{ UploadHelper::getUrlImage('other', $item->opi_picture) }}" alt="" style="max-width: 100px"></td>
					          	<td>{{ $item->opi_name }}</td>
					          	<td>{{ $item->opi_name_en }}</td>
					          	<td class="text-center">
									<a href="javascript:;" class="box_active" onclick="changeActive('{{ route('admin.view.opinion_status')}}', '{{ $item->opi_id }}', 'active')">
										@if($item->opi_status == 1)
											<i class="far fa-check-square" style="font-size: 14px;"></i>
										@else
											<i class="far fa-square" style="font-size: 14px;"></i>
										@endif
									</a>
								</td>
					          	<td class="text-center">{{ date("d/m/y H:i",$item->opi_created_time) }}</td>
					          	<td class="text-center">
					          		<a href="{{ route('admin.view.opinion_add', ['id' => $item->opi_id])}}" class=""><i class="fas fa-edit"></i> Sửa</a>
					          	</td>
					          	<td class="text-center">
									<a class="buttonDelteRecord" href="{{ route('admin.view.opinion_delete', ['id' => $item->opi_id]) }}" ><i class="fa fa-times-circle"></i> Xóa</a>
					          	</td>
					        </tr>
				      	@endforeach

		      		</tbody>
		  	  	</table>
		  	</div>

        </div>
    </section>
    @include("admin.pagination")
</div>
@endsection

