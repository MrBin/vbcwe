@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.staff.index')}}">Staff</a>
      <span class="breadcrumb-item active">Danh sách</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Danh sách nhân viên</h4>
</div>
<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="{{ $page_filter_url }}">
  	  			<div class="row">
		            <div class="col-lg">
		              <input class="form-control" name="username" placeholder="Username" type="text" value="{{ isset($params['username']) ? $params['username'] : '' }}">
		            </div>
		            <div class="col-lg mg-t-10 mg-lg-t-0">
		              <input class="form-control" name="userphone" placeholder="Phone" type="text" value="{{ isset($params['userphone']) ? $params['userphone'] : '' }}">
		            </div>
		            <div class="col-lg mg-t-10 mg-lg-t-0">
		              <input class="form-control" name="useremail" placeholder="Email" type="text" value="{{ isset($params['useremail']) ? $params['useremail'] : '' }}">
		            </div>
		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>


		  	<div class="bd rounded table-responsive">
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		                <tr class="thead-colored thead-light">
		                	<td class="wd-10p">ID</td>
							<td class="wd-10p">User</td>
							<td class="wd-10p">Email</td>
							<td class="wd-10p text-center">Trạng thái</td>
							<td class="wd-10p text-center">Ngày tạo</td>
							<td class="wd-10p text-center">Sửa</td>
							<td class="wd-10p text-center">Xóa</td>
		                </tr>
				      	@foreach ($admin_user as $user)
							<tr>
					          	<td>{{ $user->adm_id }}</td>
					          	<td>{{ $user->adm_name }}</td>
					          	<td>{{ $user->adm_email }}</td>
					          	<td class="text-center">
					          		@if($user->adm_active == 1)
					          			<i class="far fa-check-square"></i>
					          		@else
					          			<i class="far fa-square"></i>
					          		@endif
					          	</td>
					          	<td class="text-center">{{ date("d/m/y H:i",$user->adm_create_time) }}</td>
					          	<td class="text-center">
					          		<a href="/admin/staff/{{ $user->adm_id }}/edit" class=""><i class="fas fa-edit"></i> Sửa</a>
					          	</td>
					          	<td class="text-center">
					          		{!! Form::open(['url' => '/admin/staff/'. $user->adm_id , 'method' => 'DELETE', 'onsubmit' => 'return confirm("Bạn có chắc chắn muốn xóa bản ghi này?")']); !!}
						          		<button type="submit" class="button-delete"><i class="fa fa-times-circle"></i> Xóa</button>
					          		{!! Form::close() !!}
					          	</td>
					        </tr>
				      	@endforeach

		      		</tbody>
		  	  	</table>

		  	  	{!! $admin_user->appends($params)->render() !!}

		  	</div>

        </div>
    </section>
</div>
@endsection

