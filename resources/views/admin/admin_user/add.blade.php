@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.staff.index')}}">Staff</a>
      <span class="breadcrumb-item active">Thêm mới</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Thêm mới Staff</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			@include("admin.error")
            <form role="form" method="post" action="{{route('admin.staff.store')}}" enctype="multipart/form-data">
            	@csrf
            	<div class="row">
                    <label class="col-sm-2 form-control-label">Username</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('adm_name', old('adm_name') ? old('adm_name') : null, ['class' => 'form-control','placeholder' => 'Nhập username']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputPassword">Password</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">{!! Form::input('password', 'adm_password', old('adm_password') ? old('adm_password') : null, ['class' => 'form-control', 'placeholder' => 'Nhập mật khẩu']) !!}</div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputPassword">Repassword</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">{!! Form::input('password', 'repassword', old('repassword') ? old('repassword') : null, ['class' => 'form-control', 'placeholder' => 'Nhập lại mật khẩu']) !!}</div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputEmail">Email</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">{!! Form::text('adm_email', old('adm_email') ? old('adm_email') :  null, ['class' => 'form-control','placeholder' => 'Nhập email']) !!}</div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Số điện thoại</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">{!! Form::text('adm_phone', old('adm_phone') ? old('adm_phone') :  null, ['class' => 'form-control','placeholder' => 'Nhập phone']) !!}</div>
                </div>
                <div class="row">
                  	<label class="col-sm-2 form-control-label">Bộ phận</label>
                  	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                  	<select class="form-control" style="width: 100%;" name="adm_department">
	                  		<option value="0">- Chọn -</option>
	                  	  	<option value="1">IT</option>
	                  	  	<option value="2">Kinh Doanh</option>
	                  	  	<option value="3">Nhân sự</option>
	                  	</select>
	                </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="exampleInputFile">Ảnh đại diện</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                      <div class="custom-file">
                      	{!! Form::file('adm_avatar', ['class' => 'custom-file-input','id'=>'exampleInputFile']) !!}
                        <label class="custom-file-label" for="customFile">Choose ảnh</label>
                      </div>
                    </div>
                </div>
                <div class="row">
                	<label class="col-sm-2 form-control-label">Kích hoạt</label>
                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                    <div class="custom-control custom-checkbox">
	                        {!! Form::checkbox('adm_active', '1', old('adm_active') ? true : false, ['class' => 'custom-control-input', 'id' => 'adm_active']) !!}
	                        <label class="col-sm-4 form-control-label custom-control-label" for="adm_active"></label>
	                    </div>
	                </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 form-control-label">Phân quyền</label>
                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
	                    <table class="table table-striped">
	                                @foreach($allRole as $key => $value)
	                                    <tr>
	                                        <td width="" nowrap="nowrap"><b>{{ $value['label'] }}: </b></td>
	                                        <td valign="center">
	                                            @foreach($value['sub'] as $key_sub => $value_sub)
	                                                <div class="custom-control custom-checkbox" style="display: inline-block;vertical-align: middle; margin-right: 10px;">
	                                                    @if(isset($data['json_role'][$key]) && isset($data['json_role'][$key][$key_sub]) && $data['json_role'][$key][$key_sub] == 1)
	                                                        <input class="custom-control-input" type="checkbox" checked="checked" name="{{$key}}_{{$key_sub}}" value="1" id="{{$key}}_{{$key_sub}}">
	                                                    @else
	                                                        <input class="custom-control-input" type="checkbox" name="{{$key}}_{{$key_sub}}" value="1" id="{{$key}}_{{$key_sub}}">
	                                                    @endif
	                                                    <label for="{{$key}}_{{$key_sub}}" class="custom-control-label" style=" font-weight: 400 !important; font-size: 14px;">{{ $value_sub['label'] }}</label>
	                                                </div>
	                                            @endforeach
	                                        </td>
	                                    </tr>
	                                @endforeach
	                    </table>
                	</div>
                </div>
                <div class="row mg-t-20">
                	<div class="col-sm-2 mg-b-10"></div>
                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                	<button type="reset" class="btn btn-warning mg-r-20">Làm lại</button>
	                    <button type="submit" class="btn btn-primary">Thêm mới</button>
	                </div>
                </div>
            </form>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection