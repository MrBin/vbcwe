@extends('admin.layouts')
@section('CONTAINER')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Sửa thông tin <b>{{ $admin_user['adm_name'] }}</b></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.staff.index')}}">Quản lý Staff</a></li>
                        <li class="breadcrumb-item active">Sửa thông tin Staff</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                	@include("admin.error")

                    <!-- jquery validation -->
                    <div class="card ">
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="/admin/staff/{{ $admin_user['adm_id'] }}" enctype="multipart/form-data">
                        	@csrf
                            <input name="_method" type="hidden" value="PUT">
                            <div class="card-body">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="InputEmail">Email</label>
                                        {!! Form::text('adm_email', $admin_user['adm_email'] ? $admin_user['adm_email'] :  null, ['class' => 'form-control','placeholder' => 'Nhập email']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label for="InputUsername">Số điện thoại</label>
                                        {!! Form::text('adm_phone', $admin_user['adm_phone'] ? $admin_user['adm_phone'] :  null, ['class' => 'form-control','placeholder' => 'Nhập phone']) !!}
                                    </div>
                                    <div class="form-group">
    				                  	<label>Bộ phận</label>
    				                  	<select class="form-control" style="width: 100%;" name="adm_department">
    				                  		<option value="0">- Chọn -</option>
    				                  	  	<option value="1">IT</option>
    				                  	  	<option value="2">Kinh Doanh</option>
    				                  	  	<option value="3">Nhân sự</option>
    				                  	</select>
    				                </div>
    				                <div class="form-group">
    				                    <label for="exampleInputFile">Ảnh đại diện</label>
    				                    <div class="input-group">
    				                      <div class="custom-file">
    				                      	{!! Form::file('adm_avatar', ['class' => 'custom-file-input','id'=>'exampleInputFile']) !!}
    				                        <label class="custom-file-label" for="exampleInputFile">Chọn ảnh</label>
    				                      </div>
    				                      <div class="input-group-append">
    				                        <span class="input-group-text" id="">Upload</span>
    				                      </div>
    				                    </div>
    				                </div>
                                    <div class="form-group">
                                        <img src="/{{ App\Helper\ImageManager::getThumb($admin_user['adm_avatar'],'user_admin') }}">
                                    </div>
                                    
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        
                                        <label>Phân quyền</label>
                                        <table class="table table-striped">
                                            @foreach($allRole as $key => $value)
                                                <tr>
                                                    <td width="180" nowrap="nowrap"><b>{{ $value['label'] }}: </b></td>
                                                    <td>
                                                        @foreach($value['sub'] as $key_sub => $value_sub)
                                                            <div class="custom-control custom-checkbox" style="display: inline-block;vertical-align: middle; margin-right: 10px;">
                                                                @if(isset($admin_user['adm_role'][$key]) && isset($admin_user['adm_role'][$key][$key_sub]) && $admin_user['adm_role'][$key][$key_sub] == 1)
                                                                    <input class="custom-control-input" type="checkbox" name="{{$key}}_{{$key_sub}}" value="1" id="{{$key}}_{{$key_sub}}" checked>
                                                                 @else
                                                                    <input class="custom-control-input" type="checkbox" name="{{$key}}_{{$key_sub}}" value="1" id="{{$key}}_{{$key_sub}}">
                                                                @endif
                                                                <label for="{{$key}}_{{$key_sub}}" class="custom-control-label" style=" font-weight: 400 !important; font-size: 14px;">{{ $value_sub['label'] }}</label>
                                                            </div>
                                                        @endforeach
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                     
                                    </div>
                                    <div class="form-group">
                                        <label>Kích hoạt</label>
                                        <div class="custom-control custom-switch">
                                            {!! Form::checkbox('adm_active', '1', $admin_user['adm_active'] ? true : false, ['class' => 'custom-control-input', 'id' => 'adm_active']) !!}
                                            <label class="custom-control-label" for="adm_active"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Lưu</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection