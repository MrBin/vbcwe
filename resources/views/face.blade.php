<!DOCTYPE html>
<html>
<head>
<title>Facebook Login JavaScript Example</title>
<meta charset="UTF-8">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.0.min.js"></script>
<style type="text/css">
	div{
		padding: 5px 20px;
	}
</style>
</head>
<body>
	<div id="status"></div>
	<div>
		<fb:login-button scope="public_profile,email,manage_pages,pages_show_list,publish_pages,business_management,ads_management,ads_read" onlogin="checkLoginState();"></fb:login-button>
	</div>

	<div style="display: none;" id="id_user">Id user: <span></span></div>
	<div style="display: none;" id="token">Token: <span></span></div>
	<div style="display: none;" id="box_get_long_token"><a href="javascript:;" onclick="getLongAccessToken();">Get Long AccessToken</a></div>
	<div style="display: none;" id="token_long">Token long: <span></span></div>
	<div style="display: none;" id="box_get_all_page"><a href="javascript:;" onclick="getAllPage();">Get All Page</a></div>
	<div>Url get long access token: https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=1116572895355009&client_secret=aa75e4a267880c55b938868af1cc780c&fb_exchange_token=EAAP3hGLLIIEBAF5OgUHWxm3CxS4yqBIY1fiLXutURMBag2yZBBZBAOVToiiM9ZAxsaqMpXOexWUV2tZAoZA2wrH8Jk3kHTqOhoiwOrFWZCZCzpRb3jnpEmdD8rCBOLhpDQu8RwFBZBletMPFPZCSOsZBZB4cBIZBAAHIv7uBNww1nElD4RLHUayFV7vGVQwKRqPWSGlEFeMvX9d0XQZDZD</div>
	<div style="display: none;" id="box_all_page"></div>
	<div>url get list page https://graph.facebook.com/2706881792770931/accounts?access_token=EAAP3hGLLIIEBAEgzSfmrOxCq4YUrg0jKVf12SYzyo28LQVlGLW1xWjgvzuDPENTPHAmLZAtrkZAMU52gGOs9YnCrjZCmRWjBUnhAVW9VeqE4NP2iiVHitqAPX7ULlvnifxuUZATYg8j5ttcxi0ovfmtiuuNVsWN5q02NIkhJzwZDZD</div>
	<div>url get list post: https://graph.facebook.com/v6.0/111728040508057/?access_token=EAAP3hGLLIIEBAEgzSfmrOxCq4YUrg0jKVf12SYzyo28LQVlGLW1xWjgvzuDPENTPHAmLZAtrkZAMU52gGOs9YnCrjZCmRWjBUnhAVW9VeqE4NP2iiVHitqAPX7ULlvnifxuUZATYg8j5ttcxi0ovfmtiuuNVsWN5q02NIkhJzwZDZD</div>
	<div>url get list commnet của post: https://graph.facebook.com/v6.0/{post-id}/comments</div>
	<div>url get list ad account: https://graph.facebook.com/v6.0/2706881792770931/adaccounts?fields=id,account_id,business,business_name,account_status&access_token=EAAP3hGLLIIEBAGAQZAAhxIxVasT1gCvZCYS1Kxr4H7J5sbXC8aci8bueQeb9vlyhXYEcNgebrBkyWkGDlKOiRtMwNIDt60G4cuBx8P0vBZAjGPc7Jm8ChGTrXauUURhSciQiLnXDOZBOPIB2nrVCjBDsc05w3AMv2B8bZAGfbZBAZDZD</div>
	<div>url get list campain của 1 ad account https://graph.facebook.com/v6.0/act_672894893445629/campaigns?access_token=EAAP3hGLLIIEBAGAQZAAhxIxVasT1gCvZCYS1Kxr4H7J5sbXC8aci8bueQeb9vlyhXYEcNgebrBkyWkGDlKOiRtMwNIDt60G4cuBx8P0vBZAjGPc7Jm8ChGTrXauUURhSciQiLnXDOZBOPIB2nrVCjBDsc05w3AMv2B8bZAGfbZBAZDZD</div>
</body>
</html>

<script type="text/javascript">
	var userId  		= '';
	var appId			= '233206038093952';
	var appSecret		= 'dc5678662d27ae762e891b86c3a88cbe';
	var accessToken		= '';
	var longAccessToken	= '';

	function statusChangeCallback(response) {  // Called with the results from FB.getLoginStatus().
		console.log('statusChangeCallback');
		// The current login status of the person.
		if (response.status === 'connected') {   // Logged into your webpage and Facebook.
			testAPI();
			userId		= response.authResponse.userID;
			accessToken	= response.authResponse.accessToken;
			$('#id_user span').html(userId);
			$('#token span').html(accessToken);
			$('#id_user').show();
			$('#token').show();
			$("#box_get_long_token").show();
		} else {  // Not logged into your webpage or we are unable to tell.
			document.getElementById('status').innerHTML = 'Please log ' + 'into this webpage.';
		}
	}


	function checkLoginState() {               // Called when a person is finished with the Login Button.
		FB.getLoginStatus(function(response) {   // See the onlogin handler
			statusChangeCallback(response);
		});
	}


  	window.fbAsyncInit = function() {
		FB.init({
			appId      : appId,
			cookie     : true,                     // Enable cookies to allow the server to access the session.
			xfbml      : true,                     // Parse social plugins on this webpage.
			version    : 'v6.0'           // Use this Graph API version for this call.
		});


		FB.getLoginStatus(function(response) {   // Called after the JS SDK has been initialized.
			statusChangeCallback(response);        // Returns the login status.
		});
	};


	(function(d, s, id) {                      // Load the SDK asynchronously
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "https://connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));


	function testAPI() {                      // Testing Graph API after login.  See statusChangeCallback() for when this call is made.
		console.log('Welcome!  Fetching your information.... ');
		FB.api('/me', function(response) {
		console.log('Successful login for: ' + response.name);
		document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.name + '!';
		});
	}

	function getLongAccessToken(){
		var url = 'https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=' + appId + '&client_secret=' + appSecret + '&fb_exchange_token=' + accessToken;
		$.ajax({
			type: "GET",
			url: url,
			success: function(data){
				if(data.access_token){
					longAccessToken = data.access_token;
					alert(longAccessToken);
					$('#token_long span').html(longAccessToken);
					$('#token_long').show();
					$("#box_get_all_page").show();
				}else{
					longAccessToken = '';
					$('#token_long span').html('Error get long AccessToken');
					$('#token_long').show();
				}
			},
			dataType: 'json'
		});
	}

	function getAllPage(){
		var url = 'https://graph.facebook.com/' + userId + '/accounts?access_token=' + longAccessToken;
		$.ajax({
			type: "GET",
			url: url,
			success: function(data){
				if(data.data){
					$.each(data.data, function( index, value ) {
					  	var html_add = '<div class="item_page" id="page_' + value.id + '" data-token="' + value.access_token + '">';
					  		html_add += '<div>Id: ' + value.id+ '</div>'
					  		html_add += '<div>Name: ' + value.name+ '</div>';
					  		html_add += '<div><a href="javascript:;" onclick="getPost(\'' + value.id + '\')">Get Post</a></div>';
					  		html_add += '<div id="box_post_' + value.id + '"></div>';
					  		html_add += '</div>';
					  		$("#box_all_page").append(html_add);
					});
					$("#box_all_page").show();
				}
			},
			dataType: 'json'
		});
	}

	function getPost(pageId){
		var token_page 	= $("#page_" + pageId).data('token');
		var url 		= 'https://graph.facebook.com/v6.0/' + pageId + '/feed?access_token=' + token_page;
		$.ajax({
			type: "GET",
			url: url,
			success: function(data){
				$.each(data.data, function( index, value ) {
				  	var html_add = '<div class="item_post" id="post_' + value.id + '">';
				  		html_add += '<div>Id: ' + value.id + '</div>'
				  		html_add += '<div>Content: ' + value.message + '</div>';
				  		html_add += '<div>Time: ' + value.created_time + '</div>';

				  		html_add += '<div><a href="javascript:;" onclick="getComment(\'' + value.id + '\')">Get Comment</a></div>';
				  		html_add += '</div>';
				  		$("#box_post_" + pageId).append(html_add);
				});
			},
			dataType: 'json'
		});
	}
</script>