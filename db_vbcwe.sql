/*
Navicat MySQL Data Transfer

Source Server         : Mysql
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : db_vbcwe

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-10-27 14:08:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin_user
-- ----------------------------
DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE `admin_user` (
  `adm_id` int(11) NOT NULL AUTO_INCREMENT,
  `adm_loginname` varchar(100) DEFAULT NULL,
  `adm_password` varchar(100) DEFAULT NULL,
  `adm_name` varchar(255) DEFAULT NULL,
  `adm_email` varchar(255) DEFAULT NULL,
  `adm_address` varchar(255) DEFAULT NULL,
  `adm_phone` varchar(255) DEFAULT NULL,
  `adm_mobile` varchar(255) DEFAULT NULL,
  `adm_cskh` tinyint(2) DEFAULT 0,
  `adm_job` tinyint(4) NOT NULL DEFAULT 0,
  `adm_access_module` varchar(255) DEFAULT NULL,
  `adm_access_category` text DEFAULT NULL,
  `adm_json_role` text DEFAULT NULL,
  `adm_isadmin` tinyint(1) DEFAULT 0,
  `adm_active` tinyint(1) DEFAULT 1,
  `adm_delete` int(11) DEFAULT 0,
  `adm_all_category` int(1) DEFAULT NULL,
  `adm_edit_all` int(1) DEFAULT 0,
  `adm_admin_id` int(1) DEFAULT 0,
  `adm_create_time` int(1) DEFAULT 0,
  `adm_update_at` int(1) DEFAULT 0,
  PRIMARY KEY (`adm_id`),
  KEY `admin_id` (`adm_admin_id`),
  KEY `adm_isadmin` (`adm_isadmin`),
  KEY `adm_active` (`adm_active`),
  KEY `adm_cskh` (`adm_cskh`)
) ENGINE=MyISAM AUTO_INCREMENT=472 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of admin_user
-- ----------------------------
INSERT INTO `admin_user` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'Nguyễn An', 'farmer289@gmail.com', 'Số 15 ngõ 143 - Trung Kính - Trung Hòa - Cầu Giấy - Hà Nội', '(84-04) 784 7135 - (84-04) 219 2996', '', '0', '0', null, null, '{\"mtype\":{\"view\":1,\"add\":1,\"edit\":1,\"censorship\":1,\"delete\":1},\"mlevel\":{\"view\":1,\"add\":1,\"edit\":1,\"censorship\":1,\"delete\":1},\"question_sms\":{\"view\":1,\"add\":1,\"edit\":1,\"censorship\":1,\"delete\":1},\"question_practice\":{\"view\":1,\"add\":1,\"edit\":1,\"censorship\":1,\"delete\":1},\"question_compete\":{\"view\":1,\"add\":1,\"edit\":1,\"censorship\":1,\"delete\":1}}', '1', '1', '0', null, '0', '0', '0', '0');
INSERT INTO `admin_user` VALUES ('320', 'thegioithietbiso', '', 'Nguyễn An', 'nguyenan@gmail.com', null, '0909990001', '', '0', '0', null, '', '{\"dashboard\":{\"dashboard\":1},\"user\":{\"list\":1}}', '0', '0', '0', '1', '0', '1', '0', '0');
INSERT INTO `admin_user` VALUES ('440', 'linhphi', '2060c32b2b8ecbb9cce3e7b93cabfe1c', 'Linh Phi', 'linhphi9x94@gmail.com', null, '0974867773', '', '0', '0', null, '', '{\"banner\":{\"add\":1,\"list\":1,\"delete\":1},\"product\":{\"add\":1,\"edit\":1,\"delete\":1},\"category\":{\"list\":1,\"add\":1,\"edit\":1,\"delete\":1},\"staff\":{\"list\":1,\"add\":1,\"delete\":1}}', '0', '1', '0', '1', '0', '1', '0', '0');
INSERT INTO `admin_user` VALUES ('471', 'account_two', '', 'Nguyễn Văn Ac', 'farmer289@gmail.com', null, '0123456', null, '0', '0', null, null, '{\"banner\":{\"add\":1},\"product\":{\"add\":1,\"edit\":1,\"delete\":1},\"category\":{\"list\":1},\"staff\":{\"list\":1}}', '0', '1', '0', null, '0', '0', '0', '0');

-- ----------------------------
-- Table structure for banners
-- ----------------------------
DROP TABLE IF EXISTS `banners`;
CREATE TABLE `banners` (
  `ban_id` int(11) NOT NULL AUTO_INCREMENT,
  `ban_name` varchar(255) DEFAULT NULL,
  `ban_picture` varchar(255) DEFAULT NULL,
  `ban_link` text DEFAULT NULL,
  `ban_description` text DEFAULT NULL,
  `ban_target` varchar(255) DEFAULT '_blank',
  `ban_type` int(11) DEFAULT 0,
  `ban_position` tinyint(2) DEFAULT 0,
  `ban_create_time` int(11) DEFAULT 0,
  `ban_update_at` int(11) DEFAULT 0,
  `ban_active` tinyint(4) NOT NULL DEFAULT 0,
  `ban_order` int(11) NOT NULL DEFAULT 0,
  `staff_id` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ban_id`),
  KEY `ban_order` (`ban_order`) USING BTREE,
  KEY `ban_active` (`ban_active`) USING BTREE,
  KEY `ban_position` (`ban_position`) USING BTREE,
  KEY `ban_type` (`ban_type`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of banners
-- ----------------------------
INSERT INTO `banners` VALUES ('6', 'Phùng Văn Quyêt', 'qjllr1596689184.jpeg', 'dantri.com', '', '_self', '0', '1', '0', '0', '1', '11', '0');

-- ----------------------------
-- Table structure for categories_multi
-- ----------------------------
DROP TABLE IF EXISTS `categories_multi`;
CREATE TABLE `categories_multi` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) DEFAULT NULL,
  `cat_name_en` varchar(255) DEFAULT '',
  `cat_name_rewrite` varchar(266) DEFAULT NULL,
  `cat_order` int(5) DEFAULT NULL,
  `cat_picture` varchar(255) DEFAULT NULL,
  `cat_banner` varchar(255) DEFAULT NULL,
  `cat_banner_link` text DEFAULT NULL,
  `cat_background` varchar(255) DEFAULT NULL,
  `cat_description` text DEFAULT NULL,
  `cat_description_en` text DEFAULT NULL,
  `cat_meta_keyword` text DEFAULT NULL,
  `cat_meta_keyword_en` text DEFAULT NULL,
  `cat_meta_title` text DEFAULT NULL,
  `cat_meta_title_en` text DEFAULT NULL,
  `cat_meta_description` text DEFAULT NULL,
  `cat_meta_description_en` text DEFAULT NULL,
  `cat_active` int(1) DEFAULT 1,
  `lang_id` int(1) DEFAULT 1,
  `cat_parent_id` int(11) NOT NULL DEFAULT 0,
  `cat_has_child` int(11) NOT NULL DEFAULT 1,
  `cat_all_child` varchar(255) DEFAULT NULL,
  `cat_type` varchar(100) DEFAULT NULL,
  `cat_hot` tinyint(4) DEFAULT 0,
  `admin_id` int(11) DEFAULT 0,
  `cat_show_mob` tinyint(1) DEFAULT 0,
  `cat_show` int(1) DEFAULT 0,
  `cat_create_time` int(11) DEFAULT 0,
  `cat_update_at` int(11) DEFAULT 0,
  PRIMARY KEY (`cat_id`),
  KEY `cat_parent_id` (`cat_parent_id`),
  KEY `cat_order` (`cat_order`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of categories_multi
-- ----------------------------
INSERT INTO `categories_multi` VALUES ('1', '', '', 'phung-van-quyet', '12', '', null, null, null, null, null, 'Phùng Văn Quyêt 2', '', 'Phùng Văn Quyêt1', '', 'Phùng Văn Quyêt25', '', '1', '1', '0', '1', null, 'news', '0', '0', '0', '0', '1596526966', '1596594960');
INSERT INTO `categories_multi` VALUES ('2', 'Phùng Văn Quyêt 1111', '', 'phung-van-quyet-1111', '122', '', null, null, null, null, null, 'Phùng Văn Quyêt', null, 'Phùng Văn Quyêt', null, 'Phùng Văn Quyêt', null, '0', '1', '1', '1', null, 'news', '0', '0', '0', '0', '1596526966', '1596596307');
INSERT INTO `categories_multi` VALUES ('3', 'Phùng Văn Quyêt 1', 'Phùng Văn Quyêt en', 'phung-van-quyet-1', '10', '', null, null, null, null, null, '', '', '', '', '', '', '1', '1', '0', '1', null, 'event', '0', '0', '0', '0', '1596702640', '1596702640');

-- ----------------------------
-- Table structure for configuration
-- ----------------------------
DROP TABLE IF EXISTS `configuration`;
CREATE TABLE `configuration` (
  `con_id` int(11) NOT NULL AUTO_INCREMENT,
  `con_page_size` varchar(10) DEFAULT NULL,
  `con_admin_email` varchar(255) DEFAULT NULL,
  `con_site_title` varchar(255) DEFAULT NULL,
  `con_meta_description` text DEFAULT NULL,
  `con_meta_keywords` text DEFAULT NULL,
  `con_currency` varchar(20) DEFAULT NULL,
  `con_mod_rewrite` tinyint(1) DEFAULT 0,
  `con_lang_id` int(11) DEFAULT 1,
  `con_extenstion` varchar(20) DEFAULT 'html',
  `lang_id` int(11) DEFAULT 1,
  `con_contact` int(11) DEFAULT 0,
  `con_hotline` varchar(255) DEFAULT NULL,
  `con_support` varchar(30) DEFAULT NULL,
  `con_background_img` varchar(255) DEFAULT NULL,
  `con_background_color` varchar(50) DEFAULT NULL,
  `con_address` text DEFAULT NULL,
  `con_image_path` varchar(255) DEFAULT NULL,
  `con_picture_path` varchar(255) DEFAULT NULL,
  `con_background_homepage` varchar(255) DEFAULT NULL,
  `con_theme_path` varchar(255) DEFAULT NULL,
  `con_payment_account` text DEFAULT NULL,
  `con_map` text DEFAULT NULL,
  `con_menu_footer` text DEFAULT NULL,
  `con_note_tour` text DEFAULT NULL,
  `con_cancel_tour` text DEFAULT NULL,
  `con_gmail_pass` varchar(255) DEFAULT NULL,
  `con_note` varchar(255) NOT NULL,
  `con_gmail_name` varchar(255) DEFAULT NULL,
  `con_gmail_subject` varchar(255) DEFAULT NULL,
  `con_teaser` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`con_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of configuration
-- ----------------------------
INSERT INTO `configuration` VALUES ('1', '1133', 'farmer289@gmail.com', 'Title website', '', '', 'VND', '1', '1', 'html', '1', '0', '01685 480 846', null, '', '#f6ebcf', '17 Bùi Thị Xuân, Hai Bà Trưng, Hà Nội', '', '', '', '', null, null, '<div class=\"col-md-6\" style=\"color: rgb(51, 51, 51); font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;; font-size: 13px;\">\r\n	<div class=\"group-content-tb\">\r\n		<h4 style=\"font-weight: normal; line-height: 1.2;\">\r\n			Điểm đến nổi bật trong nước</h4>\r\n		<ul>\r\n			<li>\r\n				<a href=\"http://postumtravel.vn/du-lich-ha-noi/\" title=\"Du lịch Hà Nội\">Du lịch Hà Nội</a></li>\r\n			<li>\r\n				<a href=\"http://postumtravel.vn/du-lich-da-nang/\" title=\"Du lịch Đà Nẵng\">Du lịch Đà Nẵng</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Phú Quốc\">Du lịch Phú Quốc</a></li>\r\n			<li>\r\n				<a href=\"http://postumtravel.vn/du-lich-ha-long/\" title=\"Du lịch Hạ Long\">Du lịch Hạ Long</a></li>\r\n			<li>\r\n				<a href=\"http://postumtravel.vn/du-lich-hue/\" title=\"Du lịch Huế\">Du lịch Huế</a></li>\r\n			<li>\r\n				<a href=\"http://postumtravel.vn/du-lich-nha-trang/\" title=\"Du lịch Nha Trang\">Du lịch Nha Trang</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Lào Cai\">Du lịch Lào Cai</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Quảng Nam\">Du lịch Quảng Nam</a></li>\r\n			<li>\r\n				<a href=\"http://postumtravel.vn/du-lich-da-lat/\" title=\"Du lịch Đà Lạt\">Du lịch Đà Lạt</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Hải Phòng\">Du lịch Hải Phòng</a></li>\r\n			<li>\r\n				<a href=\"http://postumtravel.vn/du-lich-ninh-binh/\" title=\"Du lịch Ninh Bìn\">Du lịch Ninh Bình</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Hà Tây\">Du lịch Hà Tây</a></li>\r\n			<li>\r\n				<a href=\"http://postumtravel.vn/du-lich-lang-son/\" title=\"Du lịch Lạng Sơn\">Du lịch Lạng Sơn</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Quảng Bình\">Du lịch Quảng Bình</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Bảo Lộc\">Du lịch Bảo Lộc</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Ban Mê Thuột\">Du lịch Ban Mê Thuột</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Pleiku\">Du lịch Pleiku</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Phan Thiết\">Du lịch Phan Thiết</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Đồng Nai\">Du lịch Đồng Nai</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Vũng Tàu\">Du lịch Vũng Tàu</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Tiền Giang\">Du lịch Tiền Giang</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Hà Tiên\">Du lịch Hà Tiên</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Cần Thơ\">Du lịch Cần Thơ</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Vĩnh Long\">Du lịch Vĩnh Long</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Bình Định\">Du lịch Bình Định</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch An Giang\">Du lịch An Giang</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Ninh Thuận\">Du lịch Ninh Thuận</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Phú Yên\">Du lịch Phú Yên</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Cao Bằng\">Du lịch Cao Bằng</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Bắc Kạn\">Du lịch Bắc Kạn</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Vinh\">Du lịch Vinh</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Thanh Hóa\">Du lịch Thanh Hóa</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Sơn La\">Du lịch Sơn La</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Hà Giang\">Du lịch Hà Giang</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Sóc Trăng\">Du lịch Sóc Trăng</a></li>\r\n		</ul>\r\n	</div>\r\n</div>\r\n<div class=\"col-md-6\" style=\"color: rgb(51, 51, 51); font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;; font-size: 13px;\">\r\n	<div class=\"group-content-tb\">\r\n		<h4 style=\"font-weight: normal; line-height: 1.2;\">\r\n			Điểm đến nổi bật nước ngoài</h4>\r\n		<ul>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Malta\">Du lịch Malta</a></li>\r\n			<li>\r\n				<a href=\"http://postumtravel.vn/du-lich-campuchia/\" title=\"Du lịch Campuchia\">Du lịch Campuchia</a></li>\r\n			<li>\r\n				<a href=\"http://postumtravel.vn/du-lich-nhat-ban/\" title=\"Du lịch Nhật Bản\">Du lịch Nhật Bản</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Hoa Kỳ\">Du lịch Hoa Kỳ</a></li>\r\n			<li>\r\n				<a href=\"http://postumtravel.vn/du-lich-thai-lan/\" title=\"Du lịch Thái Lan\">Du lịch Thái Lan</a></li>\r\n			<li>\r\n				<a href=\"http://postumtravel.vn/du-lich-han-quoc/\" title=\"Du lịch Hàn Quốc\">Du lịch Hàn Quốc</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Pháp\">Du lịch Pháp</a></li>\r\n			<li>\r\n				<a href=\"http://postumtravel.vn/du-lich-singapore/\" title=\"Du lịch Singapore\">Du lịch Singapore</a></li>\r\n			<li>\r\n				<a href=\"http://postumtravel.vn/du-lich-hongkong/\" title=\"Du lịch Hồng Kông &amp; Macau\">Du lịch Hồng Kông &amp; Macau</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Úc\">Du lịch Úc</a></li>\r\n			<li>\r\n				<a href=\"http://postumtravel.vn/du-lich-malaysia/\" title=\"Du lịch Malaysia\">Du lịch Malaysia</a></li>\r\n			<li>\r\n				<a href=\"http://postumtravel.vn/du-lich-trung-quoc/\" title=\"Du lịch Trung Quốc\">Du lịch Trung Quốc</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Ai Cập\">Du lịch Ai Cập</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Nam Phi\">Du lịch Nam Phi</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Ấn Độ\">Du lịch Ấn Độ</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Trung Đông\">Du lịch Trung Đông</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Brunei\">Du lịch Brunei</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Nga\">Du lịch Nga</a></li>\r\n			<li>\r\n				<a href=\"http://postumtravel.vn/du-lich-dai-loan/\" title=\"Du lịch Đài Loan\">Du lịch Đài Loan</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Indonesia\">Du lịch Indonesia</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Israel\">Du lịch Israel</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Lào\">Du lịch Lào</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Canada\">Du lịch Canada</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch New Zealand\">Du lịch New Zealand</a></li>\r\n			<li>\r\n				<a href=\"http://postumtravel.vn/du-lich-myanmar/\" title=\"Du lịch Myanmar\">Du lịch Myanmar</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Philippines\">Du lịch Philippines</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Nepal\">Du lịch Nepal</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Tàu du lịch\">Du lịch Tàu du lịch</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Cuba\">Du lịch Cuba</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Brazil\">Du lịch Brazil</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Argentina\">Du lịch Argentina</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Chile\">Du lịch Chile</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Maldives\">Du lịch Maldives</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Kazakhstan\">Du lịch Kazakhstan</a></li>\r\n			<li>\r\n				<a href=\"#\" title=\"Du lịch Mauritius\">Du lịch Mauritius</a></li>\r\n		</ul>\r\n	</div>\r\n</div>\r\n<p>\r\n	&nbsp;</p>', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n', '', 'maixinh123', '', 'maionline115@gmail.com', null, null);

-- ----------------------------
-- Table structure for configuration_about
-- ----------------------------
DROP TABLE IF EXISTS `configuration_about`;
CREATE TABLE `configuration_about` (
  `coa_id` int(11) NOT NULL,
  `coa_picture_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_picture_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_picture_2_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_picture_2_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_picture_2_2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_picture_2_2_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_2_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_2_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_2_2` text CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_2_2_en` text CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_2_3` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_2_3_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_picture_3_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_picture_3_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_picture_3_2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_picture_3_2_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_3_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_3_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_3_2` text CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_3_2_en` text CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_3_3` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_3_3_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_picture_4_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_picture_4_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_picture_4_2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_picture_4_2_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_4_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_4_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_4_2` text CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_4_2_en` text CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_4_3` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coa_text_4_3_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `staff_id` int(11) DEFAULT 0,
  `coa_update_at` int(11) DEFAULT 0,
  PRIMARY KEY (`coa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of configuration_about
-- ----------------------------
INSERT INTO `configuration_about` VALUES ('1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'sdfs f', 's sfsfs', '0', '0');

-- ----------------------------
-- Table structure for configuration_certification
-- ----------------------------
DROP TABLE IF EXISTS `configuration_certification`;
CREATE TABLE `configuration_certification` (
  `coc_id` int(11) NOT NULL,
  `coc_picture_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_picture_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_picture_2_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_picture_2_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_picture_2_2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_picture_2_2_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_2_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_2_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_2_2` text CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_2_2_en` text CHARACTER SET utf8 DEFAULT NULL,
  `coc_picture_3_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_picture_3_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_picture_3_2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_picture_3_2_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_3_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_3_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_3_2` text CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_3_2_en` text CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_4` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_4_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_5_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_5_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_5_2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_5_2_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_6_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_6_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_6_2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_6_2_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_6_3` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_6_3_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_6_4` text CHARACTER SET utf8 DEFAULT NULL,
  `coc_text_6_4_en` text CHARACTER SET utf8 DEFAULT NULL,
  `staff_id` int(11) DEFAULT 0,
  `coc_update_at` int(11) DEFAULT 0,
  PRIMARY KEY (`coc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of configuration_certification
-- ----------------------------

-- ----------------------------
-- Table structure for configuration_introduce
-- ----------------------------
DROP TABLE IF EXISTS `configuration_introduce`;
CREATE TABLE `configuration_introduce` (
  `coi_id` int(11) NOT NULL,
  `coi_picture_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coi_picture_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coi_text_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coi_text_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coi_picture_2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coi_picture_2_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coi_text_2_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coi_text_2_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coi_text_2_2` text CHARACTER SET utf8 DEFAULT NULL,
  `coi_text_2_2_en` text CHARACTER SET utf8 DEFAULT NULL,
  `coi_picture_3` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coi_picture_3_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coi_text_3_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coi_text_3_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coi_text_3_2` text CHARACTER SET utf8 DEFAULT NULL,
  `coi_text_3_2_en` text CHARACTER SET utf8 DEFAULT NULL,
  `coi_picture_4` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coi_picture_4_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coi_text_4_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coi_text_4_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `coi_text_4_2` text CHARACTER SET utf8 DEFAULT NULL,
  `coi_text_4_2_en` text CHARACTER SET utf8 DEFAULT NULL,
  `staff_id` int(11) DEFAULT 0,
  `coi_update_at` int(11) DEFAULT 0,
  PRIMARY KEY (`coi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of configuration_introduce
-- ----------------------------
INSERT INTO `configuration_introduce` VALUES ('1', 'lwwcy1597478444.jpeg', 'cjanm1597478449.jpeg', 'sfsf', 's fsfs', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0');

-- ----------------------------
-- Table structure for configuration_member
-- ----------------------------
DROP TABLE IF EXISTS `configuration_member`;
CREATE TABLE `configuration_member` (
  `com_id` int(11) NOT NULL,
  `com_picture_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `com_picture_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `com_text_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `com_text_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `com_picture_2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `com_picture_2_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `com_text_2_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `com_text_2_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `com_text_2_2` text CHARACTER SET utf8 DEFAULT NULL,
  `com_text_2_2_en` text CHARACTER SET utf8 DEFAULT NULL,
  `com_picture_3` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `com_picture_3_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `com_text_3_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `com_text_3_1_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `com_text_3_2` text CHARACTER SET utf8 DEFAULT NULL,
  `com_text_3_2_en` text CHARACTER SET utf8 DEFAULT NULL,
  `com_text_4` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `com_text_4_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `com_text_5` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `com_text_5_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `staff_id` int(11) DEFAULT 0,
  `com_update_at` int(11) DEFAULT 0,
  PRIMARY KEY (`com_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of configuration_member
-- ----------------------------
INSERT INTO `configuration_member` VALUES ('1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0', '0');

-- ----------------------------
-- Table structure for enterprice_network
-- ----------------------------
DROP TABLE IF EXISTS `enterprice_network`;
CREATE TABLE `enterprice_network` (
  `enn_id` int(11) NOT NULL AUTO_INCREMENT,
  `enn_name` varchar(255) DEFAULT NULL,
  `enn_name_en` varchar(255) DEFAULT NULL,
  `enn_description` text DEFAULT NULL,
  `enn_description_en` text DEFAULT NULL,
  `enn_picture` varchar(255) DEFAULT NULL,
  `enn_link` text DEFAULT NULL,
  `enn_active` tinyint(4) NOT NULL DEFAULT 0,
  `enn_order` int(11) NOT NULL DEFAULT 0,
  `staff_id` int(11) NOT NULL DEFAULT 0,
  `enn_create_time` int(11) NOT NULL DEFAULT 0,
  `enn_update_at` int(11) DEFAULT 0,
  PRIMARY KEY (`enn_id`),
  KEY `enn_order` (`enn_order`) USING BTREE,
  KEY `enn_active` (`enn_active`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprice_network
-- ----------------------------

-- ----------------------------
-- Table structure for events
-- ----------------------------
DROP TABLE IF EXISTS `events`;
CREATE TABLE `events` (
  `eve_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(1) DEFAULT 1,
  `eve_category_id` int(11) DEFAULT NULL,
  `eve_article_name` varchar(255) DEFAULT NULL,
  `eve_article_id` int(11) DEFAULT NULL,
  `eve_title` varchar(255) DEFAULT NULL,
  `eve_title_en` varchar(255) DEFAULT '',
  `eve_search` varchar(255) DEFAULT '',
  `eve_alias` varchar(255) DEFAULT NULL,
  `eve_picture` varchar(100) DEFAULT NULL,
  `eve_date` int(11) DEFAULT 0,
  `eve_create_time` int(11) DEFAULT 0,
  `eve_update_at` int(11) DEFAULT 0,
  `eve_hits` int(11) DEFAULT 0,
  `eve_active` int(1) DEFAULT 1,
  `eve_hot` int(1) DEFAULT 0,
  `eve_new` int(1) DEFAULT 0,
  `eve_vip` int(1) DEFAULT 0,
  `eve_teaser` text DEFAULT NULL,
  `eve_teaser_en` text DEFAULT NULL,
  `eve_link_video` text DEFAULT NULL,
  `eve_description` text DEFAULT NULL,
  `eve_description_en` text DEFAULT NULL,
  `eve_location` varchar(255) DEFAULT NULL,
  `eve_order` int(11) DEFAULT 0,
  `eve_rewrite` varchar(255) DEFAULT NULL,
  `eve_md5` varchar(255) DEFAULT NULL,
  `eve_consult` int(11) DEFAULT 0,
  `eve_source` varchar(255) DEFAULT NULL,
  `eve_meta_description` text DEFAULT NULL,
  `eve_meta_description_en` text DEFAULT NULL,
  `eve_meta_keyword` varchar(255) DEFAULT NULL,
  `eve_meta_keyword_en` varchar(255) DEFAULT '',
  `eve_meta_title` varchar(255) DEFAULT NULL,
  `eve_meta_title_en` varchar(255) DEFAULT '',
  PRIMARY KEY (`eve_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of events
-- ----------------------------
INSERT INTO `events` VALUES ('1', '1', '3', null, null, '70 Diamond 1', '70 Diamond 1 en', '70 diamond 1', null, 'hqzqb1596702719.jpeg', '0', '1596702746', '1596702746', '0', '1', '0', '0', '0', 'sf sf', 's fsf s', null, '<p>\r\n	fsf s fs</p>', '<p>\r\n	&nbsp;fsfs</p>', null, '120', '70-diamond-1', null, '0', null, 'Tiêu đề ngắn tiếng việt 2', 'Tiêu đề ngắn tiếng việt 3', 'Tiêu đề ngắn tiếng việt0', 'Tiêu đề ngắn tiếng việt 1', 'Phùng Văn Quyêt', 'Tiêu đề ngắn tiếng việt');

-- ----------------------------
-- Table structure for feels
-- ----------------------------
DROP TABLE IF EXISTS `feels`;
CREATE TABLE `feels` (
  `fee_id` int(11) NOT NULL AUTO_INCREMENT,
  `fee_name` varchar(255) DEFAULT NULL,
  `fee_name_en` varchar(255) DEFAULT NULL,
  `fee_description` text DEFAULT NULL,
  `fee_description_en` text DEFAULT NULL,
  `fee_picture` varchar(255) DEFAULT NULL,
  `fee_link` text DEFAULT NULL,
  `fee_active` tinyint(4) DEFAULT 0,
  `fee_order` int(11) DEFAULT 0,
  `staff_id` int(11) DEFAULT 0,
  `fee_create_time` int(11) DEFAULT 0,
  `fee_update_at` int(11) DEFAULT 0,
  PRIMARY KEY (`fee_id`),
  KEY `fee_order` (`fee_order`) USING BTREE,
  KEY `fee_active` (`fee_active`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of feels
-- ----------------------------

-- ----------------------------
-- Table structure for founder
-- ----------------------------
DROP TABLE IF EXISTS `founder`;
CREATE TABLE `founder` (
  `fou_id` int(11) NOT NULL AUTO_INCREMENT,
  `fou_name` varchar(255) DEFAULT NULL,
  `fou_name_en` varchar(255) DEFAULT NULL,
  `fou_description` text DEFAULT NULL,
  `fou_description_en` text DEFAULT NULL,
  `fou_chucvu` varchar(255) DEFAULT NULL,
  `fou_chucvu_en` varchar(255) DEFAULT NULL,
  `fou_picture` varchar(255) DEFAULT NULL,
  `fou_link` text DEFAULT NULL,
  `fou_active` tinyint(4) NOT NULL DEFAULT 0,
  `fou_order` int(11) NOT NULL DEFAULT 0,
  `staff_id` int(11) NOT NULL DEFAULT 0,
  `fou_create_time` int(11) NOT NULL DEFAULT 0,
  `fou_update_at` int(11) DEFAULT 0,
  PRIMARY KEY (`fou_id`),
  KEY `fou_order` (`fou_order`) USING BTREE,
  KEY `fou_active` (`fou_active`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of founder
-- ----------------------------
INSERT INTO `founder` VALUES ('1', 'Phùng Văn Quyêt', 'Phùng Văn Quyêt en', 'Phùng Văn Quyêt', 'Phùng Văn Quyêt', 'Phùng Văn Quyêt', 'Phùng Văn Quyêt', 'yusdy1596709367.jpeg', 'Phùng Văn Quyêt', '1', '12', '0', '1596709369', '1596709497');

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `mnu_id` int(11) NOT NULL AUTO_INCREMENT,
  `mnu_name` varchar(255) DEFAULT NULL,
  `mnu_name_en` varchar(255) DEFAULT '',
  `mnu_link` text DEFAULT NULL,
  `mnu_target` varchar(10) DEFAULT '_self',
  `mnu_type` tinyint(3) DEFAULT 1,
  `mnu_order` double DEFAULT 0,
  `mnu_parent_id` int(11) DEFAULT 0,
  `mnu_position` int(11) DEFAULT 0,
  `mnu_has_child` int(1) DEFAULT 0,
  `mnu_all_child` varchar(255) DEFAULT NULL,
  `mnu_defined` varchar(255) DEFAULT NULL,
  `mnu_active` tinyint(1) DEFAULT 1,
  `lang_id` tinyint(1) DEFAULT 1,
  `mnu_picture` varchar(255) NOT NULL DEFAULT '',
  `staff_id` int(11) NOT NULL DEFAULT 0,
  `mnu_create_time` int(11) NOT NULL DEFAULT 0,
  `mnu_update_at` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`mnu_id`),
  KEY `mnu_order` (`mnu_order`),
  KEY `mnu_parent_id` (`mnu_parent_id`),
  KEY `mnu_has_child` (`mnu_has_child`),
  KEY `mnu_active` (`mnu_active`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('11', 'Phùng Văn Quyêt', 'Phùng Văn Quyêt en', 'dantri.com', '_self', '1', '12', '0', '1', '0', null, null, '1', '1', '', '0', '0', '0');

-- ----------------------------
-- Table structure for news_multi
-- ----------------------------
DROP TABLE IF EXISTS `news_multi`;
CREATE TABLE `news_multi` (
  `new_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(1) DEFAULT 1,
  `new_category_id` int(11) DEFAULT NULL,
  `new_article_name` varchar(255) DEFAULT NULL,
  `new_article_id` int(11) DEFAULT NULL,
  `new_title` varchar(255) DEFAULT NULL,
  `new_title_en` varchar(255) DEFAULT '',
  `new_search` varchar(255) DEFAULT '',
  `new_alias` varchar(255) DEFAULT NULL,
  `new_picture` varchar(100) DEFAULT NULL,
  `new_date` int(11) DEFAULT 0,
  `new_create_time` int(11) DEFAULT 0,
  `new_update_at` int(11) DEFAULT 0,
  `new_hits` int(11) DEFAULT 0,
  `new_active` int(1) DEFAULT 1,
  `new_hot` int(1) DEFAULT 0,
  `new_new` int(1) DEFAULT 0,
  `new_vip` int(1) DEFAULT 0,
  `new_teaser` text DEFAULT NULL,
  `new_teaser_en` text DEFAULT NULL,
  `new_link_video` text DEFAULT NULL,
  `new_description` text DEFAULT NULL,
  `new_description_en` text DEFAULT NULL,
  `new_location` varchar(255) DEFAULT NULL,
  `new_order` int(11) DEFAULT 0,
  `new_rewrite` varchar(255) DEFAULT NULL,
  `new_md5` varchar(255) DEFAULT NULL,
  `new_consult` int(11) DEFAULT 0,
  `new_source` varchar(255) DEFAULT NULL,
  `new_meta_description` text DEFAULT NULL,
  `new_meta_description_en` text DEFAULT NULL,
  `new_meta_keyword` varchar(255) DEFAULT NULL,
  `new_meta_keyword_en` varchar(255) DEFAULT '',
  `new_meta_title` varchar(255) DEFAULT NULL,
  `new_meta_title_en` varchar(255) DEFAULT '',
  PRIMARY KEY (`new_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news_multi
-- ----------------------------
INSERT INTO `news_multi` VALUES ('5', '1', '2', null, null, 'Tiêu đề tiếng việt search', 'Tiêu đề tiếng anh', 'tieu de tieng viet search', null, 'dvxyu1596534689.jpeg', '0', '1596534697', '1596595733', '0', '1', '0', '0', '1', 'Tiêu đề ngắn tiếng việt', 'Tiêu đề ngắn tiếng anh', null, '<p>\r\n	<img alt=\"\" src=\"/storage/upload_images/files/0c7aff289bc666983fd7.jpg\" style=\"width: 1920px; height: 2560px;\" />Ti&ecirc;u đề ngắn tiếng việt d sdfs&nbsp;</p>', '<p>\r\n	Ti&ecirc;u đề ngắn tiếng anh s s</p>', null, '12', 'tieu-de-tieng-viet-search', null, '0', null, 'Tiêu đề ngắn tiếng việt', 'Tiêu đề ngắn tiếng việt', 'Tiêu đề ngắn tiếng việt', 'Tiêu đề ngắn tiếng việt', 'Tiêu đề ngắn tiếng việt', 'Tiêu đề ngắn tiếng việt');
INSERT INTO `news_multi` VALUES ('6', '1', '2', null, null, '70 Diamond', 'Tiêu đề tiếng anh 70 Diamond', '70 diamond', null, 'mwybo1596595695.jpeg', '0', '1596595697', '1596595734', '0', '1', '0', '0', '1', '', '', null, '<p>\r\n	<img alt=\"\" src=\"/storage/upload_images/files/0c7aff289bc666983fd7.jpg\" style=\"width: 1920px; height: 2560px;\" /></p>', '', null, '0', '70-diamond', null, '0', null, '', '', '', '', '', '');

-- ----------------------------
-- Table structure for news_tag
-- ----------------------------
DROP TABLE IF EXISTS `news_tag`;
CREATE TABLE `news_tag` (
  `nt_news_id` int(11) NOT NULL,
  `nt_tag_id` int(11) NOT NULL,
  `nt_category_id` int(11) DEFAULT NULL,
  `nt_order` tinyint(3) DEFAULT 0,
  `nt_date` int(11) DEFAULT 0,
  PRIMARY KEY (`nt_news_id`,`nt_tag_id`),
  KEY `nt_tag_id` (`nt_tag_id`),
  KEY `nt_category_id` (`nt_category_id`),
  KEY `nt_order` (`nt_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news_tag
-- ----------------------------

-- ----------------------------
-- Table structure for partner
-- ----------------------------
DROP TABLE IF EXISTS `partner`;
CREATE TABLE `partner` (
  `par_id` int(11) NOT NULL AUTO_INCREMENT,
  `par_name` varchar(255) DEFAULT NULL,
  `par_name_en` varchar(255) DEFAULT NULL,
  `par_picture` varchar(255) DEFAULT NULL,
  `par_link` text DEFAULT NULL,
  `par_date` int(11) DEFAULT 0,
  `par_active` tinyint(4) NOT NULL DEFAULT 0,
  `par_order` int(11) NOT NULL DEFAULT 0,
  `staff_id` int(11) NOT NULL DEFAULT 0,
  `par_create_time` int(11) NOT NULL DEFAULT 0,
  `par_update_at` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`par_id`),
  KEY `par_order` (`par_order`) USING BTREE,
  KEY `par_active` (`par_active`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of partner
-- ----------------------------
INSERT INTO `partner` VALUES ('1', 'Phùng Văn Quyêt', 'Phùng Văn Quyêt en', 'wzroo1596699987.jpeg', 'dantri.com', '0', '1', '122', '0', '1596700327', '1596700327');

-- ----------------------------
-- Table structure for statics
-- ----------------------------
DROP TABLE IF EXISTS `statics`;
CREATE TABLE `statics` (
  `sta_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sta_category_id` int(11) DEFAULT 0,
  `sta_title` varchar(255) DEFAULT NULL,
  `sta_order` double DEFAULT 0,
  `sta_description` text DEFAULT NULL,
  `sta_date` int(11) DEFAULT 0,
  `lang_id` tinyint(1) DEFAULT 1,
  `sta_new` tinyint(1) NOT NULL DEFAULT 0,
  `sta_active` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`sta_id`),
  KEY `sta_category_id` (`sta_category_id`),
  KEY `sta_order` (`sta_order`),
  KEY `sta_date` (`sta_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of statics
-- ----------------------------

-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(255) DEFAULT NULL,
  `tag_name_rewrite` varchar(255) DEFAULT NULL,
  `tag_date` int(11) DEFAULT 0,
  PRIMARY KEY (`tag_id`),
  KEY `tag_name_rewrite` (`tag_name_rewrite`),
  KEY `tag_date` (`tag_date`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tags
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `use_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id cua vatgia.com',
  `use_active` int(11) DEFAULT 0,
  `use_loginname` varchar(100) DEFAULT NULL,
  `use_password` varchar(50) DEFAULT NULL,
  `use_birthdays` varchar(10) DEFAULT NULL,
  `use_gender` tinyint(2) DEFAULT 0,
  `use_city` int(11) DEFAULT 1,
  `use_phone` varchar(20) DEFAULT NULL,
  `use_email` varchar(100) DEFAULT NULL,
  `use_address` varchar(255) DEFAULT NULL,
  `use_date` int(11) DEFAULT 0,
  `use_group` int(11) DEFAULT 0,
  `use_name` varchar(255) DEFAULT NULL,
  `use_mobile` varchar(255) DEFAULT NULL,
  `use_avatar` varchar(255) DEFAULT '',
  `use_access_token` varchar(255) DEFAULT NULL,
  `use_security` varchar(255) DEFAULT NULL,
  `use_updated_at` int(11) DEFAULT 0,
  `use_reset_token` varchar(255) DEFAULT NULL,
  `use_career` varchar(255) DEFAULT '' COMMENT 'Ngành nghề',
  `use_company` varchar(255) DEFAULT '' COMMENT 'Công ty',
  `use_hobby` varchar(255) DEFAULT '' COMMENT 'Sở thích',
  `use_parent_id` int(11) DEFAULT NULL COMMENT 'ID người quản lý',
  `use_type` int(11) DEFAULT 0 COMMENT 'Loại user 0: User thuong, 1: NCC, 2: Dai ly',
  `use_department` int(11) DEFAULT 0,
  `use_created_time` int(11) DEFAULT 0,
  PRIMARY KEY (`use_id`),
  KEY `index_email` (`use_email`)
) ENGINE=MyISAM AUTO_INCREMENT=5754035 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '1', 'farmer289', '54781acb0b4ceb20ca0544ece420980f', null, '0', '1', null, null, null, '0', '0', 'Phùng Văn Quyết', null, '', null, '758543', null, null, '', '', '', null, '0', '0', '1587961461');
INSERT INTO `users` VALUES ('5754034', '1', 'hoanghien', '4789f288fb5c8b520041ff646f1ae529', null, '0', '1', '0963010997', null, null, '0', '0', 'Hoàng Thu Hiền', null, '', null, '147052', '0', null, '', '', '', null, '0', '0', '1593067323');

-- ----------------------------
-- Table structure for user_bill_code
-- ----------------------------
DROP TABLE IF EXISTS `user_bill_code`;
CREATE TABLE `user_bill_code` (
  `ubc_id` int(11) NOT NULL AUTO_INCREMENT,
  `ubc_user_id` int(11) DEFAULT 0,
  `ubc_location_id` int(11) DEFAULT 0,
  `ubc_bill_code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ubc_product_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ubc_quantity` int(11) DEFAULT 0,
  `ubc_unit` varchar(100) CHARACTER SET utf8 DEFAULT '',
  `ubc_picture` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ubc_picture_json` text CHARACTER SET utf8 DEFAULT NULL,
  `ubc_status` tinyint(4) DEFAULT 0,
  `ubc_is_delete` tinyint(4) DEFAULT 0,
  `ubc_status_date` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `ubc_user_note` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `ubc_staff_note` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `ubc_staff_id` int(11) DEFAULT 0,
  `ubc_return_code_id` int(11) DEFAULT 0,
  `ubc_check_code_id` int(11) DEFAULT 0,
  `ubc_return_date` int(11) DEFAULT 0,
  `ubc_created_at` int(11) DEFAULT 0,
  `ubc_updated_at` int(11) DEFAULT 0,
  PRIMARY KEY (`ubc_id`),
  UNIQUE KEY `ubc_bill_code` (`ubc_bill_code`) USING BTREE,
  KEY `ubc_user_id` (`ubc_user_id`) USING BTREE,
  KEY `ubc_status` (`ubc_status`) USING BTREE,
  KEY `ubc_created_time` (`ubc_created_at`) USING BTREE,
  KEY `ubc_update_at` (`ubc_updated_at`) USING BTREE,
  KEY `ubc_return_code_id` (`ubc_return_code_id`) USING BTREE,
  KEY `ubc_location_id` (`ubc_location_id`) USING BTREE,
  KEY `ubc_is_delete` (`ubc_is_delete`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of user_bill_code
-- ----------------------------
INSERT INTO `user_bill_code` VALUES ('1', '5754034', '0', 'abcfdfdfd', 'Chăm sóc đít', '0', '', 'lsixn1593077821.jpeg', null, '0', '1', '', 'dsfsfs', '', '0', '0', '0', '0', '0', '0');
INSERT INTO `user_bill_code` VALUES ('2', '5754034', '0', 'abcfdfdfd1', 'Chăm sóc đít', '0', '', '', null, '0', '1', '', 'fsfs', '', '0', '0', '0', '0', '1593078685', '1593078685');
INSERT INTO `user_bill_code` VALUES ('3', '5754034', '0', 'abcfdfdfddfd', 'sfsfs', '0', '', '', null, '0', '1', '', '', '', '0', '0', '0', '0', '1593079203', '1593079203');
INSERT INTO `user_bill_code` VALUES ('4', '5754034', '0', 'abcfdfdfddfd1111', 'sfsfs', '0', '', '', null, '0', '1', '', '', '', '0', '0', '0', '0', '1593079230', '1593079230');
INSERT INTO `user_bill_code` VALUES ('5', '5754034', '0', 'abcfdfdfddfd111111', 'sfsfs', '0', '', '', null, '0', '1', '', '', '', '0', '0', '0', '0', '1593079249', '1593079249');
INSERT INTO `user_bill_code` VALUES ('6', '5754034', '0', 'abcfdfdfd11111111', 'Chăm sóc đít', '0', '', '', null, '0', '1', '', '', '', '0', '0', '0', '0', '1593079409', '1593079409');
INSERT INTO `user_bill_code` VALUES ('7', '5754034', '0', 'abcfdfdfd 12222', 'Chăm sóc đít d fdf d', '0', '', '', null, '0', '1', '', '', '', '0', '0', '0', '0', '1593079486', '1593079486');
INSERT INTO `user_bill_code` VALUES ('8', '5754034', '0', 'ABCDFEDGHDXC', 'Chăm sóc mông', '12', 'cái', '', null, '3', '0', '', '1111', '', null, '0', '0', '0', '1593095370', '1593251792');
INSERT INTO `user_bill_code` VALUES ('9', '5754034', '0', 'BDEDFD123456', 'Chăm sóc đít', '12', 'cái', 'mdqqw1593183142.png', null, '3', '0', '', '', '', null, '0', '0', '0', '1593096392', '1593251801');
