<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::name('auth_admin.')->prefix('auth_admin')->group(function () {
   	Route::any('/login', 'Auth\AdminAuthenController@doLogin')->name('login');
   // Route::any('/login', 'Auth\AdminAuthenController@doLogin')->name('do_login');
    Route::get('/logout', 'Auth\AdminAuthenController@doLogout')->name('do_logout');
});

Route::get('access-denied', function () {
	echo 'Access Denied !';
})->name('access_denied');
Route::get('/index', 'Page\GoogleAnalyticController@index');
Route::get('phpinfo', function () {
	echo phpinfo();
})->name('access_denied');

Route::name('admin.')->prefix('admin')->middleware('auth.admin')->group(function () {
	require_once 'admin.php';
});

Route::name('authen.')->prefix('authen')->group(function () {
   Route::any('/login', 'Auth\UserAuthenController@login')->name('login');
   Route::get('/logout', 'Auth\UserAuthenController@logout')->name('logout');
    Route::any('/register', 'UserController@register')->name('register');
});

Route::name('user.')->prefix('user')->middleware('auth.user')->group(function () {
	Route::name('profile.')->prefix('profile')->group(function () {
		Route::any('/changepass', 'UserController@changePass')->name('changepass');
	});
});
//Route::get('add-news', 'Page\PageController@addNews')->name('add-news');
Route::group([
        'middleware' => ['setLocale']
    ], function () {
        Route::get('/', 'Page\HomeController@index')->name('index');
        Route::get('/tin-tuc', 'Page\NewsController@index')->name('news');
        Route::get('/{cat_name_rewrite}/{id}', 'Page\NewsController@category')->name('cat-news')->where('id', '[0-9]+');
        Route::get('/tin-tuc/{new_rewrite}/{id}', 'Page\NewsController@detail')->name('news_detail');
        Route::get('/dich-vu', 'Page\ServiceController@index')->name('news');
        Route::get('/dich-vu/{ser_name_rewrite}/{id}', 'Page\ServiceController@detail')->name('news-detail');

        Route::get('/gioi-thieu', 'Page\AboutController@about')->name('about');
        Route::get('/cac-sang-lap-vien', 'Page\AboutController@founder')->name('founder');
        Route::get('/cac-mang-luoi-doanh-nghiep-vung', 'Page\AboutController@memberNetwork')->name('member-network');
        Route::get('/tham-gia-mang-luoi', 'Page\AboutController@joinNetwork')->name('join-network');
        Route::get('/doi-ngu', 'Page\AboutController@team')->name('team');
        Route::get('/doi-tac', 'Page\AboutController@partner')->name('partner');
        Route::get('/ghi-nhan-cua-khach-hang-doi-tac', 'Page\AboutController@comments')->name('comments');

        Route::get('/danh-gia-binh-dang-gioi', 'Page\ServiceController@gear')->name('gear');

        Route::get('/chung-chi-edge', 'Page\ServiceController@edge')->name('edge');
        Route::get('/tu-van-doanh-nghiep', 'Page\ServiceController@tuvan')->name('tu-van');
        Route::get('/khoa-dao-tao-doanh-nghiep', 'Page\ServiceController@course')->name('course');

        Route::get('/lien-he', 'Page\ContactController@contactPage')->name('contact-page');
        Route::get('/tim-kiem', 'Page\SearchController@search')->name('search');
        Route::get('/thu-vien', 'Page\AlbumController@index')->name('album');
        Route::get('/thu-vien/{ban_name_rewrite}/{id}', 'Page\AlbumController@albumDetail')->name('album-detail');

        Route::get('/thu-vien/videos', 'Page\AlbumController@videos')->name('videos');

        Route::get('/hoat-dong-su-kien/su-kien-sap-dien-ra', 'Page\EventController@upcomingEvent')->name('upcoming');
        Route::get('/hoat-dong-su-kien/su-kien-da-dien-ra', 'Page\EventController@previousEvent')->name('previous');
        Route::get('/hoat-dong-su-kien/chien-dich', 'Page\EventController@campaign')->name('campaigns');
        Route::get('/hoat-dong-su-kien/ban-cam-ket-vi-bdg-vn', 'Page\EventController@pledge')->name('pledge');

//        Route::get('/kien-thuc-tham-khao/', 'Page\StaticController@index')->name('hub-index');
        Route::get('/kien-thuc-tham-khao/{cat_name_rewrite}/{id}', 'Page\StaticController@category')->name('hub-cate');
        Route::get('/kien-thuc-tham-khao/{cat_name_rewrite}/{sta_name_rewrite}/{id}', 'Page\StaticController@detail')->name('hub-detail');

        Route::post('/contact', 'Page\ContactController@contact')->name('contact');
        Route::post('/contact-event', 'Page\ContactController@contactEvent')->name('contact-event');
        Route::post('/register-event', 'Page\ContactController@contactEventRegister')->name('register-event-form');
        Route::post('/contact-story', 'Page\ContactController@contactStory')->name('contact-story');
        Route::post('/contact-form', 'Page\ContactController@contactForm')->name('contact-form');

        Route::get('/dang-ky-tham-gia', 'Page\ContactController@joinRegister')->name('join-register');
        Route::get('/dang-ky-su-kien', 'Page\ContactController@registerEvent')->name('register-event');
        Route::get('/dang-ky-tai-bao-cao', 'Page\ContactController@registerReport')->name('register-report');

        Route::post('/contact-join', 'Page\ContactController@contactJoin')->name('contact-join');

        Route::get('/{lang?}', 'Page\PageController@lang')->name('lang');
    });

Route::name('services.')->prefix('services')->group(function () {
   Route::name('upload.')->prefix('upload')->group(function () {
		Route::post('/', 'UploadController@upload')->name('upload');
		Route::get('/get', 'UploadController@get')->name('get');
	});
});



