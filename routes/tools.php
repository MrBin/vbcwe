<?php

use Illuminate\Http\Request;

Route::get('/', 'Tool\DashboardController@index')->name('index');

Route::name('dashboard.')->prefix('dashboard')->group(function () {
	Route::any('/', 'Tool\DashboardController@index')->name('index');
});

Route::name('facebook.')->prefix('facebook')->group(function () {
	Route::any('/accesstoken', 'Tool\AccessTokenController@index')->name('accesstoken');
	Route::get('/user-info', 'Tool\AccessTokenController@info')->name('accesstoken_info');
});

Route::name('facebook.')->prefix('facebook')->middleware('auth.accesstoken')->group(function () {
	Route::any('/', 'Tool\AccessTokenController@accesstoken')->name('adaccount');
	Route::any('/adaccount', 'Tool\FacebookAdaccountController@index')->name('adaccount');
	Route::post('/get-adaccount', 'Tool\FacebookAdaccountController@get_adaccount')->name('getadaccount');
	Route::any('/pages', 'Tool\FacebookPageController@index')->name('pages');
	Route::post('/get-pages', 'Tool\FacebookPageController@get_page')->name('getpages');
	Route::any('/post', 'Tool\FacebookPostController@index')->name('post');
	Route::post('/get-post', 'Tool\FacebookPostController@get_post')->name('getpost');
	Route::post('/load-pages', 'Tool\FacebookPostController@load_pages')->name('loadpages');
	Route::get('/campaign', 'Tool\FacebookCampaignController@index')->name('campaign');
	Route::post('/get-campain', 'Tool\FacebookCampaignController@get_campaign')->name('getcampaign');
	Route::post('/load-adaccount', 'Tool\FacebookCampaignController@load_adaccount')->name('loadadaccount');
	Route::post('/load-adset', 'Tool\FacebookCampaignController@load_adset')->name('loadadset');
	Route::get('/adset/{campaign_id?}', 'Tool\FacebookAdsetController@index')->name('adset')->where('campaign_id', '[0-9]+');
	Route::post('/get-adset', 'Tool\FacebookAdsetController@get_adset')->name('getadset');
	Route::post('/get-ads', 'Tool\FacebookAdsController@get_ads')->name('getads');
	Route::post('/load-ads', 'Tool\FacebookAdsController@load_ads')->name('loadads');
	Route::get('/ads', 'Tool\FacebookAdsController@index')->name('ads');
	Route::post('/get-post-ads', 'Tool\FacebookCampaignController@get_post')->name('getpostads');
	Route::any('/ads-insight', 'Tool\FacebookAdsController@insight')->name('adsinsight');
	Route::any('/insight', 'Tool\FacebookInsightController@index')->name('insight');
	Route::any('/get-insight', 'Tool\FacebookInsightController@insights')->name('getinsight');
});
