<?php

Route::get('/', 'Admin\DashboardController@index')->name('dashboard');

Route::name('profile.')->prefix('profile')->group(function () {
	Route::any('/update', 'Admin\AdminProfileController@update')->name('update');
	Route::any('/changepass', 'Admin\AdminProfileController@changePass')->name('changepass');
});

Route::name('dashboard.')->prefix('dashboard')->group(function () {
	Route::any('/', 'Admin\DashboardController@index')->name('index');
});

Route::name('staff.')->prefix('staff')->group(function () {
	Route::get('/', 'Admin\AdminStaffController@index')->name('index');
	Route::any('/add/{id?}', 'Admin\AdminStaffController@add')->name('add')->where('id', '[0-9]+');
	Route::post('/status', 'Admin\AdminStaffController@status')->name('status');
	Route::any('/delete/{id}', 'Admin\AdminStaffController@delete')->name('delete')->where('id', '[0-9]+');
	Route::any('/fake_login/{id}', 'Admin\AdminStaffController@fake_login')->name('fake_login')->where('id', '[0-9]+');
});

Route::name('category.')->prefix('category')->group(function () {
	Route::get('/', 'Admin\AdminCategoryController@index')->name('list');
	Route::any('/add/{id?}', 'Admin\AdminCategoryController@add')->name('add')->where('id', '[0-9]+');
	Route::any('/status', 'Admin\AdminCategoryController@status')->name('status');
	Route::any('/delete', 'Admin\AdminCategoryController@delete')->name('delete');
});

Route::name('news.')->prefix('news')->group(function () {
	Route::get('/', 'Admin\AdminNewsController@index')->name('index');
	Route::any('/add/{id?}', 'Admin\AdminNewsController@add')->name('add')->where('id', '[0-9]+');
	Route::post('/status', 'Admin\AdminNewsController@status')->name('status');
	Route::get('/delete/{id}', 'Admin\AdminNewsController@delete')->name('delete');

	Route::any('/category', 'Admin\AdminNewsController@category')->name('category');
	Route::any('/category-add/{id?}', 'Admin\AdminNewsController@categoryAdd')->name('category_add')->where('id', '[0-9]+');
	Route::post('/category-status', 'Admin\AdminNewsController@categoryStatus')->name('category_status');
	Route::any('/tags', 'Admin\AdminNewsController@tags')->name('tags');
	Route::any('/tags-add', 'Admin\AdminNewsController@tags-add')->name('tags_add');
});

Route::name('static.')->prefix('static')->group(function () {
	Route::get('/', 'Admin\AdminStaticsController@index')->name('index');
	Route::any('/add/{id?}', 'Admin\AdminStaticsController@add')->name('add')->where('id', '[0-9]+');
	Route::post('/status', 'Admin\AdminStaticsController@status')->name('status');
	Route::get('/delete/{id}', 'Admin\AdminStaticsController@delete')->name('delete');
	Route::any('/category', 'Admin\AdminStaticsController@category')->name('category');
	Route::any('/category-add/{id?}', 'Admin\AdminStaticsController@categoryAdd')->name('category_add')->where('id', '[0-9]+');
	Route::post('/category-status', 'Admin\AdminStaticsController@categoryStatus')->name('category_status');
});

Route::name('event.')->prefix('event')->group(function () {
	Route::get('/', 'Admin\AdminEventsController@index')->name('index');
	Route::any('/add/{id?}', 'Admin\AdminEventsController@add')->name('add')->where('id', '[0-9]+');
	Route::post('/status', 'Admin\AdminEventsController@status')->name('status');
	Route::get('/delete/{id}', 'Admin\AdminEventsController@delete')->name('delete')->where('id', '[0-9]+');

	Route::any('/category', 'Admin\AdminEventsController@category')->name('category');
	Route::any('/category-add/{id?}', 'Admin\AdminEventsController@categoryAdd')->name('category_add')->where('id', '[0-9]+');
	Route::post('/category-status', 'Admin\AdminEventsController@categoryStatus')->name('category_status');
});

Route::name('view.')->prefix('view')->group(function () {
	Route::get('/menu', 'Admin\AdminMenuController@index')->name('menu');
	Route::any('/menu_add/{id?}', 'Admin\AdminMenuController@add')->name('menu_add')->where('id', '[0-9]+');
	Route::any('/menu_delete/{id?}', 'Admin\AdminmenuController@delete')->name('menu_delete')->where('id', '[0-9]+');
	Route::post('/menu-status', 'Admin\AdminMenuController@status')->name('menu_status');

	Route::get('/banner', 'Admin\AdminBannerController@index')->name('banner');
	Route::any('/banner_add/{id?}', 'Admin\AdminBannerController@add')->name('banner_add')->where('id', '[0-9]+');
	Route::any('/banner_delete/{id?}', 'Admin\AdminBannerController@delete')->name('banner_delete')->where('id', '[0-9]+');
	Route::post('/banner-status', 'Admin\AdminBannerController@status')->name('banner_status');

	Route::get('/partner', 'Admin\AdminPartnerController@index')->name('partner');
	Route::any('/partner_add/{id?}', 'Admin\AdminPartnerController@add')->name('partner_add')->where('id', '[0-9]+');
	Route::any('/partner_delete/{id?}', 'Admin\AdminPartnerController@delete')->name('partner_delete')->where('id', '[0-9]+');
	Route::post('/partner-status', 'Admin\AdminPartnerController@status')->name('partner_status');

	Route::get('/achievement', 'Admin\AdminAchievementController@index')->name('achievement');
	Route::any('/achievement_add/{id?}', 'Admin\AdminAchievementController@add')->name('achievement_add')->where('id', '[0-9]+');
	Route::any('/achievement_delete/{id?}', 'Admin\AdminAchievementController@delete')->name('achievement_delete')->where('id', '[0-9]+');
	Route::post('/achievement-status', 'Admin\AdminAchievementController@status')->name('achievement_status');

	Route::get('/service', 'Admin\AdminServiceController@index')->name('service');
	Route::any('/service_add/{id?}', 'Admin\AdminServiceController@add')->name('service_add')->where('id', '[0-9]+');
	Route::any('/service_delete/{id?}', 'Admin\AdminServiceController@delete')->name('service_delete')->where('id', '[0-9]+');
	Route::post('/service-status', 'Admin\AdminServiceController@status')->name('service_status');

	Route::get('/opinion', 'Admin\AdminOpinionController@index')->name('opinion');
	Route::any('/opinion_add/{id?}', 'Admin\AdminOpinionController@add')->name('opinion_add')->where('id', '[0-9]+');
	Route::any('/opinion_delete/{id?}', 'Admin\AdminOpinionController@delete')->name('opinion_delete')->where('id', '[0-9]+');
	Route::post('/opinion-status', 'Admin\AdminOpinionController@status')->name('opinion_status');
});

Route::name('other.')->prefix('other')->group(function () {
	Route::get('/founder', 'Admin\AdminFounderController@index')->name('founder');
	Route::any('/founder_add/{id?}', 'Admin\AdminFounderController@add')->name('founder_add')->where('id', '[0-9]+');
	Route::any('/founder_delete/{id?}', 'Admin\AdminfounderController@delete')->name('founder_delete')->where('id', '[0-9]+');
	Route::post('/founder_status', 'Admin\AdminFounderController@status')->name('founder_status');

	Route::get('/enterprice_network', 'Admin\AdminEnterpriceNetworkController@index')->name('enterprice_network');
	Route::any('/enterprice_network_add/{id?}', 'Admin\AdminEnterpriceNetworkController@add')->name('enterprice_network_add')->where('id', '[0-9]+');
	Route::any('/enterprice_network_delete/{id?}', 'Admin\AdminenterpriceController@delete')->name('enterprice_network_delete')->where('id', '[0-9]+');
	Route::post('/enterprice_network-status', 'Admin\AdminEnterpriceNetworkController@status')->name('enterprice_network_status');

	Route::get('/contact', 'Admin\AdminContactController@index')->name('contact');
	Route::any('/detail/{id?}', 'Admin\AdminContactController@detail')->name('detail')->where('id', '[0-9]+');
	Route::any('/contact_delete/{id?}', 'Admin\AdminContactController@delete')->name('contact_delete')->where('id', '[0-9]+');
	Route::post('/contact-status', 'Admin\AdminContactController@status')->name('contact_status');
});

Route::name('configs.')->prefix('configs')->group(function () {
	Route::any('/', 'Admin\AdminConfigurationController@index')->name('configuration');
	Route::any('/introduce', 'Admin\AdminConfigurationIntroduceController@index')->name('introduce');
	Route::any('/about', 'Admin\AdminConfigurationAboutController@index')->name('about');
	Route::any('/certification', 'Admin\AdminConfigurationCertificationController@index')->name('certification');
	Route::any('/gears', 'Admin\AdminConfigurationGearsController@index')->name('gears');
	Route::any('/member', 'Admin\AdminConfigurationMemberController@index')->name('member');
	Route::any('/user-translate', 'Admin\AdminUserTranslateController@index')->name('user_translate');
	Route::any('/user-translate/add', 'Admin\AdminUserTranslateController@add')->name('user_translate_add');

	Route::get('/step', 'Admin\AdminItemStepController@index')->name('step');
	Route::any('/step_add/{id?}', 'Admin\AdminItemStepController@add')->name('step_add')->where('id', '[0-9]+');
	Route::any('/step_delete/{id?}', 'Admin\AdminItemStepController@delete')->name('step_delete')->where('id', '[0-9]+');
	Route::post('/step-status', 'Admin\AdminItemStepController@status')->name('step_status');
});

