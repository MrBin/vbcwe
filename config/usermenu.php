<?php
return [
	'bill'	=> [
					'info' 	=> ['label' 	=> "Vận đơn", 'is_menu'	=> 1, 'check_pers'=> 1, 'prefix' => "bills", "icon" => "ion-ios-photos-outline"],
					'sub'	=> [
								'list'	=> ['is_menu' => 1, 'check_pers' => 1, 'label' => 'Danh sách vận đơn', 'route' => 'user.bills.list'],
								'create'	=> ['is_menu' => 1, 'check_pers' => 1, 'label' => 'Khai báo vận đơn', 'route' => 'user.bills.create'],
								]
					],
];
