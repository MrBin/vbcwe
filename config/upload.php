<?php

return [

	'category'		=> env('PATH_UPLOAD', 'images') . '/category/',
	'product'		=> env('PATH_UPLOAD', 'images') . '/product/',
	'banner'		=> env('PATH_UPLOAD', 'images') . '/banner/',
	'news'			=> env('PATH_UPLOAD', 'images') . '/news/',
	'menu'			=> env('PATH_UPLOAD', 'images') . '/menu/',
	'partner'		=> env('PATH_UPLOAD', 'images') . '/partner/',
	'configuration'	=> env('PATH_UPLOAD', 'images') . '/configuration/',
	'category'	=> env('PATH_UPLOAD', 'images') . '/category/',
];
