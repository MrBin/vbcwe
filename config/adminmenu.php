<?php
return [
	'dashboard' => [
					'info' 	=> ['label' 	=> "Dashboard", 'is_menu' => 0, 'check_pers'=> 0, 'prefix' => "dashboard", "icon" => "ion-ios-home-outline"],
					'sub' 	=> [
								'index'	=> ['key' => '', 'is_menu' => 0, 'check_pers' => 0, 'label' => 'Dashboard', 'route' => 'admin.dashboard.index']
								]
					],
	'staff' => [
					'info' 	=> ['label' 	=> "Nhân viên", 'is_menu' => 1, 'check_pers'=> 1, 'prefix' => "staff", "icon" => "ion-person"],
					'sub' 	=> [
								'index'	=> ['key' => '', 'is_menu' => 1, 'check_pers' => 1, 'label' => 'Danh sách', 'route' => 'admin.staff.index'],
								'add'	=> ['key' => '', 'is_menu' => 1, 'check_pers' => 1, 'label' => 'Thêm mới', 'route' => 'admin.staff.add']
								]
					],
	'news' => [
					'info' 	=> ['label' 	=> "Tin tức", 'is_menu' => 1, 'check_pers'=> 0, 'prefix' => "news", "icon" => "ion-ios-paper-outline"],
					'sub' 	=> [
								'index'			=> ['key' => '', 'is_menu' => 1, 'check_pers' => 1, 'label' => 'Danh sách', 'route' => 'admin.news.index'],
								'add'			=> ['key' => 'status', 'is_menu' => 1, 'check_pers' => 1, 'label' => 'Thêm mới', 'route' => 'admin.news.add'],
								'category'		=> ['key' => '', 'is_menu' => 1, 'check_pers' => 1, 'label' => 'Danh mục', 'route' => 'admin.news.category'],
								'category_add'	=> ['key' => 'status', 'is_menu' => 0, 'check_pers' => 1, 'label' => 'Thêm mới danh mục', 'route' => 'admin.news.category_add'],
								'tags'			=> ['key' => '', 'is_menu' => 0, 'check_pers' => 1, 'label' => 'Tags', 'route' => 'admin.news.tags'],
								'tags_add'		=> ['key' => '', 'is_menu' => 0, 'check_pers' => 1, 'label' => 'Thêm mới Tags', 'route' => 'admin.news.tags'],
								]
					],
	'static' => [
					'info' 	=> ['label' 	=> "Kiến thức tham khảo", 'is_menu' => 1, 'check_pers'=> 0, 'prefix' => "static", "icon" => "ion-ios-paper-outline"],
					'sub' 	=> [
								'index'			=> ['key' => '', 'is_menu' => 1, 'check_pers' => 1, 'label' => 'Danh sách', 'route' => 'admin.static.index'],
								'add'			=> ['key' => 'status', 'is_menu' => 1, 'check_pers' => 1, 'label' => 'Thêm mới', 'route' => 'admin.static.add'],
								'category'		=> ['key' => '', 'is_menu' => 1, 'check_pers' => 1, 'label' => 'Danh mục', 'route' => 'admin.static.category'],
								'category_add'	=> ['key' => 'status', 'is_menu' => 0, 'check_pers' => 1, 'label' => 'Thêm mới danh mục', 'route' => 'admin.static.category_add'],
								]
					],
	'view'	=> [
					'info' 	=> ['label' 	=> "Giao diện", 'is_menu'	=> 1, 'check_pers'=> 1, 'prefix' => "view", "icon" => "ion-ios-filing-outline"],
					'sub'	=> [
								'menu'			=> ['is_menu' => 1, 'keys' => '', 'check_pers' => 1, 'label' => 'Danh sách menu', 'route' => 'admin.view.menu'],
								'menu_add'		=> ['is_menu' => 0, 'keys' => 'status', 'check_pers' => 1, 'label' => 'Thêm mới menu', 'route' => 'admin.view.menu_add'],
								'menu_delete'	=> ['is_menu' => 0, 'keys' => '', 'check_pers' => 1, 'label' => 'Xóa menu', 'route' => 'admin.view.menu_delete'],
								'banner'		=> ['is_menu' => 1, 'keys' => '', 'check_pers' => 1, 'label' => 'Banner', 'route' => 'admin.view.banner'],
								'banner_add'	=> ['is_menu' => 0, 'keys' => 'status', 'check_pers' => 1, 'label' => 'Thêm mới Banner', 'route' => 'admin.view.banner_add'],
								'banner_delete'	=> ['is_menu' => 0, 'keys' => '', 'check_pers' => 1, 'label' => 'Xóa banner', 'route' => 'admin.view.banner_delete'],
								'partner'		=> ['is_menu' => 1, 'keys' => '', 'check_pers' => 1, 'label' => 'Đối tác', 'route' => 'admin.view.partner'],
								'partner_add'	=> ['is_menu' => 0, 'keys' => 'status', 'check_pers' => 1, 'label' => 'Thêm mới đối tác', 'route' => 'admin.view.partner_add'],
								'partner_delete'	=> ['is_menu' => 0, 'keys' => '', 'check_pers' => 1, 'label' => 'Xóa đối tác', 'route' => 'admin.view.partner_delete'],
								'achievement'		=> ['is_menu' => 1, 'keys' => '', 'check_pers' => 1, 'label' => 'Thành tích', 'route' => 'admin.view.achievement'],
								'achievement_add'	=> ['is_menu' => 0, 'keys' => 'status', 'check_pers' => 1, 'label' => 'Thêm mới thành tích', 'route' => 'admin.view.achievement_add'],
								'achievement_delete'	=> ['is_menu' => 0, 'keys' => '', 'check_pers' => 1, 'label' => 'Xóa thành tích', 'route' => 'admin.view.achievement_delete'],
								'service'		=> ['is_menu' => 1, 'keys' => '', 'check_pers' => 1, 'label' => 'Sản phẩm & Dịch vụ', 'route' => 'admin.view.service'],
								'service_add'	=> ['is_menu' => 0, 'keys' => 'status', 'check_pers' => 1, 'label' => 'Thêm mới sản phẩm & Dịch vụ', 'route' => 'admin.view.service_add'],
								'service_delete'	=> ['is_menu' => 0, 'keys' => '', 'check_pers' => 1, 'label' => 'Xóa sản phẩm & Dịch vụ', 'route' => 'admin.view.service_delete'],
								'opinion'		=> ['is_menu' => 1, 'keys' => '', 'check_pers' => 1, 'label' => 'Ý kiến đối tác', 'route' => 'admin.view.opinion'],
								'opinion_add'	=> ['is_menu' => 0, 'keys' => 'status', 'check_pers' => 1, 'label' => 'Thêm mới Ý kiến đối tác', 'route' => 'admin.view.opinion_add'],
								'opinion_delete'	=> ['is_menu' => 0, 'keys' => '', 'check_pers' => 1, 'label' => 'Xóa Ý kiến đối tác', 'route' => 'admin.view.opinion_delete'],
								]
					],
	'other'	=> [
					'info' 	=> ['label' 	=> "Khác", 'is_menu'	=> 1, 'check_pers'=> 1, 'prefix' => "other", "icon" => "ion-ios-photos-outline"],
					'sub'	=> [
								'founder'					=> ['is_menu' => 1, 'keys' => '', 'check_pers' => 1, 'label' => 'Danh sách nhà sáng lập', 'route' => 'admin.other.founder'],
								'founder_add'				=> ['is_menu' => 0, 'keys' => 'status', 'check_pers' => 1, 'label' => 'Thêm mới nhà sáng lập', 'route' => 'admin.other.founder_add'],
								'founder_delete'			=> ['is_menu' => 0, 'keys' => '', 'check_pers' => 1, 'label' => 'Xóa nhà sáng lập', 'route' => 'admin.view.founder_delete'],
								'enterprice_network'		=> ['is_menu' => 1, 'keys' => '', 'check_pers' => 1, 'label' => 'Danh sách mạng lưới doanh nghiệp vùng', 'route' => 'admin.other.enterprice_network'],
								'enterprice_network_add'	=> ['is_menu' => 0, 'keys' => 'status', 'check_pers' => 1, 'label' => 'Thêm mới mạng lưới doanh nghiệp vùng', 'route' => 'admin.other.enterprice_network_add'],
								'enterprice_network_delete'	=> ['is_menu' => 0, 'keys' => '', 'check_pers' => 1, 'label' => 'Xóa mạng lưới doanh nghiệp vùng', 'route' => 'admin.view.enterprice_network_delete'],
								'contact'		=> ['is_menu' => 1, 'keys' => '', 'check_pers' => 1, 'label' => 'Danh sách liên hệ', 'route' => 'admin.other.contact'],
								'contact_add'	=> ['is_menu' => 0, 'keys' => 'status', 'check_pers' => 1, 'label' => 'Thêm mới liên hệ', 'route' => 'admin.other.contact_add'],
								'contact_delete'	=> ['is_menu' => 0, 'keys' => '', 'check_pers' => 1, 'label' => 'Xóa liên hệ', 'route' => 'admin.view.contact_delete'],
								]
					],
	'event' => [
					'info' 	=> ['label' 	=> "Sự kiện", 'is_menu' => 1, 'check_pers'=> 0, 'prefix' => "event", "icon" => "ion-ios-clock-outline"],
					'sub' 	=> [
								'index'			=> ['key' => '', 'is_menu' => 1, 'check_pers' => 1, 'label' => 'Danh sách', 'route' => 'admin.event.index'],
								'add'			=> ['key' => 'status', 'is_menu' => 1, 'check_pers' => 1, 'label' => 'Thêm mới', 'route' => 'admin.event.add'],
								'delete'		=> ['is_menu' => 0, 'keys' => '', 'check_pers' => 1, 'label' => 'Xóa', 'route' => 'admin.event.delete'],
								'category'		=> ['key' => '', 'is_menu' => 0, 'check_pers' => 1, 'label' => 'Danh mục', 'route' => 'admin.event.category'],
								'category_add'	=> ['key' => 'status', 'is_menu' => 0, 'check_pers' => 1, 'label' => 'Thêm mới danh mục', 'route' => 'admin.event.category_add'],
								]
					],
	'configs' => [
					'info' 	=> ['label' 	=> "Cấu hình", 'is_menu' => 1, 'check_pers'=> 0, 'prefix' => "configs", "icon" => "ion-ios-gear-outline"],
					'sub' 	=> [
								'configuration'	=> ['key' => '', 'is_menu' => 1, 'check_pers' => 1, 'label' => 'Cấu hình chung', 'route' => 'admin.configs.configuration'],
								'introduce'		=> ['key' => '', 'is_menu' => 1, 'check_pers' => 1, 'label' => 'Cấu hình trang Introduce', 'route' => 'admin.configs.introduce'],
								'about'			=> ['key' => 'status', 'is_menu' => 1, 'check_pers' => 1, 'label' => 'Cấu hình trang About', 'route' => 'admin.configs.about'],
								'member'		=> ['key' => '', 'is_menu' => 1, 'check_pers' => 1, 'label' => 'Cấu hình trang thành viên', 'route' => 'admin.configs.member'],
								'certification'	=> ['key' => 'status', 'is_menu' => 1, 'check_pers' => 1, 'label' => 'Cấu hình trang EDGE', 'route' => 'admin.configs.certification'],
								'gears'			=> ['key' => 'status', 'is_menu' => 1, 'check_pers' => 1, 'label' => 'Cấu hình trang GEARS', 'route' => 'admin.configs.gears'],
								'user_translate'=> ['key' => 'edit', 'is_menu' => 1, 'check_pers' => 1, 'label' => 'Ngôn ngữ', 'route' => 'admin.configs.user_translate'],
								'step'			=> ['key' => '', 'is_menu' => 1, 'check_pers' => 1, 'label' => 'Cấu hình các bước', 'route' => 'admin.configs.step'],
								'step_add'		=> ['key' => 'step_status', 'is_menu' => 0, 'check_pers' => 1, 'label' => 'Thêm mới các bước', 'route' => 'admin.configs.step_add'],
								'delete'		=> ['is_menu' => 0, 'keys' => '', 'check_pers' => 1, 'label' => 'Xóa các bước', 'route' => 'admin.configs.step_add'],
								]
					],
	'user'	=> [
					'info' 	=> ['label' 	=> "Người dùng", 'is_menu'	=> 0, 'check_pers'=> 1, 'prefix' => "user", "icon" => "ion-ios-photos-outline"],
					'sub'	=> [
								'index'		=> ['is_menu' => 0, 'keys' => 'get-detail', 'check_pers' => 1, 'label' => 'Danh sách người dùng', 'route' => 'admin.user.index'],
								]
					],
];
