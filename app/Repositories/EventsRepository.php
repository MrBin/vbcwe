<?php
namespace App\Repositories;

use App\Models\Events as Model;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;

class EventsRepository extends Repository
{
    protected $model;
	const EVENT_COMINGSON = 0;
	const EVENT_CAMPAIGNS = 1;
	const EVENT_FINISH = 2;
	const EVENT_CAMKET = 3;

    public function __construct()
    {
        $this->model = new Model;
    }

    public function getAll($where = []){
    	$category 		= $this->model->where('eve_id', '>' , '0');
    	foreach ($where as $key => $value) {
    		$category = $category->where($value[0], $value[1], $value[2]);
    	}
    	$category		= $category->orderBy('eve_order', 'ASC')
									->orderBy('eve_id', 'ASC')
									->get();

		return $category;
    }

    public function addPrefix($data)
    {
        $dataStore = [];
        foreach ($data as $key => $value) {
            $dataStore[$this->model->prefix . $key] = $value;
        }
        return $dataStore;
    }

    public static function getStatusLabels(){
   		return [
   			self::EVENT_COMINGSON => 'Sắp diễn ra',
   			self::EVENT_FINISH => 'Đã diễn ra',
            self::EVENT_CAMPAIGNS => 'Chiến dịch',
            self::EVENT_CAMKET => 'Bản cam kết',
   		];
   	}

   	public static function getStatusLabel($type){
   		$allType = self::getStatusLabels();
   		return isset($allType[$type]) ? $allType[$type] : $type;
   	}

    public function getById($id){
        return $this->model->find($id);
    }

    public function delete($eventId){
		$dataReturn	= ['status' => false, 'msg' => ''];
		$banner		= $this->getById($eventId);
    	if($banner){
			$banner->delete();
			$dataReturn['status'] 	= true;
    	}else{
    		$dataReturn['msg'] 	= "Banner not found !";
    	}

    	return $dataReturn;
    }

    public function add($id, $data){

    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];

    	// Tạo validate
    	$this->inputFields 	= [
								'category_id'			=> ['integer'],
								'new'					=> ['integer'],
								'title'					=> ['string'],
								'title_en'				=> ['string'],
								'rewrite'				=> ['string'],
								'search'				=> ['string'],
								'meta_title'			=> ['string'],
								'meta_title_en'			=> ['string'],
								'meta_keyword'			=> ['string'],
								'meta_keyword_en'		=> ['string'],
								'meta_description'		=> ['string'],
								'meta_description_en'	=> ['string'],
								'teaser'				=> ['string'],
								'teaser_en'				=> ['string'],
								'description'			=> ['string'],
								'description_en'		=> ['string'],
								'picture'				=> ['string'],
								'order'					=> ['integer'],
								'active'				=> ['integer'],
								'vip'					=> ['integer'],
								'hot'					=> ['integer'],
								'create_time'			=> ['integer'],
								'update_at'				=> ['integer'],
								];

    	$rulesValidate 	= [
							'title'			=> 'required',
							'title_en'		=> 'required',
							'rewrite'		=> 'bail|required|unique:events,eve_rewrite,' . $id . ',eve_id',
							'active'		=> 'in:0,1',
							'vip'			=> 'in:0,1',
							'hot'			=> 'in:0,1'
							];

		if($id > 0){
			if(empty($data['category_id'])) unset($rulesValidate['category_id']);
			if(empty($data['title'])) unset($rulesValidate['title']);
			if(empty($data['title_en'])) unset($rulesValidate['title_en']);
			if(empty($data['rewrite'])) unset($rulesValidate['rewrite']);
			if(empty($data['meta_title'])) unset($rulesValidate['meta_title']);
			if(empty($data['meta_title_en'])) unset($rulesValidate['meta_title_en']);
			if(empty($data['meta_keyword'])) unset($rulesValidate['meta_keyword']);
			if(empty($data['meta_keyword_en'])) unset($rulesValidate['meta_keyword_en']);
			if(empty($data['meta_description'])) unset($rulesValidate['meta_description']);
			if(empty($data['meta_description_en'])) unset($rulesValidate['meta_description_en']);
			if(empty($data['teaser'])) unset($rulesValidate['teaser']);
			if(empty($data['teaser_en'])) unset($rulesValidate['teaser_en']);
			if(empty($data['description'])) unset($rulesValidate['description']);
			if(empty($data['description_en'])) unset($rulesValidate['description_en']);
			if(empty($data['picture'])) unset($rulesValidate['picture']);
			if(empty($data['order'])) unset($rulesValidate['order']);
			if(empty($data['active'])) unset($rulesValidate['active']);
			if(empty($data['hot'])) unset($rulesValidate['hot']);
			if(empty($data['vip'])) unset($rulesValidate['vip']);
		}else{
			$data['create_time'] = time();
		}

		$data['update_at'] = time();

		if(isset($data['title'])){
			$search 	= removeAccent($data['title']);
			$search 	= strtolower($search);
			$data['search'] = $search;
		}

		$this->data				= $data;
		$this->rulesValidate	= $rulesValidate;
    	$this->messagesValidate = [
									'rewrite.unique' => 'Tên rewrite đã tồn tại',
									'parent_id.exists' => 'Danh mục cha không tồn tại'
									];

		$return				= false;
		// Gọi validate
		$dataValidate 	= $this->validate();
		// Không có lỗi
		if($dataValidate){
			if($id > 0){
	    		$events = $this->getById($id);
	    		if(empty($events)){
	    			$dataReturn['errors'][] = 'Record not found';
	    		}else{
	    			$return = $events->update($this->addPrefix($dataValidate));
	    			if($return){
	    				$dataReturn['status'] = true;
		    			$dataReturn['data'] = $events;

		    		}else{
		    			$dataReturn['errors'][] = 'System error. Try again';
		    		}
	    		}
	    	}else{
				$events	= $this->model;
				$return	= $events->create($this->addPrefix($dataValidate));
	    		if(isset($return->eve_id) && $return->eve_id > 0){
					$dataReturn['status']	= true;
					$dataReturn['data']		= $return;
	    		}else{
	    			$dataReturn['errors'][] = 'System error. Try again';
	    		}
	    	}
		}else{
			$dataReturn['errors'] 	= $this->errors();
		}

		return $dataReturn;
    }
}
