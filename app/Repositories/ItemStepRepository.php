<?php
namespace App\Repositories;

use App\Models\ItemStep as Model;
use App\Repositories\Repository;

class ItemStepRepository extends Repository
{
    protected $model;
	const ITEM_STEP_JOIN_EDGE		= 1;
	const ITEM_STEP_EVALUATE_EDGE	= 2;
	const ITEM_STEP_LEVEL_EDGE		= 3;
	const ITEM_STEP_WHY_JOINE_EDGE	= 4;
	const ITEM_STEP_FIELD_GEARS		= 5;
	const ITEM_STEP_DATA_GEARS		= 6;
	const ITEM_STEP_FOLLOW_GEARS	= 7;
	const ITEM_STEP_EVALUATE_GEARS	= 8;
    public function __construct()
    {
        $this->model = new Model;
    }

    public function addPrefix($data)
    {
        $dataStore = [];
        foreach ($data as $key => $value) {
            $dataStore[$this->model->prefix . $key] = $value;
        }
        return $dataStore;
    }

    public static function getTypeLabels(){
   		return [
			self::ITEM_STEP_JOIN_EDGE => "Quy trình tham gia EDGE",
			self::ITEM_STEP_EVALUATE_EDGE => "Khung đánh giá EDGE",
			self::ITEM_STEP_LEVEL_EDGE => "3 cấp độ chứng chỉ EDGE",
			self::ITEM_STEP_WHY_JOINE_EDGE => "Tại sao nên tham gia EDGE",
			self::ITEM_STEP_FIELD_GEARS => "10 lĩnh vực trọng điểm GEARS",
			self::ITEM_STEP_DATA_GEARS => "Bảng dữ liệu GEARS",
			self::ITEM_STEP_FOLLOW_GEARS => "Quy trình đánh giá GEARS",
			self::ITEM_STEP_EVALUATE_GEARS => "Đánh giá GEARS trong 8 tuần",
   		];
   	}

   	public static function getTypeLabel($type){
   		$allType = self::getTypeLabels();
   		return isset($allType[$type]) ? $allType[$type] : '';
   	}

    public function getById($id){
        return $this->model->find($id);
    }

    public function delete($recordId){
		$dataReturn	= ['status' => false, 'msg' => ''];
		$banner		= $this->getById($recordId);
    	if($banner){
			$banner->delete();
			$dataReturn['status'] 	= true;
    	}else{
    		$dataReturn['msg'] 	= "Record not found !";
    	}

    	return $dataReturn;
    }

    public function add($id, $data){

    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];

    	// Tạo validate
    	$this->data = $data;
    	$this->inputFields 	= [
									'title'					=> ['string'],
									'title_en'				=> ['string'],
									'teaser'				=> ['string'],
									'teaser_en'				=> ['string'],
									'picture'				=> ['string'],
									'picture_en'			=> ['string'],
									'type'					=> ['integer'],
									'order'					=> ['integer'],
									'status'				=> ['integer'],
									'created_time'			=> ['integer'],
									'updated_at'			    => ['integer'],
								];

    	$rulesValidate 	= [
							'title'		=> 'required',
							'title_en'	=> 'required',
							'status'	=> 'in:0,1',
							];

		if($id > 0){
			if(empty($data['title'])) unset($rulesValidate['title']);
			if(empty($data['title_en'])) unset($rulesValidate['title_en']);

		}else{
			$data['created_time'] = time();
		}

		$data['update_at'] = time();

        $this->data				= $data;
		$this->rulesValidate 	= $rulesValidate;
    	$this->messagesValidate = [];

		$return				= false;
		// Gọi validate
		$dataValidate 	= $this->validate();

		// Không có lỗi
		if($dataValidate){
			if($id > 0){
	    		$product = $this->getById($id);
	    		if(empty($product)){
	    			$dataReturn['errors'][] = 'Record not found';
	    		}else{
	    			$return = $product->update($this->addPrefix($dataValidate));
	    			if($return){
	    				$dataReturn['status'] = true;
		    			$dataReturn['data'] = $product;
		    		}else{
		    			$dataReturn['errors'][] = 'System error. Try again';
		    		}
	    		}
	    	}else{
				$product	= $this->model;
				$return	= $product->create($this->addPrefix($dataValidate));
	    		if(isset($return->its_id) && $return->its_id > 0){
					$dataReturn['status']	= true;
					$dataReturn['data']		= $return;
	    		}else{
	    			$dataReturn['errors'][] = 'System error. Try again';
	    		}
	    	}
		}else{
			$dataReturn['errors'] 	= $this->errors();
		}

		return $dataReturn;
    }
}
