<?php
namespace App\Repositories;

use App\Models\Configuration as Model;
use App\Repositories\Repository;
use App\Helper\Upload;

class ConfigurationRepository extends Repository
{
   protected $model;

   public function __construct()
   {
      $this->model = new Model;
   }

   public function getPictureUrl($filename, $type = 'fullsize'){
   	$url 		= Upload::getUrlImage('configuration', $filename);
   	return $url;
   }

   public function addPrefix($data)
   {
		$dataStore = [];
		foreach ($data as $key => $value) {
			$dataStore[$this->model->prefix . $key] = $value;
		}
		return $dataStore;
   }

   public function getConfiguration(){
      return $this->model->find(1);
   }

   public function update($data){

   		$id = 1;
    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];

    	// Tạo validate
    	$this->inputFields 	= [
								'website'					=> ['string'],
								'website_en'				=> ['string'],
								'address'					=> ['string'],
								'address_en'				=> ['string'],
								'web_master_tool'			=> ['text'],
								'web_master_tool_en'		=> ['text'],
								'favicon'					=> ['string'],
								'favicon_en'				=> ['string'],
								'analytics'					=> ['string'],
								'analytics_en'				=> ['string'],
								'mail'						=> ['string'],
								'mail_en'					=> ['string'],
								'hotline_1'					=> ['string'],
								'hotline_1_en'				=> ['string'],
								'hotline_2'					=> ['string'],
								'hotline_2_en'				=> ['string'],
								'logo'						=> ['string'],
								'logo_en'					=> ['string'],
								'copyright'					=> ['string'],
								'copyright_en'				=> ['string'],
								'seo_keyword'				=> ['text'],
								'seo_keyword_en'			=> ['text'],
								'seo_title'					=> ['string'],
								'seo_title_en'				=> ['string'],
								'seo_description'			=> ['text'],
								'seo_description_en'		=> ['text'],
								'banner_search'				=> ['string'],
								'banner_search_en'			=> ['string'],
								'banner_thuvien'			=> ['string'],
								'banner_thuvien_en'			=> ['string'],
								'banner_default'			=> ['string'],
								'banner_default_en'			=> ['string'],
								'banner_event'				=> ['string'],
								'banner_event_en'			=> ['string'],
								'content_contact'			=> ['text'],
								'content_contact_en'		=> ['text'],
								'banner_contact'			=> ['string'],
								'banner_contact_en'			=> ['string'],
								'title_mangluoi'			=> ['string'],
								'title_mangluoi_en'			=> ['string'],
								'banner_mangluoi'			=> ['string'],
								'banner_mangluoi_en'		=> ['string'],
								'title_member'				=> ['string'],
								'title_member_en'			=> ['string'],
								'banner_member'				=> ['string'],
								'banner_member_en'			=> ['string'],
								'footer'					=> ['text'],
								'footer_en'					=> ['text'],
								'map_contact'				=> ['string'],
								'map_contact_en'			=> ['string'],
								'add_footer'				=> ['text'],
								'add_footer_en'				=> ['text'],
								'add_header'				=> ['text'],
								'add_header_en'				=> ['text'],
								'title_email'				=> ['string'],
								'title_email_en'			=> ['string'],
								'name_email'				=> ['string'],
								'name_email_en'				=> ['string'],
								'mail_receive_order'		=> ['string'],
								'mail_receive_order_en'		=> ['string'],
								'mail_send_order'			=> ['string'],
								'mail_send_order_en'		=> ['string'],
								'mail_pass_send_order'		=> ['string'],
								'mail_pass_send_order_en'	=> ['string'],
								'linkedin'					=> ['string'],
								'linkedin_en'				=> ['string'],
								'youtube'					=> ['string'],
								'youtube_en'				=> ['string'],
								'facebook'					=> ['string'],
								'facebook_en'				=> ['string'],
								'banner_thamgia_mangluoi'			=> ['string'],
								'banner_thamgia_mangluoi_en'		=> ['string'],
								'title_thamgia_mangluoi'			=> ['string'],
								'title_thamgia_mangluoi_en'			=> ['string'],
								'description_thamgia_mangluoi'		=> ['text'],
								'description_thamgia_mangluoi_en'	=> ['text'],
								'banner_kienthuc_thamkhao'			=> ['string'],
								'banner_kienthuc_thamkhao_en'		=> ['string'],
								'title_kienthuc_thamkhao'			=> ['string'],
								'title_kienthuc_thamkhao_en'		=> ['string'],
								'description_kienthuc_thamkhao'		=> ['text'],
								'description_kienthuc_thamkhao_en'	=> ['text'],
							];

		$rulesValidate				= [];
		$this->rulesValidate		= $rulesValidate;

		$return				= false;
		// Gọi validate
		$this->data = $data;
		$dataValidate 	= $this->validate();

		// Không có lỗi
		if($dataValidate){
    		$config = $this->getConfiguration();
    		if(empty($config)){
    			$dataReturn['errors'][] = 'Record not found';
    		}else{
    			$return = $config->update($this->addPrefix($dataValidate));
    			if($return){
    				$dataReturn['status'] = true;
	    			$dataReturn['data'] = $config;
	    		}else{
	    			$dataReturn['errors'][] = 'System error. Try again';
	    		}
    		}
		}else{
			$dataReturn['errors'] 	= $this->errors();
		}

		return $dataReturn;
   }
}
