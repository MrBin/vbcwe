<?php
namespace App\Repositories;

use App\Models\Service as Model;
use App\Repositories\Repository;

class ServiceRepository extends Repository
{
    protected $model;
    const SERVICE_CONSULTANCY = 1;
    const SERVICE_COURSE = 2;
    const SERVICE_ENTERPRISE = 3;
    public function __construct()
    {
        $this->model = new Model;
    }

    public function addPrefix($data)
    {
        $dataStore = [];
        foreach ($data as $key => $value) {
            $dataStore[$this->model->prefix . $key] = $value;
        }
        return $dataStore;
    }


    public function getById($id){
        return $this->model->find($id);
    }

    public static function getTypeLabels(){
   		return [
   			self::SERVICE_CONSULTANCY => 'Tư vấn doanh nghiệp',
            self::SERVICE_COURSE => 'Khóa học doanh nghiệp',
            self::SERVICE_ENTERPRISE => 'Sản phẩm dịch vụ',
   		];
   	}

   	public static function getTypeLabel($type){
   		$allType = self::getTypeLabels();
   		return isset($allType[$type]) ? $allType[$type] : $type;
   	}

    public function delete($recordId){
		$dataReturn	= ['status' => false, 'msg' => ''];
		$banner		= $this->getById($recordId);
    	if($banner){
			$banner->delete();
			$dataReturn['status'] 	= true;
    	}else{
    		$dataReturn['msg'] 	= "Record not found !";
    	}

    	return $dataReturn;
    }

    public function add($id, $data){

    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];

    	// Tạo validate
    	$this->data = $data;
    	$this->inputFields 	= [
									'title'					=> ['string'],
									'title_en'				=> ['string'],
									'name_rewrite'			=> ['string'],
									'teaser'				=> ['string'],
									'teaser_en'				=> ['string'],
									'category_id' 			=> ['integer'],
									'type' 					=> ['integer'],
									'show_home' 			=> ['integer'],
									'icon'					=> ['string'],
									'icon_en'				=> ['string'],
									'picture'				=> ['string'],
									'picture_en'			=> ['string'],
									'meta_title'			=> ['string'],
									'meta_title_en'			=> ['string'],
									'meta_keyword'			=> ['string'],
									'meta_keyword_en'		=> ['string'],
									'meta_description'		=> ['string'],
									'meta_description_en'	=> ['string'],
									'order'					=> ['integer'],
									'status'				=> ['integer'],
									'description'			=> ['text'],
									'description_en'		=> ['text'],
									'create_time'			=> ['integer'],
									'update_at'			    => ['integer'],
								];

    	$rulesValidate 	= [
							'title'		=> 'required',
							'title_en'	=> 'required',
							'name_rewrite'	=> 'bail|required|unique:service,ser_name_rewrite,' . $id . ',ser_id',
							'active'	=> 'in:0,1',
							];

		if($id > 0){
			if(empty($data['title'])) unset($rulesValidate['title']);
			if(empty($data['title_en'])) unset($rulesValidate['title_en']);
			if(empty($data['name_rewrite'])) unset($rulesValidate['name_rewrite']);

		}else{
			$data['create_time'] = time();
		}

		$data['update_at'] = time();

        $this->data				= $data;
		$this->rulesValidate 	= $rulesValidate;
    	$this->messagesValidate = [];

		$return				= false;
		// Gọi validate
		$dataValidate 	= $this->validate();

		// Không có lỗi
		if($dataValidate){
			if($id > 0){
	    		$product = $this->getById($id);
	    		if(empty($product)){
	    			$dataReturn['errors'][] = 'Record not found';
	    		}else{
	    			$return = $product->update($this->addPrefix($dataValidate));
	    			if($return){
	    				$dataReturn['status'] = true;
		    			$dataReturn['data'] = $product;
		    		}else{
		    			$dataReturn['errors'][] = 'System error. Try again';
		    		}
	    		}
	    	}else{
				$product	= $this->model;
				$return	= $product->create($this->addPrefix($dataValidate));
	    		if(isset($return->ser_id) && $return->ser_id > 0){
					$dataReturn['status']	= true;
					$dataReturn['data']		= $return;
	    		}else{
	    			$dataReturn['errors'][] = 'System error. Try again';
	    		}
	    	}
		}else{
			$dataReturn['errors'] 	= $this->errors();
		}

		return $dataReturn;
    }
}
