<?php
namespace App\Repositories;

use App\Models\ConfigurationAbout as Model;
use App\Repositories\Repository;
use App\Helper\Upload;

class ConfigurationAboutRepository extends Repository
{
   protected $model;

   public function __construct()
   {
      $this->model = new Model;
   }

   public function addPrefix($data)
   {
		$dataStore = [];
		foreach ($data as $key => $value) {
			$dataStore[$this->model->prefix . $key] = $value;
		}
		return $dataStore;
   }

   public function getConfiguration(){
      return $this->model->find(1);
   }

   	public function update($data){

   		$id = 1;
    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];

    	// Tạo validate
    	$this->inputFields 	= [
								'picture_1'			=> ['string'],
								'picture_1_en'		=> ['string'],
								'text_1'			=> ['string'],
								'text_1_en'			=> ['string'],
								'picture_2_1'		=> ['string'],
								'picture_2_1_en'	=> ['string'],
								'picture_2_2'		=> ['string'],
								'picture_2_2_en'	=> ['string'],
								'text_2_1'			=> ['string'],
								'text_2_1_en'		=> ['string'],
								'text_2_2'			=> ['text'],
								'text_2_2_en'		=> ['text'],
								'text_2_3'			=> ['string'],
								'text_2_3_en'		=> ['string'],
								'picture_3_1'		=> ['string'],
								'picture_3_1_en'	=> ['string'],
								'picture_3_2'		=> ['string'],
								'picture_3_2_en'	=> ['string'],
								'text_3_1'			=> ['string'],
								'text_3_1_en'		=> ['string'],
								'text_3_2'			=> ['string'],
								'text_3_2_en'		=> ['string'],
								'text_3_3'			=> ['text'],
								'text_3_3_en'		=> ['text'],
								'picture_4_1'		=> ['string'],
								'picture_4_1_en'	=> ['string'],
								'picture_4_2'		=> ['string'],
								'picture_4_2_en'	=> ['string'],
								'text_4_1'			=> ['string'],
								'text_4_1_en'		=> ['string'],
								'text_4_2'			=> ['text'],
								'text_4_2_en'		=> ['text'],
								'text_4_3'			=> ['string'],
								'text_4_3_en'		=> ['string'],
								];

		$rulesValidate				= [];
		$this->rulesValidate		= $rulesValidate;

		$return			= false;
		// Gọi validate
		$data['update_at'] =  time();
		$this->data = $data;
		$dataValidate 	= $this->validate();

		// Không có lỗi
		if($dataValidate){
    		$config = $this->getConfiguration();
    		if(empty($config)){
    			$dataReturn['errors'][] = 'Record not found';
    		}else{
    			$return = $config->update($this->addPrefix($dataValidate));
    			if($return){
    				$dataReturn['status'] = true;
	    			$dataReturn['data'] = $config;
	    		}else{
	    			$dataReturn['errors'][] = 'System error. Try again';
	    		}
    		}
		}else{
			$dataReturn['errors'] 	= $this->errors();
		}

		return $dataReturn;
    }
}
