<?php
namespace App\Repositories;

use App\Models\Banner as Model;
use App\Repositories\Repository;
use App\Helper\Upload;

class BannerRepository extends Repository
{
	public static $arrType		= [1 => "Banner Ảnh", 2 => "Banner Flash", 3 => "Banner HTML"];
	public static $arrPosition	= [1 => "Banner trang chủ",  2 => "Banner Footer", 3 => "Allbum ảnh", 4 => "Videos", 5 => 'Banner tin tức'];
	public static $arrTarget	= ["_blank"=> "Trang mới", "_self"	=> "Hiện hành",];

    protected $model;

    public function __construct()
    {
        $this->model = new Model;
    }

    public function addPrefix($data)
    {
        $dataStore = [];
        foreach ($data as $key => $value) {
            $dataStore[$this->model->prefix . $key] = $value;
        }
        return $dataStore;
    }

    public static function getPictureUrl($filename, $type = 'fullsize'){
	   	$url 		= Upload::getUrlImage('banner', $filename);
	   	return $url;
   	}

    public function getById($id){
        return $this->model->find($id);
    }

    public function delete($bannerId){
		$dataReturn	= ['status' => false, 'msg' => ''];
		$banner		= $this->getById($bannerId);
    	if($banner){
			$banner->delete();
			$dataReturn['status'] 	= true;
    	}else{
    		$dataReturn['msg'] 	= "Banner not found !";
    	}

    	return $dataReturn;
    }

    public function add($id, $data){

    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];

    	// Tạo validate
    	$this->inputFields 	= [
										'name'			=> ['string'],
                                        'name_en'		=> ['text'],
										'name_rewrite'	=> ['string'],
										'picture'		=> ['string'],
										'picture_json'	=> ['text'],
										'link'			=> ['string'],
										'description'	=> ['string'],
										'target'		=> ['string'],
										'type'			=> ['integer'],
										'position'		=> ['integer'],
										'active'		=> ['integer'],
										'order'			=> ['integer'],
										'create_time' 	=> ['integer'],
										'update_at'		=> ['integer'],
										'admin_id'		=> ['integer'],
								];

    	$rulesValidate 	= [
//							'name'			=> 'required',
//                            'name_en'		=> 'required',
							'picture'		=> 'bail|required|',
							'link'			=> 'bail|required|',
							'position' 		=> 'bail|required|in:' . implode(",", array_keys(self::$arrPosition)),
							'target' 		=> 'bail|required|in:' . implode(",", array_keys(self::$arrTarget)),
							'active'		=> 'in:0,1',
							];

		if($id > 0){
			if(empty($data['name'])) unset($rulesValidate['name']);
            if(empty($data['name_en'])) unset($rulesValidate['name_en']);
			if(empty($data['picture'])) unset($rulesValidate['picture']);
			if(empty($data['link'])) unset($rulesValidate['link']);
			if(empty($data['position'])) unset($rulesValidate['position']);
			if(empty($data['target'])) unset($rulesValidate['target']);
			if(empty($data['active'])) unset($rulesValidate['active']);
		}else{
			$data['create_time'] = time();
		}

		$data['update_at'] = time();

		$this->rulesValidate 	= $rulesValidate;
    	$this->messagesValidate = [
									'type.required' => 'Bạn chưa chọn loại banner',
									'position.exists' => 'Bạn chưa chọn vị trí banner'
									];

		$return		= false;
		$this->data	= $data;
		// Gọi validate
		$dataValidate 	= $this->validate();
		// Không có lỗi
		if($dataValidate){
			if($id > 0){
	    		$banner = $this->getById($id);
	    		if(empty($banner)){
	    			$dataReturn['errors'][] = 'Record not found';
	    		}else{
	    			$return = $banner->update($this->addPrefix($dataValidate));
	    			if($return){
	    				$dataReturn['status'] = true;
		    			$dataReturn['data'] = $banner;
		    		}else{
		    			$dataReturn['errors'][] = 'System error. Try again';
		    		}
	    		}
	    	}else{
				$banner	= $this->model;
				$return	= $banner->create($this->addPrefix($dataValidate));
	    		if(isset($return->ban_id) && $return->ban_id > 0){
					$dataReturn['status']	= true;
					$dataReturn['data']		= $return;
	    		}else{
	    			$dataReturn['errors'][] = 'System error. Try again';
	    		}
	    	}
		}else{
			$dataReturn['errors'] 	= $this->errors();
		}

		return $dataReturn;
    }
}
