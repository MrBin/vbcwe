<?php
namespace App\Repositories;

use App\Models\Opinion as Model;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;

class OpinionRepository extends Repository
{
    protected $model;

    public function __construct()
    {
        $this->model = new Model;
    }

    public function getAll($where = []){
    	$category 		= $this->model->where('opi_id', '>' , '0');
    	foreach ($where as $key => $value) {
    		$category = $category->where($value[0], $value[1], $value[2]);
    	}
    	$category		= $category->orderBy('opi_order', 'ASC')
									->orderBy('opi_id', 'ASC')
									->get();

		return $category;
    }

    public function addPrefix($data)
    {
        $dataStore = [];
        foreach ($data as $key => $value) {
            $dataStore[$this->model->prefix . $key] = $value;
        }
        return $dataStore;
    }


    public function getById($id){
        return $this->model->find($id);
    }

    public function delete($eventId){
		$dataReturn	= ['status' => false, 'msg' => ''];
		$banner		= $this->getById($eventId);
    	if($banner){
			$banner->delete();
			$dataReturn['status'] 	= true;
    	}else{
    		$dataReturn['msg'] 	= "Rerord not found !";
    	}

    	return $dataReturn;
    }

    public function add($id, $data){

    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];

    	// Tạo validate
    	$this->inputFields 	= [
								'title'				=> ['string'],
								'title_en'			=> ['string'],
								'name'				=> ['string'],
								'name_en'			=> ['string'],
								'picture'			=> ['string'],
								'video'			    => ['video'],
								'description'		=> ['text'],
								'description_en'	=> ['text'],
								'order'				=> ['integer'],
								'status'			=> ['integer'],
								'created_time'		=> ['integer'],
								'updated_at'		=> ['integer'],
								];

    	$rulesValidate 	= [
							'name'		=> 'required',
							'name_en'	=> 'required',
							'status'	=> 'in:0,1',
							];

		if($id > 0){
			if(empty($data['name'])) unset($rulesValidate['name']);
			if(empty($data['name_en'])) unset($rulesValidate['name_en']);
			if(empty($data['status'])) unset($rulesValidate['status']);
		}else{
			$data['created_time'] = time();
		}

		$data['updated_at'] = time();


		$this->data				= $data;
		$this->rulesValidate	= $rulesValidate;
    	$this->messagesValidate = [];

		$return				= false;
		// Gọi validate
		$dataValidate 	= $this->validate();
		// Không có lỗi
		if($dataValidate){
			if($id > 0){
	    		$events = $this->getById($id);
	    		if(empty($events)){
	    			$dataReturn['errors'][] = 'Record not found';
	    		}else{
	    			$return = $events->update($this->addPrefix($dataValidate));
	    			if($return){
	    				$dataReturn['status'] = true;
		    			$dataReturn['data'] = $events;

		    		}else{
		    			$dataReturn['errors'][] = 'System error. Try again';
		    		}
	    		}
	    	}else{
				$events	= $this->model;
				$return	= $events->create($this->addPrefix($dataValidate));
	    		if(isset($return->opi_id) && $return->opi_id > 0){
					$dataReturn['status']	= true;
					$dataReturn['data']		= $return;
	    		}else{
	    			$dataReturn['errors'][] = 'System error. Try again';
	    		}
	    	}
		}else{
			$dataReturn['errors'] 	= $this->errors();
		}

		return $dataReturn;
    }
}
