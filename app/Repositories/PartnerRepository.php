<?php
namespace App\Repositories;

use App\Models\Partner as Model;
use App\Repositories\Repository;
use App\Helper\Upload;

class PartnerRepository extends Repository
{
    protected $model;

	const PARTNER_SPONSOR		= 1;
	const PARTNER_INTERNATIONAL	= 2;
	const PARTNER_REGION		= 3;
	const PARTNER_DOMESTIC		= 4;
    const PARTNER_MEMBER		= 5;

    public function __construct()
    {
        $this->model = new Model;
    }

    public function addPrefix($data)
    {
        $dataStore = [];
        foreach ($data as $key => $value) {
            $dataStore[$this->model->prefix . $key] = $value;
        }
        return $dataStore;
    }

    public static function getTypeLabels(){
   		return [
   			self::PARTNER_SPONSOR => 'Đối tác tài trợ và hoạt động',
   			self::PARTNER_INTERNATIONAL => 'Đối tác quốc tế',
   			self::PARTNER_REGION => 'Đối tác khu vực',
   			self::PARTNER_DOMESTIC => 'Đối tác trong nước',
            self::PARTNER_MEMBER => 'Đối tác',
   		];
   	}

   	public static function getTypeLabel($type){
   		$allType = self::getTypeLabels();
   		return isset($allType[$type]) ? $allType[$type] : '';
   	}

    public static function getPictureUrl($filename, $type = 'fullsize'){
	   	$url 		= Upload::getUrlImage('banner', $filename);
	   	return $url;
   	}

    public function getById($id){
        return $this->model->find($id);
    }

    public function delete($bannerId){
		$dataReturn	= ['status' => false, 'msg' => ''];
		$banner		= $this->getById($bannerId);
    	if($banner){
			$banner->delete();
			$dataReturn['status'] 	= true;
    	}else{
    		$dataReturn['msg'] 	= "Partner not found !";
    	}

    	return $dataReturn;
    }

    public function add($id, $data){

    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];

    	// Tạo validate
    	$this->inputFields 	= [
										'name'			=> ['string'],
										'name_en'			=> ['string'],
										'picture'		=> ['string'],
										'link'			=> ['string'],
										'active'		=> ['integer'],
										'order'			=> ['integer'],
										'type'			=> ['integer'],
										'create_time' 	=> ['integer'],
										'update_at'		=> ['integer'],
										'staff_id'		=> ['integer'],
								];

    	$rulesValidate 	= [
							'name'			=> 'required',
							'name_en'		=> 'required',
							'link'			=> 'bail|required|',
							'active'		=> 'in:0,1',
							];

		if($id > 0){
			if(empty($data['name'])) unset($rulesValidate['name']);
			if(empty($data['name_en'])) unset($rulesValidate['name_en']);
			if(empty($data['picture'])) unset($rulesValidate['picture']);
			if(empty($data['link'])) unset($rulesValidate['link']);
			if(empty($data['active'])) unset($rulesValidate['active']);
		}else{
			$data['create_time'] = time();
		}

		$data['update_at'] = time();

		$this->data = $data;
		$this->rulesValidate 	= $rulesValidate;
    	$this->messagesValidate = [];

		$return				= false;
		// Gọi validate
		$dataValidate 	= $this->validate();
		// Không có lỗi
		if($dataValidate){
			if($id > 0){
	    		$partner = $this->getById($id);
	    		if(empty($partner)){
	    			$dataReturn['errors'][] = 'Record not found';
	    		}else{
	    			$return = $partner->update($this->addPrefix($dataValidate));
	    			if($return){
	    				$dataReturn['status'] = true;
		    			$dataReturn['data'] = $partner;
		    		}else{
		    			$dataReturn['errors'][] = 'System error. Try again';
		    		}
	    		}
	    	}else{
				$partner	= $this->model;
				$return	= $partner->create($this->addPrefix($dataValidate));
	    		if(isset($return->par_id) && $return->par_id > 0){
					$dataReturn['status']	= true;
					$dataReturn['data']		= $return;
	    		}else{
	    			$dataReturn['errors'][] = 'System error. Try again';
	    		}
	    	}
		}else{
			$dataReturn['errors'] 	= $this->errors();
		}

		return $dataReturn;
    }
}
