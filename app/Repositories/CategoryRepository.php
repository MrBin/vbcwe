<?php
namespace App\Repositories;

use App\Models\Category as Model;
use App\Repositories\Repository;
use App\Helper\Category as CategoryHelper;
use Illuminate\Support\Facades\DB;

class CategoryRepository extends Repository
{
    protected $model;
    const CATEGORY_NEWS = 'news';
    const CATEGORY_STATICS = 'static';
    const CATEGORY_EVENT = 'event';

    public function __construct()
    {
        $this->model = new Model;
    }

    public function getAll($where = []){
    	$category 		= $this->model->where('cat_id', '>' , '0');
    	foreach ($where as $key => $value) {
    		$category = $category->where($value[0], $value[1], $value[2]);
    	}
    	$category		= $category->orderBy('cat_order', 'ASC')
									->orderBy('cat_id', 'ASC')
									->get();

		return $category;
    }

    public static function getTypeLabels(){
   		return [
   			self::CATEGORY_NEWS => 'Tin tức',
   			self::CATEGORY_STATICS => 'Kiến thức tham khảo',
   			self::CATEGORY_EVENT => 'Sự kiện',
   		];
   	}

   	public static function getTypeLabel($type){
   		$allType = self::getTypeLabels();
   		return isset($allType[$type]) ? $allType[$type] : '';
   	}

    public function addPrefix($data)
    {
        $dataStore = [];
        foreach ($data as $key => $value) {
            $dataStore[$this->model->prefix . $key] = $value;
        }
        return $dataStore;
    }


    public function getById($id){
        return $this->model->find($id);
    }

    public function delete($adminId){
    	$dataReturn = ['status' => false, 'msg' => ''];
    	$staff 		= $this->getById($adminId);
    	if($staff){
    		if($staff->adm_loginname == 'admin' || $adminId == 1){
    			$dataReturn['msg'] 	= "Cannot delete system admin !";
    		}else{
				$staff->delete();
				$dataReturn['status'] 	= true;
    		}
    	}else{
    		$dataReturn['msg'] 	= "Staff not found !";
    	}

    	return $dataReturn;
    }

    public function add($id, $data){

    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];

    	// Tạo validate
    	$this->inputFields 	= [
								'name'					=> ['string'],
								'name_en'				=> ['string'],
								'name_rewrite'			=> ['string'],
								'type'					=> ['string'],
								'parent_id'				=> ['integer'],
								'order'					=> ['integer'],
								'active'				=> ['integer'],
								'hot'					=> ['integer'],
								'picture'				=> ['string'],
								'meta_title'			=> ['string'],
								'meta_title_en'			=> ['string'],
								'meta_keyword'			=> ['string'],
								'meta_keyword_en'		=> ['string'],
								'meta_description'		=> ['string'],
								'meta_description_en'	=> ['string'],
								'create_time'			=> ['integer'],
								'update_at'				=> ['integer'],
								];

    	$rulesValidate 	= [
							'name'			=> 'required',
							'name_en'		=> 'required',
							'name_rewrite'	=> 'bail|unique:categories_multi,cat_name_rewrite,' . $id . ',cat_id',
							'type'			=> 'bail|required|in:' . implode(",", array_keys(self::getTypeLabels())),
							'active'		=> 'in:0,1',
							'hot'			=> 'in:0,1'
							];

		if(isset($data['parent_id']) && $data['parent_id'] > 0) $rulesValidate['parent_id'] 	= 'exists:categories_multi,cat_id';

		if($id > 0){
			if(empty($data['name'])) unset($rulesValidate['name']);
			if(empty($data['name_en'])) unset($rulesValidate['name_en']);
			if(empty($data['name_rewrite'])) unset($rulesValidate['name_rewrite']);
			if(empty($data['type'])) unset($rulesValidate['type']);
			if(empty($data['active'])) unset($rulesValidate['active']);
			if(empty($data['hot'])) unset($rulesValidate['hot']);
			if(empty($data['meta_title'])) unset($rulesValidate['meta_title']);
			if(empty($data['meta_title_en'])) unset($rulesValidate['meta_title_en']);
			if(empty($data['meta_keyword'])) unset($rulesValidate['meta_keyword']);
			if(empty($data['meta_keyword_en'])) unset($rulesValidate['meta_keyword_en']);
			if(empty($data['meta_description'])) unset($rulesValidate['meta_description']);
			if(empty($data['meta_description_en'])) unset($rulesValidate['meta_description_en']);
		}else{
			$data['create_time'] = time();
		}

		$data['update_at'] = time();

		$this->data				= $data;
		$this->rulesValidate	= $rulesValidate;
    	$this->messagesValidate = [
									'name_rewrite.unique' => 'Tên rewrite đã tồn tại',
									'parent_id.exists' => 'Danh mục cha không tồn tại',
									'name.required' => 'Tên danh mục là bắt buộc',
									'name_en.required' => 'Tên danh mục en là bắt buộc',
									];

		$return				= false;
		// Có cần reset all child không
		$checkResetAllChild	= false;
		$whereReset 		= [];
		// Gọi validate
		$dataValidate 	= $this->validate();
		// Không có lỗi
		if($dataValidate){
			if($id > 0){
	    		$category = $this->getById($id);
	    		if(empty($category)){
	    			$dataReturn['errors'][] = 'Record not found';
	    		}else{
	    			$cat_parent_id_old 	= $category->cat_parent_id;
	    			$return = $category->update($this->addPrefix($dataValidate));
	    			if($return){
	    				$dataReturn['status'] = true;
		    			$dataReturn['data'] = $category;
		    			// Có thay đổi danh mục cha thì cần reset all child
	    				if(isset($dataValidate['parent_id']) && $cat_parent_id_old != $dataValidate['parent_id']){
	    					$whereReset[] 		= ['cat_type', '=', $category->cat_type];
	    					$checkResetAllChild = true;
	    				}
		    		}else{
		    			$dataReturn['errors'][] = 'System error. Try again';
		    		}
	    		}
	    	}else{
				$category	= $this->model;
				$return	= $category->create($this->addPrefix($dataValidate));
	    		if(isset($return->cat_id) && $return->cat_id > 0){
					$dataReturn['status']	= true;
					$dataReturn['data']		= $return;
					$checkResetAllChild		= true;
					if(isset($data['type'])) $whereReset[] 		= ['cat_type', '=', $data['type']];
	    		}else{
	    			$dataReturn['errors'][] = 'System error. Try again';
	    		}
	    	}
		}else{
			$dataReturn['errors'] 	= $this->errors();
		}

		if($checkResetAllChild){
			$this->resetAllChild($whereReset);
		}

		return $dataReturn;
    }

    public function resetAllChild($where = []){
		$arrayCategory	= CategoryHelper::getAllCategory($where, false);
		$arrayCategory	= array_values($arrayCategory);

		// Lặp từ trên xuống dưới để lấy các cat con( dựa vào level)
		for($i = 0; $i < count($arrayCategory); $i++) {
			$listid	= $arrayCategory[$i]['id']; // Lấy id của chính nó
			// Lặp các danh mục tiếp theo nếu level của danh mục tiếp theo lớn hơn thì đấy chính là cấp con
			$cat_has_child = 0;
			for($j = $i + 1; $j < count($arrayCategory); $j++) {
				if($arrayCategory[$j]['level'] > $arrayCategory[$i]['level']){
					$listid	.= ", " . $arrayCategory[$j]['id'];
					$cat_has_child = 1;
				}else{
					// Đã hết cấp con
					break;
				}
			}
			$listid	= convertListToListId($listid, 1);

			// Cập nhật database
			DB::table('categories_multi')->where('cat_id', intval($arrayCategory[$i]['id']))->update(['cat_has_child' => 1, 'cat_all_child' => $listid]);
		}
	}
}
