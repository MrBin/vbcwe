<?php
namespace App\Repositories;

use App\Models\Statics as Model;
use App\Repositories\Repository;

class StaticsRepository extends Repository
{
    protected $model;

    public function __construct()
    {
        $this->model = new Model;
    }

    public function addPrefix($data)
    {
        $dataStore = [];
        foreach ($data as $key => $value) {
            $dataStore[$this->model->prefix . $key] = $value;
        }
        return $dataStore;
    }


    public function getById($id){
        return $this->model->find($id);
    }

    public function delete($recordId){
		$dataReturn	= ['status' => false, 'msg' => ''];
		$banner		= $this->getById($recordId);
    	if($banner){
			$banner->delete();
			$dataReturn['status'] 	= true;
    	}else{
    		$dataReturn['msg'] 	= "Record not found !";
    	}

    	return $dataReturn;
    }

    public function add($id, $data){

    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];

    	// Tạo validate
    	$this->data = $data;
    	$this->inputFields 	= [
									'category_id'			=> ['integer'],
									'title'					=> ['string'],
									'title_en'				=> ['string'],
									'name_rewrite'			=> ['string'],
									'teaser'				=> ['string'],
									'teaser_en'				=> ['string'],
									'picture'				=> ['string'],
									'picture_en'			=> ['string'],
									'meta_title'			=> ['string'],
									'meta_title_en'			=> ['string'],
									'meta_keyword'			=> ['string'],
									'meta_keyword_en'		=> ['string'],
									'meta_description'		=> ['string'],
									'meta_description_en'	=> ['string'],
									'type'					=> ['integer'],
									'order'					=> ['integer'],
									'status'				=> ['integer'],
									'description'			=> ['text'],
									'description_en'		=> ['text'],
									'created_time'			=> ['integer'],
									'updated_at'			    => ['integer'],
								];

    	$rulesValidate 	= [
							'category_id'	=> 'required',
							'title'		=> 'required',
							'title_en'	=> 'required',
							'name_rewrite'	=> 'bail|required|unique:statics_new,sta_name_rewrite,' . $id . ',sta_id',
							'active'	=> 'in:0,1',
							];

		if($id > 0){
			if(empty($data['category_id'])) unset($rulesValidate['category_id']);
			if(empty($data['title'])) unset($rulesValidate['title']);
			if(empty($data['title_en'])) unset($rulesValidate['title_en']);
			if(empty($data['name_rewrite'])) unset($rulesValidate['name_rewrite']);

		}else{
			$data['created_time'] = time();
		}

		$data['updated_at'] = time();

        $this->data				= $data;
		$this->rulesValidate 	= $rulesValidate;
    	$this->messagesValidate = [];

		$return				= false;
		// Gọi validate
		$dataValidate 	= $this->validate();

		// Không có lỗi
		if($dataValidate){
			if($id > 0){
	    		$product = $this->getById($id);
	    		if(empty($product)){
	    			$dataReturn['errors'][] = 'Record not found';
	    		}else{
	    			$return = $product->update($this->addPrefix($dataValidate));
	    			if($return){
	    				$dataReturn['status'] = true;
		    			$dataReturn['data'] = $product;
		    		}else{
		    			$dataReturn['errors'][] = 'System error. Try again';
		    		}
	    		}
	    	}else{
				$product	= $this->model;
				$return	= $product->create($this->addPrefix($dataValidate));
	    		if(isset($return->sta_id) && $return->sta_id > 0){
					$dataReturn['status']	= true;
					$dataReturn['data']		= $return;
	    		}else{
	    			$dataReturn['errors'][] = 'System error. Try again';
	    		}
	    	}
		}else{
			$dataReturn['errors'] 	= $this->errors();
		}

		return $dataReturn;
    }
}
