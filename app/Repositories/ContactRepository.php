<?php
namespace App\Repositories;

use App\Models\Contact as Model;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;

class ContactRepository extends Repository
{
    protected $model;
	const CONTACT_NEWS = 1;
	const CONTACT_ADVISORY = 2;
	const CONTACT_DOCUMENT = 3;
	const CONTACT_EVENT = 4;
	const CONTACT_STORY = 5;
    public function __construct()
    {
        $this->model = new Model;
    }

    public function getAll($where = []){
    	$contact 		= $this->model->where('con_id', '>' , '0');
    	foreach ($where as $key => $value) {
    		$contact = $contact->where($value[0], $value[1], $value[2]);
    	}
    	$contact		= $contact->orderBy('con_id', 'DESC')
									->get();

		return $contact;
    }

    public function addPrefix($data)
    {
        $dataStore = [];
        foreach ($data as $key => $value) {
            $dataStore[$this->model->prefix . $key] = $value;
        }
        return $dataStore;
    }

    public static function getTypeLabels(){
   		return [
   			self::CONTACT_NEWS => 'Đăng ký nhận bản tin',
   			self::CONTACT_ADVISORY => 'Đăng ký tham gia',
   			self::CONTACT_DOCUMENT => 'Đăng ký nhận tài liệu',
   			self::CONTACT_EVENT => 'Đăng ký tham gia sự kiện',
   			self::CONTACT_STORY => 'Chia sẻ câu chuyện',
   		];
   	}

   	public static function getTypeLabel($type){
   		$allType = self::getTypeLabels();
   		return isset($allType[$type]) ? $allType[$type] : '';
   	}

    public function getById($id){
        return $this->model->find($id);
    }

    public function delete($founderId){
		$dataReturn	= ['status' => false, 'msg' => ''];
		$founder	= $this->getById($founderId);
    	if($founder){
			$founder->delete();
			$dataReturn['status'] 	= true;
    	}else{
    		$dataReturn['msg'] 	= "Contact not found !";
    	}

    	return $dataReturn;
    }

    public function add($id, $data, $type = 0){

    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];

    	// Tạo validate
    	$this->inputFields 	= [
								'title'			=> ['string'],
								'name'			=> ['string'],
								'email'			=> ['string'],
								'position'      => ['string'],
								'company_name'  => ['string'],
								'search'		=> ['string'],
								'status'		=> ['integer'],
								'area'	        => ['string'],
                                'website'	    => ['string'],
                                'who'			=> ['string'],
                                'sex'			=> ['string'],
                                'scale'			=> ['integer'],
                                'address'	    => ['string'],
                                'phone'	        => ['string'],
                                'need'	        => ['integer'],
                                'type'	        => ['integer'],
								'description'	=> ['string'],
								'created_time'	=> ['integer'],
								'updated_at'	=> ['integer'],
								];
    	switch ($type){
            case 1:
                $rulesValidate 	= [
                    'name'	=> 'required',
                    'description'	=> 'required',
                ];
                break;
            case 2:
                $rulesValidate 	= [
                    'name'	=> 'required',
                    'title'	=> 'required',
                    'email'	=> 'required|email',
                    'description'	=> 'required',
                ];
                break;
            case 3:
                $rulesValidate 	= [
                    'name'	=> 'required',
                    'sex'	=> 'required',
                    'email'	=> 'required|email',
                    'company_name'	=> 'required',
                    'position'	=> 'required',
                    'phone'	=> 'required',
                    'need'	=> 'required',
                ];
                break;
            case 4:
                $rulesValidate 	= [
                    'name'	=> 'required',
                    'sex'	=> 'required',
                    'email'	=> 'required|email',
                    'company_name'	=> 'required',
                    'position'	=> 'required',
                    'phone'	=> 'required',
                ];
                break;
            case 5:
                $rulesValidate 	= [
                    'who'	=> 'required',
                    'area'	=> 'required',
                    'address'	=> 'required',
                    'scale'	=> 'required',
                    'website'	=> 'required',
                    'email'	=> 'required|email',
                    'company_name'	=> 'required',
                    'position'	=> 'required',
                    'phone'	=> 'required',
                ];
                break;
            default:
                $rulesValidate 	= [
                    'name'	=> 'required',
                    'email'	=> 'required|email',
                    'position'      => 'required',
                    'company_name'	=> 'required'
                ];
        }

		$data['created_time'] = time();

		$this->data				= $data;
		$this->rulesValidate	= $rulesValidate;
    	$this->messagesValidate = [

								 ];
		$return				= false;
		// Gọi validate
		$dataValidate 	= $this->validate();
		// Không có lỗi
		if($dataValidate){
			if($id > 0){
	    		$news = $this->getById($id);
	    		if(empty($news)){
	    			$dataReturn['errors'][] = 'Record not found';
	    		}else{
	    			$return = $news->update($this->addPrefix($dataValidate));
	    			if($return){
	    				$dataReturn['status'] = true;
		    			$dataReturn['data'] = $news;

		    		}else{
		    			$dataReturn['errors'][] = 'System error. Try again';
		    		}
	    		}
	    	}else{
				$news	= $this->model;
				$return	= $news->create($this->addPrefix($dataValidate));
	    		if(isset($return->con_id) && $return->con_id > 0){
					$dataReturn['status']	= true;
					$dataReturn['data']		= $return;
	    		}else{
	    			$dataReturn['errors'][] = 'System error. Try again';
	    		}
	    	}
		}else{
			$dataReturn['errors'] 	= $this->errors();
		}

		return $dataReturn;
    }
}
