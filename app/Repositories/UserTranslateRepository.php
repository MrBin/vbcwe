<?php
namespace App\Repositories;

use App\Models\UserTranslate as Model;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;

class UserTranslateRepository extends Repository
{
    protected $model;
    public static $arrLang	= ["vi"=> "Tiếng Việt", "en" => "English",];
    public function __construct()
    {
        $this->model = new Model;
    }

    public function getAll($where = [], $order = []){
    	$category 		= $this->model->where('ust_id', '>' , '0');
    	foreach ($where as $key => $value) {
    		$category = $category->where($value[0], $value[1], $value[2]);
    	}
    	$category		= $category->get();

		return $category;
    }

    public function addPrefix($data)
    {
        $dataStore = [];
        foreach ($data as $key => $value) {
            $dataStore[$this->model->prefix . $key] = $value;
        }
        return $dataStore;
    }


    public function getById($id){
        return $this->model->find($id);
    }

    public function getByKeywordMd5($keyword){
        return $this->model->where('ust_keyword_md5', $keyword)->first();
    }

    public function add($data){

    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];
    	//dd($dataReturn);
    	// Tạo validate
    	$this->inputFields 	= [
									'keyword'		=> ['string'],
									'vi'			=> ['string'],
									'en'			=> ['string'],
									'created_time'	=> ['integer'],
									'updated_at'	=> ['integer'],
									];

    	$rulesValidate 	= [
							'keyword'	=> 'required',
						];
		$data['updated_at'] = time();

		$this->rulesValidate 	= $rulesValidate;

		$return		= false;
		$this->data = $data;
		// Gọi validate
		$dataValidate 	= $this->validate();

		// Không có lỗi
		if($dataValidate){

			DB::statement('REPLACE INTO user_translate (ust_keyword, ust_keyword_md5, ust_vi, ust_en, ust_created_time, ust_updated_at) values (?, ?, ?, ?, ?, ?)',
						[$dataValidate['keyword'], md5($dataValidate['keyword']), $dataValidate['vi'], $dataValidate['en'], time(), $dataValidate['updated_at']]);

			/*// Lưu database
			DB::table('user_translate')->insertOrIgnore([
			    [
					'ust_keyword'		=> $dataValidate['keyword'],
					'ust_keyword_md5'	=> md5($dataValidate['keyword']),
					'ust_vi'			=> $dataValidate['vi'],
					'ust_en'			=> $dataValidate['en'],
					'ust_updated_at'	=> $dataValidate['updated_at']
			    ],
			]);*/
			$dataReturn['status'] = true;

		}else{
			$dataReturn['errors'] 	= $this->errors();
		}

		return $dataReturn;
    }
}
