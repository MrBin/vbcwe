<?php
namespace App\Repositories;

use App\Models\ConfigurationCertification as Model;
use App\Repositories\Repository;
use App\Helper\Upload;

class ConfigurationCertificationRepository extends Repository
{
   protected $model;

   public function __construct()
   {
      $this->model = new Model;
   }

   public function addPrefix($data)
   {
		$dataStore = [];
		foreach ($data as $key => $value) {
			$dataStore[$this->model->prefix . $key] = $value;
		}
		return $dataStore;
   }

   public function getConfiguration(){
      return $this->model->find(1);
   }

   public function update($data){

   	$id = 1;
    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];

    	// Tạo validate
    	$this->inputFields 	= [
								'picture_1'			=> ['string'],
								'picture_1_en'		=> ['string'],
								'text_1'			=> ['text'],
								'text_1_en'			=> ['text'],
								'picture_2_1'		=> ['string'],
								'picture_2_1_en'	=> ['string'],
								'picture_2_2'		=> ['string'],
								'picture_2_2_en'	=> ['string'],
								'text_2_1'			=> ['text'],
								'text_2_1_en'		=> ['text'],
								'text_2_2'			=> ['text'],
								'text_2_2_en'		=> ['text'],
								'picture_3_1'		=> ['string'],
								'picture_3_1_en'	=> ['string'],
								'picture_3_2'		=> ['string'],
								'picture_3_2_en'	=> ['string'],
								'text_3_1'			=> ['text'],
								'text_3_1_en'		=> ['text'],
								'text_3_2'			=> ['text'],
								'text_3_2_en'		=> ['text'],
								'text_4'			=> ['text'],
								'text_4_en'			=> ['text'],
								'text_5_1'			=> ['text'],
								'text_5_1_en'		=> ['text'],
								'text_5_2'			=> ['text'],
								'text_5_2_en'		=> ['text'],
								'text_6_1'			=> ['text'],
								'text_6_1_en'		=> ['text'],
								'text_6_2'			=> ['text'],
								'text_6_2_en'		=> ['text'],
								'text_6_3'			=> ['text'],
								'text_6_3_en'		=> ['text'],
								'text_6_4'			=> ['text'],
								'text_6_4_en'		=> ['text'],
								];

		$rulesValidate				= [];
		$this->rulesValidate		= $rulesValidate;

		$return			= false;
		// Gọi validate
		$data['update_at'] =  time();
		$this->data = $data;
		$dataValidate 	= $this->validate();

		// Không có lỗi
		if($dataValidate){
    		$config = $this->getConfiguration();
    		if(empty($config)){
    			$dataReturn['errors'][] = 'Record not found';
    		}else{
    			$return = $config->update($this->addPrefix($dataValidate));
    			if($return){
    				$dataReturn['status'] = true;
	    			$dataReturn['data'] = $config;
	    		}else{
	    			$dataReturn['errors'][] = 'System error. Try again';
	    		}
    		}
		}else{
			$dataReturn['errors'] 	= $this->errors();
		}

		return $dataReturn;
   }
}
