<?php

namespace App\Repositories;
use App\Models\AdminUser;
use App\Helper\ApiSso;
use Request;
use Config;
use App\Repositories\Repository;

class AdminUserRepository extends Repository
{
    protected $adminUser;

    public static $adminNotEdit = ['admin', 'administrator'];

    public function __construct()
    {
        $this->adminUser = new AdminUser;
    }
    public function addPrefix($data)
    {
        $dataStore = [];
        foreach ($data as $key => $value) {
            $dataStore[$this->adminUser->prefix . $key] = $value;
        }
        return $dataStore;
    }
    public function getByUserName($user_name){
		$info	= $this->adminUser->where('adm_loginname', $user_name)->where('adm_active', 1)->first();
		if($info){
			return $info;
		}
		return false;
    }

    public function getById($id){
		$info	= $this->adminUser->where('adm_id', $id)->first();
		if($info){
			return $info;
		}
		return false;
    }

    public function setLogout(){
    	Request::session()->forget(['ADMIN_LOGGED', 'ADMIN_USER_NAME', 'ADMIN_USER_PASSWORD', 'ADMIN_TOKEN']);
    }

    public function getAllMenuRoleAdmin(){
		$arrayMenu 	= Config("adminmenu");
		return $arrayMenu;
	}

	public function getAllMenuAdmin(){
		$allMenuRole 	= self::getAllMenuRoleAdmin();
		$arrayReturn 	= array();
		foreach($allMenuRole as $key => $value){
			if(isset($value['is_menu']) && $value['is_menu'] == 1){
				$arrayReturn[$key] 	= $value;
				foreach($value['sub'] as $key_sub => $value_sub){
					// Xóa bỏ những item không phải là menu
					if(!isset($value_sub['is_menu']) || $value_sub['is_menu'] != 1){
						unset($arrayReturn[$key]['sub'][$key_sub]);
					}
				}
			}
		}

		return $arrayReturn;
	}

	public function add($id, $data){

    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];
    	if($id == 1) return $dataReturn;

    	// Tạo validate
    	$this->inputFields 	= [
								'loginname'	=> ['string'],
								'name'		=> ['string'],
								'phone'		=> ['string'],
								'email'		=> ['string'],
								'active' 	=> ['integer'],
								'password'  => ['string'],
								'json_role' => ['string'],
								'create_time'			=> ['integer'],
								'update_at'				=> ['integer'],
								];

    	$rulesValidate 	= [
							'loginname'	=> 'bail|required|not_in:admin|unique:admin_user,adm_loginname,' . $id . ',adm_id',
							'email'		=> 'bail|required|email',
							'name'		=> 'bail|required',
							'phone'		=> 'bail|required|min:10|max:11',
							'password'  => 'bail|required|min:6',
							'active' 	=> 'in:0,1'
							];

		if(!empty($data['password'])){
			$data['password'] = $this->generatePass($data['password']);
		}

		if(empty($data['password'])) unset($data['password']);

		if($id > 0){
			if(empty($data['loginname'])) unset($rulesValidate['loginname']);
			if(empty($data['email'])) unset($rulesValidate['email']);
			if(empty($data['name'])) unset($rulesValidate['name']);
			if(empty($data['phone'])) unset($rulesValidate['phone']);
			if(empty($data['password'])) unset($rulesValidate['password']);
			if(empty($data['active'])) unset($rulesValidate['active']);
		}
		$this->rulesValidate 	= $rulesValidate;
    	$this->messagesValidate = [
									'loginname.unique' => 'Tên đăng nhập đã tồn tại',
									'loginname.not_in' => 'Tên đăng nhập không hợp lệ',
									'email.email' => "Email không đúng định dạng",
									];

		$return = false;
		if($id > 0) $data['update_at'] = time();
		$data['create_time'] = time();

		// Gọi validate
		$this->data = $data;
		$dataValidate 	= $this->validate();

		// Không có lỗi
		if($dataValidate){
			if($id > 0){
	    		$staff = $this->getById($id);
	    		if(empty($staff)){
	    			$dataReturn['errors'][] = 'Record not found';
	    		}else{
	    			$return = $staff->update($this->addPrefix($dataValidate));
	    			if($return){
	    				$dataReturn['status'] = true;
		    			$dataReturn['data'] = $staff;
		    		}else{
		    			$dataReturn['errors'][] = 'System error. Try again';
		    		}
	    		}
	    	}else{
				$staff	= $this->adminUser;
				$return	= $staff->create($this->addPrefix($dataValidate));
	    		if(isset($return->adm_id) && $return->adm_id > 0){
	    			$dataReturn['status'] = true;
	    			$dataReturn['data'] = $return;
	    		}else{
	    			$dataReturn['errors'][] = 'System error. Try again';
	    		}
	    	}
		}else{
			$dataReturn['errors'] 	= $this->errors();
		}


		return $dataReturn;
    }

    public function generatePass($password, $sercurity_code = ''){
    	return md5($password);
    }

	public function changePass($adminId, $newpassword){
    	$dataReturn = ['status' => false, 'msg' => ''];
    	$staff 		= $this->getById($adminId);
    	if($staff){
    		$staff->adm_password = md5($newpassword);
			$staff->save();
			$dataReturn['status'] 	= true;
    	}else{
    		$dataReturn['msg'] 	= "Staff not found !";
    	}

    	return $dataReturn;
    }

    public function delete($adminId){
    	$dataReturn = ['status' => false, 'msg' => ''];
    	$staff 		= $this->getById($adminId);
    	if($staff){
    		if($staff->adm_loginname == 'admin' || $adminId == 1){
    			$dataReturn['msg'] 	= "Cannot delete system admin !";
    		}else{
				$staff->delete();
				$dataReturn['status'] 	= true;
    		}
    	}else{
    		$dataReturn['msg'] 	= "Staff not found !";
    	}

    	return $dataReturn;
    }
}
