<?php
namespace App\Repositories;

use App\Models\Achievement as Model;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;

class AchievementRepository extends Repository
{
    protected $model;

    public function __construct()
    {
        $this->model = new Model;
    }

    public function getAll($where = []){
    	$contact 		= $this->model->where('tht_id', '>' , '0');
    	foreach ($where as $key => $value) {
    		$contact = $contact->where($value[0], $value[1], $value[2]);
    	}
    	$contact		= $contact->orderBy('tht_id', 'ASC')
									->get();

		return $category;
    }

    public function addPrefix($data)
    {
        $dataStore = [];
        foreach ($data as $key => $value) {
            $dataStore[$this->model->prefix . $key] = $value;
        }
        return $dataStore;
    }


    public function getById($id){
        return $this->model->find($id);
    }

    public function delete($recordId){
		$dataReturn	= ['status' => false, 'msg' => ''];
		$banner		= $this->getById($recordId);
    	if($banner){
			$banner->delete();
			$dataReturn['status'] 	= true;
    	}else{
    		$dataReturn['msg'] 	= "Record not found !";
    	}

    	return $dataReturn;
    }

    public function add($id, $data){

    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];

    	// Tạo validate
    	$this->inputFields 	= [
								'index'			=> ['string'],
								'title'			=> ['string'],
								'title_en'		=> ['string'],
								'icon'			=> ['string'],
								'status'		=> ['integer'],
								'order'			=> ['integer'],
								'created_time'	=> ['integer'],
								'updated_at'	=> ['integer'],
								];

    	$rulesValidate 	= [
							'index'		=> 'required',
							'title'		=> 'required',
							'title_en'	=> 'required'
							];

		if($id > 0){
			if(empty($data['index'])) unset($rulesValidate['index']);
			if(empty($data['title'])) unset($rulesValidate['title']);
			if(empty($data['title_en'])) unset($rulesValidate['title_en']);
		}else{
			$data['created_time'] = time();
		}

		$data['updated_at'] = time();

		$this->data				= $data;
		$this->rulesValidate	= $rulesValidate;
    	$this->messagesValidate = [

								 ];

		$return				= false;
		// Gọi validate
		$dataValidate 	= $this->validate();
		// Không có lỗi
		if($dataValidate){
			if($id > 0){
	    		$news = $this->getById($id);
	    		if(empty($news)){
	    			$dataReturn['errors'][] = 'Record not found';
	    		}else{
	    			$return = $news->update($this->addPrefix($dataValidate));
	    			if($return){
	    				$dataReturn['status'] = true;
		    			$dataReturn['data'] = $news;

		    		}else{
		    			$dataReturn['errors'][] = 'System error. Try again';
		    		}
	    		}
	    	}else{
				$news	= $this->model;
				$return	= $news->create($this->addPrefix($dataValidate));
	    		if(isset($return->tht_id) && $return->tht_id > 0){
					$dataReturn['status']	= true;
					$dataReturn['data']		= $return;
	    		}else{
	    			$dataReturn['errors'][] = 'System error. Try again';
	    		}
	    	}
		}else{
			$dataReturn['errors'] 	= $this->errors();
		}

		return $dataReturn;
    }
}
