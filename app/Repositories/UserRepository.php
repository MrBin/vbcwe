<?php

namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\Users;
use Request;

class UserRepository extends Repository
{
    protected $model;

    public function __construct()
    {
        $this->model = new Users;
    }

    public function addPrefix($data)
    {
        $dataStore = [];
        foreach ($data as $key => $value) {
            $dataStore[$this->model->prefix . $key] = $value;
        }
        return $dataStore;
    }
    public function getById($id){
		$info	= $this->model->where('use_id', $id)->first();
		if($info){
			return $info;
		}
		return false;
    }
    public function getByUserName($user_name){
		$info	= $this->model->where('use_loginname', $user_name)->first();
		if($info){
			return $info;
		}
		return false;
    }

    public function changePass($userId, $newpassword){
    	$dataReturn = ['status' => false, 'msg' => ''];
    	$user 		= $this->getById($userId);
    	if($user){
    		$user->use_password = $this->generatePassword($newpassword, $user->use_security);
			$user->save();
			$dataReturn['status'] 	= true;
    	}else{
    		$dataReturn['msg'] 	= "User not found !";
    	}

    	return $dataReturn;
    }

    public function register($data){
    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];

    	// Tạo validate

    	$this->inputFields 	= [
									'name'			=> ['string'],
									'loginname'		=> ['string'],
									'phone'			=> ['string'],
									'password'		=> ['string'],
									'active'		=> ['integer'],
									'created_time'	=> ['integer'],
									'updated_at'		=> ['integer'],
									'admin_id'		=> ['integer'],
								];

    	$rulesValidate 	= [
							'name'		=> 'required',
							'loginname'	=> 'required|unique:users,use_loginname,0,use_id',
							'phone'		=> 'required',
							'password'	=> 'required',
							'active'	=> 'in:0,1',
							];

		$this->rulesValidate 	= $rulesValidate;
    	$this->messagesValidate = [
									'loginname.unique' => 'Tên đăng nhập đã tồn tại',
									];

		$return				= false;
		// Gọi validate
		$this->data = $data;
		$dataValidate 	= $this->validate();
		// Không có lỗi
		if($dataValidate){
			// Lấy lại password
			$security_code 	= rand(100000, 999999);
			$dataValidate['password'] 	= $this->generatePassword($dataValidate['password'], $security_code);
			$dataValidate['security'] 	= $security_code;
			$user	= $this->model;
			$return	= $user->create($this->addPrefix($dataValidate));
    		if(isset($return->use_id) && $return->use_id > 0){
				$dataReturn['status']	= true;
				$dataReturn['data']		= $return;
    		}else{
    			$dataReturn['errors'][] = 'Hệ thống xảy ra lỗi. Vui lòng thử lại';
    		}
		}else{
			$dataReturn['errors'] 	= $this->errors();
		}

		return $dataReturn;
    }

    public function generatePassword($password, $security_code = ''){
    	return md5($password . md5($password . $security_code));
    }
}
