<?php
namespace App\Repositories;

use App\Models\EnterpriceNetwork as Model;
use App\Repositories\Repository;
use App\Helper\Upload;

class EnterpriceNetworkRepository extends Repository
{
    protected $model;

    public function __construct()
    {
        $this->model = new Model;
    }

    public function addPrefix($data)
    {
        $dataStore = [];
        foreach ($data as $key => $value) {
            $dataStore[$this->model->prefix . $key] = $value;
        }
        return $dataStore;
    }

    public static function getPictureUrl($filename, $type = 'fullsize'){
	   	$url 		= Upload::getUrlImage('other', $filename);
	   	return $url;
   	}

    public function getById($id){
        return $this->model->find($id);
    }

    public function delete($founderId){
		$dataReturn	= ['status' => false, 'msg' => ''];
		$founder		= $this->getById($founderId);
    	if($founder){
			$founder->delete();
			$dataReturn['status'] 	= true;
    	}else{
    		$dataReturn['msg'] 	= "Enterprice Network not found !";
    	}

    	return $dataReturn;
    }

    public function add($id, $data){

    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];

    	// Tạo validate
    	$this->inputFields 	= [
										'name'				=> ['string'],
										'name_en'			=> ['string'],
										'picture'			=> ['string'],
										'link'				=> ['string'],
										'description'		=> ['string'],
										'description_en'	=> ['string'],
										'active'			=> ['integer'],
										'order'				=> ['integer'],
										'create_time'		=> ['integer'],
										'update_at'			=> ['integer'],
										'staff_id'			=> ['integer'],
								];

    	$rulesValidate 	= [
							'name'		=> 'required',
							'name_en'	=> 'required',
							'picture'	=> 'bail|required|',
							'active'	=> 'in:0,1',
							];

		if($id > 0){
			if(empty($data['name'])) unset($rulesValidate['name']);
			if(empty($data['name_en'])) unset($rulesValidate['name_en']);
			if(empty($data['description'])) unset($rulesValidate['description']);
			if(empty($data['description_en'])) unset($rulesValidate['description_en']);
			if(empty($data['picture'])) unset($rulesValidate['picture']);
			if(empty($data['link'])) unset($rulesValidate['link']);
			if(empty($data['active'])) unset($rulesValidate['active']);
			if(empty($data['order'])) unset($rulesValidate['order']);
		}else{
			$data['create_time'] = time();
		}

		$data['update_at'] = time();

		$this->rulesValidate 	= $rulesValidate;
    	$this->messagesValidate = [];

		$return		= false;
		$this->data	= $data;
		// Gọi validate
		$dataValidate 	= $this->validate();
		// Không có lỗi
		if($dataValidate){
			if($id > 0){
	    		$founder = $this->getById($id);
	    		if(empty($founder)){
	    			$dataReturn['errors'][] = 'Record not found';
	    		}else{
	    			$return = $founder->update($this->addPrefix($dataValidate));
	    			if($return){
	    				$dataReturn['status'] = true;
		    			$dataReturn['data'] = $founder;
		    		}else{
		    			$dataReturn['errors'][] = 'System error. Try again';
		    		}
	    		}
	    	}else{
				$founder	= $this->model;
				$return	= $founder->create($this->addPrefix($dataValidate));
	    		if(isset($return->enn_id) && $return->enn_id > 0){
					$dataReturn['status']	= true;
					$dataReturn['data']		= $return;
	    		}else{
	    			$dataReturn['errors'][] = 'System error. Try again';
	    		}
	    	}
		}else{
			$dataReturn['errors'] 	= $this->errors();
		}

		return $dataReturn;
    }
}
