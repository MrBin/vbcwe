<?php
namespace App\Repositories;

use App\Models\Product as Model;
use App\Repositories\Repository;
use App\Helper\Product as ProductHelper;

class ProductRepository extends Repository
{
    protected $model;

    public function __construct()
    {
        $this->model = new Model;
    }

    public function addPrefix($data)
    {
        $dataStore = [];
        foreach ($data as $key => $value) {
            $dataStore[$this->model->prefix . $key] = $value;
        }
        return $dataStore;
    }


    public function getById($id){
        return $this->model->find($id);
    }

    public function delete($adminId){
    	$dataReturn = ['status' => false, 'msg' => ''];
    	$product 	= $this->getById($adminId);
    	if($product){
			$product->delete();
			$dataReturn['status'] 	= true;
    	}else{
    		$dataReturn['msg'] 	= "Product not found !";
    	}

    	return $dataReturn;
    }

    public function add($id, $data){

    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];

    	// Tạo validate
    	$this->data = $data;
    	$this->inputFields 	= [
									'category_id'			=> ['integer'],
									'name'					=> ['string'],
									'name_rewrite'			=> ['string'],
									'price'					=> ['integer'],
									'sale_price'			=> ['integer'],
									'picture'				=> ['string'],
									'picture_json'			=> ['string'],
									'meta_title'			=> ['string'],
									'meta_keyword'			=> ['string'],
									'meta_description'	=> ['string'],
									'type'					=> ['integer'],
									'order'					=> ['integer'],
									'hot'						=> ['integer'],
									'active'					=> ['integer'],
									'quantity'				=> ['integer'],
									'description'			=> ['text'],
								];

    	$rulesValidate 	= [
    						'category_id'	=> 'required|exists:categories_multi,cat_id',
							'name'			=> 'required',
							'name_rewrite'	=> 'bail|required|unique:products,pro_name_rewrite,' . $id . ',pro_id',
							'type'			=> 'bail|required|in:' . implode(",", array_keys(ProductHelper::$arrType)),
							'picture' 		=> 'required',
							'price' 		=> 'required',
							'sale_price' 	=> 'required',
							'active'		=> 'in:0,1',
							'hot'			=> 'in:0,1'
							];

		if($id > 0){
			if(empty($data['category_id'])) unset($rulesValidate['category_id']);
			if(empty($data['name'])) unset($rulesValidate['name']);
			if(empty($data['name_rewrite'])) unset($rulesValidate['name_rewrite']);
			if(empty($data['price'])) unset($rulesValidate['price']);
			if(empty($data['sale_price'])) unset($rulesValidate['sale_price']);
			if(empty($data['picture'])) unset($rulesValidate['picture']);
			if(empty($data['type'])) unset($rulesValidate['type']);
			if(empty($data['hot'])) unset($rulesValidate['hot']);
			if(empty($data['active'])) unset($rulesValidate['active']);
		}

		$this->rulesValidate 	= $rulesValidate;
    	$this->messagesValidate = [
									'name_rewrite.unique' => 'Tên rewrite đã tồn tại',
									'category_id.exists' => 'Danh mục không tồn tại'
									];

		$return				= false;
		// Gọi validate
		$dataValidate 	= $this->validate();
		// Không có lỗi
		if($dataValidate){
			if($id > 0){
	    		$product = $this->getById($id);
	    		if(empty($product)){
	    			$dataReturn['errors'][] = 'Record not found';
	    		}else{
	    			$return = $product->update($this->addPrefix($dataValidate));
	    			if($return){
	    				$dataReturn['status'] = true;
		    			$dataReturn['data'] = $product;
		    		}else{
		    			$dataReturn['errors'][] = 'System error. Try again';
		    		}
	    		}
	    	}else{
				$product	= $this->model;
				$return	= $product->create($this->addPrefix($dataValidate));
	    		if(isset($return->pro_id) && $return->pro_id > 0){
					$dataReturn['status']	= true;
					$dataReturn['data']		= $return;
	    		}else{
	    			$dataReturn['errors'][] = 'System error. Try again';
	    		}
	    	}
		}else{
			$dataReturn['errors'] 	= $this->errors();
		}

		return $dataReturn;
    }
}
