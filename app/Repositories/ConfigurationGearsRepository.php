<?php
namespace App\Repositories;

use App\Models\ConfigurationGears as Model;
use App\Repositories\Repository;
use App\Helper\Upload;

class ConfigurationGearsRepository extends Repository
{
   protected $model;

   public function __construct()
   {
      $this->model = new Model;
   }


   public function addPrefix($data)
   {
		$dataStore = [];
		foreach ($data as $key => $value) {
			$dataStore[$this->model->prefix . $key] = $value;
		}
		return $dataStore;
   }

   public function getConfiguration(){
      return $this->model->find(1);
   }

   public function update($data){

   		$id = 1;
    	$dataReturn = ['status' => false, 'data' => false, "errors" => []];

    	// Tạo validate
    	$this->inputFields 	= [
					'text_1'		=> ['string'],
					'text_1_en'		=> ['string'],
					'picture_2'		=> ['string'],
					'picture_2_en'	=> ['string'],
					'text_2_1'		=> ['string'],
					'text_2_1_en'	=> ['string'],
					'text_2_2'		=> ['text'],
					'text_2_2_en'	=> ['text'],
					'text_3'		=> ['string'],
					'text_3_en'		=> ['string'],
					'text_4'		=> ['string'],
					'text_4_en'		=> ['string'],
					'text_5'		=> ['string'],
					'text_5_en'		=> ['string'],
					'text_6'		=> ['string'],
					'text_6_en'		=> ['string'],
					'text_7'		=> ['string'],
					'text_7_en'		=> ['string'],
					'picture_7'		=> ['string'],
					'picture_7_en'	=> ['string'],
				];

		$rulesValidate				= [];
		$this->rulesValidate		= $rulesValidate;

		$return			= false;
		// Gọi validate
		$data['updated_at'] =  time();
		$this->data = $data;
		$dataValidate 	= $this->validate();

		// Không có lỗi
		if($dataValidate){
    		$config = $this->getConfiguration();
    		if(empty($config)){
    			$dataReturn['errors'][] = 'Record not found';
    		}else{
    			$return = $config->update($this->addPrefix($dataValidate));
    			if($return){
    				$dataReturn['status'] = true;
	    			$dataReturn['data'] = $config;
	    		}else{
	    			$dataReturn['errors'][] = 'System error. Try again';
	    		}
    		}
		}else{
			$dataReturn['errors'] 	= $this->errors();
		}

		return $dataReturn;
   }
}
