<?php
namespace App\Helper;

use App\Repositories\UserRepository as Model;
use Request;
class Users
{

    public function __construct(){

    }

    public static function isLogged(){
    	// Not logged redirect login
		$value 		= Request::session()->get('USER_LOGGED');
		$username	= Request::session()->get('USER_USER_NAME');
		if($value === true && $username){
			return true;
		}

		return false;
    }

    public static function setLogged($user_name, $password){
    	Request::session()->put('USER_LOGGED', true);
		Request::session()->put('USER_USER_NAME', $user_name);
		Request::session()->put('USER_USER_PASSWORD', $password);
    }

    public static function setLogout(){
    	Request::session()->forget(['USER_LOGGED', 'USER_USER_NAME', 'USER_USER_PASSWORD']);
    }

    public static function checkLogin($user_name, $password){

    	if($user_name == "" || $password == "") return false;

		$user	= new Model;
		$info	= $user->getByUserName($user_name);

    	if($info){
    		$password_check	= $user->generatePassword($password, $info->use_security);
    		if($info->use_password == $password_check){
    			self::setLogged($user_name, $password_check);
    			return true;
    		}
    	}

    	return false;
    }

    public static function routeNotCheckPermission(){
    	return ['admin.home', 'admin.profile.changepass'];
    }

    public static function getInfoLogged(){

  		$username	= Request::session()->get('USER_USER_NAME');

		$adminUser	= new Model;
		$info		= $adminUser->getByUserName($username);

		if($info && $info->use_active == 1){
			// Kiểm tra password có đúng không
			$password 	= Request::session()->get('USER_USER_PASSWORD');
			if($password == $info->use_password) return $info;
		}

		return false;
    }

    public function checkInfoLogin($user_name, $password){
    	$dataUser = $this->getByUserName($user_name);
    	if($dataUser){
    		if($dataUser->adm_id > 0 && (md5($password) == $dataUser->adm_password)){
    			self::setLogged($user_name, $password);
    			return true;
    		}else{
    			return false;
    		}
    	}
    }

    public static function getPermissionLogged(){
		$staffRole	= [];
		$infoStaff	= app()->USER_INFO;

		// Lấy toàn bộ quyền có thể có để so sánh
		$adminUser 	= new Model;
		$allRole	= $adminUser->getAllMenuRoleAdmin();

		if(isset($infoStaff->adm_isadmin) && $infoStaff->adm_isadmin == 1){
			foreach ($allRole as $key => $value) {
				$staffRole[$key]['info']	= $value['info'];
				$staffRole[$key]['sub']		= $value['sub'];
				// Bóc tiếp để lấy list quyền con bên trong
				foreach ($value['sub'] as $key_sub => $value_sub) {
					$key_sub 	= trim($key_sub);

					if(isset($value_sub['keys']) && $value_sub['keys']){
						$temp 	= explode(",", $value_sub['keys']);
						foreach ($temp as $value_key) {
							if(!isset($staffRole[$key]['sub'][$value_key])){
								$staffRole[$key]['sub'][$value_key] = 1;
							}
						}
					}
				}
			}

			return $staffRole;
		}

		$roleTemp = [];
		$staffRole 	= json_decode($infoStaff->adm_json_role, 1);
		// Lặp trả kết quả phù hợp
		foreach ($staffRole as $key => $value) {
			$roleTemp[$key] 	= [];
			foreach ($value as $key_sub ) {
				$roleTemp[$key][$key_sub] = $key_sub;
			}
		}

		$dataRole = [];
		// Lặp trả kết quả phù hợp
		if($roleTemp){
			foreach ($roleTemp as $key => $value) {
				if(isset($allRole[$key])){
					$dataRole[$key]['info']	= $allRole[$key]['info'];
					$dataRole[$key]['sub']	= [];
					// Bóc tiếp để lấy list quyền của module này
					if($value){
						foreach ($value as $key_sub => $value_sub) {
							$key_sub 	= trim($key_sub);
							if(isset($allRole[$key]['sub'][$key_sub])){

								$dataRole[$key]['sub'][$key_sub] = $allRole[$key]['sub'][$key_sub];
								if(isset($allRole[$key]['sub'][$key_sub]['keys']) && $allRole[$key]['sub'][$key_sub]['keys']){
									$temp 	= explode(",", $allRole[$key]['sub'][$key_sub]['keys']);
									foreach ($temp as $value_key) {
										if(!isset($dataRole[$key]['sub'][$value_key])){
											$dataRole[$key]['sub'][$value_key] = 1;
										}
									}
								}
							}
	    				}
					}
				}
			}
		}


		// Lặp tiếp để lấy các quyền không check quyền
		foreach ($allRole as $key => $value) {
			if(isset($dataRole[$key])){
				$staffRole[$key] 	= $dataRole[$key];
			}else{
				if($value['info']['check_pers'] != 1){
					$staffRole[$key]			= [];
					$staffRole[$key]['info']	= $value['info'];
					$staffRole[$key]['sub']		= [];
				}
			}

			if(isset($staffRole[$key])){
				// Lặp để lấy các quyền con không check quyền
				if(isset($value['sub'])){
					foreach ($value['sub'] as $key_sub => $value_sub) {
						if($value_sub['check_pers'] != 1){
							$staffRole[$key]['sub'][$key_sub] = $value_sub;
							if(isset($allRole[$key]['sub'][$key_sub]['keys']) && $allRole[$key]['sub'][$key_sub]['keys']){
								$temp 	= explode(",", $allRole[$key]['sub'][$key_sub]['keys']);
								foreach ($temp as $value_key) {
									if(!isset($staffRole[$key]['sub'][$value_key])){
										$staffRole[$key]['sub'][$value_key] = 1;
									}
								}
							}
						}
					}
				}
			}
		}

		return $staffRole;
	}

	/**
     * [getMenuLogged description]
     * @return [type] [description]
     */
    public static function getMenuLogged(){
		$dataReturn		= [];
		$allMenuRole	= app()->USER_ROLE;

		foreach($allMenuRole as $key => $value){
			if(isset($value['info']['is_menu']) && $value['info']['is_menu'] == 1){
				$dataReturn[$key] 	= $value;
				foreach($value['sub'] as $key_sub => $value_sub){
					// Xóa bỏ những item không phải là menu
					if(!isset($value_sub['is_menu']) || $value_sub['is_menu'] != 1){
						unset($dataReturn[$key]['sub'][$key_sub]);
					}
				}
			}
		}

    	return $dataReturn;
    }

	public static function checkPermission($module, $per = ''){

    	if(empty($module)) return false;
    	$infoStaff		= app()->USER_INFO;
    	$staffRole 		= app()->USER_ROLE;

    	if(isset($infoStaff->adm_isadmin) && $infoStaff->adm_isadmin == 1) return true;
    	// Là nhân viên thường thì lặp để check
    	// Check quyền module
		if(isset($staffRole[$module])){
			// Check quyền action
			if(!empty($per)){
				// Không thuộc mục nào thì phắn
				if(isset($staffRole[$module]['sub'][$per])) return true;
			}else{
				return true;
			}
		}

    	return false;
    }

    public static function checkPermissionSave($module, $per = ''){

    	if(empty($module)) return false;
    	$infoStaff		= app()->USER_INFO;

    	$adminUser 		= new Model;
    	// Lấy all permisstion
    	$allRole	= $adminUser->getAllMenuRoleAdmin();
    	// Không thuộc mục nào thì phắn
    	if(!isset($allRole[$module])) return false;

		// Nếu là admin tổng thì cho vào luôn
    	if(isset($infoStaff->adm_isadmin) && $infoStaff->adm_isadmin == 1){
    		return true;
    	}

    	// Là nhân viên thường thì lặp để check
    	if(isset($infoStaff->adm_json_role)){
    		$staffRole 	= json_decode($infoStaff->adm_json_role, 1);

    		// Check quyền module
    		if(isset($staffRole[$module]) || $allRole[$module]['check_pers'] != 1){
    			// Check quyền action
    			if(!empty($per)){
    				// Không thuộc mục nào thì phắn
    				if(!isset($allRole[$module]['sub'][$per])) return false;

    				if( $allRole[$module]['sub'][$per]['check_pers'] != 1 || (isset($staffRole[$module]) && isset($staffRole[$module][$per]) && $staffRole[$module][$per] == 1)){
    					return true;
    				}else{
    					return false;
    				}
    			}
    			return true;
    		}
    	}

    	return false;
    }

    public static function changePassLogged($newpassword){
    	$infoStaff		= app()->USER_INFO;
    	$dataReturn 	= self::changePass(app()->USER_INFO->adm_id, $newpassword);
    	if($dataReturn['status'] == true){
    		// Gán lại sesstion
    		self::setLogged(app()->USER_INFO->adm_loginname, $newpassword);
    	}

    	return $dataReturn;
    }

    public static function changePass($adminId, $newpassword){
    	$dataReturn = ['status' => false, 'msg' => ""];
    	if($adminId <= 0 || $newpassword == ''){
    		$dataReturn['msg'] 	= "Staff not found or New password is empty";
    		return $dataReturn;
    	}

    	$adminUser 	= new Model;
    	$result 	= $adminUser->changePass($adminId, $newpassword);
    	if(isset($result['status']) && $result['status']){
    		$dataReturn['status'] = true;
    	}else{
    		$dataReturn['msg'] = isset($result['msg']) ? $result['msg'] : 'ChangePass fail!';
    	}

    	return $dataReturn;
    }
}
