<?php
namespace App\Helper;

use App\Repositories\UserTranslateRepository as Model;
use Request;
use Response;
use Cookie;
use DB;
class UserTranslate
{
	const LANGUAGE_VI = 'vi';
	const LANGUAGE_EN = 'en';
   	public function __construct(){

   	}

   	public static function getLanguages(){
   		return [self::LANGUAGE_VI => 'Tiếng Việt', self::LANGUAGE_EN => 'Tiếng Anh'];
   	}

   	public static function getLanguage($lang){
   		$allLang = self::getLanguages();
   		return isset($allLang[$lang]) ? $allLang[$lang] : $lang;
   	}

   	public static function getAllTranslate($lang = ''){

		$user_translate	= new Model;
		$where	= [];
		if(empty($lang)) $lang = self::getLangCurrent();

		$data	= $user_translate->getAll($where);

 		$arrayTranslate 	= [];
		foreach ($data as $key => $value) {
			switch ($lang) {
				case 'vi':
					$arrayTranslate[$value->ust_keyword_md5]	= $value->ust_vi;
					break;

				case 'en':
					$arrayTranslate[$value->ust_keyword_md5]	= $value->ust_en;
					break;
			}
		}

		return $arrayTranslate;
   	}

   	public static function translate($keyword){

   		$allTranslate	= app()->USER_TRANSLATE ? app()->USER_TRANSLATE : [];

   		$variable = trim($keyword);
		$variable = str_replace("\'","'",$variable);
		$variable = str_replace("'","''",$variable);

		if (isset($allTranslate[md5(trim($variable))])){
			if($allTranslate[md5(trim($variable))] !=''){
				return $allTranslate[md5(trim($variable))];
			}else{
				return "";
			}
		}else{
			// Lưu database
			$data = [
				'keyword'	=> $variable,
				'vi'		=> $variable,
				'en'		=> $variable,
			];
			$user_translate	= new Model;
			$dataReturn		= $user_translate->add($data);
		}
   	}
   	/*public static function getLangCurrent(){
   		$lang = Cookie::get('LANGUAGE_CURRENT', 'vi');
   		return $lang;
   	}

   	public static function setLangCurrent($lang){
		$minutes = 43200;
		Cookie::queue(Cookie::forget('LANGUAGE_CURRENT'));
		Cookie::queue(Cookie::make('LANGUAGE_CURRENT', $lang, $minutes));
   	}*/

   	public static function getLangCurrent(){
   		$lang = isset($_COOKIE['LANGUAGE_CURRENT']) ? $_COOKIE['LANGUAGE_CURRENT'] : '';
   		return $lang;
   	}

   	public static function setLangCurrent($lang){
		$time  = time() + 2592000;
		setcookie('LANGUAGE_CURRENT', $lang, $time, '/');
   	}
}
