<?php
namespace App\Helper;

use App\Repositories\BannerRepository as Model;
use App\Transformers\BannerTransformer;
use Request;
use App\Helper\Upload;
class Banner
{
	public static $arrType		= [1 => "Banner Ảnh", 2 => "Banner Flash", 3 => "Banner HTML"];
	public static $arrPosition	= [1 => "Banner trang chủ",  2 => "Banner Footer", 3 => "Allbum ảnh", 4 => "Videos"];
	public static $arrTarget	= ["_blank"=> "Trang mới", "_self"	=> "Hiện hành",];

   public function __construct(){

   }

   public static function getPictureUrl($filename, $type = 'fullsize'){
   	$url 		= Upload::getUrlImage('banner', $filename);
   	return $url;
   }
}
