<?php
namespace App\Helper;

use App\Repositories\ProductRepository as Model;
use App\Transformers\ProductTransformer;
use Request;
use App\Helper\Upload;
class Product
{
	public static $arrType	= ['0' => 'Sản phẩm', '1' => 'Voucher'];
    public function __construct(){

   }

   public static function getPictureUrl($filename){
   	$url 		= Upload::getUrlImage('product', $filename);
   	return $url;
   }

   public static function getAllPicture($dataPicture = '', $getFull = 1){
    	$arrReturn 	= [];

    	if($dataPicture){
    		$temp 	= json_decode(base64_decode($dataPicture), true);

    		if($temp){
    			foreach ($temp as $value) {
					$filename	= isset($value['filename']) ? trim($value['filename']) : '';
					$is_main		= isset($value['is_main']) ? intval($value['is_main']) : 0;
					$url			= Upload::getUrlImage('product', $filename);
    				if(!empty($filename) && !empty($url)){
    					if($getFull == 1){
    						$arrReturn[] 	= ['filename' => $filename, 'url' => $url, 'is_main' => $is_main];
    					}else{
    						$arrReturn[] 	= $filename;
    					}
    				}
    			}
    		}
    	}

    	return $arrReturn;
   }

   public static function generatePictureJson($dataPicture = [], $pictureMain = ''){
    	$arrReturn 	= [];
    	if($dataPicture){

			foreach ($dataPicture as $value) {
				$filename	= trim($value);
				$url			= Upload::getUrlImage('product', $filename);
				$is_main		= 0;
				if(!empty($filename) && !empty($url)){
					if($pictureMain != "" && $pictureMain == $filename) $is_main = 1;
					$arrReturn[] 	= ['filename' => $filename, 'url' => $url, 'is_main' => $is_main];
				}
			}
    	}

    	if($arrReturn) return base64_encode(json_encode($arrReturn));

    	return '';
   }
}
