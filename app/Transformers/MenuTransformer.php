<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 10/1/18
 * Time: 12:50
 */

namespace App\Transformers;

use App\Helper\Transformers\TransformerAbstract;

class MenuTransformer extends TransformerAbstract
{
	public $defaultIncludes = [];

	public $availableIncludes = [
		'childs', 'parent', 'icon'
	];

	public function transform(&$menu)
	{
		if(!isset($menu) || !$menu || $menu->mnu_id == null){
			$this->defaultIncludes	=	[];
			return [];
		}

		$data 	= [];
		if(isset($menu->mnu_id)) $data['id']							= (int) $menu->mnu_id;
		if(isset($menu->mnu_name)) $data['name']						= (string) $menu->mnu_name;
		if(isset($menu->mnu_parent_id)) $data['parent_id']			= (int) $menu->mnu_parent_id;
		if(isset($menu->mnu_link)) $data['link']						= (string) $menu->mnu_link;
		if(isset($menu->mnu_icon)) $data['icon']						= (string) $menu->mnu_icon;
		if(isset($menu->mnu_scroll)) $data['scroll']					= (string) $menu->mnu_scroll;
		if(isset($menu->mnu_target)) $data['target']					= (string) $menu->mnu_target;
		if(isset($menu->mnu_active)) $data['active']					= (int) $menu->mnu_active;
		if(isset($menu->mnu_position)) $data['position']			= (string) $menu->mnu_position;
		if(isset($menu->mnu_create_time)) $data['create_time']	= (int) $menu->mnu_create_time;
		if(isset($menu->mnu_order)) $data['order']					= (int) $menu->mnu_order;
		if(isset($menu->mnu_staff_id)) $data['staff_id']			= (int) $menu->mnu_staff_id;

		$data = $this->filterFromFields($data);

		return $data;
	}
}