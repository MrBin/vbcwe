<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 10/1/18
 * Time: 12:50
 */

namespace App\Transformers;

use App\Helper\Transformers\TransformerAbstract;

class AdminUserTransformer extends TransformerAbstract
{
	public $defaultIncludes		= [];
	public $availableIncludes	= [];

	public function transform(&$admin_user)
	{
		if(!isset($admin_user) || $admin_user->adm_id == null){
			$this->defaultIncludes	=	[];
			return [];
		}

		$data 	= [];
		if(isset($admin_user->adm_id)) $data['id']					= (int) $admin_user->adm_id;
		if(isset($admin_user->adm_loginname)) $data['loginname']	= (string) $admin_user->adm_loginname;
		if(isset($admin_user->adm_name)) $data['name']				= (string) $admin_user->adm_name;
		if(isset($admin_user->adm_email)) $data['email']			= (string) $admin_user->adm_email;
		if(isset($admin_user->adm_phone)) $data['phone']			= (string) $admin_user->adm_phone;
		if(isset($admin_user->adm_address)) $data['address']		= (int) $admin_user->adm_address;
		if(isset($admin_user->adm_active)) $data['active']			= (int) $admin_user->adm_active;
		if(isset($admin_user->adm_json_role)){
			$role				= $admin_user->adm_json_role ? json_decode($admin_user->adm_json_role, true) : [];
			$data['role']	= $role;
		}

		$data	= $this->filterFromFields($data);

		return $data;
	}
}