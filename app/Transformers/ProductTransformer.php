<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 10/1/18
 * Time: 12:50
 */

namespace App\Transformers;

use App\Helper\Transformers\TransformerAbstract;
use App\Helper\Product;
use App\Transformers\CategoryTransformer;
use App\Transformers\ProductPictureTransformer;

class ProductTransformer extends TransformerAbstract
{
	public $defaultIncludes = ['category'];

	public $availableIncludes = [
		'childs', 'parent', 'products', 'picture', 'category'
	];

	public function transform(&$product)
	{
		if(!isset($product) || $product->pro_id == null){
			$this->defaultIncludes	=	[];
			return [];
		}

		$data 	= [];
		if(isset($product->pro_id))	$data['id']										= (int) $product->pro_id;
		if(isset($product->pro_name))	$data['name']									= (string) $product->pro_name;
		if(isset($product->pro_name_rewrite))	$data['name_rewrite']			= (string) $product->pro_name_rewrite;
		if(isset($product->pro_meta_title))	$data['meta_title']					= (string) $product->pro_meta_title;
		if(isset($product->pro_meta_keyword))	$data['meta_keyword']			= (string) $product->pro_meta_keyword;
		if(isset($product->pro_meta_description))	$data['meta_description']	= (string) $product->pro_meta_description;
		if(isset($product->pro_quantity))	$data['quantity']						= (int) $product->pro_quantity;
		if(isset($product->pro_price))	$data['price']								= (int) $product->pro_price;
		if(isset($product->pro_sale_price))	$data['sale_price']					= (int) $product->pro_sale_price;
		if(isset($product->pro_active))	$data['active']							= (int) $product->pro_active;
		if(isset($product->pro_type))	$data['type']									= (string) $product->pro_type;
		if(isset($product->pro_show))	$data['show']									= (int) $product->pro_show;
		if(isset($product->pro_hot))	$data['hot']									= (int) $product->pro_hot;
		if(isset($product->pro_create_time))	$data['create_time']				= (int) $product->pro_create_time;
		if(isset($product->pro_order))	$data['order']								= (int) $product->pro_order;
		if(isset($product->pro_picture)) $data['picture'] 							= array('small' 	=> Product::getPictureUrl($product->pro_picture),
																												'medium' => Product::getPictureUrl($product->pro_picture),
																												'fullsize' => Product::getPictureUrl($product->pro_picture)
																												);
		$data = $this->filterFromFields($data);

		return $data;
	}

	public function includePicture($row)
	{
		return $this->item($row->pro_picture, new ProductPictureTransformer());
	}

	public function includeCategory($row)
	{
		$row->category =	$row->category ?? [];

		return $this->item($row->category, new CategoryTransformer());
	}
}