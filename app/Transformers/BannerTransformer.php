<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 10/1/18
 * Time: 12:50
 */

namespace App\Transformers;

use App\Helper\Transformers\TransformerAbstract;
use App\Helper\Banner;
class BannerTransformer extends TransformerAbstract
{
	public $defaultIncludes		= [];
	public $availableIncludes	= [];

	public function transform(&$banner)
	{
		if(!isset($banner) || $banner->ban_id == null){
			$this->defaultIncludes	=	[];
			return [];
		}

		$data 	= [];
		if(isset($banner->ban_id))	$data['id']							= (int) $banner->ban_id;
		if(isset($banner->ban_name))	$data['name']					= (string) $banner->ban_name;
		if(isset($banner->ban_picture))	$data['picture']			= \App\Helper\Banner::getPictureUrl($banner->ban_picture);
		if(isset($banner->ban_position))	$data['position']			= (string) $banner->ban_position;
		if(isset($banner->ban_target))	$data['target']			= (string) $banner->ban_target;
		if(isset($banner->ban_type))	$data['type']					= (string) $banner->ban_type;
		if(isset($banner->ban_link))	$data['link']					= (string) $banner->ban_link;
		if(isset($banner->ban_description))	$data['description']	= (int) $banner->ban_description;
		if(isset($banner->ban_start_time))	$data['start_time']	= (int) $banner->ban_start_time;
		if(isset($banner->ban_end_time))	$data['end_time']			= (int) $banner->ban_end_time;
		if(isset($banner->ban_active))	$data['active']			= (int) $banner->ban_active;
		if(isset($banner->ban_date))	$data['create_time']			= (int) $banner->ban_date;

		$data = $this->filterFromFields($data);

		return $data;
	}
}