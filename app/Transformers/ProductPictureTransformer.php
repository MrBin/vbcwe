<?php
namespace App\Transformers;

use App\Helper\Transformers\TransformerAbstract;
class ProductPictureTransformer extends TransformerAbstract
{

	public function transform($pic)
	{
		// Các key cần filter theo fields truyền trên đường dẫn
		$data = [
			'mini'	=>	'1111', //pictureProductThumb($pic, 50, 50)
			'small'	=>	'1111', //pictureProductThumb($pic, 120, 120)
			'medium'	=>	'1111', //pictureProductThumb($pic, 320, 320)
			'larger'	=>	'1111', //pictureProductThumb($pic, 400, 400)
			'fullsize'	=>	'1111', //pictureProductFullsize($pic)
		];

		$data = $this->filterFromFields($data);
		return $data;
	}

}