<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 10/1/18
 * Time: 12:50
 */

namespace App\Transformers;

use App\Helper\Transformers\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
	public $defaultIncludes = [];

	public $availableIncludes = [
		'childs', 'parent', 'products', 'picture', 'icon'
	];

	public function transform(&$category)
	{
		if(!isset($category) || !$category || $category->cat_id == null){
			$this->defaultIncludes	=	[];
			return [];
		}

		$data 	= [];
		if(isset($category->cat_id)) $data['id']										= (int) $category->cat_id;
		if(isset($category->cat_name)) $data['name']									= (string) $category->cat_name;
		if(isset($category->cat_parent_id)) $data['parent_id']					= (int) $category->cat_parent_id;
		if(isset($category->cat_name_rewrite)) $data['name_rewrite']			= (string) $category->cat_name_rewrite;
		if(isset($category->cat_picture)) $data['picture']							= (string) $category->cat_picture;
		if(isset($category->cat_meta_title)) $data['meta_title']					= (string) $category->cat_meta_title;
		if(isset($category->cat_meta_keyword)) $data['meta_keyword']			= (string) $category->cat_meta_keyword;
		if(isset($category->cat_meta_description)) $data['meta_description']	= (string) $category->cat_meta_description;
		if(isset($category->cat_active)) $data['active']							= (int) $category->cat_active;
		if(isset($category->cat_type)) $data['type']									= (string) $category->cat_type;
		if(isset($category->cat_show)) $data['show']									= (int) $category->cat_show;
		if(isset($category->cat_hot)) $data['hot']									= (int) $category->cat_hot;
		if(isset($category->cat_create_time)) $data['create_time']				= (int) $category->cat_create_time;
		if(isset($category->cat_order)) $data['order']								= (int) $category->cat_order;


		$data = $this->filterFromFields($data);

		return $data;
	}

	/*public function includeProducts($row)
	{
		$row->products =	$row->products ?? [];
		return $this->collection($row->products, new ProductTransformer());
	}*/
}