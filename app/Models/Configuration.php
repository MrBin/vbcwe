<?php
namespace App\Models;
use App\Models\Model;
class Configuration extends Model
{

	protected $table		= 'configuration';
	protected $primaryKey	= 'con_id';
	protected $guarded		= [];
	//protected $fillable	= [];
	public $timestamps		= false;
	public $prefix			= 'con_';
	const FIELD_FOR_DEFAULT	= ['con_id'];
	const FIELD_FOR_LISTING	= ['*'];
	const FIELD_FOR_DETAIL	= ['*'];
}
?>

