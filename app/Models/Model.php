<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{

	const DB_SERVER 	= 'slaves';

	protected 	$arrJoinRelation	= [];
	protected static $unguarded		= true;

	const FIELD_FOR_LISTING			= [];

	function __construct(array $attributes = [])
	{
		parent::__construct($attributes);
		//$this->setConnection(static::DB_SERVER);
	}

	public function setConnection($name){
		return parent::setConnection($name);
	}

	/*public function scopeSelectFieldListing($query)
	{
		if(is_array(static::FIELD_FOR_LISTING) && !empty(static::FIELD_FOR_LISTING))
		{
			$query->select(static::FIELD_FOR_LISTING);
		}
		return $query;
	}

	static public function scopeUseForceHistory($query)
	{
		return $query->getModel()->setConnection('force_history');
	}

	static public function scopeUseSlave($query)
	{
		return $query->getModel()->setConnection('slaves');
	}

	static public function scopeUseMaster($query)
	{
		return $query->getModel()->setConnection('master');
	}

	public function newInstance($attributes = [], $exists = false)
	{
		$model 	=	parent::newInstance($attributes, $exists);
		$model->setTable($this->getTable());

		return $model;
	}*/
}