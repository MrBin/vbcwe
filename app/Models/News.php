<?php
namespace App\Models;

use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use App\Models\Model;

class News extends Model implements Searchable
{

	protected $table			= 'news_multi';
	protected $primaryKey		= 'new_id';
	protected $guarded			= ['new_isadmin'];
	//protected $fillable		= [];
	public $timestamps			= false;
	public $prefix				= 'new_';
	public $defaultFieldsSelect	= ['new_id', 'new_title', 'new_category_id'];

	const FIELD_FOR_DEFAULT		= ['new_id', 'new_title', 'new_category_id'];
	const KEY_PRODUCT			= 'new_id';
	const KEY_CATEGORY			= 'new_category_id';
	public $searchableType      = 'Tin tức';

	/* RELATION SHIP FUNCTION */
	public function category(){
		return $this->hasOne(
			\App\Models\Category::class,
			'cat_id',
			static::KEY_CATEGORY
		)->where('cat_type', '=', 'news')->select(\App\Models\Category::FIELD_FOR_DEFAULT);
	}

	public static function getNewsHome(){
        return static::with(['category'])->where('new_active', 1)->orderBy('new_create_time', 'DESC')->limit(6)->get();
	}

	public static function getNewsDetail($id){
        return static::where('new_active', 1)->where('new_id', $id)->get();
	}

    public function getSearchResult(): SearchResult
    {
        $url = route('search','name=' . $this->new_id);

        return new SearchResult(
            $this,
            $this->new_title,
            $url
        );
    }
}
?>

