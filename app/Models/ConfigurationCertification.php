<?php
namespace App\Models;
use App\Models\Model;
class ConfigurationCertification extends Model
{

	protected $table		= 'configuration_certification';
	protected $primaryKey	= 'coc_id';
	protected $guarded		= [];
	//protected $fillable	= [];
	public $timestamps		= false;
	public $prefix			= 'coc_';
	const FIELD_FOR_DEFAULT	= ['coc_id'];
	const FIELD_FOR_LISTING	= ['*'];
	const FIELD_FOR_DETAIL	= ['*'];
}
?>

