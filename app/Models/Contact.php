<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table			= 'contact';
	protected $primaryKey		= 'con_id';
	protected $guarded			= [];
	//protected $fillable		= [];
	public $timestamps			= false;
	public $prefix				= 'con_';
	public $defaultFieldsSelect	= ['*'];
	const FIELD_FOR_DEFAULT		= ['*'];

}
