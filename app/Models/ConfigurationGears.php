<?php
namespace App\Models;
use App\Models\Model;
class ConfigurationGears extends Model
{

	protected $table		= 'configuration_gears';
	protected $primaryKey	= 'cog_id';
	protected $guarded		= [];
	//protected $fillable	= [];
	public $timestamps		= false;
	public $prefix			= 'cog_';
	const FIELD_FOR_DEFAULT	= ['cog_id'];
	const FIELD_FOR_LISTING	= ['*'];
	const FIELD_FOR_DETAIL	= ['*'];
}
?>

