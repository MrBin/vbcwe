<?php
namespace App\Models;
use App\Models\Model;
class Partner extends Model
{

	protected $table			= 'partner';
	protected $primaryKey	= 'par_id';
	protected $guarded		= [];
	//protected $fillable		= [];
	public $timestamps		= false;
	public $prefix				= 'par_';

	const FIELD_FOR_DEFAULT			= ['par_id', 'par_name', 'par_name_en', 'par_picture', 'par_link'];
	const FIELD_FOR_LISTING			= ['par_id', 'par_name', 'par_name_en', 'par_picture', 'par_link', 'par_active', 'par_order', 'par_create_time', 'par_update_at', 'staff_id'];
	const FIELD_FOR_DETAIL			= ['*'];
}
?>

