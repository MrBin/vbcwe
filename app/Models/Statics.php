<?php
namespace App\Models;
use App\Models\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

//use Illuminate\Database\Eloquent\Model;
class Statics extends Model implements Searchable
{

	protected $table			= 'statics_new';
	protected $primaryKey		= 'sta_id';
	protected $guarded			= [];
	//protected $fillable		= [];
	public $timestamps			= false;
	public $prefix				= 'sta_';
	public $defaultFieldsSelect	= ['sta_id', 'sta_title', 'sta_title_en', 'sta_name_rewrite'];
	const FIELD_FOR_DEFAULT		= ['sta_id', 'sta_title', 'sta_title_en', 'sta_name_rewrite'];
	const KEY_PRODUCT			= 'sta_id';
	const KEY_CATEGORY			= 'sta_category_id';
	public $searchableType      = 'Kiến thức tham khảo';

	/* RELATION SHIP FUNCTION */
	public function category(){
		return $this->hasOne(
			\App\Models\Category::class,
			'cat_id',
			static::KEY_CATEGORY
		)->where('cat_type', '=', 'static')->select(\App\Models\Category::FIELD_FOR_DEFAULT);
	}

	public static function getServicesDetail($id){
        return static::where('sta_status', 1)->where('sta_id', $id)->first();
	}

    public function getSearchResult(): SearchResult
    {
        $url = route('search', 'name=' . $this->sta_id);

        return new SearchResult(
            $this,
            $this->sta_title,
            $url
        );
    }
}
?>

