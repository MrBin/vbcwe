<?php
namespace App\Models;
use App\Models\Model;
class ConfigurationIntroduce extends Model
{

	protected $table		= 'configuration_introduce';
	protected $primaryKey	= 'coi_id';
	protected $guarded		= [];
	//protected $fillable	= [];
	public $timestamps		= false;
	public $prefix			= 'coi_';
	const FIELD_FOR_DEFAULT	= ['coi_id'];
	const FIELD_FOR_LISTING	= ['*'];
	const FIELD_FOR_DETAIL	= ['*'];
}
?>

