<?php
namespace App\Models;
use App\Models\Model;
class Category extends Model
{

	protected $table			= 'categories_multi';
	protected $primaryKey	= 'cat_id';
	protected $guarded		= [];
	//protected $fillable		= [];
	public $timestamps				= false;
	public $prefix						= 'cat_';
	const FIELD_FOR_DEFAULT			= ['cat_id', 'cat_name', 'cat_name_en', 'cat_name_rewrite'];
	const FIELD_FOR_LISTING			= ['cat_id', 'cat_name', 'cat_name_en', 'cat_parent_id', 'cat_name_rewrite', 'cat_picture', 'cat_meta_title', 'cat_meta_keyword', 'cat_meta_description', 'cat_active', 'cat_type', 'cat_show', 'cat_hot', 'cat_create_time', 'cat_order'];
	const FIELD_FOR_DETAIL			= ['*'];
}
?>

