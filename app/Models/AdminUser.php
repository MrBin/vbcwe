<?php
namespace App\Models;
use App\Models\Model;

class AdminUser extends Model
{

	protected $table			= 'admin_user';
	protected $primaryKey	= 'adm_id';
	protected $guarded		= ['adm_isadmin'];
	//protected $fillable		= [];
	public $timestamps		= false;
	public $prefix				= 'adm_';
	public $defaultFieldsSelect = ['adm_id', 'adm_name', 'adm_category_id'];
}
?>

