<?php
namespace App\Models;
use App\Models\Model;
class Banner extends Model
{

	protected $table			= 'banners';
	protected $primaryKey	= 'ban_id';
	protected $guarded		= [];
	//protected $fillable		= [];
	public $timestamps		= false;
	public $prefix				= 'ban_';

	const FIELD_FOR_DEFAULT			= ['ban_id', 'ban_name', 'ban_picture', 'ban_link', 'ban_name_rewrite'];
	const FIELD_FOR_LISTING			= ['ban_id', 'ban_name', 'ban_name_rewrite', 'ban_picture', 'ban_link', 'ban_description', 'ban_target', 'ban_type', 'ban_position', 'ban_date', 'ban_active', 'ban_order', 'ban_end_time', 'ban_staff_id'];
	const FIELD_FOR_DETAIL			= ['*'];
}
?>

