<?php
namespace App\Models;
use App\Models\Model;
class Opinion extends Model
{

	protected $table		= 'opinion';
	protected $primaryKey	= 'opi_id';
	protected $guarded		= [];
	//protected $fillable		= [];
	public $timestamps		= false;
	public $prefix				= 'opi_';

	const FIELD_FOR_DEFAULT			= ['opi_id', 'opi_name', 'opi_title', 'opi_lang'];
	const FIELD_FOR_LISTING			= ['opi_id', 'opi_name', 'opi_title', 'opi_lang'];
	const FIELD_FOR_DETAIL			= ['*'];
}
?>


