<?php
namespace App\Models;
use App\Models\Model;
class ConfigurationAbout extends Model
{

	protected $table		= 'configuration_about';
	protected $primaryKey	= 'coa_id';
	protected $guarded		= [];
	//protected $fillable	= [];
	public $timestamps		= false;
	public $prefix			= 'coa_';
	const FIELD_FOR_DEFAULT	= ['coa_id'];
	const FIELD_FOR_LISTING	= ['*'];
	const FIELD_FOR_DETAIL	= ['*'];
}
?>

