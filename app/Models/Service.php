<?php
namespace App\Models;
use App\Models\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

//use Illuminate\Database\Eloquent\Model;
class Service extends Model implements Searchable
{

	protected $table			= 'service';
	protected $primaryKey		= 'ser_id';
	protected $guarded			= [];
	//protected $fillable		= [];
	public $timestamps			= false;
	public $prefix				= 'ser_';
	public $defaultFieldsSelect	= ['ser_id', 'ser_title', 'ser_title_en', 'ser_name_rewrite'];
	const FIELD_FOR_DEFAULT		= ['ser_id', 'ser_title', 'ser_title_en', 'ser_name_rewrite'];
	const KEY_PRODUCT			= 'ser_id';
	public $searchableType      = 'Dịch vụ';

	public static function getServicesDetail($id){
        return static::where('ser_status', 1)->where('ser_id', $id)->first();
	}

    public function getSearchResult(): SearchResult
    {
        $url = route('search', 'name=' . $this->ser_id);

        return new SearchResult(
            $this,
            $this->ser_title,
            $url
        );
    }
}
?>

