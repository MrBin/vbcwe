<?php
namespace App\Models;
use App\Models\Model;

class Users extends Model
{

	protected $table			= 'users';
	protected $primaryKey		= 'use_id';
	protected $guarded			= [];
	//protected $fillable		= [];
	public $timestamps			= false;
	public $prefix				= 'use_';
	public $defaultFieldsSelect	= ['use_id', 'use_name', 'use_loginname', 'use_phone'];
	const FIELD_FOR_DEFAULT 	= ['use_id', 'use_name', 'use_loginname', 'use_phone'];
}
?>

