<?php
namespace App\Models;
use App\Models\Model;
class Founder extends Model
{

	protected $table		= 'founder';
	protected $primaryKey	= 'fou_id';
	protected $guarded		= [];
	//protected $fillable		= [];
	public $timestamps		= false;
	public $prefix				= 'fou_';

	const FIELD_FOR_DEFAULT			= ['fou_id', 'fou_name', 'fou_picture', 'fou_link'];
	const FIELD_FOR_LISTING			= ['fou_id', 'fou_name', 'fou_name_en', 'fou_picture', 'fou_link', 'fou_description', 'fou_description_en', 'fou_chucvu', 'fou_chucvu_en', 'fou_active', 'fou_order', 'fou_create_time', 'fou_update_at', 'staff_id'];
	const FIELD_FOR_DETAIL			= ['*'];
}
?>


