<?php
namespace App\Models;
use App\Models\Model;

class UserTranslate extends Model
{

	protected $table			= 'user_translate';
	protected $primaryKey		= 'ust_id';
	protected $guarded			= [];
	//protected $fillable		= [];
	public $timestamps			= false;
	public $prefix				= 'ust_';
	public $defaultFieldsSelect	= ['ust_id', 'ust_keyword', 'ust_vi', 'ust_en'];

	const FIELD_FOR_DEFAULT		= ['ust_id', 'ust_keyword', 'ust_vi', 'ust_en'];
}
?>