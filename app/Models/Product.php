<?php
namespace App\Models;
use App\Models\Model;
//use Illuminate\Database\Eloquent\Model;
class Product extends Model
{

	protected $table			= 'product';
	protected $primaryKey		= 'pro_id';
	protected $guarded			= [];
	//protected $fillable		= [];
	public $timestamps			= false;
	public $prefix				= 'pro_';
	public $defaultFieldsSelect	= ['pro_id', 'pro_title', 'pro_title_en'];
	const FIELD_FOR_DEFAULT		= ['pro_id', 'pro_title', 'pro_title_en'];
	const KEY_PRODUCT			=	'pro_id';
	const KEY_CATEGORY			=	'pro_category_id';
}
?>

