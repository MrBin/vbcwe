<?php
namespace App\Models;
use App\Models\Model;
class EnterpriceNetwork extends Model
{

	protected $table		= 'enterprice_network';
	protected $primaryKey	= 'enn_id';
	protected $guarded		= [];
	//protected $fillable		= [];
	public $timestamps		= false;
	public $prefix				= 'enn_';

	const FIELD_FOR_DEFAULT			= ['enn_id', 'enn_name', 'enn_picture', 'enn_link'];
	const FIELD_FOR_LISTING			= ['enn_id', 'enn_name', 'enn_name_en', 'enn_picture', 'enn_link', 'enn_description', 'enn_description_en', 'enn_active', 'enn_order', 'enn_create_time', 'enn_update_at', 'staff_id'];
	const FIELD_FOR_DETAIL			= ['*'];
}
?>


