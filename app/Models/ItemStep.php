<?php
namespace App\Models;
use App\Models\Model;

//use Illuminate\Database\Eloquent\Model;
class ItemStep extends Model
{

	protected $table			= 'item_step';
	protected $primaryKey		= 'its_id';
	protected $guarded			= [];
	//protected $fillable		= [];
	public $timestamps			= false;
	public $prefix				= 'its_';
	public $defaultFieldsSelect	= ['its_id', 'its_title', 'its_title_en'];
	const FIELD_FOR_DEFAULT		= ['its_id', 'its_title', 'its_title_en'];
}
?>

