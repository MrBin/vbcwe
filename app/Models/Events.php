<?php
namespace App\Models;
use App\Models\Model;

class Events extends Model
{

	protected $table			= 'events';
	protected $primaryKey		= 'eve_id';
	protected $guarded			= ['eve_isadmin'];
	//protected $fillable		= [];
	public $timestamps			= false;
	public $prefix				= 'eve_';
	public $defaultFieldsSelect	= ['eve_id', 'eve_title', 'eve_category_id'];

	const FIELD_FOR_DEFAULT		= ['eve_id', 'eve_title', 'eve_category_id'];
	const KEY_PRODUCT			= 'eve_id';
	const KEY_CATEGORY			= 'eve_category_id';

	/* RELATION SHIP FUNCTION */
	public function category(){
		return $this->hasOne(
			\App\Models\Category::class,
			'cat_id',
			static::KEY_CATEGORY
		)->where('cat_type', '=', 'event')->select(\App\Models\Category::FIELD_FOR_DEFAULT);
	}
}
?>

