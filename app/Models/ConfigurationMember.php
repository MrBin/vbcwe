<?php
namespace App\Models;
use App\Models\Model;
class ConfigurationMember extends Model
{

	protected $table		= 'configuration_member';
	protected $primaryKey	= 'com_id';
	protected $guarded		= [];
	//protected $fillable	= [];
	public $timestamps		= false;
	public $prefix			= 'com_';
	const FIELD_FOR_DEFAULT	= ['com_id'];
	const FIELD_FOR_LISTING	= ['*'];
	const FIELD_FOR_DETAIL	= ['*'];
}
?>

