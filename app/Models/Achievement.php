<?php
namespace App\Models;
use App\Models\Model;
//use Illuminate\Database\Eloquent\Model;
class Achievement extends Model
{

	protected $table			= 'thanhtich';
	protected $primaryKey		= 'tht_id';
	protected $guarded			= [];
	//protected $fillable		= [];
	public $timestamps			= false;
	public $prefix				= 'tht_';
	public $defaultFieldsSelect	= ['tht_id', 'tht_title', 'tht_title_en'];
	const FIELD_FOR_DEFAULT		= ['tht_id', 'tht_title', 'tht_title_en'];
}
?>

