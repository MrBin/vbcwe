<?php
namespace App\Http\View;

use Illuminate\View\View;
use Config;
class PageComposer{
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct(){

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view){
        $infoUser  = app()->USER_INFO;
        $menuUser  = Config("usermenu");

        $view->with(['infoUser' => $infoUser, 'menuUser' => $menuUser]);
    }
}