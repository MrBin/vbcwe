<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\News as NewsModel;
use App\Repositories\NewsRepository;
use App\Repositories\CategoryRepository;
use App\Helper\Category as CategoryHelper;
use App\Helper\Upload as UploadHelper;
use Config;

use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class AdminNewsController extends AdminController
{
	public $news_repository;
	public $category_repository;
	public $category_helper;
	public function __construct(NewsRepository $news_repository, CategoryRepository $category_repository, CategoryHelper $category_helper){
		$this->news_repository	= $news_repository;
		$this->category_repository	= $category_repository;
		$this->category_helper	= $category_helper;
	}

  	public function index(Request $request){

		$limit		= $request->input('limit', 25);
		$title		= $request->input('title');
		$category	= $request->input('category');

   		$dataSearch = ['title' => $title, 'category' => $category];

   		$title_search = removeAccent($title);
   		$title_search = strtolower($title_search);
	   	$news 		= NewsModel::where('new_id', '>' , 0);
	   	if($title_search != "") $news = $news->where('new_search', 'LIKE', '%' . $title_search . '%');
	   	if($category > 0) $news = $news->where('new_category_id', '=', $category);

		$news	= $news->with(['category'])->orderBy('new_create_time', 'DESC')->paginate($limit);

   		return  view('admin.news.list', [	'view' 		=> 'list',
											'news'		=> $news,
											'pagination'=> [
																'total'			=> $news->total(),
																'current_page'	=> $news->currentPage(),
																'perpage'		=> $news->perPage(),
																'links'			=> $news->total() > $news->perPage() ? $news->appends($dataSearch)->links() : '',
															],
											'dataSearch'	=> $dataSearch
											]
   										);
   	}

   	public function add(Request $request, $id = 0){

		$redirect	= $request->input('url_redirect', base64_encode(route('admin.news.index')));
		$category_id 			= 0;
		$title					= '';
		$title_en				= '';
		$rewrite 				= '';
		$meta_title				= '';
		$meta_title_en			= '';
		$meta_keyword			= '';
		$meta_keyword_en		= '';
		$meta_description		= '';
		$meta_description_en	= '';
		$teaser 				= '';
		$teaser_en 				= '';
		$description			= '';
		$description_en			= '';
		$picture 				= '';
		$order					= 0;
		$active					= 0;
		$id						= intval($id);

		if($id > 0){
			// Lấy thông tin
			$info = $this->news_repository->getById($id);
			if(empty($info)) die('Record not found !');

			$category_id			= $info->new_category_id;
			$title					= $info->new_title;
			$title_en				= $info->new_title_en;
			$rewrite 				= $info->new_rewrite;
			$meta_title				= $info->new_meta_title;
			$meta_title_en			= $info->new_meta_title_en;
			$meta_keyword			= $info->new_meta_keyword;
			$meta_keyword_en		= $info->new_meta_keyword_en;
			$meta_description		= $info->new_meta_description;
			$meta_description_en	= $info->new_meta_description_en;
			$teaser 				= $info->new_teaser;
			$teaser_en 				= $info->new_teaser_en;
			$description			= $info->new_description;
			$description_en			= $info->new_description_en;
			$picture 				= $info->new_picture;
			$order					= $info->new_order;
			$active					= $info->new_active;
		}

		$category_id			= $request->input('category_id', $category_id);
		$title					= $request->input('title', $title);
		$rewrite 				= removeTitle($title);
		$title_en				= $request->input('title_en', $title_en);
		$meta_title				= $request->input('meta_title', $meta_title);
		$meta_title_en			= $request->input('meta_title_en', $meta_title_en);
		$meta_keyword			= $request->input('meta_keyword', $meta_keyword);
		$meta_keyword_en		= $request->input('meta_keyword_en', $meta_keyword_en);
		$meta_description		= $request->input('meta_description', $meta_description);
		$meta_description_en	= $request->input('meta_description_en', $meta_description_en);
		$teaser					= $request->input('teaser', $teaser);
		$teaser_en				= $request->input('teaser_en', $teaser_en);
		$description			= $request->input('description', $description);
		$description_en			= $request->input('description_en', $description_en);
		$picture				= $request->input('picture', $picture);
		$order					= $request->input('order', $order);
		$picture_data 		 	= UploadHelper::getAllPicture('news', $picture, 1);
		$data = [
			'id' 					=> $id,
			'category_id' 			=> $category_id,
			'title'					=> $title,
			'title_en'				=> $title_en,
			'rewrite'				=> $rewrite,
			'meta_title'			=> $meta_title,
			'meta_title_en'			=> $meta_title_en,
			'meta_keyword'			=> $meta_keyword,
			'meta_keyword_en'		=> $meta_keyword_en,
			'meta_description'		=> $meta_description,
			'meta_description_en'	=> $meta_description_en,
			'teaser'				=> $teaser,
			'teaser_en'				=> $teaser_en,
			'description'			=> $description,
			'description_en'		=> $description_en,
			'picture'				=> $picture,
			'picture_data' 			=> $picture_data,
			'order'					=> $order,
			'active'				=> $active,
		];

		$action		= $request->input('action');
		$errors 	= [];

		$allCategory 	= $this->category_helper::getAllCategory([['cat_type', '=', 'news']], false);

		if($action == 'execute'){
			$data['active'] 	= $request->input('active', 0);

			$dataReturn	= $this->news_repository->add($id, $data);

			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}

   		return  view('admin.news.add', [
										'data' => $data,
										'allCategory' => $allCategory,
										'errors' => $errors
										]
									);
   	}

   	public function delete(Request $request, $id){
		$redirect = $request->input('url_redirect', base64_encode(route('admin.news.index')));

		// Gọi function xóa
		$result = $this->news_repository->delete($id);
		if(isset($result['status']) && $result['status'] == true){
			echo '<script>alert("Delete news success"); window.location.href="' . base64_decode($redirect) . '"</script>';
		}else{
			$error = isset($result['msg']) ? $result['msg'] : '';
		echo '<script>alert("Delelte Error: ' . $error . '"); window.location.href="' . base64_decode($redirect) . '"</script>';
		}
   }


   	public function status(Request $request){
		$redirect	= $request->input('url_redirect', base64_encode(route('admin.news.index')));
		$record_id	= $request->input('record_id', 0);
		$type		= $request->input('type', '');

		// Lấy thông tin
		$info = $this->news_repository->getById($record_id);
		if(empty($info)) return response()->json(array('status'=> false, 'msg'=> 'Record not found'), 200);

		$dataUpdate 	= [];
		switch ($type) {
			case 'vip':
				$status 	= abs($info->new_vip - 1);
				$dataUpdate['vip'] = $status;
				break;

			case 'active':
				$status 	= abs($info->new_active - 1);
				$dataUpdate['active'] = $status;
				break;
		}

		if($dataUpdate){
			$dataReturn	= $this->news_repository->add($record_id, $dataUpdate);

			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return response()->json(array('status'=> true, 'data' => $status), 200);
			}

			$errors = isset($dataReturn['errors']) ? implode(",", $dataReturn['errors']) : '';
		}else{
			$errors = 'Type update not found';
		}

   		return response()->json(array('status'=> false, 'msg'=> $errors), 200);
   	}

    public function category(Request $request){

		$name			= $request->input('name');
		$dataSearch = ['name' => $name];
	   	$param 		= [['cat_type', '=', $this->category_repository::CATEGORY_NEWS]];
	   	if($name != "") $param[] = ['cat_name', 'like', '%'. $name . '%'];
	   	$category 	= $this->category_helper::getAllCategory($param);
	   	$arrType 	= $this->category_repository->getTypeLabels();

	   	return  view('admin.news.category_list', [	'category'		=> $category,
													'dataSearch'	=> $dataSearch,
													'arrType' 		=> $arrType,
													'infoStaff'		=> app()->ADMIN_INFO
													]
	   												);

	}

	public function categoryAdd(Request $request, $id = 0){

		$redirect				= $request->input('url_redirect', base64_encode(route('admin.news.category_add')));
		$name					= '';
		$name_en				= '';
		$name_rewrite			= '';
		$meta_title				= '';
		$meta_title_en			= '';
		$meta_keyword			= '';
		$meta_keyword_en		= '';
		$meta_description		= '';
		$meta_description_en	= '';
		$picture				= '';
		$type					= $this->category_repository::CATEGORY_NEWS;
		$parent_id				= 0;
		$order					= 0;
		$active					= 0;
		$id						= intval($id);

		if($id > 0){
			// Lấy thông tin
			$info = $this->category_repository->getById($id);
			if(empty($info)) die('Record not found !');

			$name					= $info->cat_name;
			$name_en				= $info->cat_name_en;
			$name_rewrite			= $info->cat_name_rewrite;
			$picture				= $info->cat_picture;
			$parent_id				= $info->cat_parent_id;
			$order					= $info->cat_order;
			$meta_title				= $info->cat_meta_title;
			$meta_title_en			= $info->cat_meta_title_en;
			$meta_keyword			= $info->cat_meta_keyword;
			$meta_keyword_en		= $info->cat_meta_keyword_en;
			$meta_description		= $info->cat_meta_description;
			$meta_description_en	= $info->cat_meta_description_en;
			$active					= $info->cat_active;
		}

		$name				= $request->input('name', $name);
		$name_en			= $request->input('name_en', $name_en);
		$name_rewrite		= $request->input('name_rewrite', $name_rewrite);
		if($name_rewrite == "") $name_rewrite = removeTitle($name);
		$picture			= $request->input('picture', $picture);
		$picture_data 		 = UploadHelper::getAllPicture('category', $picture, 1);
		$parent_id			= $request->input('parent_id', $parent_id);
		$order				= $request->input('order', $order);
		$meta_title_en			= $request->input('meta_title_en', $meta_title_en);
		$meta_keyword_en		= $request->input('meta_keyword_en', $meta_keyword_en);
		$meta_description_en	= $request->input('meta_description_en', $meta_description_en);
		$data = [
					'id'					=> $id,
					'name_rewrite'			=> $name_rewrite,
					'name'					=> $name,
					'name_en'				=> $name_en,
					'picture'				=> $picture,
					'picture_data'			=> $picture_data,
					'type'					=> $type,
					'parent_id'				=> $parent_id,
					'order'					=> $order,
					'meta_title'			=> $meta_title,
					'meta_title_en'			=> $meta_title_en,
					'meta_keyword'			=> $meta_keyword,
					'meta_keyword_en'		=> $meta_keyword_en,
					'meta_description'		=> $meta_description,
					'meta_description_en'	=> $meta_description_en,
					'active'				=> $active,
				];

		$allCategory 	= [];
		$temp = $this->category_helper::getAllCategory([['cat_type', '=', $type]]);

		if(isset($temp[$type])) $allCategory 	= $temp[$type];

   		$arrType 	= $this->category_repository::getTypeLabels();

		$action		= $request->input('action');
		$errors 		= [];

		if($action == 'execute'){
			$data['active'] = $request->input('active', 0);
			$dataReturn	= $this->category_repository->add($id, $data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}

   		return  view('admin.news.category_add', [
												'view'			=> 'add',
												'data'			=> $data,
												'allCategory'	=> $allCategory,
												'arrType' 		=> $arrType,
												'errors'		=> $errors
												]
											);
	}

	public function categoryStatus(Request $request){
		$redirect	= $request->input('url_redirect', base64_encode(route('admin.news.category')));
		$record_id	= $request->input('record_id', 0);
		$type		= $request->input('type', '');

		// Lấy thông tin
		$info = $this->category_repository->getById($record_id);
		if(empty($info)) return response()->json(array('status'=> false, 'msg'=> 'Record not found'), 200);

		$dataUpdate 	= [];
		switch ($type) {

			case 'active':
				$status 	= abs($info->cat_active - 1);
				$dataUpdate['active'] = $status;
				break;
		}

		if($dataUpdate){
			$dataReturn	= $this->category_repository->add($record_id, $dataUpdate);

			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return response()->json(array('status'=> true, 'data' => $status), 200);
			}

			$errors = isset($dataReturn['errors']) ? implode(",", $dataReturn['errors']) : '';
		}else{
			$errors = 'Type update not found';
		}

   		return response()->json(array('status'=> false, 'msg'=> $errors), 200);
   	}
}
