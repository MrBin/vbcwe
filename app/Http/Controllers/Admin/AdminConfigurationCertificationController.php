<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\ConfigurationCertificationRepository;
use App\Helper\Upload as UploadHelper;
use Config;

class AdminConfigurationCertificationController extends AdminController
{
	public $configuration_repository;
	public function __construct(ConfigurationCertificationRepository $configuration_repository){
		$this->configuration_repository	= $configuration_repository;
	}

	public function index(Request $request){

		$redirect	= base64_encode(route('admin.configs.certification'));

		// Lấy thông tin
		$info = $this->configuration_repository->getConfiguration();
		if(empty($info)) die('Record not found !');
		$picture_1		= $info->coc_picture_1;
		$picture_1_en	= $info->coc_picture_1_en;
		$text_1			= $info->coc_text_1;
		$text_1_en		= $info->coc_text_1_en;
		$picture_2_1	= $info->coc_picture_2_1;
		$picture_2_1_en	= $info->coc_picture_2_1_en;
		$picture_2_2	= $info->coc_picture_2_2;
		$picture_2_2_en	= $info->coc_picture_2_2_en;
		$text_2_1		= $info->coc_text_2_1;
		$text_2_1_en	= $info->coc_text_2_1_en;
		$text_2_2		= $info->coc_text_2_2;
		$text_2_2_en	= $info->coc_text_2_2_en;
		$picture_3_1	= $info->coc_picture_3_1;
		$picture_3_1_en	= $info->coc_picture_3_1_en;
		$picture_3_2	= $info->coc_picture_3_2;
		$picture_3_2_en	= $info->coc_picture_3_2_en;
		$text_3_1		= $info->coc_text_3_1;
		$text_3_1_en	= $info->coc_text_3_1_en;
		$text_3_2		= $info->coc_text_3_2;
		$text_3_2_en	= $info->coc_text_3_2_en;
		$text_4			= $info->coc_text_4;
		$text_4_en		= $info->coc_text_4_en;
		$text_5_1		= $info->coc_text_5_1;
		$text_5_1_en	= $info->coc_text_5_1_en;
		$text_5_2		= $info->coc_text_5_2;
		$text_5_2_en	= $info->coc_text_5_2_en;
		$text_6_1		= $info->coc_text_6_1;
		$text_6_1_en	= $info->coc_text_6_1_en;
		$text_6_2		= $info->coc_text_6_2;
		$text_6_2_en	= $info->coc_text_6_2_en;
		$text_6_3		= $info->coc_text_6_3;
		$text_6_3_en	= $info->coc_text_6_3_en;
		$text_6_4		= $info->coc_text_6_4;
		$text_6_4_en	= $info->coc_text_6_4_en;

		$picture_1		= $request->input('picture_1', $picture_1);
		$picture_1_en	= $request->input('picture_1_en', $picture_1_en);
		$text_1			= $request->input('text_1', $text_1);
		$text_1_en		= $request->input('text_1_en', $text_1_en);
		$picture_2_1	= $request->input('picture_2_1', $picture_2_1);
		$picture_2_1_en	= $request->input('picture_2_1_en', $picture_2_1_en);
		$picture_2_2	= $request->input('picture_2_2', $picture_2_2);
		$picture_2_2_en	= $request->input('picture_2_2_en', $picture_2_2_en);
		$text_2_1		= $request->input('text_2_1', $text_2_1);
		$text_2_1_en	= $request->input('text_2_1_en', $text_2_1_en);
		$text_2_2		= $request->input('text_2_2', $text_2_2);
		$text_2_2_en	= $request->input('text_2_2_en', $text_2_2_en);
		$picture_3_1	= $request->input('picture_3_1', $picture_3_1);
		$picture_3_1_en	= $request->input('picture_3_1_en', $picture_3_1_en);
		$picture_3_2	= $request->input('picture_3_2', $picture_3_2);
		$picture_3_2_en	= $request->input('picture_3_2_en', $picture_3_2_en);
		$text_3_1		= $request->input('text_3_1', $text_3_1);
		$text_3_1_en	= $request->input('text_3_1_en', $text_3_1_en);
		$text_3_2		= $request->input('text_3_2', $text_3_2);
		$text_3_2_en	= $request->input('text_3_2_en', $text_3_2_en);
		$text_4			= $request->input('text_4', $text_4);
		$text_4_en		= $request->input('text_4_en', $text_4_en);
		$text_5_1		= $request->input('text_5_1', $text_5_1);
		$text_5_1_en	= $request->input('text_5_1_en', $text_5_1_en);
		$text_5_2		= $request->input('text_5_2', $text_5_2);
		$text_5_2_en	= $request->input('text_5_2_en', $text_5_2_en);
		$text_6_1		= $request->input('text_6_1', $text_6_1);
		$text_6_1_en	= $request->input('text_6_1_en', $text_6_1_en);
		$text_6_2		= $request->input('text_6_2', $text_6_2);
		$text_6_2_en	= $request->input('text_6_2_en', $text_6_2_en);
		$text_6_3		= $request->input('text_6_3', $text_6_3);
		$text_6_3_en	= $request->input('text_6_3_en', $text_6_3_en);
		$text_6_4		= $request->input('text_6_4', $text_6_4);
		$text_6_4_en	= $request->input('text_6_4_en', $text_6_4_en);

		$picture_data_1			= UploadHelper::getAllPicture('config', $picture_1, 1);
		$picture_data_1_en		= UploadHelper::getAllPicture('config', $picture_1_en, 1);
		$picture_data_2_1		= UploadHelper::getAllPicture('config', $picture_2_1, 1);
		$picture_data_2_1_en	= UploadHelper::getAllPicture('config', $picture_2_1_en, 1);
		$picture_data_2_2		= UploadHelper::getAllPicture('config', $picture_2_2, 1);
		$picture_data_2_2_en	= UploadHelper::getAllPicture('config', $picture_2_2_en, 1);
		$picture_data_3_1		= UploadHelper::getAllPicture('config', $picture_3_1, 1);
		$picture_data_3_1_en	= UploadHelper::getAllPicture('config', $picture_3_1_en, 1);
		$picture_data_3_2		= UploadHelper::getAllPicture('config', $picture_3_2, 1);
		$picture_data_3_2_en	= UploadHelper::getAllPicture('config', $picture_3_2_en, 1);


		$data = [
				'picture_1'			=> $picture_1,
				'picture_1_en'		=> $picture_1_en,
				'text_1'			=> $text_1,
				'text_1_en'			=> $text_1_en,
				'picture_2_1'		=> $picture_2_1,
				'picture_2_1_en'	=> $picture_2_1_en,
				'picture_2_2'		=> $picture_2_2,
				'picture_2_2_en'	=> $picture_2_2_en,
				'text_2_1'			=> $text_2_1,
				'text_2_1_en'		=> $text_2_1_en,
				'text_2_2'			=> $text_2_2,
				'text_2_2_en'		=> $text_2_2_en,
				'picture_3_1'		=> $picture_3_1,
				'picture_3_1_en'	=> $picture_3_1_en,
				'picture_3_2'		=> $picture_3_2,
				'picture_3_2_en'	=> $picture_3_2_en,
				'text_3_1'			=> $text_3_1,
				'text_3_1_en'		=> $text_3_1_en,
				'text_3_2'			=> $text_3_2,
				'text_3_2_en'		=> $text_3_2_en,
				'text_4'			=> $text_4,
				'text_4_en'			=> $text_4_en,
				'text_5_1'			=> $text_5_1,
				'text_5_1_en'		=> $text_5_1_en,
				'text_5_2'			=> $text_5_2,
				'text_5_2_en'		=> $text_5_2_en,
				'text_6_1'			=> $text_6_1,
				'text_6_1_en'		=> $text_6_1_en,
				'text_6_2'			=> $text_6_2,
				'text_6_2_en'		=> $text_6_2_en,
				'text_6_3'			=> $text_6_3,
				'text_6_3_en'		=> $text_6_3_en,
				'text_6_4'			=> $text_6_4,
				'text_6_4_en'		=> $text_6_4_en,
				'picture_data_1'		=> $picture_data_1,
				'picture_data_1_en'		=> $picture_data_1_en,
				'picture_data_2_1'		=> $picture_data_2_1,
				'picture_data_2_1_en'	=> $picture_data_2_1_en,
				'picture_data_2_2'		=> $picture_data_2_2,
				'picture_data_2_2_en'	=> $picture_data_2_2_en,
				'picture_data_3_1'		=> $picture_data_3_1,
				'picture_data_3_1_en'	=> $picture_data_3_1_en,
				'picture_data_3_2'		=> $picture_data_3_2,
				'picture_data_3_2_en'	=> $picture_data_3_2_en,
				];

		$action		= $request->input('action');
		$errors 		= [];

		if($action == 'execute'){
			$dataReturn	= $this->configuration_repository->update($data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}


   		return  view('admin.config.certification', [
												'data'			=> $data,
												'errors'		=> $errors
												]
											);
   	}
}