<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Banner as BannerModel;
use App\Repositories\BannerRepository;
use App\Helper\Upload as UploadHelper;
use Config;

class AdminBannerController extends AdminController
{
	public $banner_repository;
	public function __construct(BannerRepository $banner_repository){
		$this->banner_repository	= $banner_repository;
	}

   	public function index(Request $request){

		$limit		= 25;
		$name		= $request->input('name');
		$position	= intval($request->input('position', 0));

		$dataSearch	= ['name' => $name, 'position' => $position];

		$banners	= new BannerModel();
		if($position > 0) $banners	= $banners->where('ban_position', '=', $position);
		if($name	!= "") $banners = $banners->where('ban_name', 'like', '%'. $name . '%');

		$banners		= $banners->paginate($limit);

		$arrType					= $this->banner_repository::$arrType;
		$arrPosition				= $this->banner_repository::$arrPosition;
		$arrTarget					= $this->banner_repository::$arrTarget;

		return  view('admin.banner.index', [
										'banners'		=> $banners,
										'arrPosition'	=> $arrPosition,
										'arrType'		=> $arrType,
										'arrTarget'		=> $arrTarget,
										'pagination'=> [
																'total'			=> $banners->total(),
																'current_page'	=> $banners->currentPage(),
																'perpage'		=> $banners->perPage(),
																'links'			=> $banners->total() > $banners->perPage() ? $banners->appends($dataSearch)->links() : '',
															],
										'dataSearch'=> $dataSearch
										]
									);
   	}

   public function add(Request $request, $id = 0){

		$redirect		= $request->input('url_redirect', base64_encode(route('admin.view.banner')));
		$name			= '';
       $name_en         = '';
		$picture		= '';
		$picture_data	= [];
		$link			= '';
		$description	= '';
		$target			= '';
		$type			= 0;
		$position		= -1;
		$name_rewrite   = '';
		$date			= '';
		$active			= '';
		$order			= '';
		$start_time		= '';
		$end_time		= '';
		$admin_id		= '';
		$id				= intval($id);

		if($id > 0){
			// Lấy thông tin
			$info = $this->banner_repository->getById($id);
			if(empty($info)) die('Record not found !');

			$name			= $info->ban_name;
            $name_en	    = $info->ban_name_en;
			$picture		= $info->ban_picture;
			$name_rewrite 	= removeTitle($name);
			$picture_data   = UploadHelper::getAllPictures('banner', $info->ban_picture_json, 0);
			$link			= $info->ban_link;
			$description	= $info->ban_description;
			$target			= $info->ban_target;
			$type			= $info->ban_type;
			$position		= $info->ban_position;
			$date			= $info->ban_date;
			$active			= $info->ban_active;
			$order			= $info->ban_order;
			$start_time		= $info->ban_start_time;
			$end_time		= $info->ban_end_time;
		}

		$name			= $request->input('name', $name);
        $name_en	    = $request->input('name_en', $name_en);
		$picture		= $request->input('picture', $picture);
		$picture_data	= $request->input('picture_data', $picture_data);
		$link			= $request->input('link', $link);
		$description	= $request->input('description', $description);
		$target			= $request->input('target', $target);
		$type			= $request->input('type', $type);
		$position		= $request->input('position', $position);
		$date			= $request->input('date', $date);
		$order			= $request->input('order', $order);
		$start_time		= $request->input('start_time', $start_time);
		$end_time		= $request->input('end_time', $end_time);

		$picture_json 	= UploadHelper::generatePictureJson('banner', $picture_data, $picture);
		$picture_data 	= UploadHelper::getAllPictures('banner', $picture_json, 1);

		$data = [
					'id'			=> $id,
					'name'			=> $name,
                    'name_en'	    => $name_en,
					'name_rewrite'  => $name_rewrite,
					'picture'		=> $picture,
					'picture_data' 	=> $picture_data,
					'picture_json' 	=> $picture_json,
					'link'			=> $link,
					'description'	=> $description,
					'target'		=> $target,
					'type'			=> $type,
					'position'		=> $position,
					'date'			=> $date,
					'active'		=> $active,
					'order'			=> $order,
					'start_time'	=> $start_time,
					'end_time'		=> $end_time,
				];

		$arrType		= $this->banner_repository::$arrType;
		$arrPosition	= $this->banner_repository::$arrPosition;
		$arrTarget		= $this->banner_repository::$arrTarget;

		$action		= $request->input('action');
		$errors 		= [];

		if($action == 'execute'){

			$data['active'] 	= $request->input('active', 0);
			$dataReturn	= $this->banner_repository->add($id, $data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}


   		return  view('admin.banner.add', [
											'data'			=> $data,
											'arrType'		=> $arrType,
											'arrPosition'	=> $arrPosition,
											'arrTarget'		=> $arrTarget,
											'errors'		=> $errors
											]
										);
   }

   	public function delete(Request $request, $id){
	   	$redirect = $request->input('url_redirect', base64_encode(route('admin.view.banner')));

	   	// Gọi function xóa
	   	$result = $this->banner_repository->delete($id);
	   	if(isset($result['status']) && $result['status'] == true){
	   		echo '<script>alert("Delete banner success"); window.location.href="' . base64_decode($redirect) . '"</script>';
	   	}else{
	   		$error = isset($result['msg']) ? $result['msg'] : '';
	   		echo '<script>alert("Delelte Error: ' . $error . '"); window.location.href="' . base64_decode($redirect) . '"</script>';
	   	}
   	}


   public function status(Request $request){
		$redirect	= $request->input('url_redirect', base64_encode(route('admin.staff.list')));
		$record_id	= $request->input('record_id', 0);
		$type			= $request->input('type', '');

		if($record_id <= 0 || $type == '') return response()->json(array('status'=> false, 'msg'=> 'Input param incorrect'), 200);

		// Lấy thông tin
		$info = $this->banner_repository->getById($record_id);
		if(empty($info)) return response()->json(array('status'=> false, 'msg'=> 'Record not found'), 200);

		$status 	= 0;
		$dataUpdate 	= [];
		switch ($type) {
			case 'active':
				$status 	= abs($info->ban_active - 1);
				$dataUpdate 	= ['active' => $status];
				break;

			case 'hot':
				$status 	= abs($info->ban_hot - 1);
				$dataUpdate 	= ['hot' => $status];
				break;
		}

		if(empty($dataUpdate)) return response()->json(array('status'=> false, 'msg'=> 'Type update not support'), 200);

		$dataReturn	= $this->banner_repository->add($record_id, $dataUpdate);

		if(isset($dataReturn['status']) && $dataReturn['status'] == true){
			return response()->json(array('status'=> true, 'data' => $status), 200);
		}

		$errors = isset($dataReturn['errors']) ? implode(",", $dataReturn['errors']) : '';

   	return response()->json(array('status'=> false, 'msg'=> $errors), 200);
   }
}
