<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\AdminUser as AdminUserModel;
use App\Repositories\AdminUserRepository;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class AdminStaffController extends AdminController
{
	public $admin_user_repository;
	public function __construct(AdminUserRepository $admin_user_repository){
		$this->admin_user_repository	= $admin_user_repository;
	}

   public function index(Request $request){

   	$limit		= $request->input('limit', 25);
   	$loginname	= $request->input('loginname');
   	$email		= $request->input('email');

   	$dataSearch = ['loginname' => $loginname, 'email' => $email];
   	$allRole 	= $this->admin_user_repository->getAllMenuRoleAdmin();
   	$staffs 		= AdminUserModel::where('adm_loginname', '<>' , 'admin')->where('adm_delete', '=', 0);
   	if($loginname != "") $staffs = $staffs->where('adm_loginname', '=', $loginname);
   	if($email != "") $staffs = $staffs->where('adm_email', '=', $email);

	$staffs			= $staffs->paginate($limit);

   	return  view('admin.staff.index', [
														'staffs'		=> $staffs,
														'allRole' 		=> $allRole,
														'pagination' => [
																			'total'			=> $staffs->total(),
																			'current_page'	=> $staffs->currentPage(),
																			'perpage'		=> $staffs->perPage(),
																			'links'			=> $staffs->total() > $staffs->perPage() ? $staffs->appends($dataSearch)->links() : '',
																		],
														'dataSearch'	=> $dataSearch,
														'infoStaff'		=> app()->ADMIN_INFO
   													]
   												);
   }

   public function add(Request $request, $id = 0){

	$redirect	= $request->input('url_redirect', base64_encode(route('admin.staff.index')));
	$loginname	= '';
	$name		= '';
	$phone		= '';
	$password	= '';
	$email		= '';
	$json_role	= '';
	$active 	= 0;
	$id			= intval($id);

	if($id > 0){
		// Lấy thông tin
		$info = $this->admin_user_repository->getById($id);
		if(empty($info)) die('Record not found !');

		$loginname	= $info->adm_loginname;
		$name		= $info->adm_name;
		$phone		= $info->adm_phone;
		$email		= $info->adm_email;
		$active		= $info->adm_active;
		$json_role	= $info->adm_json_role;
	}

		$loginname	= $request->input('loginname', $loginname);
		$name		= $request->input('name', $name);
		$phone		= $request->input('phone', $phone);
		$email		= $request->input('email', $email);
		$password	= $request->input('password', '');

		$allRole 	= $this->admin_user_repository->getAllMenuRoleAdmin();
		$data = [
					'id' 		=> $id,
					'loginname'	=> $loginname,
					'name'		=> $name,
					'phone'		=> $phone,
					'email'		=> $email,
					'password'	=> $password,
					'json_role' => $json_role,
					'active' 	=> $active
				];

		$action		= $request->input('action');
		$errors 	= [];

		if($action == 'execute'){

			// Lặp để lấy các quyền đc post lên
			$dataRole 	= [];
			foreach ($allRole as $key => $value) {
				foreach ($value['sub'] as $key_sub => $value_sub) {
					$field = $key . "_" . $key_sub;
					$check = $request->input($field);
					if($check == 1){
						if(!isset($dataRole[$key])) $dataRole[$key] = [];
						$dataRole[$key][$key_sub] = 1;
					}
				}
			}

			$data['json_role'] 	= $dataRole ? json_encode($dataRole) : '';
			$data['active'] 	= $request->input('active', 0);

			$dataReturn	= $this->admin_user_repository->add($id, $data);

			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}

		// Gán lại role
		$data['json_role'] = $data['json_role'] ? json_decode($data['json_role'], true) : [];
   		return  view('admin.staff.add', [
										'data' => $data,
										'allRole' => $allRole,
										'errors' => $errors
										]
									);
   }

   public function delete(Request $request, $id){
   	$redirect = $request->input('url_redirect', base64_encode(route('admin.staff.list')));

   	// Gọi function xóa
   	$result = $this->admin_user_repository->delete($id);
   	if(isset($result['status']) && $result['status'] == true){
   		echo '<script>alert("Delete acount success"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}else{
   		$error = isset($result['msg']) ? $result['msg'] : '';
   		echo '<script>alert("Delelte Error: ' . $error . '"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}
   }


   public function status(Request $request){
		$redirect	= $request->input('url_redirect', base64_encode(route('admin.staff.list')));
		$record_id	= $request->input('record_id', 0);
		$type			= $request->input('type', '');

		// Lấy thông tin
		$info = $this->admin_user_repository->getById($record_id);
		if(empty($info)) return response()->json(array('status'=> false, 'msg'=> 'Record not found'), 200);

		$status 	= abs($info->adm_active - 1);
		$dataReturn	= $this->admin_user_repository->add($record_id, ['active' => $status]);

		if(isset($dataReturn['status']) && $dataReturn['status'] == true){
			return response()->json(array('status'=> true, 'data' => $status), 200);
		}

		$errors = isset($dataReturn['errors']) ? implode(",", $dataReturn['errors']) : '';

   	return response()->json(array('status'=> false, 'msg'=> $errors), 200);
   }

   public function fake_login(Request $request, $id){

   	// Check quyền
   	$infoStaff		= app()->ADMIN_INFO;
   	if($infoStaff->adm_isadmin != 1) die('Access Denied');

		// Lấy thông tin
		$info = $this->admin_user_repository->getById($id);
		if(empty($info)) die('Record not found !');

		//$this->admin_user_helper::setLogged($info->adm_loginname, $info->adm_password);

		echo '<script>window.parent.location.reload();</script>';

		//return redirect(route('admin.home'));
   }
}
