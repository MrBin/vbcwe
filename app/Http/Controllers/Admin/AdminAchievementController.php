<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Achievement as AchievementModel;
use App\Repositories\AchievementRepository;
use App\Helper\Upload as UploadHelper;
use Config;

class AdminAchievementController extends AdminController
{
	public $achievement_repository;
	public function __construct(AchievementRepository $achievement_repository){
		$this->achievement_repository	= $achievement_repository;
	}

   	public function index(Request $request){

		$limit		= 25;
		$title		= $request->input('title');

		$dataSearch	= ['title' => $title];
		$achievements	= new AchievementModel();
		if($title	!= "") $achievements = $achievements->where('tht_title', 'like', '%'. $title . '%');

		$achievements		= $achievements->paginate($limit);

		return  view('admin.achievement.index', [
										'achievements'		=> $achievements,
										'pagination'=> [
																'total'			=> $achievements->total(),
																'current_page'	=> $achievements->currentPage(),
																'perpage'		=> $achievements->perPage(),
																'links'			=> $achievements->total() > $achievements->perPage() ? $achievements->appends($dataSearch)->links() : '',
															],
										'dataSearch'=> $dataSearch
										]
									);
   	}

   public function add(Request $request, $id = 0){

		$redirect	= $request->input('url_redirect', base64_encode(route('admin.view.achievement')));
		$index		= "";
		$title		= "";
		$title_en	= "";
		$icon		= "";
		$order		= "";
		$status		= "";

		$id				= intval($id);

		if($id > 0){
			// Lấy thông tin
			$info = $this->achievement_repository->getById($id);
			if(empty($info)) die('Record not found !');

			$index		= $info->tht_index;
			$title		= $info->tht_title;
			$title_en	= $info->tht_title_en;
			$icon		= $info->tht_icon;
			$order		= $info->tht_order;
			$status		= $info->tht_status;
		}

		$index		= $request->input('index', $index);
		$title		= $request->input('title', $title);
		$title_en	= $request->input('title_en', $title_en);
		$icon		= $request->input('icon', $icon);
		$order		= $request->input('order', $order);
		$status		= $request->input('status', $status);



		$icon_data 	= UploadHelper::getAllPicture('config', $icon, 1);
		$data = [
					'id'		=> $id,
					'index'		=> $index,
					'title'		=> $title,
					'title_en'	=> $title_en,
					'icon'		=> $icon,
					'order'		=> $order,
					'status'	=> $status,
					'icon_data' => $icon_data,
				];

		$action		= $request->input('action');
		$errors 		= [];

		if($action == 'execute'){

			$data['status'] 	= $request->input('status', 0);
			$dataReturn	= $this->achievement_repository->add($id, $data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}


   		return  view('admin.achievement.add', [
											'data'			=> $data,
											'errors'		=> $errors
											]
										);
   }

   public function delete(Request $request, $id){
   	$redirect = $request->input('url_redirect', base64_encode(route('admin.view.achievement')));

   	// Gọi function xóa
   	$result = $this->achievement_repository->delete($id);
   	if(isset($result['status']) && $result['status'] == true){
   		echo '<script>alert("Delete achievement success"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}else{
   		$error = isset($result['msg']) ? $result['msg'] : '';
   		echo '<script>alert("Delelte Error: ' . $error . '"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}
   }


   public function status(Request $request){
		$redirect	= $request->input('url_redirect', base64_encode(route('admin.view.achievement')));
		$record_id	= $request->input('record_id', 0);
		$type			= $request->input('type', '');

		if($record_id <= 0 || $type == '') return response()->json(array('status'=> false, 'msg'=> 'Input param incorrect'), 200);

		// Lấy thông tin
		$info = $this->achievement_repository->getById($record_id);
		if(empty($info)) return response()->json(array('status'=> false, 'msg'=> 'Record not found'), 200);

		$status 	= 0;
		$dataUpdate 	= [];
		switch ($type) {
			case 'active':
				$status 	= abs($info->tht_status - 1);
				$dataUpdate 	= ['status' => $status];
				break;
		}

		if(empty($dataUpdate)) return response()->json(array('status'=> false, 'msg'=> 'Type update not support'), 200);

		$dataReturn	= $this->achievement_repository->add($record_id, $dataUpdate);

		if(isset($dataReturn['status']) && $dataReturn['status'] == true){
			return response()->json(array('status'=> true, 'data' => $status), 200);
		}

		$errors = isset($dataReturn['errors']) ? implode(",", $dataReturn['errors']) : '';

   	return response()->json(array('status'=> false, 'msg'=> $errors), 200);
   }
}