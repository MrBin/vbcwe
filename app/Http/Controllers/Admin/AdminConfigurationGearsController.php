<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\ConfigurationGearsRepository;
use App\Helper\Upload as UploadHelper;
use Config;

class AdminConfigurationGearsController extends AdminController
{
	public $configuration_repository;
	public function __construct(ConfigurationGearsRepository $configuration_repository){
		$this->configuration_repository	= $configuration_repository;
	}

	public function index(Request $request){

		$redirect	= base64_encode(route('admin.configs.gears'));

		// Lấy thông tin
		$info = $this->configuration_repository->getConfiguration();
		if(empty($info)) die('Record not found !');
		$text_1			= $info->cog_text_1;
		$text_1_en		= $info->cog_text_1_en;
		$picture_2		= $info->cog_picture_2;
		$picture_2_en	= $info->cog_picture_2_en;
		$text_2_1		= $info->cog_text_2_1;
		$text_2_1_en	= $info->cog_text_2_1_en;
		$text_2_2		= $info->cog_text_2_2;
		$text_2_2_en	= $info->cog_text_2_2_en;
		$text_3			= $info->cog_text_3;
		$text_3_en		= $info->cog_text_3_en;
		$text_4			= $info->cog_text_4;
		$text_4_en		= $info->cog_text_4_en;
		$text_5			= $info->cog_text_5;
		$text_5_en		= $info->cog_text_5_en;
		$text_6			= $info->cog_text_6;
		$text_6_en		= $info->cog_text_6_en;
		$text_7			= $info->cog_text_7;
		$text_7_en		= $info->cog_text_7_en;
		$picture_7		= $info->cog_picture_7;
		$picture_7_en	= $info->cog_picture_7_en;

		$text_1			= $request->input('text_1', $text_1);
		$text_1_en		= $request->input('text_1_en', $text_1_en);
		$picture_2		= $request->input('picture_2', $picture_2);
		$picture_2_en	= $request->input('picture_2_en', $picture_2_en);
		$text_2_1		= $request->input('text_2_1', $text_2_1);
		$text_2_1_en	= $request->input('text_2_1_en', $text_2_1_en);
		$text_2_2		= $request->input('text_2_2', $text_2_2);
		$text_2_2_en	= $request->input('text_2_2_en', $text_2_2_en);
		$text_3			= $request->input('text_3', $text_3);
		$text_3_en		= $request->input('text_3_en', $text_3_en);
		$text_4			= $request->input('text_4', $text_4);
		$text_4_en		= $request->input('text_4_en', $text_4_en);
		$text_5			= $request->input('text_5', $text_5);
		$text_5_en		= $request->input('text_5_en', $text_5_en);
		$text_6			= $request->input('text_6', $text_6);
		$text_6_en		= $request->input('text_6_en', $text_6_en);
		$text_7			= $request->input('text_7', $text_7);
		$text_7_en		= $request->input('text_7_en', $text_7_en);
		$picture_7		= $request->input('picture_7', $picture_7);
		$picture_7_en	= $request->input('picture_7_en', $picture_7_en);

		$picture_data_2			= UploadHelper::getAllPicture('config', $picture_2, 1);
		$picture_data_2_en		= UploadHelper::getAllPicture('config', $picture_2_en, 1);
		$picture_data_7			= UploadHelper::getAllPicture('config', $picture_7, 1);
		$picture_data_7_en		= UploadHelper::getAllPicture('config', $picture_7_en, 1);

		$data = [
				'text_1'			=> $text_1,
				'text_1_en'			=> $text_1_en,
				'picture_2'			=> $picture_2,
				'picture_2_en'		=> $picture_2_en,
				'text_2_1'			=> $text_2_1,
				'text_2_1_en'		=> $text_2_1_en,
				'text_2_2'			=> $text_2_2,
				'text_2_2_en'		=> $text_2_2_en,
				'text_3'			=> $text_3,
				'text_3_en'			=> $text_3_en,
				'text_4'			=> $text_4,
				'text_4_en'			=> $text_4_en,
				'text_5'			=> $text_5,
				'text_5_en'			=> $text_5_en,
				'text_6'			=> $text_6,
				'text_6_en'			=> $text_6_en,
				'text_7'			=> $text_7,
				'text_7_en'			=> $text_7_en,
				'picture_7'			=> $picture_7,
				'picture_7_en'		=> $picture_7_en,
				'picture_data_2'	=> $picture_data_2,
				'picture_data_2_en'	=> $picture_data_2_en,
				'picture_data_7'	=> $picture_data_7,
				'picture_data_7_en'	=> $picture_data_7_en,
				];

		$action		= $request->input('action');
		$errors 		= [];

		if($action == 'execute'){
			$dataReturn	= $this->configuration_repository->update($data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}


   		return  view('admin.config.gears', [
												'data'			=> $data,
												'errors'		=> $errors
												]
											);
   	}
}