<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ItemStep as ItemStepModel;
use App\Repositories\ItemStepRepository;
use App\Repositories\CategoryRepository;
use App\Helper\Category as CategoryHelper;
use App\Helper\Upload as UploadHelper;
use Config;

class AdminItemStepController extends AdminController
{
	public $item_step_repository;

	public function __construct(ItemStepRepository $item_step_repository){
		$this->item_step_repository	= $item_step_repository;
	}

   	public function index(Request $request){

		$limit		= 25;
		$title		= $request->input('title');
		$type		= intval($request->input('type', 0));

		$dataSearch	= ['title' => $title, 'type' => intval($type)];
		$statics	= new ItemStepModel();
		if($title	!= "") $statics = $statics->where('its_title', 'like', '%'. $title . '%');
		if($type	> 0) $statics = $statics->where('its_type', '=', $type);

		$statics		= $statics->paginate($limit);

		return  view('admin.item_step.index', [
										'statics'		=> $statics,
										'pagination'=> [
																'total'			=> $statics->total(),
																'current_page'	=> $statics->currentPage(),
																'perpage'		=> $statics->perPage(),
																'links'			=> $statics->total() > $statics->perPage() ? $statics->appends($dataSearch)->links() : '',
															],
										'dataSearch'=> $dataSearch,
										'allType' => $this->item_step_repository::getTypeLabels(),
										]
									);
   	}

   	public function add(Request $request, $id = 0){

		$redirect	= $request->input('url_redirect', base64_encode(route('admin.configs.step')));

		$title					= "";
		$title_en				= "";
		$order					= '';
		$status					= 0;
		$type					= 0;
		$picture				= "";
		$teaser					= "";
		$teaser_en				= "";

		$id				= intval($id);

		if($id > 0){
			// Lấy thông tin
			$info = $this->item_step_repository->getById($id);
			if(empty($info)) die('Record not found !');

			$title					= $info->its_title;
			$title_en				= $info->its_title_en;
			$order					= $info->its_order;
			$type					= $info->its_type;
			$status					= $info->its_status;
			$picture				= $info->its_picture;
			$teaser					= $info->its_teaser;
			$teaser_en				= $info->its_teaser_en;

		}

		$type					= $request->input('type', $type);
		$title					= $request->input('title', $title);
		$title_en				= $request->input('title_en', $title_en);
		$order					= $request->input('order', $order);
		$status					= $request->input('status', $status);
		$picture				= $request->input('picture', $picture);
		$teaser					= $request->input('teaser', $teaser);
		$teaser_en				= $request->input('teaser_en', $teaser_en);

		$picture_data	= UploadHelper::getAllPicture('other', $picture, 1);
		$data = [
				'id'					=> $id,
				'type'			=> $type,
				'title'					=> $title,
				'title_en'				=> $title_en,
				'order'					=> $order,
				'status'				=> $status,
				'picture'				=> $picture,
				'teaser'				=> $teaser,
				'teaser_en'				=> $teaser_en,
				'picture_data'			=> $picture_data,
				];

		$action	= $request->input('action');
		$errors	= [];
		if($action == 'execute'){

			$data['status'] 	= $request->input('status', 0);
			$dataReturn	= $this->item_step_repository->add($id, $data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}


   		return  view('admin.item_step.add', [
											'data'			=> $data,
											'errors'		=> $errors,
											'allType' 	=> $this->item_step_repository::getTypeLabels()
											]
										);
   	}

   	public function delete(Request $request, $id){
   		$redirect = $request->input('url_redirect', base64_encode(route('admin.configs.step')));

	   	// Gọi function xóa
	   	$result = $this->item_step_repository->delete($id);
	   	if(isset($result['status']) && $result['status'] == true){
	   		echo '<script>alert("Delete static success"); window.location.href="' . base64_decode($redirect) . '"</script>';
	   	}else{
	   		$error = isset($result['msg']) ? $result['msg'] : '';
	   		echo '<script>alert("Delelte Error: ' . $error . '"); window.location.href="' . base64_decode($redirect) . '"</script>';
	   	}
   	}


   	public function status(Request $request){
		$redirect	= $request->input('url_redirect', base64_encode(route('admin.configs.step')));
		$record_id	= $request->input('record_id', 0);
		$type		= $request->input('type', '');

		if($record_id <= 0 || $type == '') return response()->json(array('status'=> false, 'msg'=> 'Input param incorrect'), 200);

		// Lấy thông tin
		$info = $this->item_step_repository->getById($record_id);
		if(empty($info)) return response()->json(array('status'=> false, 'msg'=> 'Record not found'), 200);

		$status 	= 0;
		$dataUpdate 	= [];
		switch ($type) {
			case 'active':
				$status 	= abs($info->its_status - 1);
				$dataUpdate 	= ['status' => $status];
				break;
		}

		if(empty($dataUpdate)) return response()->json(array('status'=> false, 'msg'=> 'Type update not support'), 200);

		$dataReturn	= $this->item_step_repository->add($record_id, $dataUpdate);

		if(isset($dataReturn['status']) && $dataReturn['status'] == true){
			return response()->json(array('status'=> true, 'data' => $status), 200);
		}

		$errors = isset($dataReturn['errors']) ? implode(",", $dataReturn['errors']) : '';

   		return response()->json(array('status'=> false, 'msg'=> $errors), 200);
    }
}
