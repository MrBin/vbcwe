<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Upload as UploadHelper;
use Illuminate\Http\Request;
use App\Models\Opinion as OpinionModel;
use App\Repositories\OpinionRepository;
use App\Helper\UserTranslate as UserTranslateHelper;
use Config;

class AdminOpinionController extends AdminController
{
	public $opinion_repository;
	public function __construct(OpinionRepository $opinion_repository){
		$this->opinion_repository	= $opinion_repository;
	}

   	public function index(Request $request){

		$limit		= 25;
		$name		= $request->input('name');

		$dataSearch	= ['name' => $name];
		$opinions	= new OpinionModel();
		if($name	!= "") $opinions = $opinions->where('opi_name', 'like', '%'. $name . '%');

		$opinions		= $opinions->paginate($limit);

		return  view('admin.opinion.index', [
										'opinions'		=> $opinions,
										'pagination'=> [
																'total'			=> $opinions->total(),
																'current_page'	=> $opinions->currentPage(),
																'perpage'		=> $opinions->perPage(),
																'links'			=> $opinions->total() > $opinions->perPage() ? $opinions->appends($dataSearch)->links() : '',
															],
										'dataSearch'=> $dataSearch
										]
									);
   	}

   public function add(Request $request, $id = 0){

		$redirect	= $request->input('url_redirect', base64_encode(route('admin.view.opinion')));

		$title			= "";
		$title_en		= "";
		$name			= "";
		$name_en		= "";
		$order			= 0;
		$status			= 0;
		$picture		= "";
		$video		    = "";
		$description	= "";
		$description_en	= "";

		$id				= intval($id);

		if($id > 0){
			// Lấy thông tin
			$info = $this->opinion_repository->getById($id);
			if(empty($info)) die('Record not found !');

			$title			= $info->opi_title;
			$title_en		= $info->opi_title_en;
			$name			= $info->opi_name;
			$name_en		= $info->opi_name_en;
			$order			= $info->opi_order;
			$picture 		= $info->opi_picture;
			$video 		    = $info->opi_video;
			$status			= $info->opi_status;
			$description	= $info->opi_description;
			$description_en	= $info->opi_description_en;
		}

		$title			= $request->input('title', $title);
		$title_en		= $request->input('title_en', $title_en);
		$name			= $request->input('name', $name);
		$name_en		= $request->input('name_en', $name_en);
		$order			= $request->input('order', $order);
		$status			= $request->input('status', $status);
		$picture				= $request->input('picture', $picture);
		$video				= $request->input('video', $video);
		$description	= $request->input('description', $description);
		$description_en	= $request->input('description_en', $description_en);
		$picture				= $request->input('picture', $picture);
		$picture_data 		 	= UploadHelper::getAllPicture('other', $picture, 1);

		$picture_data 	= UploadHelper::getAllPicture('other', $picture, 1);

		$data = [
				'id'				=> $id,
				'title'				=> $title,
				'title_en'			=> $title_en,
				'name'				=> $name,
				'name_en'			=> $name_en,
				'order'				=> $order,
				'picture'			=> $picture,
				'video'			    => $video,
				'picture_data'		=> $picture_data,
				'status'			=> $status,
				'description'		=> $description,
				'description_en'	=> $description_en,
				];

		$action		= $request->input('action');
		$errors 		= [];

		if($action == 'execute'){

			$data['status'] 	= $request->input('status', 0);
			$dataReturn	= $this->opinion_repository->add($id, $data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}


   		return  view('admin.opinion.add', [
											'data'			=> $data,
											'arrLang' 		=> UserTranslateHelper::getLanguages(),
											'errors'		=> $errors
											]
										);
   }

   public function delete(Request $request, $id){
   	$redirect = $request->input('url_redirect', base64_encode(route('admin.view.opinion')));

   	// Gọi function xóa
   	$result = $this->opinion_repository->delete($id);
   	if(isset($result['status']) && $result['status'] == true){
   		echo '<script>alert("Delete record success"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}else{
   		$error = isset($result['msg']) ? $result['msg'] : '';
   		echo '<script>alert("Delelte Error: ' . $error . '"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}
   }


   public function status(Request $request){
		$redirect	= $request->input('url_redirect', base64_encode(route('admin.view.opinion')));
		$record_id	= $request->input('record_id', 0);
		$type		= $request->input('type', '');

		if($record_id <= 0 || $type == '') return response()->json(array('status'=> false, 'msg'=> 'Input param incorrect'), 200);

		// Lấy thông tin
		$info = $this->opinion_repository->getById($record_id);
		if(empty($info)) return response()->json(array('status'=> false, 'msg'=> 'Record not found'), 200);

		$status 	= 0;
		$dataUpdate 	= [];
		switch ($type) {
			case 'active':
				$status 	= abs($info->opi_status - 1);
				$dataUpdate 	= ['status' => $status];
				break;
		}

		if(empty($dataUpdate)) return response()->json(array('status'=> false, 'msg'=> 'Type update not support'), 200);

		$dataReturn	= $this->opinion_repository->add($record_id, $dataUpdate);

		if(isset($dataReturn['status']) && $dataReturn['status'] == true){
			return response()->json(array('status'=> true, 'data' => $status), 200);
		}

		$errors = isset($dataReturn['errors']) ? implode(",", $dataReturn['errors']) : '';

   	return response()->json(array('status'=> false, 'msg'=> $errors), 200);
   }
}
