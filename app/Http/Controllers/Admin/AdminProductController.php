<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product as ProductModel;
use App\Helper\Product as ProductHelper;
use App\Helper\Category as CategoryHelper;
use App\Repositories\ProductRepository;
use App\Transformers\ProductTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class AdminProductController extends Controller
{
	public $product_repository;
	public $product_helper;
	public function __construct(ProductRepository $product_repository, ProductHelper $product_helper){
		$this->product_repository   = $product_repository;
		$this->product_helper       = $product_helper;
	}

	public function index(Request $request){

		$limit			= $request->input('limit', 25);
		$category_id	= $request->input('category_id');
		$name				= $request->input('name');

   	$dataSearch = ['category_id' => $category_id, 'name' => $name];

   	$products 		= new ProductModel();
   	if($category_id > 0) $products = $products->where('pro_category_id', '=', $category_id);
   	if($name != "") $products = $products->where('pro_name', '=', $name);
   	$products  		= $products->with(['category']);
		$products		= $products->paginate($limit);

		$transformer	= new ProductTransformer();
		$data				= transformer_collection($products, $transformer, []);
		//dd($data);
   	return  view('admin.product.index', [	'view' => 'list',
														'products'		=> $data,
														'total'			=> $products->total(),
														'current_page'	=> $products->currentPage(),
														'perpage'		=> $products->perPage(),
														'pagination' 	=> $products->links(),
														'dataSearch'	=> $dataSearch
   													]
   												);
	}

   public function add(Request $request, $id = 0){

		$redirect			= $request->input('url_redirect', base64_encode(route('admin.product.add')));
		$name					= '';
		$name_rewrite		= '';
		$meta_title			= '';
		$meta_keyword		= '';
		$meta_description	= '';
		$picture				= '';
		$picture_data		= [];
		$type					= 0;
		$order				= 0;
		$hot					= 0;
		$price				= 0;
		$sale_price			= 0;
		$quantity			= 0;
		$category_id		= 0;
		$description		= '';
		$id					= intval($id);

		if($id > 0){
			// Lấy thông tin
			$info = $this->product_repository->getById($id);
			if(empty($info)) die('Record not found !');

			$name               = $info->pro_name;
			$name_rewrite       = $info->pro_name_rewrite;
			$meta_title         = $info->pro_meta_title;
			$meta_keyword       = $info->pro_meta_keyword;
			$meta_description   = $info->pro_meta_description;
			$picture            = $info->pro_picture;
			$picture_data 		  = ProductHelper::getAllPicture($info->pro_picture_json, 0);
			$type               = $info->pro_type;
			$order              = $info->pro_order;
			$hot                = $info->pro_hot;
			$price              = $info->pro_price;
			$sale_price         = $info->pro_sale_price;
			$quantity           = $info->pro_quantity;
			$category_id        = $info->pro_category_id;
			$description        = $info->pro_description;
		}

		$name					= $request->input('name', $name);
		$name_rewrite		= $request->input('name_rewrite', $name_rewrite);
		$meta_title			= $request->input('meta_title', $meta_title);
		$meta_keyword		= $request->input('meta_keyword', $meta_keyword);
		$meta_description	= $request->input('meta_description', $meta_description);
		$picture				= $request->input('picture', $picture);
		$picture_data		= $request->input('picture_data', $picture_data);
		$type					= $request->input('type', $type);
		$order				= $request->input('order', $order);
		$hot					= $request->input('hot', $hot);
		$price				= $request->input('price', $price);
		$sale_price			= $request->input('sale_price', $sale_price);
		$quantity			= $request->input('quantity', $quantity);
		$category_id		= $request->input('category_id', $category_id);
		$description		= $request->input('description', $description);

		$picture_json 		= ProductHelper::generatePictureJson($picture_data, $picture);
		$data 	= [
					'id'                => $id,
					'name'              => $name,
					'name_rewrite'      => $name_rewrite,
					'meta_title'        => $meta_title,
					'meta_keyword'      => $meta_keyword,
					'meta_description'  => $meta_description,
					'picture'           => $picture,
					'picture_json' 		=> $picture_json,
					'picture_all' 		=> ProductHelper::getAllPicture($picture_json),
					'type'              => $type,
					'order'             => $order,
					'hot'               => $hot,
					'price'             => $price,
					'sale_price'        => $sale_price,
					'quantity'          => $quantity,
					'category_id'       => $category_id,
					'description'       => $description,
				];

		$allCategory	= CategoryHelper::getAllCategory([['cat_type', '=', 'product']], false);

		$action			= $request->input('action');
		$errors			= [];

		if($action == 'execute'){
			dd($data);
			$dataReturn	= $this->product_repository->add($id, $data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}


	return  view('admin.product.index', [
											'view'          => 'add',
											'data'          => $data,
											'allCategory'   => $allCategory,
											'errors'        => $errors
											]
										);
   }

   public function status(Request $request){
		$redirect	= $request->input('url_redirect', base64_encode(route('admin.product.list')));
		$record_id	= $request->input('record_id', 0);
		$type			= $request->input('type', '');

		if($record_id <= 0 || $type == '') return response()->json(array('status'=> false, 'msg'=> 'Input param incorrect'), 200);

		// Lấy thông tin
		$info = $this->product_repository->getById($record_id);
		if(empty($info)) return response()->json(array('status'=> false, 'msg'=> 'Record not found'), 200);

		$status 	= abs($info->pro_active - 1);
		$dataUpdate 	= [];
		switch ($type) {
			case 'active':
				$status 	= abs($info->pro_active - 1);
				$dataUpdate 	= ['active' => $status];
				break;

			case 'hot':
				$status 	= abs($info->pro_hot - 1);
				$dataUpdate 	= ['hot' => $status];
				break;
		}

		if(empty($dataUpdate)) return response()->json(array('status'=> false, 'msg'=> 'Type update not support'), 200);

		$dataReturn	= $this->product_repository->add($record_id, $dataUpdate);

		if(isset($dataReturn['status']) && $dataReturn['status'] == true){
			return response()->json(array('status'=> true, 'data' => $status), 200);
		}

		$errors = isset($dataReturn['errors']) ? implode(",", $dataReturn['errors']) : '';

   	return response()->json(array('status'=> false, 'msg'=> $errors), 200);
   }


   public function delete(Request $request, $id){
   	$redirect = $request->input('url_redirect', base64_encode(route('admin.product.list')));
   	// Gọi function xóa
   	$result = $this->product_repository->delete($id);
   	if(isset($result['status']) && $result['status'] == true){
   		echo '<script>alert("Delete product success"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}else{
   		$error = isset($result['msg']) ? $result['msg'] : '';
   		echo '<script>alert("Delelte Error: ' . $error . '"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}
   }
}