<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Helper\AdminUser;
class AdminProfileController extends AdminController
{
	public function __construct(){

	}

   public function update(Request $request){

   	/*$infoStaff 	= app()->ADMIN_INFO;

   	$adm_name	= $request->input('name', $infoStaff->adm_name);
   	$adm_name	= $request->input('name', $infoStaff->adm_name);
   	$adm_email	= $request->input('email', $infoStaff->adm_email);
   	$adm_phone	= $request->input('phone', $infoStaff->adm_phone);
   	$infoStaff 	= [
   						'name' => app()->ADMIN_INFO->adm_name,
   						'phone' => app()->ADMIN_INFO->adm_name,
   						'name' => app()->ADMIN_INFO->adm_name,
   						'name' => app()->ADMIN_INFO->adm_name,
   						];

   	return  view('admin.profile.index', [	'view' => 'update',
   														'infoStaff' => $infoStaff,
   														'header' => 'This is header',
   														'footer' => 'this is footer', ]);*/
   }

   public function changePass(Request $request){

		$old_password	= $request->input('old_password');
		$password		= $request->input('password');
		$re_password	= $request->input('re_password');
		$action 			= $request->input('action');

		$errors 	= [];
		$isSuccess 		= 0;
		if($action == 'changepass'){
			$infoStaff 	= app()->ADMIN_INFO;
			// Kiểm tra xem password cũ có đúng ko
			if($infoStaff->adm_password != md5($old_password)){
				$errors[] = 'Mật khẩu cũ không chính xác';
			}else{
				if($password == '' || $password != $re_password){
					$errors[] = 'Password và Re-password không giống nhau';
				}
			}

			if(!$errors){
				$result = AdminUser::changePassLogged($password);
				if(isset($result['status']) && $result['status'] == true){
					$isSuccess = 1;
				}else{
					$errors[] = isset($result['status']) ? $result['status'] : 'ChangePass Error!';
				}
			}
		}


   	return  view('admin.profile.changepass', [	'view' => 'changepass',
												'isSuccess' => $isSuccess,
												'errors' => $errors
												]
											);
   }
}
