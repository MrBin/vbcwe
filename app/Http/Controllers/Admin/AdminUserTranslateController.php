<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\UserTranslateRepository;
use App\Models\UserTranslate as UserTranslateModel;
use App\Helper\UserTranslate as UserTranslate;
use Config;

class AdminUserTranslateController extends AdminController
{
	public $configuration_repository;
	public function __construct(UserTranslateRepository $configuration_repository){
		$this->configuration_repository	= $configuration_repository;
	}

   	public function index(Request $request){
		$redirect	= base64_encode(route('admin.configs.user_translate'));
		$limit		= 25;
		$dataSearch	= [];
		$keyword	= $request->input('keyword', '');
		// Lấy thông tin
		$data 		= UserTranslateModel::orderby('ust_id', 'DESC');
	    if($keyword != "") $data = $data->where('ust_keyword', 'LIKE', '%' . $keyword . '%');
	    $dataSearch['keyword']	= $keyword;
		$data	= $data->paginate($limit);

   		return  view('admin.config.user_translate', [
														'data'		=> $data,
														'pagination'=> [
																			'total'			=> $data->total(),
																			'current_page'	=> $data->currentPage(),
																			'perpage'		=> $data->perPage(),
																			'links'			=> $data->total() > $data->perPage() ? $data->appends($dataSearch)->links() : '',
																		],
														'dataSearch'	=> $dataSearch
														]
   										);
   }

   public function add(Request $request){
		$record_id	= $request->input('record_id', 0);
		$keyword_vi	= $request->input('keyword_vi', '');
		$keyword_en	= $request->input('keyword_en', '');

		$model = $this->configuration_repository->getByKeywordMd5($record_id);

		if($model){
			$data = [
				'keyword'	=> $model->ust_keyword,
				'vi'		=> $keyword_vi,
				'en'		=> $keyword_en,
			];

			$dataReturn = $this->configuration_repository->add($data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return response()->json(array('status'=> true), 200);
			}
		}else{
			return response()->json(array('status'=> false, 'message' => 'Keword not found !'), 200);
		}
   }
}