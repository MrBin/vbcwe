<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Models\Users;
class AdminUserController extends AdminController
{
	public $user_repository;
	public function __construct(UserRepository $user_repository){
		parent::__construct();
		$this->user_repository = $user_repository;
	}

	public function index(Request $request){
		$staff_id	= app()->ADMIN_INFO->use_id;
		$limit		= $request->input('limit', 25);
		$page		= $request->input('page', 1);
		$date_start	= $request->input('date_start', date("d/m/Y", (time() - 365*86400)));
		$date_end	= $request->input('date_end', date("d/m/Y", time()));
		$username	= $request->input('username');
		$phone		= $request->input('phone');
		$status		= $request->input('status');
		if($status !== NULL){
			$status = intval($status);
		}else{
			$status = -1;
		}

		$date_start_int	= convertDateTime($date_start, '00:00:00');
		$date_end_int	= convertDateTime($date_end, '23:59:59');

		$dataSearch		= ['username' => $username, 'phone' => $phone, 'status' => $status, 'date_start' => $date_start, 'date_end' => $date_end];

	   	$users 		= Users::orderBy('use_created_time', 'DESC');
	   	if($phone != "") $users = $users->where('use_phone', '=', $phone);
	   	if($username != "") $users = $users->where('use_loginname', '=', $username);
	   	if($date_start_int >= 0) $users = $users->where('use_created_time', '>=', $date_start_int);
	   	if($date_end_int >= 0) $users = $users->where('use_created_time', '<=', $date_end_int);

		$users	= $users->paginate($limit);

		return view('admin.user.index')->with([
									'users'			=> $users,
									'pagination' => [
											'total'			=> $users->total(),
											'current_page'	=> $users->currentPage(),
											'perpage'		=> $users->perPage(),
											'links'			=> $users->total() > $users->perPage() ? $users->appends($dataSearch)->links() : '',
										],
									'dataSearch'	=> $dataSearch
							]);
	}

	public function create(Request $request, $id = 0){
		die();
		$staff_id 		= app()->ADMIN_INFO->use_id;
		$redirect		= $request->input('url_redirect', base64_encode(route('admin.bills.create')));
		$bill_code		= '';
		$product_name	= '';
		$quantity 		= 0;
		$unit			= '';
		$picture		= '';
		$staff_note		= '';
		$user_note 		= '';
		$status 		= $this->user_repository::STATUS_NEW;
		$id				= intval($id);

		if($id > 0){
			// Lấy thông tin
			$info = $this->user_repository->getById($id);
			if(empty($info) || $info->use_is_delete != 0) die('Không tìm thấy thông tin mã vận đơn !');

			$bill_code		= $info->use_bill_code;
			$product_name	= $info->use_product_name;
			$quantity		= $info->use_quantity;
			$unit			= $info->use_unit;
			$picture		= $info->use_picture;
			$staff_note		= $info->use_staff_note;
			$user_note		= $info->use_user_note;
			$status			= $info->use_status;
		}

		$bill_code		= $request->input('bill_code', $bill_code);
		$product_name	= $request->input('product_name', $product_name);
		$unit			= $request->input('product_unit', $unit);
		$quantity		= $request->input('product_quantity', $quantity);
		$picture		= $request->input('product_picture', $picture);
		$staff_note		= $request->input('note', $staff_note);
		$status			= $request->input('status', $status);

		$picture_all 	= [];
		$url 	= Upload::getUrlImage('product', $picture);
		if(!empty($url)) $picture_all[] = ['filename' => $picture, 'url' => $url, 'is_main' => 1];

		$data = [
				'id'			=> $id,
				'bill_code'		=> $bill_code,
				'product_name'	=> $product_name,
				'quantity'		=> $quantity,
				'unit'			=> $unit,
				'status'		=> $status,
				'picture'		=> $picture,
				'staff_note'	=> $staff_note,
				'user_note'		=> $user_note,
				'picture_all' 	=> $picture_all,
				'staff_id' 		=> $staff_id,
				];

		$action	= $request->input('action');
		$errors	= [];
		if($action == 'execute'){

			$dataReturn	= $this->user_repository->create($id, $data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				$msg 	= "Thêm mới vận đơn thành công!";
				if($id > 0) $msg 	= "Cập nhật vận đơn thành công!";
				$param = [];
				if($id > 0) $param['id'] = $id;
				return redirect()->route('admin.bills.create', $param)->with('success', $msg);
			}

			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}

   		return  view('admin.bill.create', [
											'data'		=> $data,
											'arrStatus' => UserRepository::getStatusLabels(),
											'errors'	=> $errors
											]
										);
	}

	public function getDetail(Request $request){

		$id			= $request->input('id', 0);
		$staff_id	= app()->ADMIN_INFO->use_id;
		$model		= $this->user_repository->getById($id, ['user']);

		if($model){
			$data = $model->toArray();
			if(isset($data['use_picture']) && $data['use_picture']){
				$data['use_picture_url'] = Upload::getUrlImage('product', $data['use_picture']);
			}
			$data['use_status_text'] = $this->user_repository::getStatusLabels()[$data['use_status']];
			$data['use_created_time_text'] = date("d/m/Y H:i:s", $data['use_created_time']);
			$data['use_updated_at_text'] = date("d/m/Y H:i:s", $data['use_updated_at']);
			return response()->json(['status' => true, 'data' => $data], 200);
		}else{
			$msg 	= 'Không tìm thấy thông tin mã vận đơn';
			return response()->json(array('status'=> false, 'msg' => $msg), 200);
		}

		return response()->json(array('status'=> false));
	}

	public function delete(Request $request){
		$id			= $request->input('id', 0);
		$staff_id	= app()->ADMIN_INFO->use_id;
		$model		= $this->user_repository->getById($id);

		if($model && $model->use_user_id == $staff_id && $this->user_repository::userCanDelete($model->use_status)){
			$model->use_is_delete = 1;
			$model->save();
			return response()->json(['status' => true], 200);
		}else{
			$msg 	= 'Không tìm thấy thông tin mã vận đơn hoặc mã vận đơn không được phép xóa';
			return response()->json(array('status'=> false, 'msg' => $msg), 200);
		}

		return response()->json(array('status'=> false));
	}

	public function status(Request $request){
		$redirect	= $request->input('url_redirect', base64_encode(route('admin.user.list')));
		$record_id	= $request->input('record_id', 0);
		$type		= $request->input('type', '');

		// Lấy thông tin
		$info = $this->user_repository->getById($record_id);
		if(empty($info)) return response()->json(array('status'=> false, 'msg'=> 'Record not found'), 200);

		$status				= abs($info->use_active - 1);
		$info->use_active	= $status;
		$info->save();

		return response()->json(array('status'=> true, 'data' => $status), 200);
   }
}
