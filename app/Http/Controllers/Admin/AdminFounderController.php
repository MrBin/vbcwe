<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Founder as FounderModel;
use App\Repositories\FounderRepository;
use App\Helper\Upload as UploadHelper;
use Config;

class AdminFounderController extends AdminController
{
	public $founder_repository;
	public function __construct(FounderRepository $founder_repository){
		$this->founder_repository	= $founder_repository;
	}

   	public function index(Request $request){

		$limit		= 25;
		$name		= $request->input('name');

		$dataSearch	= ['name' => $name];

		$founders	= new FounderModel();
		if($name	!= "") $founders = $founders->where('fou_name', 'like', '%'. $name . '%');

		$founders	= $founders->paginate($limit);

		return  view('admin.founder.index', [
										'founders'		=> $founders,
										'pagination'=> [
																'total'			=> $founders->total(),
																'current_page'	=> $founders->currentPage(),
																'perpage'		=> $founders->perPage(),
																'links'			=> $founders->total() > $founders->perPage() ? $founders->appends($dataSearch)->links() : '',
															],
										'dataSearch'=> $dataSearch
										]
									);
   	}

   public function add(Request $request, $id = 0){

		$redirect		= $request->input('url_redirect', base64_encode(route('admin.other.founder')));
		$name			= '';
		$name_en		= '';
		$picture		= '';
		$link			= '';
		$type			= 0;
		$description	= '';
		$description_en	= '';
		$chucvu			= '';
		$chucvu_en		= '';
		$active			= '';
		$order			= '';
		$staff_id		= '';
		$active			= 0;
		$id				= intval($id);

		if($id > 0){
			// Lấy thông tin
			$info = $this->founder_repository->getById($id);
			if(empty($info)) die('Record not found !');

			$name			= $info->fou_name;
			$name_en		= $info->fou_name_en;
			$picture		= $info->fou_picture;
			$link			= $info->fou_link;
			$type			= $info->fou_type;
			$description	= $info->fou_description;
			$description_en	= $info->fou_description_en;
			$chucvu			= $info->fou_chucvu;
			$chucvu_en		= $info->fou_chucvu_en;
			$active			= $info->fou_active;
			$order			= $info->fou_order;
		}

		$name			= $request->input('name', $name);
		$name_en		= $request->input('name_en', $name_en);
		$picture		= $request->input('picture', $picture);
		$link			= $request->input('link', $link);
		$type			= $request->input('type', $type);
		$description	= $request->input('description', $description);
		$description_en	= $request->input('description_en', $description_en);
		$chucvu			= $request->input('chucvu', $chucvu);
		$chucvu_en		= $request->input('chucvu_en', $chucvu_en);
		$order			= $request->input('order', $order);


		$picture_data 	= UploadHelper::getAllPicture('other', $picture, 1);
		$data = [
					'id'				=> $id,
					'name'				=> $name,
					'name_en'			=> $name_en,
					'picture'			=> $picture,
					'picture_data' 		=> $picture_data,
					'link'				=> $link,
					'type'				=> $type,
					'description'		=> $description,
					'description_en'	=> $description_en,
					'chucvu'			=> $chucvu,
					'chucvu_en'			=> $chucvu_en,
					'order'				=> $order,
					'active' 			=> $active,
				];

		$action		= $request->input('action');
		$errors 		= [];

		if($action == 'execute'){

			$data['active'] 	= $request->input('active', 0);
			$dataReturn	= $this->founder_repository->add($id, $data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}


   		return  view('admin.founder.add', [
											'data'			=> $data,
											'errors'		=> $errors,
											'allType' 		=> $this->founder_repository::getTypeLabels()
											]
										);
   }

   public function delete(Request $request, $id){
   	$redirect = $request->input('url_redirect', base64_encode(route('admin.other.founder')));

   	// Gọi function xóa
   	$result = $this->founder_repository->delete($id);
   	if(isset($result['status']) && $result['status'] == true){
   		echo '<script>alert("Delete founder success"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}else{
   		$error = isset($result['msg']) ? $result['msg'] : '';
   		echo '<script>alert("Delelte Error: ' . $error . '"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}
   }


   public function status(Request $request){
		$redirect	= $request->input('url_redirect', base64_encode(route('admin.other.founder')));
		$record_id	= $request->input('record_id', 0);
		$type			= $request->input('type', '');

		if($record_id <= 0 || $type == '') return response()->json(array('status'=> false, 'msg'=> 'Input param incorrect'), 200);

		// Lấy thông tin
		$info = $this->founder_repository->getById($record_id);
		if(empty($info)) return response()->json(array('status'=> false, 'msg'=> 'Record not found'), 200);

		$status 	= 0;
		$dataUpdate 	= [];
		switch ($type) {
			case 'active':
				$status 	= abs($info->fou_active - 1);
				$dataUpdate 	= ['active' => $status];
				break;

			case 'hot':
				$status 	= abs($info->fou_hot - 1);
				$dataUpdate 	= ['hot' => $status];
				break;
		}

		if(empty($dataUpdate)) return response()->json(array('status'=> false, 'msg'=> 'Type update not support'), 200);

		$dataReturn	= $this->founder_repository->add($record_id, $dataUpdate);

		if(isset($dataReturn['status']) && $dataReturn['status'] == true){
			return response()->json(array('status'=> true, 'data' => $status), 200);
		}

		$errors = isset($dataReturn['errors']) ? implode(",", $dataReturn['errors']) : '';

   	return response()->json(array('status'=> false, 'msg'=> $errors), 200);
   }
}
