<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Statics as StaticsModel;
use App\Repositories\StaticsRepository;
use App\Repositories\CategoryRepository;
use App\Helper\Category as CategoryHelper;
use App\Helper\Upload as UploadHelper;
use Config;

class AdminStaticsController extends AdminController
{
	public $statics_repository;
	public $category_helper;
	public $category_repository;
	public function __construct(StaticsRepository $statics_repository, CategoryRepository $category_repository, CategoryHelper $category_helper){
		$this->statics_repository	= $statics_repository;
		$this->category_repository	= $category_repository;
		$this->category_helper	= $category_helper;
	}

   	public function index(Request $request){

		$limit		= 25;
		$title		= $request->input('title');

		$dataSearch	= ['title' => $title];
		$statics	= new StaticsModel();
		if($title	!= "") $statics = $statics->where('sta_title', 'like', '%'. $title . '%');

		$statics		= $statics->paginate($limit);

		return  view('admin.static.index', [
										'statics'		=> $statics,
										'pagination'=> [
																'total'			=> $statics->total(),
																'current_page'	=> $statics->currentPage(),
																'perpage'		=> $statics->perPage(),
																'links'			=> $statics->total() > $statics->perPage() ? $statics->appends($dataSearch)->links() : '',
															],
										'dataSearch'=> $dataSearch
										]
									);
   	}

   public function add(Request $request, $id = 0){

		$redirect	= $request->input('url_redirect', base64_encode(route('admin.static.index')));

		$title					= "";
		$title_en				= "";
		$rewrite                = "";
		$order					= '';
		$category_id			= 0;
		$status					= 0;
		$picture				= "";
		$teaser					= "";
		$teaser_en				= "";
		$description			= "";
		$description_en			= "";
		$meta_title				= '';
		$meta_title_en			= '';
		$meta_keyword			= '';
		$meta_keyword_en		= '';
		$meta_description		= '';
		$meta_description_en	= '';

		$id				= intval($id);

		if($id > 0){
			// Lấy thông tin
			$info = $this->statics_repository->getById($id);
			if(empty($info)) die('Record not found !');

			$category_id			= $info->sta_category_id;
			$title					= $info->sta_title;
			$title_en				= $info->sta_title_en;
			$rewrite 				= $info->sta_name_rewrite;
			$order					= $info->sta_order;
			$status					= $info->sta_status;
			$picture				= $info->sta_picture;
			$teaser					= $info->sta_teaser;
			$teaser_en				= $info->sta_teaser_en;
			$description			= $info->sta_description;
			$description_en			= $info->sta_description_en;
			$meta_title				= $info->sta_meta_title;
			$meta_title_en			= $info->sta_meta_title_en;
			$meta_keyword			= $info->sta_meta_keyword;
			$meta_keyword_en		= $info->sta_meta_keyword_en;
			$meta_description		= $info->sta_meta_description;
			$meta_description_en	= $info->sta_meta_description_en;
		}

		$category_id			= $request->input('category_id', $category_id);
		$title					= $request->input('title', $title);
		$title_en				= $request->input('title_en', $title_en);
		$rewrite 				= removeTitle($title);
		$order					= $request->input('order', $order);
		$status					= $request->input('status', $status);
		$picture				= $request->input('picture', $picture);
		$teaser					= $request->input('teaser', $teaser);
		$teaser_en				= $request->input('teaser_en', $teaser_en);
		$description			= $request->input('description', $description);
		$description_en			= $request->input('description_en', $description_en);
		$meta_title				= $request->input('meta_title', $meta_title);
		$meta_title_en			= $request->input('meta_title_en', $meta_title_en);
		$meta_keyword			= $request->input('meta_keyword', $meta_keyword);
		$meta_keyword_en		= $request->input('meta_keyword_en', $meta_keyword_en);
		$meta_description		= $request->input('meta_description', $meta_description);
		$meta_description_en	= $request->input('meta_description_en', $meta_description_en);

		$picture_data	= UploadHelper::getAllPicture('static', $picture, 1);
		$data = [
				'id'					=> $id,
				'category_id'			=> $category_id,
				'title'					=> $title,
				'title_en'				=> $title_en,
				'name_rewrite'			=> $rewrite,
				'order'					=> $order,
				'status'				=> $status,
				'picture'				=> $picture,
				'teaser'				=> $teaser,
				'teaser_en'				=> $teaser_en,
				'description'			=> $description,
				'description_en'		=> $description_en,
				'meta_title'			=> $meta_title,
				'meta_title_en'			=> $meta_title_en,
				'meta_keyword'			=> $meta_keyword,
				'meta_keyword_en'		=> $meta_keyword_en,
				'meta_description'		=> $meta_description,
				'meta_description_en'	=> $meta_description_en,
				'picture_data'			=> $picture_data,
				];

		$action		= $request->input('action');
		$errors 		= [];

		$allCategory 	= $this->category_helper::getAllCategory([['cat_type', '=', $this->category_repository::CATEGORY_STATICS]], false);

		if($action == 'execute'){

			$data['status'] 	= $request->input('status', 0);
			$dataReturn	= $this->statics_repository->add($id, $data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}


   		return  view('admin.static.add', [
											'data'			=> $data,
											'errors'		=> $errors,
											'allCategory' 	=> $allCategory
											]
										);
   }

   public function delete(Request $request, $id){
   	$redirect = $request->input('url_redirect', base64_encode(route('admin.static.index')));

   	// Gọi function xóa
   	$result = $this->statics_repository->delete($id);
   	if(isset($result['status']) && $result['status'] == true){
   		echo '<script>alert("Delete static success"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}else{
   		$error = isset($result['msg']) ? $result['msg'] : '';
   		echo '<script>alert("Delelte Error: ' . $error . '"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}
   }


   	public function status(Request $request){
		$redirect	= $request->input('url_redirect', base64_encode(route('admin.static.index')));
		$record_id	= $request->input('record_id', 0);
		$type			= $request->input('type', '');

		if($record_id <= 0 || $type == '') return response()->json(array('status'=> false, 'msg'=> 'Input param incorrect'), 200);

		// Lấy thông tin
		$info = $this->statics_repository->getById($record_id);
		if(empty($info)) return response()->json(array('status'=> false, 'msg'=> 'Record not found'), 200);

		$status 	= 0;
		$dataUpdate 	= [];
		switch ($type) {
			case 'active':
				$status 	= abs($info->sta_status - 1);
				$dataUpdate 	= ['status' => $status];
				break;
		}

		if(empty($dataUpdate)) return response()->json(array('status'=> false, 'msg'=> 'Type update not support'), 200);

		$dataReturn	= $this->statics_repository->add($record_id, $dataUpdate);

		if(isset($dataReturn['status']) && $dataReturn['status'] == true){
			return response()->json(array('status'=> true, 'data' => $status), 200);
		}

		$errors = isset($dataReturn['errors']) ? implode(",", $dataReturn['errors']) : '';

   		return response()->json(array('status'=> false, 'msg'=> $errors), 200);
    }

    public function category(Request $request){

		$name		= $request->input('name');
	   	$dataSearch = ['name' => $name];
	   	$param 		= [['cat_type', '=', $this->category_repository::CATEGORY_STATICS]];
	   	if($name != "") $param[] = ['cat_name', 'like', '%'. $name . '%'];
	   	$category 	= $this->category_helper::getAllCategory($param);
	   	$arrType 	= $this->category_repository->getTypeLabels();

	   	return  view('admin.static.category_list', [	'view' 			=> 'list',
																'category'		=> $category,
																'arrType' 		=> $arrType,
																'dataSearch'	=> $dataSearch,
																'infoStaff'		=> app()->ADMIN_INFO
		   													]
	   												);

	}

	public function categoryAdd(Request $request, $id = 0){

		$redirect				= $request->input('url_redirect', base64_encode(route('admin.static.category_add')));
		$name					= '';
		$name_en				= '';
		$name_rewrite			= '';
		$meta_title				= '';
		$meta_title_en			= '';
		$meta_keyword			= '';
		$meta_keyword_en		= '';
		$meta_description		= '';
		$meta_description_en	= '';
		$picture				= '';
		$type					= $this->category_repository::CATEGORY_STATICS;
		$parent_id				= 0;
		$order					= 0;
		$active					= 0;
		$id						= intval($id);

		if($id > 0){
			// Lấy thông tin
			$info = $this->category_repository->getById($id);
			if(empty($info)) die('Record not found !');

			$name					= $info->cat_name;
			$name_en				= $info->cat_name_en;
			$name_rewrite			= $info->cat_name_rewrite;
			$picture				= $info->cat_picture;
			$parent_id				= $info->cat_parent_id;
			$order					= $info->cat_order;
			$meta_title				= $info->cat_meta_title;
			$meta_title_en			= $info->cat_meta_title_en;
			$meta_keyword			= $info->cat_meta_keyword;
			$meta_keyword_en		= $info->cat_meta_keyword_en;
			$meta_description		= $info->cat_meta_description;
			$meta_description_en	= $info->cat_meta_description_en;
			$active					= $info->cat_active;
		}

		$name				= $request->input('name', $name);
		$name_en			= $request->input('name_en', $name_en);
		$name_rewrite		= $request->input('name_rewrite', $name_rewrite);
		if($name_rewrite == "") $name_rewrite = removeTitle($name);
		$picture			= $request->input('picture', $picture);
		$picture_data 		 = UploadHelper::getAllPicture('category', $picture, 1);
		$parent_id			= $request->input('parent_id', $parent_id);
		$order				= $request->input('order', $order);
		$meta_title_en			= $request->input('meta_title_en', $meta_title_en);
		$meta_keyword_en		= $request->input('meta_keyword_en', $meta_keyword_en);
		$meta_description_en	= $request->input('meta_description_en', $meta_description_en);
		$data = [
					'id'					=> $id,
					'name_rewrite'			=> $name_rewrite,
					'name'					=> $name,
					'name_en'				=> $name_en,
					'picture'				=> $picture,
					'picture_data'			=> $picture_data,
					'type'					=> $type,
					'parent_id'				=> $parent_id,
					'order'					=> $order,
					'meta_title'			=> $meta_title,
					'meta_title_en'			=> $meta_title_en,
					'meta_keyword'			=> $meta_keyword,
					'meta_keyword_en'		=> $meta_keyword_en,
					'meta_description'		=> $meta_description,
					'meta_description_en'	=> $meta_description_en,
					'active'				=> $active,
				];

		$allCategory 	= [];
		$temp = $this->category_helper::getAllCategory([['cat_type', '=', $type]]);

		if(isset($temp[$type])) $allCategory 	= $temp[$type];

		$action		= $request->input('action');
		$errors 		= [];

		if($action == 'execute'){
			$data['active'] = $request->input('active', 0);
			$dataReturn	= $this->category_repository->add($id, $data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}

   		return  view('admin.static.category_add', [
												'view'			=> 'add',
												'data'			=> $data,
												'allCategory'	=> $allCategory,
												'errors'		=> $errors
												]
											);
	}

	public function categoryStatus(Request $request){
		$redirect	= $request->input('url_redirect', base64_encode(route('admin.static.category')));
		$record_id	= $request->input('record_id', 0);
		$type		= $request->input('type', '');

		// Lấy thông tin
		$info = $this->category_repository->getById($record_id);
		if(empty($info)) return response()->json(array('status'=> false, 'msg'=> 'Record not found'), 200);

		$dataUpdate 	= [];
		switch ($type) {

			case 'active':
				$status 	= abs($info->cat_active - 1);
				$dataUpdate['active'] = $status;
				break;
		}

		if($dataUpdate){
			$dataReturn	= $this->category_repository->add($record_id, $dataUpdate);

			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return response()->json(array('status'=> true, 'data' => $status), 200);
			}

			$errors = isset($dataReturn['errors']) ? implode(",", $dataReturn['errors']) : '';
		}else{
			$errors = 'Type update not found';
		}

   		return response()->json(array('status'=> false, 'msg'=> $errors), 200);
   	}
}
