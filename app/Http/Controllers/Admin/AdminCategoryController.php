<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Category as CategoryModel;
use App\Repositories\CategoryRepository;
use App\Helper\Category as CategoryHelper;
use App\Transformers\CategoryTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Config;

class AdminCategoryController extends AdminController
{
	public $category_repository;
	public $category_helper;
	public function __construct(CategoryRepository $category_repository, CategoryHelper $category_helper){
		$this->category_repository	= $category_repository;
		$this->category_helper	= $category_helper;
	}

   public function index(Request $request){

		$name			= $request->input('name');
		$parent_id	= $request->input('parent_id');

   	$dataSearch = ['name' => $name, 'parent_id' => $parent_id];

   	$category 	= $this->category_helper::getAllCategory([]);
   	$arrType 	= $this->category_helper::$arrType;

   	return  view('admin.category.index', [	'view' 			=> 'list',
															'category'		=> $category,
															'arrType' 		=> $arrType,
															'dataSearch'	=> $dataSearch,
															'infoStaff'		=> app()->ADMIN_INFO
	   													]
   												);

   }

   public function add(Request $request, $id = 0){

		$redirect		= $request->input('url_redirect', base64_encode(route('admin.category.list')));
		$name				= '';
		$name_rewrite	= '';
		$meta_title 	= '';
		$meta_keyword 	= '';
		$meta_description = '';
		$picture			= '';
		$type				= '';
		$parent_id		= 0;
		$order			= 0;
		$id				= intval($id);

		if($id > 0){
			// Lấy thông tin
			$info = $this->category_repository->getById($id);
			if(empty($info)) die('Record not found !');

			$name					= $info->cat_name;
			$name_rewrite		= $info->cat_name_rewrite;
			$picture				= $info->cat_picture;
			$type					= $info->cat_type;
			$parent_id			= $info->cat_parent_id;
			$order				= $info->cat_order;
			$meta_title			= $info->cat_meta_title;
			$meta_keyword		= $info->cat_meta_keyword;
			$meta_description	= $info->cat_meta_description;
		}

		$name					= $request->input('name', $name);
		$name_rewrite		= $request->input('name_rewrite', $name_rewrite);
		$picture				= $request->input('picture', $picture);
		$type					= $request->input('type', $type);
		$parent_id			= $request->input('parent_id', $parent_id);
		$order				= $request->input('order', $order);
		$meta_title			= $request->input('meta_title', $meta_title);
		$meta_keyword		= $request->input('meta_keyword', $meta_keyword);
		$meta_description	= $request->input('meta_description', $meta_description);
		$data = [
					'id'						=> $id,
					'name_rewrite'			=> $name_rewrite,
					'name'					=> $name,
					'picture'				=> $picture,
					'type'					=> $type,
					'parent_id'				=> $parent_id,
					'order'					=> $order,
					'meta_title'			=> $meta_title,
					'meta_keyword'			=> $meta_keyword,
					'meta_description'	=> $meta_description,
				];

		$allCategory 	= [];
		if($type != '') $temp = $this->category_helper::getAllCategory([['cat_type', '=', $type]]);
		if(isset($temp[$type])) $allCategory 	= $temp[$type];
   	$arrType 	= $this->category_helper::$arrType;

		$action		= $request->input('action');
		$errors 		= [];

		if($action == 'execute'){

			$dataReturn	= $this->category_repository->add($id, $data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}


   	return  view('admin.category.index', [
														'view'			=> 'add',
														'data'			=> $data,
														'allCategory'	=> $allCategory,
														'allType' 		=> $arrType,
														'errors'			=> $errors
   													]
   												);
   }

   public function delete(Request $request, $id){
   	die('Access Denied');
   	$redirect = $request->input('url_redirect', base64_encode(route('admin.category.list')));

   	// Gọi function xóa
   	$result = $this->category_repository->delete($id);
   	if(isset($result['status']) && $result['status'] == true){
   		echo '<script>alert("Delete acount success"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}else{
   		$error = isset($result['msg']) ? $result['msg'] : '';
   		echo '<script>alert("Delelte Error: ' . $error . '"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}
   }


   public function status(Request $request){
		$redirect	= $request->input('url_redirect', base64_encode(route('admin.staff.list')));
		$record_id	= $request->input('record_id', 0);
		$type			= $request->input('type', '');

		if($record_id <= 0 || $type == '') return response()->json(array('status'=> false, 'msg'=> 'Input param incorrect'), 200);

		// Lấy thông tin
		$info = $this->category_repository->getById($record_id);
		if(empty($info)) return response()->json(array('status'=> false, 'msg'=> 'Record not found'), 200);

		$status 	= abs($info->cat_active - 1);
		$dataUpdate 	= [];
		switch ($type) {
			case 'active':
				$status 	= abs($info->cat_active - 1);
				$dataUpdate 	= ['active' => $status];
				break;

			case 'hot':
				$status 	= abs($info->cat_hot - 1);
				$dataUpdate 	= ['hot' => $status];
				break;
		}

		if(empty($dataUpdate)) return response()->json(array('status'=> false, 'msg'=> 'Type update not support'), 200);

		$dataReturn	= $this->category_repository->add($record_id, $dataUpdate);

		if(isset($dataReturn['status']) && $dataReturn['status'] == true){
			return response()->json(array('status'=> true, 'data' => $status), 200);
		}

		$errors = isset($dataReturn['errors']) ? implode(",", $dataReturn['errors']) : '';

   	return response()->json(array('status'=> false, 'msg'=> $errors), 200);
   }
}