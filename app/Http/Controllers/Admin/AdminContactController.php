<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Contact as ContactModel;
use App\Repositories\ContactRepository;
use Config;

class AdminContactController extends AdminController
{
	public $contact_repository;
	public function __construct(ContactRepository $contact_repository){
		$this->contact_repository	= $contact_repository;
	}

   	public function index(Request $request){

		$limit		= 25;
		$name		= $request->input('name');
		$type		= intval($request->input('type', 0));

		$dataSearch	= ['name' => $name, 'type' => $type];

		$contacts	= new ContactModel();
		if($name	!= "") $contacts = $contacts->where('con_name', 'like', '%'. $name . '%');
		if($type	> 0) $contacts = $contacts->where('con_type', '=', $type);

		$contacts	= $contacts->orderBy('con_id', 'DESC')->paginate($limit);

		return  view('admin.contact.index', [
										'contacts'	=> $contacts,
										'pagination'=> [
																'total'			=> $contacts->total(),
																'current_page'	=> $contacts->currentPage(),
																'perpage'		=> $contacts->perPage(),
																'links'			=> $contacts->total() > $contacts->perPage() ? $contacts->appends($dataSearch)->links() : '',
															],
										'dataSearch'=> $dataSearch,
										'allType' => $this->contact_repository::getTypeLabels(),
										]
									);
   	}

   	public function detail(Request $request, $id = 0){
        $id	= intval($id);
        $data = [];

        $data = $this->contact_repository->getById($id);

        if(empty($data)) die('Record not found !');

        $arrScale = [
            1 => '0 - 50 nhân viên',
            2 => '51 - 200 nhân viên',
            3 => '201 - 500 nhân viên',
            4 => '500 - 1000 nhân viên',
            5 => 'Hơn 1000 nhân viên'
        ];

        $arrNeed = [
            0 => 'Tìm hiểu về VBCWE',
            1 => 'Gia nhập mạng lưới',
            2 => 'Tư vấn về các công cụ đánh giá BĐG trong doanh nghiệp',
            3 => 'Tìm hiểu về các thông tin khác'
        ];

        return  view('admin.contact.detail', [
            'data' => $data,
            'arrScale' => $arrScale,
            'arrNeed' => $arrNeed
        ]);
   	}

   public function add(Request $request, $id = 0){

		$redirect		= $request->input('url_redirect', base64_encode(route('admin.other.contact')));
		$name			= '';
		$title			= '';
		$email			= '';
		$description	= '';
		$active			= 0;
		$order			= '';
		$staff_id		= '';
		$id				= intval($id);

		if($id > 0){
			// Lấy thông tin
			$info = $this->contact_repository->getById($id);
			if(empty($info)) die('Record not found !');

			$name			= $info->con_name;
			$email			= $info->con_email;
			$title			= $info->con_title;
			$description	= $info->con_description;
			$active			= $info->con_status;
			$order			= $info->con_order;
		}

		$name			= $request->input('name', $name);
		$email			= $request->input('email', $email);
		$title			= $request->input('title', $title);
		$description	= $request->input('description', $description);

		$data = [
					'id'			=> $id,
					'name'			=> $name,
					'email'			=> $email,
					'title'			=> $title,
					'description'	=> $description,
					'status'		=> $active,
				];

		$action		= $request->input('action');
		$errors 		= [];

		if($action == 'execute'){

			$data['status'] 	= $request->input('active', 0);
			$dataReturn	= $this->contact_repository->add($id, $data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}


   		return  view('admin.contact.add', [
											'data'			=> $data,
											'errors'		=> $errors
											]
										);
   }

   public function delete(Request $request, $id){
   	$redirect = $request->input('url_redirect', base64_encode(route('admin.other.contact')));

   	// Gọi function xóa
   	$result = $this->contact_repository->delete($id);
   	if(isset($result['status']) && $result['status'] == true){
   		echo '<script>alert("Delete contact success"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}else{
   		$error = isset($result['msg']) ? $result['msg'] : '';
   		echo '<script>alert("Delelte Error: ' . $error . '"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}
   }


   public function status(Request $request){
		$redirect	= $request->input('url_redirect', base64_encode(route('admin.other.contact')));
		$record_id	= $request->input('record_id', 0);
		$type			= $request->input('type', '');

		if($record_id <= 0 || $type == '') return response()->json(array('status'=> false, 'msg'=> 'Input param incorrect'), 200);

		// Lấy thông tin
		$info = $this->contact_repository->getById($record_id);
		if(empty($info)) return response()->json(array('status'=> false, 'msg'=> 'Record not found'), 200);

		$status 	= 0;
		$dataUpdate 	= [];
		switch ($type) {
			case 'active':
				$status 	= abs($info->con_status - 1);
				$dataUpdate 	= ['status' => $status];
				break;

			case 'hot':
				$status 	= abs($info->con_hot - 1);
				$dataUpdate 	= ['hot' => $status];
				break;
		}

		if(empty($dataUpdate)) return response()->json(array('status'=> false, 'msg'=> 'Type update not support'), 200);

		$dataReturn	= $this->contact_repository->add($record_id, $dataUpdate);

		if(isset($dataReturn['status']) && $dataReturn['status'] == true){
			return response()->json(array('status'=> true, 'data' => $status), 200);
		}

		$errors = isset($dataReturn['errors']) ? implode(",", $dataReturn['errors']) : '';

   	return response()->json(array('status'=> false, 'msg'=> $errors), 200);
   }
}
