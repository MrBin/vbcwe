<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\ConfigurationRepository;
use App\Helper\Upload as UploadHelper;
use App\Helper\UserTranslate as UserTranslate;
use Config;

class AdminConfigurationController extends AdminController
{
	public $configuration_repository;
	public function __construct(ConfigurationRepository $configuration_repository){
		$this->configuration_repository	= $configuration_repository;
	}

   	public function index(Request $request){
		$redirect	= base64_encode(route('admin.configs.configuration'));
		// Lấy thông tin
		$info = $this->configuration_repository->getConfiguration();
		if(empty($info)) die('Record not found !');
		$website					= $info->con_website;
		$website_en					= $info->con_website_en;
		$address					= $info->con_address;
		$address_en					= $info->con_address_en;
		$web_master_tool			= $info->con_web_master_tool;
		$web_master_tool_en			= $info->con_web_master_tool_en;
		$favicon					= $info->con_favicon;
		$favicon_en					= $info->con_favicon_en;
		$analytics					= $info->con_analytics;
		$analytics_en				= $info->con_analytics_en;
		$mail						= $info->con_mail;
		$mail_en					= $info->con_mail_en;
		$hotline_1					= $info->con_hotline_1;
		$hotline_1_en				= $info->con_hotline_1_en;
		$hotline_2					= $info->con_hotline_2;
		$hotline_2_en				= $info->con_hotline_2_en;
		$logo						= $info->con_logo;
		$logo_en					= $info->con_logo_en;
		$copyright					= $info->con_copyright;
		$copyright_en				= $info->con_copyright_en;
		$seo_keyword				= $info->con_seo_keyword;
		$seo_keyword_en				= $info->con_seo_keyword_en;
		$seo_title					= $info->con_seo_title;
		$seo_title_en				= $info->con_seo_title_en;
		$seo_description			= $info->con_seo_description;
		$seo_description_en			= $info->con_seo_description_en;
		$banner_search				= $info->con_banner_search;
		$banner_search_en			= $info->con_banner_search_en;
		$banner_thuvien				= $info->con_banner_thuvien;
		$banner_thuvien_en			= $info->con_banner_thuvien_en;
		$banner_default				= $info->con_banner_default;
		$banner_default_en			= $info->con_banner_default_en;
		$banner_event				= $info->con_banner_event;
		$banner_event_en			= $info->con_banner_event_en;
		$content_contact			= $info->con_content_contact;
		$content_contact_en			= $info->con_content_contact_en;
		$banner_contact				= $info->con_banner_contact;
		$banner_contact_en			= $info->con_banner_contact_en;
		$title_mangluoi				= $info->con_title_mangluoi;
		$title_mangluoi_en			= $info->con_title_mangluoi_en;
		$banner_mangluoi			= $info->con_banner_mangluoi;
		$banner_mangluoi_en			= $info->con_banner_mangluoi_en;
		$title_member				= $info->con_title_member;
		$title_member_en			= $info->con_title_member_en;
		$banner_member				= $info->con_banner_member;
		$banner_member_en			= $info->con_banner_member_en;
		$footer						= $info->con_footer;
		$footer_en					= $info->con_footer_en;
		$map_contact				= $info->con_map_contact;
		$map_contact_en				= $info->con_map_contact_en;
		$add_footer					= $info->con_add_footer;
		$add_footer_en				= $info->con_add_footer_en;
		$add_header					= $info->con_add_header;
		$add_header_en				= $info->con_add_header_en;
		$title_email				= $info->con_title_email;
		$title_email_en				= $info->con_title_email_en;
		$name_email					= $info->con_name_email;
		$name_email_en				= $info->con_name_email_en;
		$mail_receive_order			= $info->con_mail_receive_order;
		$mail_receive_order_en		= $info->con_mail_receive_order_en;
		$mail_send_order			= $info->con_mail_send_order;
		$mail_send_order_en			= $info->con_mail_send_order_en;
		$mail_pass_send_order		= $info->con_mail_pass_send_order;
		$mail_pass_send_order_en	= $info->con_mail_pass_send_order_en;
		$linkedin					= $info->con_linkedin;
		$linkedin_en				= $info->con_linkedin_en;
		$youtube					= $info->con_youtube;
		$youtube_en					= $info->con_youtube_en;
		$facebook					= $info->con_facebook;
		$facebook_en				= $info->con_facebook_en;
		$banner_thamgia_mangluoi			= $info->con_banner_thamgia_mangluoi;
		$banner_thamgia_mangluoi_en			= $info->con_banner_thamgia_mangluoi_en;
		$title_thamgia_mangluoi				= $info->con_title_thamgia_mangluoi;
		$title_thamgia_mangluoi_en			= $info->con_title_thamgia_mangluoi_en;
		$description_thamgia_mangluoi		= $info->con_description_thamgia_mangluoi;
		$description_thamgia_mangluoi_en	= $info->con_description_thamgia_mangluoi_en;
		$banner_kienthuc_thamkhao			= $info->con_banner_kienthuc_thamkhao;
		$banner_kienthuc_thamkhao_en		= $info->con_banner_kienthuc_thamkhao_en;
		$title_kienthuc_thamkhao			= $info->con_title_kienthuc_thamkhao;
		$title_kienthuc_thamkhao_en			= $info->con_title_kienthuc_thamkhao_en;
		$description_kienthuc_thamkhao		= $info->con_description_kienthuc_thamkhao;
		$description_kienthuc_thamkhao_en	= $info->con_description_kienthuc_thamkhao_en;

		$website					= $request->input('website', $website);
		$website_en					= $request->input('website_en', $website_en);
		$address					= $request->input('address', $address);
		$address_en					= $request->input('address_en', $address_en);
		$web_master_tool			= $request->input('web_master_tool', $web_master_tool);
		$web_master_tool_en			= $request->input('web_master_tool_en', $web_master_tool_en);
		$favicon					= $request->input('favicon', $favicon);
		$favicon_en					= $request->input('favicon_en', $favicon_en);
		$analytics					= $request->input('analytics', $analytics);
		$analytics_en				= $request->input('analytics_en', $analytics_en);
		$mail						= $request->input('mail', $mail);
		$mail_en					= $request->input('mail_en', $mail_en);
		$hotline_1					= $request->input('hotline_1', $hotline_1);
		$hotline_1_en				= $request->input('hotline_1_en', $hotline_1_en);
		$hotline_2					= $request->input('hotline_2', $hotline_2);
		$hotline_2_en				= $request->input('hotline_2_en', $hotline_2_en);
		$logo						= $request->input('logo', $logo);
		$logo_en					= $request->input('logo_en', $logo_en);
		$copyright					= $request->input('copyright', $copyright);
		$copyright_en				= $request->input('copyright_en', $copyright_en);
		$seo_keyword				= $request->input('seo_keyword', $seo_keyword);
		$seo_keyword_en				= $request->input('seo_keyword_en', $seo_keyword_en);
		$seo_title					= $request->input('seo_title', $seo_title);
		$seo_title_en				= $request->input('seo_title_en', $seo_title_en);
		$seo_description			= $request->input('seo_description', $seo_description);
		$seo_description_en			= $request->input('seo_description_en', $seo_description_en);
		$banner_search				= $request->input('banner_search', $banner_search);
		$banner_search_en			= $request->input('banner_search_en', $banner_search_en);
		$banner_thuvien				= $request->input('banner_thuvien', $banner_thuvien);
		$banner_thuvien_en			= $request->input('banner_thuvien_en', $banner_thuvien_en);
		$banner_default				= $request->input('banner_default', $banner_default);
		$banner_default_en			= $request->input('banner_default_en', $banner_default_en);
		$banner_event				= $request->input('banner_event', $banner_event);
		$banner_event_en			= $request->input('banner_event_en', $banner_event_en);
		$content_contact			= $request->input('content_contact', $content_contact);
		$content_contact_en			= $request->input('content_contact_en', $content_contact_en);
		$banner_contact				= $request->input('banner_contact', $banner_contact);
		$banner_contact_en			= $request->input('banner_contact_en', $banner_contact_en);
		$title_mangluoi				= $request->input('title_mangluoi', $title_mangluoi);
		$title_mangluoi_en			= $request->input('title_mangluoi_en', $title_mangluoi_en);
		$banner_mangluoi			= $request->input('banner_mangluoi', $banner_mangluoi);
		$banner_mangluoi_en			= $request->input('banner_mangluoi_en', $banner_mangluoi_en);
		$title_member				= $request->input('title_member', $title_member);
		$title_member_en			= $request->input('title_member_en', $title_member_en);
		$banner_member				= $request->input('banner_member', $banner_member);
		$banner_member_en			= $request->input('banner_member_en', $banner_member_en);
		$footer						= $request->input('footer', $footer);
		$footer_en					= $request->input('footer_en', $footer_en);
		$map_contact				= $request->input('map_contact', $map_contact);
		$map_contact_en				= $request->input('map_contact_en', $map_contact_en);
		$add_footer					= $request->input('add_footer', $add_footer);
		$add_footer_en				= $request->input('add_footer_en', $add_footer_en);
		$add_header					= $request->input('add_header', $add_header);
		$add_header_en				= $request->input('add_header_en', $add_header_en);
		$title_email				= $request->input('title_email', $title_email);
		$title_email_en				= $request->input('title_email_en', $title_email_en);
		$name_email					= $request->input('name_email', $name_email);
		$name_email_en				= $request->input('name_email_en', $name_email_en);
		$mail_receive_order			= $request->input('mail_receive_order', $mail_receive_order);
		$mail_receive_order_en		= $request->input('mail_receive_order_en', $mail_receive_order_en);
		$mail_send_order			= $request->input('mail_send_order', $mail_send_order);
		$mail_send_order_en			= $request->input('mail_send_order_en', $mail_send_order_en);
		$mail_pass_send_order		= $request->input('mail_pass_send_order', $mail_pass_send_order);
		$mail_pass_send_order_en	= $request->input('mail_pass_send_order_en', $mail_pass_send_order_en);
		$linkedin					= $request->input('linkedin', $linkedin);
		$linkedin_en				= $request->input('linkedin_en', $linkedin_en);
		$youtube					= $request->input('youtube', $youtube);
		$youtube_en					= $request->input('youtube_en', $youtube_en);
		$facebook					= $request->input('facebook', $facebook);
		$facebook_en				= $request->input('facebook_en', $facebook_en);

		$banner_thamgia_mangluoi			= $request->input('banner_thamgia_mangluoi', $banner_thamgia_mangluoi);
		$banner_thamgia_mangluoi_en			= $request->input('banner_thamgia_mangluoi_en', $banner_thamgia_mangluoi_en);
		$title_thamgia_mangluoi				= $request->input('title_thamgia_mangluoi', $title_thamgia_mangluoi);
		$title_thamgia_mangluoi_en			= $request->input('title_thamgia_mangluoi_en', $title_thamgia_mangluoi_en);
		$description_thamgia_mangluoi		= $request->input('description_thamgia_mangluoi', $description_thamgia_mangluoi);
		$description_thamgia_mangluoi_en	= $request->input('description_thamgia_mangluoi_en', $description_thamgia_mangluoi_en);
		$banner_kienthuc_thamkhao			= $request->input('banner_kienthuc_thamkhao', $banner_kienthuc_thamkhao);
		$banner_kienthuc_thamkhao_en		= $request->input('banner_kienthuc_thamkhao_en', $banner_kienthuc_thamkhao_en);
		$title_kienthuc_thamkhao			= $request->input('title_kienthuc_thamkhao', $title_kienthuc_thamkhao);
		$title_kienthuc_thamkhao_en			= $request->input('title_kienthuc_thamkhao_en', $title_kienthuc_thamkhao_en);
		$description_kienthuc_thamkhao		= $request->input('description_kienthuc_thamkhao', $description_kienthuc_thamkhao);
		$description_kienthuc_thamkhao_en	= $request->input('description_kienthuc_thamkhao_en', $description_kienthuc_thamkhao_en);

		$favicon_data				= UploadHelper::getAllPicture('config', $favicon, 1);
		$favicon_en_data			= UploadHelper::getAllPicture('config', $favicon_en, 1);
		$logo_data					= UploadHelper::getAllPicture('config', $logo, 1);
		$logo_en_data				= UploadHelper::getAllPicture('config', $logo_en, 1);
		$banner_search_data			= UploadHelper::getAllPicture('config', $banner_search, 1);
		$banner_search_en_data		= UploadHelper::getAllPicture('config', $banner_search_en, 1);
		$banner_thuvien_data		= UploadHelper::getAllPicture('config', $banner_thuvien, 1);
		$banner_thuvien_en_data		= UploadHelper::getAllPicture('config', $banner_thuvien_en, 1);
		$banner_default_data		= UploadHelper::getAllPicture('config', $banner_default, 1);
		$banner_default_en_data		= UploadHelper::getAllPicture('config', $banner_default_en, 1);
		$banner_event_data			= UploadHelper::getAllPicture('config', $banner_event, 1);
		$banner_event_en_data		= UploadHelper::getAllPicture('config', $banner_event_en, 1);
		$banner_contact_data		= UploadHelper::getAllPicture('config', $banner_contact, 1);
		$banner_contact_en_data		= UploadHelper::getAllPicture('config', $banner_contact_en, 1);
		$banner_mangluoi_data		= UploadHelper::getAllPicture('config', $banner_mangluoi, 1);
		$banner_mangluoi_en_data	= UploadHelper::getAllPicture('config', $banner_mangluoi_en, 1);
		$banner_member_data			= UploadHelper::getAllPicture('config', $banner_member, 1);
		$banner_member_en_data		= UploadHelper::getAllPicture('config', $banner_member_en, 1);

		$banner_thamgia_mangluoi_data			= UploadHelper::getAllPicture('config', $banner_thamgia_mangluoi, 1);
		$banner_thamgia_mangluoi_en_data		= UploadHelper::getAllPicture('config', $banner_thamgia_mangluoi_en, 1);
		$banner_kienthuc_thamkhao_data			= UploadHelper::getAllPicture('config', $banner_kienthuc_thamkhao, 1);
		$banner_kienthuc_thamkhao_en_data		= UploadHelper::getAllPicture('config', $banner_kienthuc_thamkhao_en, 1);

		$data = [
					'website'					=> $website,
					'website_en'				=> $website_en,
					'address'					=> $address,
					'address_en'				=> $address_en,
					'web_master_tool'			=> $web_master_tool,
					'web_master_tool_en'		=> $web_master_tool_en,
					'favicon'					=> $favicon,
					'favicon_en'				=> $favicon_en,
					'analytics'					=> $analytics,
					'analytics_en'				=> $analytics_en,
					'mail'						=> $mail,
					'mail_en'					=> $mail_en,
					'hotline_1'					=> $hotline_1,
					'hotline_1_en'				=> $hotline_1_en,
					'hotline_2'					=> $hotline_2,
					'hotline_2_en'				=> $hotline_2_en,
					'logo'						=> $logo,
					'logo_en'					=> $logo_en,
					'copyright'					=> $copyright,
					'copyright_en'				=> $copyright_en,
					'seo_keyword'				=> $seo_keyword,
					'seo_keyword_en'			=> $seo_keyword_en,
					'seo_title'					=> $seo_title,
					'seo_title_en'				=> $seo_title_en,
					'seo_description'			=> $seo_description,
					'seo_description_en'		=> $seo_description_en,
					'banner_search'				=> $banner_search,
					'banner_search_en'			=> $banner_search_en,
					'banner_thuvien'			=> $banner_thuvien,
					'banner_thuvien_en'			=> $banner_thuvien_en,
					'banner_default'			=> $banner_default,
					'banner_default_en'			=> $banner_default_en,
					'banner_event'				=> $banner_event,
					'banner_event_en'			=> $banner_event_en,
					'content_contact'			=> $content_contact,
					'content_contact_en'		=> $content_contact_en,
					'banner_contact'			=> $banner_contact,
					'banner_contact_en'			=> $banner_contact_en,
					'title_mangluoi'			=> $title_mangluoi,
					'title_mangluoi_en'			=> $title_mangluoi_en,
					'banner_mangluoi'			=> $banner_mangluoi,
					'banner_mangluoi_en'		=> $banner_mangluoi_en,
					'title_member'				=> $title_member,
					'title_member_en'			=> $title_member_en,
					'banner_member'				=> $banner_member,
					'banner_member_en'			=> $banner_member_en,
					'footer'					=> $footer,
					'footer_en'					=> $footer_en,
					'map_contact'				=> $map_contact,
					'map_contact_en'			=> $map_contact_en,
					'add_footer'				=> $add_footer,
					'add_footer_en'				=> $add_footer_en,
					'add_header'				=> $add_header,
					'add_header_en'				=> $add_header_en,
					'title_email'				=> $title_email,
					'title_email_en'			=> $title_email_en,
					'name_email'				=> $name_email,
					'name_email_en'				=> $name_email_en,
					'mail_receive_order'		=> $mail_receive_order,
					'mail_receive_order_en'		=> $mail_receive_order_en,
					'mail_send_order'			=> $mail_send_order,
					'mail_send_order_en'		=> $mail_send_order_en,
					'mail_pass_send_order'		=> $mail_pass_send_order,
					'mail_pass_send_order_en'	=> $mail_pass_send_order_en,
					'linkedin'					=> $linkedin,
					'linkedin_en'				=> $linkedin_en,
					'youtube'					=> $youtube,
					'youtube_en'				=> $youtube_en,
					'facebook'					=> $facebook,
					'facebook_en'				=> $facebook_en,
					'banner_thamgia_mangluoi'			=> $banner_thamgia_mangluoi,
					'banner_thamgia_mangluoi_en'		=> $banner_thamgia_mangluoi_en,
					'title_thamgia_mangluoi'			=> $title_thamgia_mangluoi,
					'title_thamgia_mangluoi_en'			=> $title_thamgia_mangluoi_en,
					'description_thamgia_mangluoi'		=> $description_thamgia_mangluoi,
					'description_thamgia_mangluoi_en'	=> $description_thamgia_mangluoi_en,
					'banner_kienthuc_thamkhao'			=> $banner_kienthuc_thamkhao,
					'banner_kienthuc_thamkhao_en'		=> $banner_kienthuc_thamkhao_en,
					'title_kienthuc_thamkhao'			=> $title_kienthuc_thamkhao,
					'title_kienthuc_thamkhao_en'		=> $title_kienthuc_thamkhao_en,
					'description_kienthuc_thamkhao'		=> $description_kienthuc_thamkhao,
					'description_kienthuc_thamkhao_en'	=> $description_kienthuc_thamkhao_en,
					'favicon_data' 				=> $favicon_data,
					'favicon_en_data' 			=> $favicon_en_data,
					'logo_data' 				=> $logo_data,
					'logo_en_data' 				=> $logo_en_data,
					'banner_search_data'		=> $banner_search_data,
					'banner_search_en_data'		=> $banner_search_en_data,
					'banner_thuvien_data'		=> $banner_thuvien_data,
					'banner_thuvien_en_data'	=> $banner_thuvien_en_data,
					'banner_default_data'		=> $banner_default_data,
					'banner_default_en_data'	=> $banner_default_en_data,
					'banner_event_data'			=> $banner_event_data,
					'banner_event_en_data'		=> $banner_event_en_data,
					'banner_contact_data'		=> $banner_contact_data,
					'banner_contact_en_data'	=> $banner_contact_en_data,
					'banner_mangluoi_data'		=> $banner_mangluoi_data,
					'banner_mangluoi_en_data'	=> $banner_mangluoi_en_data,
					'banner_member_data'		=> $banner_member_data,
					'banner_member_en_data'		=> $banner_member_en_data,
					'banner_thamgia_mangluoi_data' => $banner_thamgia_mangluoi_data,
					'banner_thamgia_mangluoi_en_data' => $banner_thamgia_mangluoi_en_data,
					'banner_kienthuc_thamkhao_data' => $banner_kienthuc_thamkhao_data,
					'banner_kienthuc_thamkhao_en_data' => $banner_kienthuc_thamkhao_en_data,
				];

		$action		= $request->input('action');
		$errors 		= [];

		if($action == 'execute'){

			$dataReturn	= $this->configuration_repository->update($data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}


   	return  view('admin.config.configuration', [
														'data'			=> $data,
														'errors'			=> $errors
   													]
   												);
   }
}