<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\ConfigurationIntroduceRepository;
use App\Helper\Upload as UploadHelper;
use Config;

class AdminConfigurationIntroduceController extends AdminController
{
	public $configuration_repository;
	public function __construct(ConfigurationIntroduceRepository $configuration_repository){
		$this->configuration_repository	= $configuration_repository;
	}

   public function index(Request $request){

		$redirect	= base64_encode(route('admin.configs.introduce'));

		// Lấy thông tin
		$info = $this->configuration_repository->getConfiguration();
		if(empty($info)) die('Record not found !');
		$picture_1		= $info->coi_picture_1;
		$picture_1_en	= $info->coi_picture_1_en;
		$text_1			= $info->coi_text_1;
		$text_1_en		= $info->coi_text_1_en;
		$picture_2		= $info->coi_picture_2;
		$picture_2_en	= $info->coi_picture_2_en;
		$text_2_1		= $info->coi_text_2_1;
		$text_2_1_en	= $info->coi_text_2_1_en;
		$text_2_2		= $info->coi_text_2_2;
		$text_2_2_en	= $info->coi_text_2_2_en;
		$picture_3		= $info->coi_picture_3;
		$picture_3_en	= $info->coi_picture_3_en;
		$text_3_1		= $info->coi_text_3_1;
		$text_3_1_en	= $info->coi_text_3_1_en;
		$text_3_2		= $info->coi_text_3_2;
		$text_3_2_en	= $info->coi_text_3_2_en;
		$picture_4		= $info->coi_picture_4;
		$picture_4_en	= $info->coi_picture_4_en;
		$text_4_1		= $info->coi_text_4_1;
		$text_4_1_en	= $info->coi_text_4_1_en;
		$text_4_2		= $info->coi_text_4_2;
		$text_4_2_en	= $info->coi_text_4_2_en;

		$picture_1		= $request->input('picture_1', $picture_1);
		$picture_1_en	= $request->input('picture_1_en', $picture_1_en);
		$text_1			= $request->input('text_1', $text_1);
		$text_1_en		= $request->input('text_1_en', $text_1_en);
		$picture_2		= $request->input('picture_2', $picture_2);
		$picture_2_en	= $request->input('picture_2_en', $picture_2_en);
		$text_2_1		= $request->input('text_2_1', $text_2_1);
		$text_2_1_en	= $request->input('text_2_1_en', $text_2_1_en);
		$text_2_2		= $request->input('text_2_2', $text_2_2);
		$text_2_2_en	= $request->input('text_2_2_en', $text_2_2_en);
		$picture_3		= $request->input('picture_3', $picture_3);
		$picture_3_en	= $request->input('picture_3_en', $picture_3_en);
		$text_3_1		= $request->input('text_3_1', $text_3_1);
		$text_3_1_en	= $request->input('text_3_1_en', $text_3_1_en);
		$text_3_2		= $request->input('text_3_2', $text_3_2);
		$text_3_2_en	= $request->input('text_3_2_en', $text_3_2_en);
		$picture_4		= $request->input('picture_4', $picture_4);
		$picture_4_en	= $request->input('picture_4_en', $picture_4_en);
		$text_4_1		= $request->input('text_4_1', $text_4_1);
		$text_4_1_en	= $request->input('text_4_1_en', $text_4_1_en);
		$text_4_2		= $request->input('text_4_2', $text_4_2);
		$text_4_2_en	= $request->input('text_4_2_en', $text_4_2_en);

		$picture_data_1		= UploadHelper::getAllPicture('config', $picture_1, 1);
		$picture_data_1_en	= UploadHelper::getAllPicture('config', $picture_1_en, 1);
		$picture_data_2		= UploadHelper::getAllPicture('config', $picture_2, 1);
		$picture_data_2_en	= UploadHelper::getAllPicture('config', $picture_2_en, 1);
		$picture_data_3		= UploadHelper::getAllPicture('config', $picture_3, 1);
		$picture_data_3_en	= UploadHelper::getAllPicture('config', $picture_3_en, 1);
		$picture_data_4		= UploadHelper::getAllPicture('config', $picture_4, 1);
		$picture_data_4_en	= UploadHelper::getAllPicture('config', $picture_4_en, 1);

		$data = [
				'picture_1'			=> $picture_1,
				'picture_1_en'		=> $picture_1_en,
				'text_1'			=> $text_1,
				'text_1_en'			=> $text_1_en,
				'picture_2'			=> $picture_2,
				'picture_2_en'		=> $picture_2_en,
				'text_2_1'			=> $text_2_1,
				'text_2_1_en'		=> $text_2_1_en,
				'text_2_2'			=> $text_2_2,
				'text_2_2_en'		=> $text_2_2_en,
				'picture_3'			=> $picture_3,
				'picture_3_en'		=> $picture_3_en,
				'text_3_1'			=> $text_3_1,
				'text_3_1_en'		=> $text_3_1_en,
				'text_3_2'			=> $text_3_2,
				'text_3_2_en'		=> $text_3_2_en,
				'picture_4'			=> $picture_4,
				'picture_4_en'		=> $picture_4_en,
				'text_4_1'			=> $text_4_1,
				'text_4_1_en'		=> $text_4_1_en,
				'text_4_2'			=> $text_4_2,
				'text_4_2_en'		=> $text_4_2_en,
				'picture_data_1'	=> $picture_data_1,
				'picture_data_1_en'	=> $picture_data_1_en,
				'picture_data_2'	=> $picture_data_2,
				'picture_data_2_en'	=> $picture_data_2_en,
				'picture_data_3'	=> $picture_data_3,
				'picture_data_3_en'	=> $picture_data_3_en,
				'picture_data_4'	=> $picture_data_4,
				'picture_data_4_en'	=> $picture_data_4_en,
				];

		$action		= $request->input('action');
		$errors 		= [];

		if($action == 'execute'){

			$dataReturn	= $this->configuration_repository->update($data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}


   	return  view('admin.config.introduce', [
												'data'			=> $data,
												'errors'		=> $errors
												]
											);
   }
}