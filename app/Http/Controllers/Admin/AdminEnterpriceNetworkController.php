<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\EnterpriceNetwork as EnterpriceNetworkModel;
use App\Repositories\EnterpriceNetworkRepository;
use App\Helper\Upload as UploadHelper;
use Config;

class AdminEnterpriceNetworkController extends AdminController
{
	public $enterprice_repository;
	public function __construct(EnterpriceNetworkRepository $enterprice_repository){
		$this->enterprice_repository	= $enterprice_repository;
	}

   	public function index(Request $request){

		$limit		= 25;
		$name		= $request->input('name');

		$dataSearch	= ['name' => $name];

		$enterprices	= new EnterpriceNetworkModel();
		if($name	!= "") $enterprices = $enterprices->where('enn_name', 'like', '%'. $name . '%');

		$enterprices	= $enterprices->paginate($limit);

		return  view('admin.enterprice_network.index', [
										'enterprices'	=> $enterprices,
										'pagination'=> [
																'total'			=> $enterprices->total(),
																'current_page'	=> $enterprices->currentPage(),
																'perpage'		=> $enterprices->perPage(),
																'links'			=> $enterprices->total() > $enterprices->perPage() ? $enterprices->appends($dataSearch)->links() : '',
															],
										'dataSearch'=> $dataSearch
										]
									);
   	}

   public function add(Request $request, $id = 0){

		$redirect		= $request->input('url_redirect', base64_encode(route('admin.other.enterprice_network')));
		$name			= '';
		$name_en		= '';
		$picture		= '';
		$link			= '';
		$description	= '';
		$description_en	= '';
		$chucvu			= '';
		$chucvu_en		= '';
		$active			= '';
		$order			= '';
		$staff_id		= '';
		$active			= 0;
		$id				= intval($id);

		if($id > 0){
			// Lấy thông tin
			$info = $this->enterprice_repository->getById($id);
			if(empty($info)) die('Record not found !');

			$name			= $info->enn_name;
			$name_en		= $info->enn_name_en;
			$picture		= $info->enn_picture;
			$link			= $info->enn_link;
			$description	= $info->enn_description;
			$description_en	= $info->enn_description_en;
			$active			= $info->enn_active;
			$order			= $info->enn_order;
		}

		$name			= $request->input('name', $name);
		$name_en		= $request->input('name_en', $name_en);
		$picture		= $request->input('picture', $picture);
		$link			= $request->input('link', $link);
		$description	= $request->input('description', $description);
		$description_en	= $request->input('description_en', $description_en);
		$order			= $request->input('order', $order);


		$picture_data 	= UploadHelper::getAllPicture('other', $picture, 1);
		$data = [
					'id'				=> $id,
					'name'				=> $name,
					'name_en'			=> $name_en,
					'picture'			=> $picture,
					'picture_data' 		=> $picture_data,
					'link'				=> $link,
					'description'		=> $description,
					'description_en'	=> $description_en,
					'order'				=> $order,
					'active' 			=> $active,
				];

		$action		= $request->input('action');
		$errors 		= [];

		if($action == 'execute'){

			$data['active'] 	= $request->input('active', 0);
			$dataReturn	= $this->enterprice_repository->add($id, $data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}


   		return  view('admin.enterprice_network.add', [
											'data'			=> $data,
											'errors'		=> $errors
											]
										);
   }

   public function delete(Request $request, $id){
   	$redirect = $request->input('url_redirect', base64_encode(route('admin.other.enterprice_network')));

   	// Gọi function xóa
   	$result = $this->enterprice_repository->delete($id);
   	if(isset($result['status']) && $result['status'] == true){
   		echo '<script>alert("Delete enterprice network success"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}else{
   		$error = isset($result['msg']) ? $result['msg'] : '';
   		echo '<script>alert("Delelte Error: ' . $error . '"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}
   }


   public function status(Request $request){
		$redirect	= $request->input('url_redirect', base64_encode(route('admin.other.enterprice_network')));
		$record_id	= $request->input('record_id', 0);
		$type			= $request->input('type', '');

		if($record_id <= 0 || $type == '') return response()->json(array('status'=> false, 'msg'=> 'Input param incorrect'), 200);

		// Lấy thông tin
		$info = $this->enterprice_repository->getById($record_id);
		if(empty($info)) return response()->json(array('status'=> false, 'msg'=> 'Record not found'), 200);

		$status 	= 0;
		$dataUpdate 	= [];
		switch ($type) {
			case 'active':
				$status 	= abs($info->enn_active - 1);
				$dataUpdate 	= ['active' => $status];
				break;

			case 'hot':
				$status 	= abs($info->enn_hot - 1);
				$dataUpdate 	= ['hot' => $status];
				break;
		}

		if(empty($dataUpdate)) return response()->json(array('status'=> false, 'msg'=> 'Type update not support'), 200);

		$dataReturn	= $this->enterprice_repository->add($record_id, $dataUpdate);

		if(isset($dataReturn['status']) && $dataReturn['status'] == true){
			return response()->json(array('status'=> true, 'data' => $status), 200);
		}

		$errors = isset($dataReturn['errors']) ? implode(",", $dataReturn['errors']) : '';

   	return response()->json(array('status'=> false, 'msg'=> $errors), 200);
   }
}
