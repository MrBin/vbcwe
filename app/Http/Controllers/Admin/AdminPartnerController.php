<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Partner as PartnerModel;
use App\Repositories\PartnerRepository;
use App\Helper\Upload as UploadHelper;
use Config;

class AdminPartnerController extends AdminController
{
	public $partner_repository;
	public function __construct(PartnerRepository $partner_repository){
		$this->partner_repository	= $partner_repository;
	}

   	public function index(Request $request){

		$limit		= 25;
		$name		= $request->input('name');

		$dataSearch	= ['name' => $name];
		$partners	= new PartnerModel();
		if($name	!= "") $partners = $partners->where('par_name', 'like', '%'. $name . '%');

		$partners		= $partners->orderBy('par_order', 'ASC')->paginate($limit);

		return  view('admin.partner.index', [
										'partners'		=> $partners,
										'pagination'=> [
																'total'			=> $partners->total(),
																'current_page'	=> $partners->currentPage(),
																'perpage'		=> $partners->perPage(),
																'links'			=> $partners->total() > $partners->perPage() ? $partners->appends($dataSearch)->links() : '',
															],
										'dataSearch'=> $dataSearch
										]
									);
   	}

   public function add(Request $request, $id = 0){

		$redirect		= $request->input('url_redirect', base64_encode(route('admin.view.partner')));
		$name			= '';
		$name_en		= '';
		$picture		= '';
		$link			= '';
		$active			= '';
		$order			= '';
		$type 			= 0;
		$staff_id		= '';
		$id				= intval($id);

		if($id > 0){
			// Lấy thông tin
			$info = $this->partner_repository->getById($id);
			if(empty($info)) die('Record not found !');

			$name			= $info->par_name;
			$name_en		= $info->par_name_en;
			$picture		= $info->par_picture;
			$link			= $info->par_link;
			$active			= $info->par_active;
			$order			= $info->par_order;
			$type			= $info->par_type;
		}

		$name			= $request->input('name', $name);
		$name_en		= $request->input('name_en', $name_en);
		$picture		= $request->input('picture', $picture);
		$link			= $request->input('link', $link);
		$order			= $request->input('order', $order);
		$type			= $request->input('type', $type);

		$picture_data 	= UploadHelper::getAllPicture('partner', $picture, 1);
		$data = [
					'id'			=> $id,
					'name'			=> $name,
					'name_en'		=> $name_en,
					'picture'		=> $picture,
					'picture_data' 	=> $picture_data,
					'link'			=> $link,
					'active'		=> $active,
					'order'			=> $order,
					'type'			=> $type,
				];

		$action		= $request->input('action');
		$errors 		= [];

		if($action == 'execute'){

			$data['active'] 	= $request->input('active', 0);
			$dataReturn	= $this->partner_repository->add($id, $data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}


   		return  view('admin.partner.add', [
											'data'			=> $data,
											'errors'		=> $errors,
											'allType'		=> $this->partner_repository::getTypeLabels(),
											]
										);
   }

   public function delete(Request $request, $id){
   	$redirect = $request->input('url_redirect', base64_encode(route('admin.view.partner')));

   	// Gọi function xóa
   	$result = $this->partner_repository->delete($id);
   	if(isset($result['status']) && $result['status'] == true){
   		echo '<script>alert("Delete partner success"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}else{
   		$error = isset($result['msg']) ? $result['msg'] : '';
   		echo '<script>alert("Delelte Error: ' . $error . '"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}
   }


   public function status(Request $request){
		$redirect	= $request->input('url_redirect', base64_encode(route('admin.view.partner')));
		$record_id	= $request->input('record_id', 0);
		$type			= $request->input('type', '');

		if($record_id <= 0 || $type == '') return response()->json(array('status'=> false, 'msg'=> 'Input param incorrect'), 200);

		// Lấy thông tin
		$info = $this->partner_repository->getById($record_id);
		if(empty($info)) return response()->json(array('status'=> false, 'msg'=> 'Record not found'), 200);

		$status 	= 0;
		$dataUpdate 	= [];
		switch ($type) {
			case 'active':
				$status 	= abs($info->par_active - 1);
				$dataUpdate 	= ['active' => $status];
				break;
		}

		if(empty($dataUpdate)) return response()->json(array('status'=> false, 'msg'=> 'Type update not support'), 200);

		$dataReturn	= $this->partner_repository->add($record_id, $dataUpdate);

		if(isset($dataReturn['status']) && $dataReturn['status'] == true){
			return response()->json(array('status'=> true, 'data' => $status), 200);
		}

		$errors = isset($dataReturn['errors']) ? implode(",", $dataReturn['errors']) : '';

   	return response()->json(array('status'=> false, 'msg'=> $errors), 200);
   }
}
