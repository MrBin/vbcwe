<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Service as ServiceModel;
use App\Repositories\ServiceRepository;
use App\Helper\Upload as UploadHelper;
use Config;

class AdminServiceController extends AdminController
{
	public $service_repository;
	public function __construct(ServiceRepository $service_repository){
		$this->service_repository	= $service_repository;
	}

   	public function index(Request $request){

		$limit		= 25;
		$title		= $request->input('title');

		$dataSearch	= ['title' => $title];
		$services	= new ServiceModel();
		if($title	!= "") $services = $services->where('ser_title', 'like', '%'. $title . '%');

		$services		= $services->paginate($limit);

		return  view('admin.service.index', [
										'services'		=> $services,
										'pagination'=> [
																'total'			=> $services->total(),
																'current_page'	=> $services->currentPage(),
																'perpage'		=> $services->perPage(),
																'links'			=> $services->total() > $services->perPage() ? $services->appends($dataSearch)->links() : '',
															],
										'dataSearch'=> $dataSearch,
										'allType' 	=> $this->service_repository::getTypeLabels(),
										]
									);
   	}

   public function add(Request $request, $id = 0){

		$redirect	= $request->input('url_redirect', base64_encode(route('admin.view.service')));

		$title					= "";
		$title_en				= "";
		$rewrite                = "";
		$icon					= "";
		$type					= 0;
		$order					= "";
		$status					= 0;
		$picture				= "";
		$teaser					= "";
		$teaser_en				= "";
		$description			= "";
		$description_en			= "";
		$meta_title				= '';
		$meta_title_en			= '';
		$meta_keyword			= '';
		$meta_keyword_en		= '';
		$meta_description		= '';
		$meta_description_en	= '';

		$id				= intval($id);

		if($id > 0){
			// Lấy thông tin
			$info = $this->service_repository->getById($id);
			if(empty($info)) die('Record not found !');

			$title					= $info->ser_title;
			$title_en				= $info->ser_title_en;
			$rewrite 				= $info->ser_name_rewrite;
			$icon					= $info->ser_icon;
			$order					= $info->ser_order;
			$type					= $info->ser_type;
			$status					= $info->ser_status;
			$picture				= $info->ser_picture;
			$teaser					= $info->ser_teaser;
			$teaser_en				= $info->ser_teaser_en;
			$description			= $info->ser_description;
			$description_en			= $info->ser_description_en;
			$meta_title				= $info->ser_meta_title;
			$meta_title_en			= $info->ser_meta_title_en;
			$meta_keyword			= $info->ser_meta_keyword;
			$meta_keyword_en		= $info->ser_meta_keyword_en;
			$meta_description		= $info->ser_meta_description;
			$meta_description_en	= $info->ser_meta_description_en;
		}

		$title					= $request->input('title', $title);
		$title_en				= $request->input('title_en', $title_en);
		$rewrite 				= removeTitle($title);
		$icon					= $request->input('icon', $icon);
		$order					= $request->input('order', $order);
		$type					= $request->input('type', $type);
		$status					= $request->input('status', $status);
		$picture				= $request->input('picture', $picture);
		$teaser					= $request->input('teaser', $teaser);
		$teaser_en				= $request->input('teaser_en', $teaser_en);
		$description			= $request->input('description', $description);
		$description_en			= $request->input('description_en', $description_en);
		$meta_title				= $request->input('meta_title', $meta_title);
		$meta_title_en			= $request->input('meta_title_en', $meta_title_en);
		$meta_keyword			= $request->input('meta_keyword', $meta_keyword);
		$meta_keyword_en		= $request->input('meta_keyword_en', $meta_keyword_en);
		$meta_description		= $request->input('meta_description', $meta_description);
		$meta_description_en	= $request->input('meta_description_en', $meta_description_en);

		$icon_data		= UploadHelper::getAllPicture('config', $icon, 1);
		$picture_data	= UploadHelper::getAllPicture('config', $picture, 1);
		$data = [
				'id'					=> $id,
				'title'					=> $title,
				'title_en'				=> $title_en,
				'name_rewrite'			=> $rewrite,
				'icon'					=> $icon,
				'order'					=> $order,
				'type'					=> $type,
				'status'				=> $status,
				'picture'				=> $picture,
				'teaser'				=> $teaser,
				'teaser_en'				=> $teaser_en,
				'description'			=> $description,
				'description_en'		=> $description_en,
				'meta_title'			=> $meta_title,
				'meta_title_en'			=> $meta_title_en,
				'meta_keyword'			=> $meta_keyword,
				'meta_keyword_en'		=> $meta_keyword_en,
				'meta_description'		=> $meta_description,
				'meta_description_en'	=> $meta_description_en,
				'icon_data'				=> $icon_data,
				'picture_data'			=> $picture_data,
				];

		$action		= $request->input('action');
		$errors 		= [];

		if($action == 'execute'){

			$data['status'] 	= $request->input('status', 0);
			$dataReturn	= $this->service_repository->add($id, $data);
			if(isset($dataReturn['status']) && $dataReturn['status'] == true){
				return redirect(base64_decode($redirect));
			}
			$errors = isset($dataReturn['errors']) ? $dataReturn['errors'] : [];
		}


   		return  view('admin.service.add', [
											'data'			=> $data,
											'errors'		=> $errors,
											'allType' 		=> $this->service_repository::getTypeLabels(),
											]
										);
   }

   public function delete(Request $request, $id){
   	$redirect = $request->input('url_redirect', base64_encode(route('admin.view.service')));

   	// Gọi function xóa
   	$result = $this->service_repository->delete($id);
   	if(isset($result['status']) && $result['status'] == true){
   		echo '<script>alert("Delete service success"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}else{
   		$error = isset($result['msg']) ? $result['msg'] : '';
   		echo '<script>alert("Delelte Error: ' . $error . '"); window.location.href="' . base64_decode($redirect) . '"</script>';
   	}
   }


   public function status(Request $request){
		$redirect	= $request->input('url_redirect', base64_encode(route('admin.view.service')));
		$record_id	= $request->input('record_id', 0);
		$type			= $request->input('type', '');

		if($record_id <= 0 || $type == '') return response()->json(array('status'=> false, 'msg'=> 'Input param incorrect'), 200);

		// Lấy thông tin
		$info = $this->service_repository->getById($record_id);
		if(empty($info)) return response()->json(array('status'=> false, 'msg'=> 'Record not found'), 200);

		$status 	= 0;
		$dataUpdate 	= [];
		switch ($type) {
			case 'active':
				$status 	= abs($info->ser_status - 1);
				$dataUpdate 	= ['status' => $status];
				break;
			case 'show_home':
				$status 	= abs($info->ser_show_home - 1);
				$dataUpdate 	= ['show_home' => $status];
				break;
		}

		if(empty($dataUpdate)) return response()->json(array('status'=> false, 'msg'=> 'Type update not support'), 200);

		$dataReturn	= $this->service_repository->add($record_id, $dataUpdate);

		if(isset($dataReturn['status']) && $dataReturn['status'] == true){
			return response()->json(array('status'=> true, 'data' => $status), 200);
		}

		$errors = isset($dataReturn['errors']) ? implode(",", $dataReturn['errors']) : '';

   	return response()->json(array('status'=> false, 'msg'=> $errors), 200);
   }
}
