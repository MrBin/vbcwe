<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Http\Request;
use App\Models\Users;
use DB;
class DashboardController extends AdminController
{
    public function __construct(){

    }

    public function index(Request $request){
        $infoStaff 	= app()->ADMIN_INFO;
        $count_user = Users::count();

        $count_new			= 1;
        $count_send_haiqua	= 2;
        $count_wait_return	= 3;
        return view('admin.dashboard.index')->with([
                'count_user' 		=> $count_user,
                'count_new' 		=> $count_new,
                'count_send_haiqua' 		=> $count_send_haiqua,
                'count_wait_return' 		=> $count_wait_return,
            ]
        );
    }

}
