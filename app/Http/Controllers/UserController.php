<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Helper\Users;
class UserController
{
	public function __construct(){

	}

	public function index(Request $request){

	}

	public function register(Request $request){
		$username		= $request->input('username');
		$loginname		= $request->input('loginname');
		$phone			= $request->input('phone');
		$password		= $request->input('password');
		$re_password	= $request->input('re_password');
		$action			= $request->input('action');

	   	$data = [
					'name'			=> $username,
					'loginname'		=> $loginname,
					'phone' 		=> $phone,
					'password'		=> $password,
					'active' 		=> 1,
					'created_time'	=> time(),
					'update_at'		=> time()
	   			];
	   	$errors 	= [];
	   	if($action == "execute"){
			$user	= new UserRepository();
			$result	= $user->register($data);
	   		if($result){
	   			return redirect()->route('authen.login');
	   		}else{
	   			$errors = isset($result['errors']) ? $result['errors'] : [];
	   		}
	   	}

		return view('frontend.register')->with(['errors' => $errors]);
	}

	public function login(Request $request){
		$username 	= $request->input('username');
		$loginname 	= $request->input('loginname');
	   	$password 	= $request->input('password');
	   	$action 	= $request->input('action');

	   	$data = [
					'name'			=> $username,
					'loginname'		=> $loginname,
					'password'		=> $password,
					'active' 		=> 1,
					'created_time'	=> time(),
					'update_at'		=> time()
	   			];
	   	$errors 	= [];
	   	if($action == "execute"){
			$user	= new UserRepository();
			$result	= $user->register($data);
	   		if($result){
	   			return redirect()->route('authen.login');
	   		}else{
	   			$errors = isset($result['errors']) ? $result['errors'] : [];
	   		}
	   	}

		return view('frontend.login')->with(['errors' => $errors]);
	}

	public function changePass(Request $request){
		$old_password	= $request->input('old_password');
		$password		= $request->input('password');
		$re_password	= $request->input('re_password');
		$action 		= $request->input('action');

		$errorMsg 		= '';
		$isSuccess 		= 0;
		if($action == 'changepass'){
			$infoUser 	= app()->USER_INFO;
			$user	= new UserRepository();
			// Kiểm tra xem password cũ có đúng ko
			if( $user->generatePassword($old_password, $infoUser->use_security)  != $infoUser->use_password){
				$errorMsg = 'Mật khẩu cũ không chính xác';
			}else{
				if($password == '' || $password != $re_password){
					$errorMsg = 'Password và Re-password không giống nhau';
				}
			}

			if($errorMsg == ''){
				$result = $user->changePass($infoUser->use_id, $password);

				if(isset($result['status']) && $result['status'] == true){
					\App\Helper\Users::setLogged($infoUser->use_loginname, $user->generatePassword($password, $infoUser->use_security));
					return redirect()->route('user.index')->with('success', "Đổi mật khẩu thành công");
				}else{
					$errorMsg = isset($result['status']) ? $result['status'] : 'Đổi mật khẩu thất bại!';
					return redirect()->route('user.profile.changepass', $param)->with('success', $msg);
				}
			}
		}


   		return  view('frontend.profile.changepass', [	'view' => 'changepass',
															'msg' => $errorMsg
		   													]
		   												);

	}
}
