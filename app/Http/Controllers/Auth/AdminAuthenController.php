<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper\AdminUser;
class AdminAuthenController extends Controller
{
	protected $admin_user;

	public function __construct(AdminUser $admin_user){
		$this->admin_user 	= $admin_user;
	}

   public function doLogin(Request $request){
	   	$user_name 	= $request->username;
	   	$password 	= $request->password;

	   	$action		= $request->input('action');
		$errors 		= [];

		if($action == 'login'){
			$request->validate([
	           'username' => 'required',
	           'password' => 'required|min:6'
	       ]);
	       if($this->admin_user::checkLogin($user_name, $password)){
	           return redirect()->route('admin.dashboard');
	       }
	    }

       return view('admin.login');
   }


   public function doLogout(Request $request){

		$this->admin_user::setLogout();

   		return redirect()->route('auth_admin.login');
   }
}
