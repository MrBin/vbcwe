<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper\Users;
class UserAuthenController extends Controller
{
	protected $user;

	public function __construct(Users $user){
		$this->user 	= $user;
	}

    public function login(Request $request){
		$user_name		= $request->input('username');
		$password		= $request->input('password');
		$url_redirect 	= urlencode(route('user.index'));
		$url_redirect	= urldecode($request->input('url_redirect', $url_redirect));
		$action			= $request->input('action');

	   	if($action == "login"){
	   		if($this->user::checkLogin($user_name, $password)){
	   			return redirect($url_redirect);
	   		}else{
	   			return redirect()->route('authen.login');
	   		}
	   	}

  		return view('frontend.login')->with(['url_redirect' => $url_redirect]);
    }

  	public function logout(Request $request){

		$this->user::setLogout();
   		return redirect()->route('authen.login');
   	}
}
