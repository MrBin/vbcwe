<?php

namespace App\Http\Controllers\Page;

use App\Helper\Menu as MenuHelper;
use App\Http\Controllers\Controller;
use App\Models\Configuration;
use App\Models\EnterpriceNetwork as EnterpriceNetworkModel;
use App\Models\Founder as FounderModel;
use App\Models\Opinion as OpinionModel;
use App\Models\Partner as PartnerModel;
use App\Repositories\ConfigurationAboutRepository;
use App\Repositories\ConfigurationMemberRepository;
use App\Repositories\ConfigurationRepository;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public $configuration_repository;
    public $configuration_member_repository;
    public $configuration_main_repository;

	public function __construct(ConfigurationAboutRepository $configuration_repository, ConfigurationMemberRepository $configuration_member_repository, ConfigurationRepository $configuration_main_repository){
		$this->configuration_repository	        = $configuration_repository;
		$this->configuration_member_repository	= $configuration_member_repository;
		$this->configuration_main_repository	= $configuration_main_repository;
	}

    public function about(){

        $dataAbout = $this->configuration_repository->getConfiguration();

        //Lấy config
        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        return view('users.about', [
            'htmlMenu'      => $htmlMenu,
            'dataConfigs'   => $dataConfigs,
            'dataAbout'     => $dataAbout
        ]);
    }

    public function founder(){

        //Lấy config
        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        $founders	            = new FounderModel();
        $dataFoundersTop	    = $founders->where('fou_type', 1)->limit(2)->get();
        $dataFoundersBottom	    = $founders->where('fou_type', 1)->skip(2)->take(3)->get();
        $dataFoundersBottom2	= $founders->where('fou_type', 1)->skip(5)->take(5)->get();

        return view('users.founder', [
            'htmlMenu'      => $htmlMenu,
            'dataConfigs'   => $dataConfigs,
            'dataFoundersTop'     => $dataFoundersTop,
            'dataFoundersBottom'     => $dataFoundersBottom,
            'dataFoundersBottom2' => $dataFoundersBottom2
        ]);
    }

    public function memberNetwork(){

        //Lấy config
        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        // Lấy ảnh thành viên
        $enterprices	    = new EnterpriceNetworkModel();
        $dataEnterprices    = $enterprices->get();

        $dataMember = $this->configuration_main_repository->getConfiguration();

        return view('users.member-network', [
            'htmlMenu'      => $htmlMenu,
            'dataConfigs'   => $dataConfigs,
            'dataEnterprices'     => $dataEnterprices,
            'dataMember' => $dataMember
        ]);
    }

    public function joinNetwork(){

        //Lấy config
        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        // Lấy ảnh thành viên
        $dataJoin = $this->configuration_member_repository->getConfiguration();

        return view('users.join-network', [
            'htmlMenu'      => $htmlMenu,
            'dataConfigs'   => $dataConfigs,
            'dataJoin' => $dataJoin
        ]);
    }

    public function team(){

        $teamBig    = [];
        $teamList   = [];

        //Lấy config
        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        $team       = new FounderModel();
        $teamBig    = $team->where('fou_type', 2)->where('fou_active', 1)->first();
        if(isset($teamBig->fou_id) && $teamBig->fou_id > 0){
            $teamList   = $team->where('fou_type', 2)->where('fou_active', 1)->whereNotIn('fou_id', [$teamBig->fou_id])->get();
        }

        return view('users.team', [
            'htmlMenu'      => $htmlMenu,
            'dataConfigs'   => $dataConfigs,
            'teamBig'       => $teamBig,
            'teamList'      => $teamList
        ]);
    }

    public function partner(){

        $dataSponsor = [];
        $dataInternational = [];
        $dataRegion = [];
        $dataDomestic = [];
        $dataPartners = [];

        //Lấy config
        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        // Đối tác
        $partners           = new PartnerModel();
        $dataSponsor        = $partners->where('par_active', 1)->where('par_type', 1)->limit(2)->get();
        $dataInternational  = $partners->where('par_active', 1)->where('par_type', 2)->get();
        $dataRegion         = $partners->where('par_active', 1)->where('par_type', 3)->limit(3)->get();
        $dataDomestic       = $partners->where('par_active', 1)->where('par_type', 4)->get();

        return view('users.partner', [
            'htmlMenu'      => $htmlMenu,
            'dataConfigs'   => $dataConfigs,
            'dataSponsor'    => $dataSponsor,
            'dataInternational' => $dataInternational,
            'dataRegion' => $dataRegion,
            'dataDomestic' => $dataDomestic,
            'dataPartners' => $dataPartners,
        ]);
    }

    public function comments(){
        $dataOpinions       = [];
        $dataOpinionsBig    = [];

        $opinions	    = new OpinionModel();
        $dataOpinionsBig = $opinions->where('opi_status', 1)->whereNotNull('opi_video')->orderBy('opi_created_time', 'DESC')->limit(2)->get();
        $dataOpinions	= $opinions->where('opi_status', 1)->orderBy('opi_created_time', 'DESC')->skip(2)->take(6)->get();


	     // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        return view('users.comment', [
            'htmlMenu'      => $htmlMenu,
            'dataConfigs'   => $dataConfigs,
            'dataOpinions'  => $dataOpinions,
            'dataOpinionsBig'  => $dataOpinionsBig
        ]);
    }
}
