<?php

namespace App\Http\Controllers\Page;

use App\Helper\Category as CategoryHelper;
use App\Helper\Menu as MenuHelper;
use App\Http\Controllers\Controller;
use App\Models\Banner as BannerModel;
use App\Models\Configuration;
use App\Models\News as NewsModel;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;

class AlbumController extends PageController
{
    public $category_helper;
    public $category_repository;
    public function __construct(CategoryRepository $category_repository, CategoryHelper $category_helper){
		$this->category_helper	= $category_helper;
		$this->category_repository	= $category_repository;
	}

    public function index(){

        $arrAlbumJson   = [];
        $album          = new BannerModel();
        $dataAlbumBig   = $album->where('ban_active', 1)->where('ban_position', 3)->orderBy('ban_create_time', 'DESC')->first();

        $param 		= [['cat_type', '=', $this->category_repository::CATEGORY_NEWS]];
	    $category 	= $this->category_helper::getAllCategory($param);

        // Lấy list ảnh
        $arrAlbumJson   = json_decode($dataAlbumBig['ban_picture_json'], 1);
        $albumRelated   = self::getAlbumRelated($dataAlbumBig->ban_id, $dataAlbumBig->ban_position);

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        // Lấy tin tức
        $limit  = 6;
        $dataNews   = NewsModel::where('new_id', '>' , 0)->orderBy('new_create_time', 'DESC')->limit($limit)->get();

		return view('users.album', [
		    'dataAlbumBig'  => $dataAlbumBig,
		    'arrAlbumJson'  => $arrAlbumJson,
		    'albumRelated'  => $albumRelated,
		    'htmlMenu'      => $htmlMenu,
		    'dataConfigs'   => $dataConfigs,
		    'newsRelated'      => $dataNews,
		    'category'      => $category,
        ]);
	}

    public function videos(){

        $videoBig   = [];
        $videos      = new BannerModel();
        $videoBig   = $videos->where('ban_active', 1)->where('ban_position', 4)->orderBy('ban_create_time', 'DESC')->first();

        // Lấy list ảnh
        $albumRelated   = self::getAlbumRelated($videoBig->ban_id, $videoBig->ban_position);

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        // Lấy tin tức
        $limit  = 6;
        $dataNews   = NewsModel::where('new_id', '>' , 0)->orderBy('new_create_time', 'DESC')->limit($limit)->get();

        return view('users.videos', [
            'videoBig'      => $videoBig,
            'albumRelated'  => $albumRelated,
            'htmlMenu'      => $htmlMenu,
            'dataConfigs'   => $dataConfigs,
            'newsRelated'      => $dataNews
        ]);
    }

	public function albumDetail(Request $request){
	    $iAlbum = $request->id;

        $album          = new BannerModel();
        $detailAlbum    = $album->where('ban_active', 1)->where('ban_position', 3)->where('ban_id', $iAlbum)->first();

        $param 		= [['cat_type', '=', $this->category_repository::CATEGORY_NEWS]];
	    $category 	= $this->category_helper::getAllCategory($param);

        $arrAlbumJson   = json_decode($detailAlbum['ban_picture_json'], 1);
        $albumRelated   = self::getAlbumRelated($iAlbum, $detailAlbum->ban_position);

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        // Lấy tin tức
        $limit  = 6;
        $dataNews   = NewsModel::where('new_id', '>' , 0)->orderBy('new_create_time', 'DESC')->limit($limit)->get();

        return view('users.album', [
		    'dataAlbumBig'  => $detailAlbum,
		    'arrAlbumJson'  => $arrAlbumJson,
		    'albumRelated'  => $albumRelated,
		    'htmlMenu'      => $htmlMenu,
		    'dataConfigs'   => $dataConfigs,
		    'newsRelated'      => $dataNews,
		    'category'      => $category
        ]);
	}

	public static function getAlbumRelated($iAl, $type = 0){
	    $limit = 9;
	    $dataReturn = [];

        $album      = new BannerModel();
	    $dataReturn  = $album->where('ban_active', 1)->where('ban_position', $type)->whereNotIn('ban_id', [$iAl])->orderBy('ban_create_time', 'DESC')->paginate($limit);
        return $dataReturn;
	}
}
