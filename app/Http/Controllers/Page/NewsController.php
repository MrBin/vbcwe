<?php

namespace App\Http\Controllers\Page;

use App\Helper\Category as CategoryHelper;
use App\Helper\Menu as MenuHelper;
use App\Http\Controllers\Controller;
use App\Models\Banner as BannerModel;
use App\Models\Configuration;
use App\Models\News;
use App\Models\News as NewsModel;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public $category_helper;
    public $category_repository;
    public function __construct(CategoryRepository $category_repository, CategoryHelper $category_helper){
		$this->category_helper	= $category_helper;
		$this->category_repository	= $category_repository;
	}

    public function index(){
        $param 		= [['cat_type', '=', $this->category_repository::CATEGORY_NEWS]];
	    $category 	= $this->category_helper::getAllCategory($param);

        $limit  = 6;
        $news   = NewsModel::where('new_id', '>' , 0)->orderBy('new_create_time', 'DESC');

        $newsBig        = $news->with(['category'])->first();
        $news	        = $news->with(['category'])->whereNotIn('new_id', [$newsBig->new_id])->paginate($limit);
        $newsRelated    = self::getNewsRelated($newsBig->new_id);

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

		return view('users.news', [
		    'dataNews'      => $news,
		    'newsBig'       => $newsBig,
		    'newsRelated'   => $newsRelated,
		    'htmlMenu'      => $htmlMenu,
		    'dataConfigs'   => $dataConfigs,
		    'category'      => $category,
		    'pagination'=> [
                                'total'			=> $news->total(),
                                'current_page'	=> $news->currentPage(),
                                'perpage'		=> $news->perPage(),
                                'links'			=> $news->total() > $news->perPage() ? $news->links() : '',
                            ],
        ]);
	}

	public function category(Request $request){

	    $iCate = $request->id;
	    $newsRelated = [];

        $param 		= [['cat_type', '=', $this->category_repository::CATEGORY_NEWS]];
	    $category 	= $this->category_helper::getAllCategory($param);

        $limit  = 6;
        $news   = NewsModel::where('new_id', '>' , 0)->where('new_category_id', $iCate)->orderBy('new_create_time', 'DESC');

        if(!empty($iCate)){
            $news   = $news->where('new_category_id', $iCate);
        }

        $newsBig        = $news->with(['category'])->first();

        if(!empty($newsBig)){
            $news	        = $news->with(['category'])->whereNotIn('new_id', [$newsBig->new_id])->paginate($limit);
            $newsRelated    = self::getNewsRelated($newsBig->new_id);
        }
        else{
            $news	        = $news->with(['category'])->paginate($limit);
        }

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

		return view('users.news', [
		    'dataNews'      => $news,
		    'newsBig'       => $newsBig,
		    'newsRelated'   => $newsRelated,
		    'htmlMenu'      => $htmlMenu,
		    'dataConfigs'   => $dataConfigs,
		    'category'      => $category,
		    'pagination'=> [
                                'total'			=> $news->total(),
                                'current_page'	=> $news->currentPage(),
                                'perpage'		=> $news->perPage(),
                                'links'			=> $news->total() > $news->perPage() ? $news->links() : ''
                            ],
        ]);
	}

	public function detail(Request $request){
	    $param 		= [['cat_type', '=', $this->category_repository::CATEGORY_NEWS]];
	    $category 	= $this->category_helper::getAllCategory($param);

	    $iNews = $request->id;
	    $newsDetail = News::getNewsDetail($iNews);

	    $newsRelated    = self::getNewsRelated($iNews);

	    //Lấy banner
        $banners	    = new BannerModel();
        $dataBanners	= $banners->where('ban_position', 5)->where('ban_active', 1)->first();

	    // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

		return view('users.news_detail', [
		    'newsDetail'    => $newsDetail,
		    'htmlMenu'      => $htmlMenu,
		    'dataConfigs'   => $dataConfigs,
		    'category'      => $category,
		    'newsRelated'   => $newsRelated,
		    'dataBanners'   => $dataBanners
        ]);
	}

	public static function getNewsRelated($iNews){
	    $limit = 6;
	    $dataReturn = [];

	    $news           = NewsModel::where('new_id', '>' , 0)->orderBy('new_create_time', 'DESC');
        $dataReturn     = $news->with(['category'])->whereNotIn('new_id', [$iNews])->limit($limit)->get();

        return $dataReturn;
	}
}
