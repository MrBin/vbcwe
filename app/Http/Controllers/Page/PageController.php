<?php

namespace App\Http\Controllers\Page;
use App\Models\News as Model;
use Illuminate\Http\Request;
use App\Helper\UserTranslate as UserTranslate;
use Cookie;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use App\Repositories\NewsRepository;

class PageController
{
	public $arrayTranslate;
	public $currentLang;
	public $news_repository;
	public function __construct(){

	    $this->model = new Model;
//	    $this->news_repository	= $news_repository;
		// Lấy toàn bộ ngôn ngữ ra
		$arrayTranslate = UserTranslate::getAllTranslate();

		$currentLang = UserTranslate::getLangCurrent();

		// Lưu thông tin để sau dùng
		app()->singleton('USER_TRANSLATE', function() use ($arrayTranslate){
            return $arrayTranslate;
       	});

       	$this->arrayTranslate = $arrayTranslate;
	}

	public function addPrefix($data)
    {
        $dataStore = [];
        foreach ($data as $key => $value) {
            $dataStore[$this->model->prefix . $key] = $value;
        }
        return $dataStore;
    }

	public function addNews(){
	    $url = 'http://localhost:8013/api/api-news.php';
	    $client = new Client();

	    $response = $client->get($url);

        $dataJson = $response->getBody();

        $dataGet = \GuzzleHttp\json_decode($dataJson);
        $i = 0;
        foreach ($dataGet as $item){
            $i++;
            $dataImage = \GuzzleHttp\json_decode($item->img);

            $data = [
                'id'                    => $i,
                'category_id' 			=> 1,
                'title'					=> $item->name,
                'title_en'				=> $item->name_en,
                'rewrite'				=> $item->slug,
                'meta_title'			=> $item->name,
                'meta_title_en'			=> $item->name_en,
                'meta_keyword'			=> $item->s_key,
                'meta_keyword_en'		=> $item->s_key_en,
                'meta_description'		=> $item->s_des,
                'meta_description_en'	=> $item->s_des_en,
                'teaser'				=> $item->short_content,
                'teaser_en'				=> $item->short_content_en,
                'description'			=> $item->content,
                'description_en'		=> $item->content_en,
                'picture'				=> $dataImage->name,
                'order'					=> $i,
                'active'				=> 1,
                'create_time'           => $item->create_time
            ];

            $news	= $this->model->create($this->addPrefix($data));
        }
	}

	public function index(){
	    $currentTranslate = UserTranslate::getLangCurrent();
	}

	public function lang(Request $request){
	   	$lang       = $request->lang;
	    $setLang    = UserTranslate::setLangCurrent($lang);
	    return $setLang;
	}

	public function en(){
		$cookie = Cookie::make('name', 'value', 120);
//		dd($cookie);
		return response('view')->withCookie($cookie);
	}


}
