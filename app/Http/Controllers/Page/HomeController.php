<?php

namespace App\Http\Controllers\Page;
use App\Helper\Menu as MenuHelper;
use App\Models\Achievement as AchievementModel;
use App\Models\Banner as BannerModel;
use App\Models\Configuration;
use App\Models\EnterpriceNetwork as EnterpriceNetworkModel;
use App\Models\News;
use App\Models\Opinion as OpinionModel;
use App\Models\Service as ServiceModel;
use App\Repositories\ServiceRepository;
use Illuminate\Http\Request;
use App\Helper\UserTranslate as UserTranslate;
class HomeController extends PageController
{
    public $service_repository;
	public function __construct(ServiceRepository $service_repository){
	    $this->service_repository	= $service_repository;
		parent::__construct();
	}

	public function index(){

        // Tin tức trang chủ
	    $newsHome       = News::getNewsHome();

        // Đối tác
//	    $partners       = new PartnerModel();
//	    $dataPartners   = $partners->where('par_type', 5)->where('par_active', 1)->limit(12)->get();
        $enterprices	    = new EnterpriceNetworkModel();
        $dataEnterprices    = $enterprices->orderBy('enn_order', 'ASC')->get();

        // Thành tích
	    $achievements	    = new AchievementModel();
	    $dataAchievements   = $achievements->where('tht_status', 1)->limit(3)->get();

        // Ý kiến đối tác
	    $opinions	    = new OpinionModel();
	    $dataOpinions	= $opinions->limit(6)->get();

        // Sự kiện dịch vụ
        $services	    = new ServiceModel();
        $dataServices   = $services->where('ser_type', $this->service_repository::SERVICE_ENTERPRISE)->limit(3)->get();

        //Lấy banner
        $bannerPosition = 1;
        $banners	    = new BannerModel();
        $dataBanners	= $banners->where('ban_position', $bannerPosition)->limit(3)->get();

        //Lấy config
        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

		return view('users.home', [
		    'newsHome'          => $newsHome,
		    'dataEnterprices'   => $dataEnterprices,
		    'dataAchievements'  => $dataAchievements,
		    'dataOpinions'      => $dataOpinions,
		    'dataServices'      => $dataServices,
		    'dataBanners'       => $dataBanners,
		    'dataConfigs'       => $dataConfigs,
		    'htmlMenu'          => $htmlMenu
		]);

//		UserTranslate::translate('Hoàng Quốc Việt');
//		var_dump($this->arrayTranslate);
	}
	public function abc(){
		echo "3333";
	}
}
