<?php

namespace App\Http\Controllers\Page;

use App\Helper\Menu as MenuHelper;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use App\Models\Configuration;
use App\Repositories\ContactRepository;
use Illuminate\Http\Request;

class ContactController extends AdminController
{
    public $contact_repository;
	public function __construct(ContactRepository $contact_repository){
		$this->contact_repository	= $contact_repository;
	}

	public function contact(Request $request){
        $email  = $request->input('email');
        $name   = $request->input('name');
        $con_position       = $request->input('con_position');
        $con_company_name   = $request->input('con_company_name');
        $action		        = $request->input('action');
        $dataReturn         = [];

        $data = [
				'email'			=> $email,
				'name'			=> $name,
				'position'	    => $con_position,
				'company_name'	=> $con_company_name,
				'type'          => 1
				];

        if($action == 'action'){
            $dataReturn	= $this->contact_repository->add(0, $data);
        }

        return response()->json($dataReturn);

	}

    public function contactEvent(Request $request){

        $email  = $request->input('event-email');
        $phone  = $request->input('event-phone');
        $name   = $request->input('event-name');
        $sex    = $request->input('event-sex');
        $con_position       = $request->input('event-position');
        $con_company_name   = $request->input('event-company');
        $action		        = $request->input('event-action');
        $need              = $request->input('event-need');
        $dataReturn         = [];
        $type               = $request->input('event-type');

        $data = [
            'email'			=> $email,
            'need'			=> $need,
            'phone'			=> $phone,
            'name'			=> $name,
            'position'	    => $con_position,
            'company_name'	=> $con_company_name,
            'sex'           => $sex,
            'type'          => $type
        ];

        if($action == 'action'){
            $dataReturn	= $this->contact_repository->add(0, $data, 3);
        }

        return response()->json($dataReturn);

    }

    public function contactEventRegister(Request $request){

        $email  = $request->input('event-email');
        $phone  = $request->input('event-phone');
        $name   = $request->input('event-name');
        $sex    = $request->input('event-sex');
        $con_position       = $request->input('event-position');
        $con_company_name   = $request->input('event-company');
        $action		        = $request->input('event-action');
        $dataReturn         = [];
        $type               = $request->input('event-type');

        $data = [
            'email'			=> $email,
            'phone'			=> $phone,
            'name'			=> $name,
            'position'	    => $con_position,
            'company_name'	=> $con_company_name,
            'sex'           => $sex,
            'type'          => $type
        ];

        if($action == 'action'){
            $dataReturn	= $this->contact_repository->add(0, $data, 4);
        }

        return response()->json($dataReturn);

    }

	public function contactStory(Request $request){
        $name   = $request->input('name');
        $story  = $request->input('story');
        $action		        = $request->input('action');
        $dataReturn         = [];

        $data = [
            'name'			=> $name,
            'description'	=> $story,
            'type'          => 5
        ];

        if($action == 'action'){
            $dataReturn	= $this->contact_repository->add(0, $data, 1);
        }

        return response()->json($dataReturn);
    }

    public function contactForm(Request $request){
        $email          = $request->input('email');
        $name           = $request->input('name');
        $title          = $request->input('title');
        $description    = $request->input('description');
        $action		        = $request->input('action');
        $dataReturn         = [];

        $data = [
            'email'			=> $email,
            'name'			=> $name,
            'title'	        => $title,
            'description'	=> $description,
            'type'          => 5
        ];

        if($action == 'action'){
            $dataReturn	= $this->contact_repository->add(0, $data, 2);
        }

        return response()->json($dataReturn);

    }

	public function contactPage(){
	    //Lấy config
        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        return view('users.contact', [
            'htmlMenu'      => $htmlMenu,
            'dataConfigs'   => $dataConfigs
        ]);
	}

	public function joinRegister(){

        //Lấy config
        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        return view('users.join-register', [
            'htmlMenu'      => $htmlMenu,
            'dataConfigs'   => $dataConfigs,
        ]);
    }

    public function registerEvent(){

        //Lấy config
        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        return view('users.register-event', [
            'htmlMenu'      => $htmlMenu,
            'dataConfigs'   => $dataConfigs,
        ]);
    }

    public function registerReport(){

        //Lấy config
        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        return view('users.register-report', [
            'htmlMenu'      => $htmlMenu,
            'dataConfigs'   => $dataConfigs,
        ]);
    }

    public function contactJoin(Request $request){
        $email          = $request->input('join-email');
        $company_name           = $request->input('join-name');
        $join_area      = $request->input('join-area');
        $join_address   = $request->input('join-address');
        $join_scale     = $request->input('join-scale');
        $join_website   = $request->input('join-website');
        $join_who       = $request->input('join-who');
        $join_position  = $request->input('join-position');
        $join_phone     = $request->input('join-phone');
        $join_need      = $request->input('join-need');
        $action		    = $request->input('join-action');
        $dataReturn     = [];

        $data = [
            'email'			=> $email,
            'company_name'			=> $company_name,
            'area'	        => $join_area,
            'website'	    => $join_website,
            'who'			=> $join_who,
            'scale'			=> $join_scale,
            'address'	    => $join_address,
            'phone'	        => $join_phone,
            'need'	        => $join_need,
            'position'	    => $join_position,
            'type'          => 2
        ];

        if($action == 'action'){
            $dataReturn	= $this->contact_repository->add(0, $data, 5);
        }

        return response()->json($dataReturn);

    }
}
