<?php

namespace App\Http\Controllers\Page;

use App\Helper\Category as CategoryHelper;
use App\Http\Controllers\Controller;
use App\Helper\Menu as MenuHelper;
use App\Models\ItemStep as ItemStepModel;
use App\Models\News;
use App\Repositories\CategoryRepository;
use App\Repositories\ConfigurationCertificationRepository;
use App\Repositories\ConfigurationGearsRepository;
use App\Repositories\ItemStepRepository;
use App\Repositories\ServiceRepository;
use Illuminate\Http\Request;
use App\Models\Configuration;
use App\Models\Service as ServiceModel;

class ServiceController extends Controller
{
    public $service_repository;
    public $category_helper;
    public $category_repository;
    public $configuration_repository;
    public $item_step_repository;
    public $configuration_gears_repository;
    public function __construct(ItemStepRepository $item_step_repository, ServiceRepository $service_repository, CategoryRepository $category_repository, CategoryHelper $category_helper, ConfigurationCertificationRepository $configuration_repository, ConfigurationGearsRepository $configuration_gears_repository){
		$this->service_repository	= $service_repository;
		$this->category_helper	= $category_helper;
		$this->category_repository	= $category_repository;
		$this->configuration_repository	= $configuration_repository;
		$this->item_step_repository	= $item_step_repository;
		$this->configuration_gears_repository	= $configuration_gears_repository;
	}

    public function index(){
        $limit  = 6;
        $services   = ServiceModel::where('ser_status', 1)->orderBy('ser_create_time', 'DESC');

        $param 		= [['cat_type', '=', $this->category_repository::CATEGORY_NEWS]];
	    $category 	= $this->category_helper::getAllCategory($param);

        $servicesBig        = $services->first();
        $services	        = $services->whereNotIn('ser_id', [$servicesBig->ser_id])->paginate($limit);
        $servicesRelated    = self::servicesRelate();

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

		return view('users.services', [
		    'dataServices'      => $services,
		    'servicesBig'       => $servicesBig,
		    'newsRelated'       => $servicesRelated,
		    'htmlMenu'          => $htmlMenu,
		    'dataConfigs'       => $dataConfigs,
		    'category'      => $category,
		    'pagination'        => [
                                    'total'			=> $services->total(),
                                    'current_page'	=> $services->currentPage(),
                                    'perpage'		=> $services->perPage(),
                                    'links'			=> $services->total() > $services->perPage() ? $services->links() : '',
                                ],
        ]);
	}

	public function detail(Request $request){
	    $param 		= [['cat_type', '=', $this->category_repository::CATEGORY_NEWS]];
	    $category 	= $this->category_helper::getAllCategory($param);

        $iService           = $request->id;
	    $servicesDetail     = ServiceModel::getServicesDetail($iService);
	    $servicesRelated    = self::servicesRelate();

	    // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

		return view('users.services-detail', [
		    'servicesDetail'    => $servicesDetail,
		    'htmlMenu'          => $htmlMenu,
		    'dataConfigs'       => $dataConfigs,
		    'category'          => $category,
		    'newsRelated'       => $servicesRelated
        ]);
	}

	public function servicesRelate(){
        $limit = 6;
	    $dataReturn = [];

        $dataReturn       = News::where('new_active', 1)->orderBy('new_create_time', 'DESC')->limit($limit)->get();

        return $dataReturn;
	}

	public function gear(){

        $dataStepField      = [];
        $dataStepField2     = [];
        $dataStepTable      = [];
        $dataProcess        = [];
        $dataEvaluate       = [];

        $itemStep	        = new ItemStepModel();
        $dataStepField      = $itemStep->where('its_type', $this->item_step_repository::ITEM_STEP_FIELD_GEARS)->limit(8)->get();
        $dataStepField2     = $itemStep->where('its_type', $this->item_step_repository::ITEM_STEP_FIELD_GEARS)->skip(8)->take(10)->get();
        $dataStepTable      = $itemStep->where('its_type', $this->item_step_repository::ITEM_STEP_DATA_GEARS)->limit(4)->get();
        $dataProcess        = $itemStep->where('its_type', $this->item_step_repository::ITEM_STEP_FOLLOW_GEARS)->limit(3)->get();
        $dataEvaluate       = $itemStep->where('its_type', $this->item_step_repository::ITEM_STEP_EVALUATE_GEARS)->limit(7)->get();

        //Lấy config
        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        $info = $this->configuration_gears_repository->getConfiguration();

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);


        return view('users.services-gear', [
            'htmlMenu'      => $htmlMenu,
            'dataConfigs'   => $dataConfigs,
            'dataGears'     => $info,
            'dataStepField'    => $dataStepField,
            'dataStepField2'   => $dataStepField2,
            'dataStepTable'    => $dataStepTable,
            'dataProcess'      => $dataProcess,
            'dataEvaluate'      => $dataEvaluate
        ]);
    }

    public function edge(){

        $dataStepWhy        = [];
        $dataStepProcess    = [];
        $dataEvaluate       = [];
        $dataLevel          = [];
        $info = $this->configuration_repository->getConfiguration();

        $itemStep	        = new ItemStepModel();
        $dataStepWhy        = $itemStep->where('its_type', $this->item_step_repository::ITEM_STEP_WHY_JOINE_EDGE)->limit(3)->get();
        $dataStepProcess    = $itemStep->where('its_type', $this->item_step_repository::ITEM_STEP_JOIN_EDGE)->limit(5)->get();
        $dataEvaluate       = $itemStep->where('its_type', $this->item_step_repository::ITEM_STEP_EVALUATE_EDGE)->limit(3)->get();
        $dataLevel          = $itemStep->where('its_type', $this->item_step_repository::ITEM_STEP_LEVEL_EDGE)->limit(3)->get();



        //Lấy config
        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);


        return view('users.edge', [
            'htmlMenu'          => $htmlMenu,
            'dataConfigs'       => $dataConfigs,
            'dataEdge'          => $info,
            'dataStepWhy'       => $dataStepWhy,
            'dataStepProcess'   => $dataStepProcess,
            'dataEvaluate'      => $dataEvaluate,
            'dataLevel'         => $dataLevel
        ]);
    }

    public function tuvan(){

        //Lấy config
        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        // Sự kiện dịch vụ
        $services	    = new ServiceModel();
        $dataServices   = $services->where('ser_type', $this->service_repository::SERVICE_CONSULTANCY)->get();

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);


        return view('users.tuvan', [
            'htmlMenu'      => $htmlMenu,
            'dataConfigs'   => $dataConfigs,
            'dataServices'  => $dataServices
        ]);
    }

    public function course(){

        //Lấy config
        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        // Sự kiện dịch vụ
        $services	    = new ServiceModel();
        $dataServices   = $services->where('ser_type', $this->service_repository::SERVICE_COURSE)->get();

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);


        return view('users.course', [
            'htmlMenu'      => $htmlMenu,
            'dataConfigs'   => $dataConfigs,
            'dataServices'  => $dataServices
        ]);
    }


}
