<?php

namespace App\Http\Controllers\Page;

use App\Helper\Menu as MenuHelper;
use App\Http\Controllers\Controller;
use App\Models\Configuration;
use App\Models\Statics as StaticsModel;
use Illuminate\Http\Request;

class StaticController extends Controller
{

    public function category(Request $request){

        $iCate = $request->id;

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        $limit      = 6;
        $static     = StaticsModel::where('sta_status', 1)->where('sta_category_id', $iCate)->orderBy('sta_created_time', 'DESC');

        if(!empty($iCate)){
            $static   = $static->where('sta_category_id', $iCate);
        }

        $staticBig        = $static->with(['category'])->first();

        if(!empty($staticBig)){
            $static	        = $static->with(['category'])->whereNotIn('sta_id', [$staticBig->sta_id])->paginate($limit);
        }
        else{
            $static	        = $static->with(['category'])->paginate($limit);
        }

//        dd($staticBig);

        return view('users.static', [
            'staticBig'     => $staticBig,
            'dataStatic'    => $static,
            'htmlMenu'      => $htmlMenu,
            'dataConfigs'   => $dataConfigs,
            'pagination'=> [
                                'total'			=> $static->total(),
                                'current_page'	=> $static->currentPage(),
                                'perpage'		=> $static->perPage(),
                                'links'			=> $static->total() > $static->perPage() ? $static->links() : '',
                            ],
        ]);

    }

    public function detail(Request $request){

        $dataHub        = [];
        $iStatic		= $request->id;

        $statics	    = new StaticsModel();
        if($iStatic > 0){
            $dataHub		= $statics->where('sta_status', 1)->where('sta_id', $iStatic)->first();
        }
        else{
            $dataHub		= $statics->where('sta_status', 1)->first();
            $iStatic        = $dataHub->sta_id;
        }

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        return view('users.static-detail', [
            'dataHub'       => $dataHub,
            'htmlMenu'      => $htmlMenu,
            'dataConfigs'   => $dataConfigs,
        ]);
    }

    public static function getStaticRelated($iSta){
        $limit = 6;
        $dataReturn = [];

        $events         = StaticsModel::where('sta_status', 1)->orderBy('sta_created_time', 'DESC');
        $dataReturn     = $events->whereNotIn('sta_id', [$iSta])->limit($limit)->get();

        return $dataReturn;
    }
}
