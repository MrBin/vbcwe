<?php

namespace App\Http\Controllers\Page;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GoogleAnalyticController extends Controller
{
    function index(){
        $client = new Google_Client();
        $client->setAuthConfig(__DIR__ . '/client_secrets.json');
        $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
    }
}
