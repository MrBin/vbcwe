<?php

namespace App\Http\Controllers\Page;

use App\Http\Controllers\Controller;
use Spatie\Searchable\Search;
use App\Models\News as NewsModel;
use App\Models\Service as ServiceModel;
use App\Helper\Menu as MenuHelper;
use App\Models\Configuration;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request){
        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        $keyword = $request->name;

        $searchResults = (new Search())
            ->registerModel(NewsModel::class, ['new_title', 'new_description', 'new_teaser', 'new_title_en', 'new_description_en', 'new_teaser_en']) //apply search on field name and description
            //Config partial match or exactly match
//            ->registerModel(ServiceModel::class, ['ser_title', 'ser_teaser', 'ser_description', 'ser_title_en', 'ser_teaser_en', 'ser_description_en'])
            ->perform($keyword);

        return view('users.search', [
            'htmlMenu'      => $htmlMenu,
		    'dataConfigs'   => $dataConfigs,
            'searchResults' => $searchResults,
            'keyword' => $keyword
        ]);
    }
}
