<?php

namespace App\Http\Controllers\Page;

use App\Helper\Menu as MenuHelper;
use App\Http\Controllers\Controller;
use App\Models\Configuration;
use App\Models\Events as EventsModel;
use App\Models\News as NewsModel;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function upcomingEvent(){

        $dataUpcomingEvent  = [];
        $eventsRelated      = [];

        $events 		    = EventsModel::where('eve_active', 1);
        $dataUpcomingEvent  = $events->where('eve_new', 0)->orderBy('eve_create_time', 'DESC')->first();

        if(isset($dataUpcomingEvent->eve_id) && $dataUpcomingEvent->eve_id > 0){
            $eventsRelated      = self::getEventsRelated($dataUpcomingEvent->eve_id);
        }

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        return view('users.event', [
            'dataUpcomingEvent'     => $dataUpcomingEvent,
            'htmlMenu'              => $htmlMenu,
            'dataConfigs'           => $dataConfigs,
            'eventsRelated'         => $eventsRelated
        ]);
    }

    public function previousEvent(){

        $dataPreviousEvent  = [];
        $eventsRelated      = [];

        $events 		    = EventsModel::where('eve_active', 1);
        $dataPreviousEvent  = $events->where('eve_new', 2)->orderBy('eve_create_time', 'DESC')->first();

        if(isset($dataPreviousEvent->eve_id) && $dataPreviousEvent->eve_id > 0){
            $eventsRelated      = self::getEventsRelated($dataPreviousEvent->eve_id);
        }

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        return view('users.previous-event', [
            'dataPreviousEvent'     => $dataPreviousEvent,
            'htmlMenu'              => $htmlMenu,
            'dataConfigs'           => $dataConfigs,
            'eventsRelated'         => $eventsRelated
        ]);
    }

    public function campaign(){

        $dataCampaignEvent  = [];
        $eventsRelated      = [];

        $events 		    = EventsModel::where('eve_active', 1);
        $dataCampaignEvent  = $events->where('eve_new', 1)->orderBy('eve_create_time', 'DESC')->first();

        if(isset($dataCampaignEvent->eve_id) && $dataCampaignEvent->eve_id > 0){
            $eventsRelated      = self::getEventsRelated($dataCampaignEvent->eve_id);
        }

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        return view('users.campaign-event', [
            'dataCampaignEvent'     => $dataCampaignEvent,
            'htmlMenu'              => $htmlMenu,
            'dataConfigs'           => $dataConfigs,
            'eventsRelated'         => $eventsRelated
        ]);
    }

    public function pledge(){
        $dataCampaignEvent = [];

        // Lấy menu
        $menu       = new MenuHelper();
        $dataMenu   = $menu->getAllMenu([], 0);
        $htmlMenu   = $menu->getMenuView($dataMenu);

        $configs        = new Configuration();
        $dataConfigs	= $configs->first();

        $events 		    = EventsModel::where('eve_active', 1);
        $dataCampaignEvent  = $events->where('eve_new', 3)->orderBy('eve_create_time', 'DESC')->first();

        return view('users.pledge', [
            'htmlMenu'              => $htmlMenu,
            'dataConfigs'           => $dataConfigs,
            'dataCampaignEvent'     => $dataCampaignEvent,
        ]);
    }

    public static function getEventsRelated($iEvents){
        $limit = 6;
        $dataReturn = [];

        $events         = EventsModel::where('eve_active', 1)->orderBy('eve_create_time', 'DESC');
        $dataReturn     = $events->whereNotIn('eve_id', [$iEvents])->where('eve_new', '<', 3)->limit($limit)->get();

        return $dataReturn;
    }
}
