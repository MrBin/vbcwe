<?php

namespace App\Http\Controllers\Tool;
use Illuminate\Http\Request;
use App\Helper\FaceBookHelper;
use App\Repositories\FacebookCampaignRepository;
use App\Repositories\FacebookAdsetRepository;
use App\Repositories\FacebookAdaccountRepository;
use App\Models\UserFacebook;
use App\Models\UserFacebookAdaccount;
class FacebookInsightController
{
	protected $facebook;
	protected $facebook_adset;
	protected $face_campaign;
	public function __construct(FaceBookHelper $facebook, FacebookAdsetRepository $facebook_adset, FacebookCampaignRepository $face_campaign){
		$this->facebook = $facebook;
		$this->facebook_adset = $facebook_adset;
		$this->face_campaign = $face_campaign;
	}

	public function index(Request $request){
		$userId 	= app()->USER_INFO->use_id;

		// Lấy các tk facebook user này có
		$arrFace	= \App\Models\UserFacebook::select('usf_id as id', 'usf_fb_id as fb_id','usf_fb_name as fb_name')
												->where('usf_user_id', '=', $userId)
												->get()->toArray();
		return view('module.tool.facebook_insight.index')->with(['arrFace' => $arrFace]);
	}

	public function insights(Request $request){
		$userInfo	= app()->USER_INFO;
		$userId		= $userInfo->use_id;
		$facebookId	= $request->input("facebook_id");
		$type_view	= $request->input("type_view");
		$mode_view	= $request->input("mode_view");
		$from_date	= $request->input("from_date");
		$to_date	= $request->input("to_date");

		$temp = $this->facebook::getInsights($userId, $facebookId, $type_view, $from_date, $to_date, $mode_view);

		$dataReturn = ['status' => false, 'data' => [], 'msg' => ''];
		if(isset($temp['status']) && $temp['status'] && isset($temp['data'])){
			$dataReturn['status']	= true;
			$dataReturn['data']		= $temp['data'];
		}else{
			$dataReturn['data']		= isset($temp['msg']) && $temp['msg'] ? $temp['msg'] : 'Chưa có dữ liệu';
		}

		return response()->json($dataReturn, 200);
	}


}
