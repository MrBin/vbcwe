<?php

namespace App\Http\Controllers\Tool;
use Illuminate\Http\Request;
use App\Helper\FaceBookHelper;
use App\Repositories\FacebookCampaignRepository;
use App\Repositories\FacebookAdsetRepository;
use App\Repositories\FacebookAdsRepository;
use App\Repositories\FacebookAdaccountRepository;
use App\Repositories\FacebookPostRepository;
use App\Models\UserFacebook;
use App\Models\UserFacebookAdaccount;
class FacebookAdsController
{
	protected $facebook;
	protected $facebook_adset;
	protected $facebook_ads;
	public function __construct(FaceBookHelper $facebook, FacebookAdsetRepository $facebook_adset, FacebookAdsRepository $facebook_ads){
		$this->facebook = $facebook;
		$this->facebook_adset = $facebook_adset;
		$this->facebook_ads = $facebook_ads;
	}

	public function index(Request $request){
		$userId			= app()->USER_INFO->use_id;
		$facebook_id	= $request->input('facebook_id');
		$adaccount_id	= $request->input('adaccount_id');
		$campaign_id	= $request->input('campaign_id');

		$params 	= ['campaign_id' => $campaign_id, 'facebook_id' => $facebook_id, 'adaccount_id' => $adaccount_id];

		// Lấy các tk facebook user này có
		$arrFace	= \App\Models\UserFacebook::select('usf_id as id', 'usf_fb_id as fb_id','usf_fb_name as fb_name')
												->where('usf_user_id', '=', $userId)
												->get()->toArray();

		$arrAdaccount 	= [];
		if($facebook_id > 0){
			// Lấy các tk facebook user này có
			$model			= new \App\Repositories\FacebookAdaccountRepository;
			$arrAdaccount	= $model->get([['ufad_user_facebook_id', '=', $facebook_id], ['ufad_user_id', '=', $userId]],[], ['ufad_id as id', 'ufad_adaccount_name as adaccount_name'])->toArray();
		}

		$dataFilter = [['ufad_user_id', '=', $userId]];
		if($campaign_id > 0){
			$dataFilter[] 	= ['ufad_user_campaign_id', '=', $campaign_id];
		}
		if($adaccount_id > 0){
			$dataFilter[] 	= ['ufad_user_adaccount_id', '=', $adaccount_id];
		}
		if($campaign_id > 0){
			$dataFilter[] 	= ['ufad_user_campaign_id', '=', $campaign_id];
		}

		$ads		= $this->facebook_ads->get($dataFilter, ['facebook', 'adaccount', 'campaign', 'adset']);

		return view('module.tool.facebook_ads.index')->with(['arrFace' => $arrFace, 'arrAdaccount' => $arrAdaccount, 'ads' => $ads, 'params' => $params]);
	}

	public function load_ads(Request $request){
		$userInfo		= app()->USER_INFO;
		$userId			= $userInfo->use_id;
		$user_adset_id	= $request->input("adset_id");

		$dataFilter = [['ufad_user_id', '=', $userId], ['ufad_user_adset_id', '=', $user_adset_id]];

		$ads	= $this->facebook_ads->get($dataFilter);

		return response()->json(array('status'=> true, 'data' => $ads), 200);
	}

	public function get_ads(Request $request){
		$userInfo		= app()->USER_INFO;
		$userId			= $userInfo->use_id;
		$user_adset_id	= $request->input("user_adset_id");

		$user_adaccount_id 	= 0;
		$user_facebook_id 	= 0;
		$user_campaign_id 	= 0;
		$adset_id 		= '';
		$access_token	= '';

		$info 		= $this->facebook_adset->getById($user_adset_id);

		if(!empty($info) && $info->ufa_user_id == $userId){
			$user_facebook_id	= $info->ufa_user_facebook_id;
			$user_adaccount_id	= $info->ufa_user_adaccount_id;
			$user_campaign_id	= $info->ufa_user_campaign_id;
			$adset_id			= $info->ufa_adset_id;
			$access_token		= $this->facebook->getAccessTokenFbById($user_facebook_id, $userId);
		}

		// Lấy data từ facebook
		$data = $this->facebook::getAds($adset_id, $access_token);

		if(isset($data['data']) && $data['data']){
			foreach ($data['data'] as $key => $value) {

				$id					= isset($value['id']) ? $value['id'] : '';
				$name				= isset($value['name']) ? $value['name'] : '';
				$effective_status	= isset($value['effective_status']) ? $value['effective_status'] : '';
				$status				= isset($value['status']) ? $value['status'] : '';
				$date				= isset($value['created_time']) ? $value['created_time'] : '';

				$post_id		= '';
				$user_post_id	= 0;
				if(isset($value['creative']) && isset($value['creative']['effective_object_story_id'])){
					$post_id		= $value['creative']['effective_object_story_id'];
					// Tìm user_post_id tương ứng
					$model_post = new \App\Repositories\FacebookPostRepository;
					$info_post 	= $model_post->get([
													['ufpo_post_id', '=', $post_id],
													['ufpo_user_id', '=', $userId],
													['ufpo_user_facebook_id', '=', $user_facebook_id],
												])->first();
					if(!empty($info_post)){
						$user_post_id 	= $info_post->ufpo_id;
					}
				}
				if($id != ""){
					$temp = [
							'user_facebook_id'		=> $user_facebook_id,
							'user_adaccount_id'		=> $user_adaccount_id,
							'user_campaign_id'		=> $user_campaign_id,
							'user_adset_id'			=> $user_adset_id,
							'user_id'				=> $userId,
							'ad_id'					=> $id,
							'ad_name'				=> $name,
							'ad_status'				=> $status,
							'ad_effective_status'	=> $effective_status,
							'post_id' 				=> $post_id,
							'user_post_id' 			=> $user_post_id,
							'date'					=> $date,
							'update_at'				=> time(),
						];
					$result 	= $this->facebook_ads->save($temp);
				}
			}
			return response()->json(array('status'=> true, 'data' => $data), 200);
		}else{
			return response()->json(array('status'=> false, 'msg' => 'Không tìm thấy các campaign của tài khoản này'), 200);
		}

		return response()->json(array('status'=> false));
	}

	public function insight(Request $request){
		$dataReturn 	= ['status' => false, 'data' => [], 'msg' => ''];
		$userInfo	= app()->USER_INFO;
		$userId		= $userInfo->use_id;
		$mode_view	= $request->input('mode_view');
		$from_date	= $request->input('from_date');
		$to_date	= $request->input('to_date');
		$ads_id	= $request->input("ads_id");

		$data = $this->facebook::getInsightsAds($userId, $ads_id, $from_date, $to_date, $mode_view);
		if(isset($data['status']) && $data['status'] && isset($data['data'])){
			$dataReturn['status']	= true;
			$dataReturn['data']		= $data['data'];
		}else{
			$dataReturn['msg'] 	= isset($data['msg']) && $data['msg'] ? $data['msg'] : 'Chưa có dữ liệu';
		}

		return response()->json($dataReturn, 200);
	}
}
