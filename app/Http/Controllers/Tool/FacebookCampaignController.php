<?php

namespace App\Http\Controllers\Tool;
use Illuminate\Http\Request;
use App\Helper\FaceBookHelper;
use App\Repositories\FacebookCampaignRepository;
use App\Repositories\FacebookAdaccountRepository;
use App\Repositories\FacebookAdsetRepository;
use App\Repositories\FacebookAdsRepository;
use App\Repositories\FacebookAdsPostRepository;
use App\Repositories\FacebookPostRepository;
use App\Models\UserFacebook;
use App\Models\UserFacebookAdaccount;
class FacebookCampaignController
{
	protected $facebook;
	protected $facebook_campaign;
	protected $face_adaccount;
	public function __construct(FaceBookHelper $facebook, FacebookCampaignRepository $facebook_campaign, FacebookAdaccountRepository $face_adaccount){
		$this->facebook = $facebook;
		$this->facebook_campaign = $facebook_campaign;
		$this->face_adaccount = $face_adaccount;
	}

	public function index(Request $request){
		$userId				= app()->USER_INFO->use_id;
		$user_facebook_id	= $request->input('fillter_facebook');
		$user_adaccount_id	= $request->input('fillter_adaccount');
		$campaign_id		= $request->input('campaign_id');

		$params 	= ['campaign_id' => $campaign_id, 'fillter_facebook' => $user_facebook_id, 'fillter_adaccount' => $user_adaccount_id];

		// Lấy các tk facebook user này có
		$arrFace	= \App\Models\UserFacebook::select('usf_id as id', 'usf_fb_id as fb_id','usf_fb_name as fb_name')->where('usf_user_id', '=', $userId)->get()->toArray();

		$arrAdaccount 	= [];
		if($user_facebook_id > 0){
			// Lấy các tk facebook user này có
			$arrAdaccount 		= $this->face_adaccount->get([['ufad_user_facebook_id', '=', $user_facebook_id], ['ufad_user_id', '=', $userId]],[], ['ufad_id as id', 'ufad_adaccount_name as adaccount_name'])->toArray();
		}
		$dataFilter 	= [['uac_user_id', '=', $userId]];
		if($campaign_id != "") $dataFilter[] = ['uac_campaign_id', '=', $campaign_id];
		if($user_facebook_id > 0) $dataFilter[] = ['uac_user_facebook_id', '=', $user_facebook_id];
		if($user_adaccount_id > 0) $dataFilter[] = ['uac_user_adaccount_id', '=', $user_adaccount_id];

		$campaigns		= $this->facebook_campaign->get($dataFilter);
		return view('module.tool.facebook_campaign.index')->with(['arrFace' => $arrFace, 'campaigns' => $campaigns, 'arrAdaccount' => $arrAdaccount, 'params' => $params]);
	}

	public function load_adaccount(Request $request){
		$userInfo			= app()->USER_INFO;
		$userId				= $userInfo->use_id;
		$user_facebook_id	= $request->input("fb_id");

		$adaccounts 	= \App\Models\UserFacebookAdaccount::select('ufad_id as id', 'ufad_adaccount_id as adaccount_id', 'ufad_adaccount_name as adaccount_name')
						->where('ufad_user_id', '=', $userId)
						->where('ufad_user_facebook_id', '=', $user_facebook_id)->get()->toArray();
		return response()->json(array('status'=> true, 'data' => $adaccounts), 200);
	}

	public function load_adset(Request $request){
		$userInfo			= app()->USER_INFO;
		$userId				= $userInfo->use_id;
		$user_campaign_id	= $request->input("campaign_id");

		$dataFilter = [['ufa_user_id', '=', $userId], ['ufa_user_campaign_id', '=', $user_campaign_id]];

		$model 		= new \App\Repositories\FacebookAdsetRepository;
		$adsets		= $model->get($dataFilter, ['facebook', 'adaccount', 'campaign']);

		return response()->json(array('status'=> true, 'data' => $adsets), 200);
	}

	public function get_campaign(Request $request){
		$userInfo			= app()->USER_INFO;
		$userId				= $userInfo->use_id;
		$user_facebook_id	= $request->input("fb_id");
		$user_adaccount_id	= $request->input("adaccount_id");

		$adaccount_id	= '';
		$access_token	= '';

		$info 		= $this->face_adaccount->getById($user_adaccount_id);
		if(!empty($info) && $info->ufad_user_id == $userId && $info->ufad_user_facebook_id == $user_facebook_id){
			$adaccount_id 	= $info->ufad_adaccount_id;
			$access_token	= $this->facebook->getAccessTokenFbById($user_facebook_id, $userId);
		}

		// Lấy data từ facebook
		$data = $this->facebook::getCampaign($adaccount_id, $access_token);

		if(isset($data['data']) && $data['data']){
			foreach ($data['data'] as $key => $value) {

				$id		= isset($value['id']) ? $value['id'] : '';
				$name	= isset($value['name']) ? $value['name'] : '';
				$status	= isset($value['effective_status']) ? $value['effective_status'] : '';
				$date	= isset($value['created_time']) ? $value['created_time'] : '';

				if($id != ""){
					$temp = [
							'user_facebook_id' 	=> $user_facebook_id,
							'user_adaccount_id'	=> $user_adaccount_id,
							'adaccount_id'		=> $adaccount_id,
							'user_id'			=> $userId,
							'campaign_id'		=> $id,
							'campaign_name'		=> $name,
							'campaign_status'	=> $status,
							'date'				=> $date,
							'update_at'			=> time(),
						];
					$result 	= $this->facebook_campaign->save($temp);
				}
			}
			return response()->json(array('status'=> true, 'data' => $data), 200);
		}else{
			return response()->json(array('status'=> false, 'msg' => 'Không tìm thấy các campaign của tài khoản này'), 200);
		}

		return response()->json(array('status'=> false));
	}

	/**
	 * [get_post description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function get_post(Request $request){
		$userInfo			= app()->USER_INFO;
		$userId				= $userInfo->use_id;
		$user_campaign_id	= $request->input("campaign_id");

		$user_adaccount_id 	= 0;
		$user_facebook_id 	= 0;
		$adaccount_id	= '';
		$campaign_id 	= '';
		$access_token	= '';

		$info 		= $this->facebook_campaign->getById($user_campaign_id);

		if(!empty($info) && $info->uac_user_id == $userId){
			$user_facebook_id	= $info->uac_user_facebook_id;
			$user_adaccount_id	= $info->uac_user_adaccount_id;
			$adaccount_id		= $info->uac_adaccount_id;
			$campaign_id		= $info->uac_campaign_id;
			$access_token		= $this->facebook->getAccessTokenFbById($user_facebook_id, $userId);
		}

		// Tìm các nhóm quảng cáo của campaign này
		$model 		= new \App\Repositories\FacebookAdsetRepository;
		$dataAdset 	= $model->get([['ufa_user_id' , '=', $userId], ['ufa_user_campaign_id', '=', $user_campaign_id]]);

		if(!empty($dataAdset)){
			foreach ($dataAdset as $key => $value) {
				$adset_id 	= $value->ufa_adset_id;
				// Lấy data từ facebook
				$data = $this->facebook::getPostOfAds($adset_id, $access_token);

				if(isset($data['ads']) && isset($data['ads']['data'])){
					foreach ($data['ads']['data'] as $key_ads => $value_ads) {
						if(isset($value_ads['id']) && isset($value_ads['creative']) && isset($value_ads['creative']['effective_object_story_id'])){
							$post_id 	= $value_ads['creative']['effective_object_story_id'];
							$ad_id 		= $value_ads['id'];
							// Tìm ad id tương ứng
							$model_ads 	= new \App\Repositories\FacebookAdsRepository;
							$info_ads 	= $model_ads->get([
															['ufad_ad_id', '=', $ad_id],
															['ufad_user_id', '=', $userId],
															['ufad_user_adset_id', '=', $value['ufa_id']],
															['ufad_user_campaign_id', '=', $user_campaign_id],
															['ufad_user_adaccount_id', '=', $user_adaccount_id],
															['ufad_user_facebook_id', '=', $user_facebook_id],
														])->first();

							if(!empty($info_ads)){
								$user_ad_id 	= $info_ads->ufad_id;
								$temp = [
											'user_facebook_id'	=> $user_facebook_id,
											'user_adaccount_id'	=> $user_adaccount_id,
											'user_ad_id'		=> $user_ad_id,
											'user_adset_id'		=> $value['ufa_id'],
											'user_campaign_id'	=> $user_campaign_id,
											'user_id'			=> $userId,
											'ad_id'				=> $ad_id,
											'post_id'			=> $post_id,
											'update_at'			=> time(),
										];

								$model_post 	= new \App\Repositories\FacebookAdsPostRepository;
								$result 	= $model_post->save($temp);
							}
						}
					}
				}
			}
		}else{
			return response()->json(array('status'=> false, 'msg' => 'Chưa có các nhóm quảng cáo của chiến dịch này'), 200);
		}

		return response()->json(array('status'=> true), 200);
	}

	public function getInsight(Request $request, $id_ads, $date){
		$userInfo			= app()->USER_INFO;
		$userId				= $userInfo->use_id;

		$a = $this->facebook::getInsightsAdsOneDay($userId, $id_ads, $date);
		dd($a);
	}
}
