<?php

namespace App\Http\Controllers\Tool;
use Illuminate\Http\Request;
use App\Helper\FaceBookHelper;
use App\Repositories\FacebookPostRepository;
use App\Repositories\FacebookPageRepository;
use App\Models\UserFacebook;
use App\Models\UserFacebookPage;
class FacebookPostController
{
	protected $facebook;
	protected $facebook_post;
	protected $facebook_page;
	public function __construct(FaceBookHelper $facebook, FacebookPostRepository $facebook_post, FacebookPageRepository $facebook_page){
		$this->facebook = $facebook;
		$this->facebook_post = $facebook_post;
		$this->facebook_page = $facebook_page;
	}

	public function index(Request $request){
		$userId 	= app()->USER_INFO->use_id;
		// Lấy các tk facebook user này có
		$arrFace	= \App\Models\UserFacebook::select('usf_id as id', 'usf_fb_id as fb_id','usf_fb_name as fb_name')->where('usf_user_id', '=', $userId)->get()->toArray();

		$posts		= $this->facebook_post->get([['ufpo_user_id', '=', $userId]], ['facebook', 'page']);
		return view('module.tool.facebook_page.post')->with(['arrFace' => $arrFace, 'posts' => $posts]);
	}

	public function load_pages(Request $request){
		$userInfo			= app()->USER_INFO;
		$userId				= $userInfo->use_id;
		$user_facebook_id	= $request->input("fb_id");

		$pages 		= \App\Models\UserFacebookPage::select('ufp_id as id', 'ufp_page_id as page_id', 'ufp_page_name as page_name')
						->where('ufp_user_id', '=', $userId)
						->where('ufp_user_facebook_id', '=', $user_facebook_id)->get()->toArray();
		return response()->json(array('status'=> true, 'data' => $pages), 200);
	}

	public function get_post(Request $request){
		$userInfo	= app()->USER_INFO;
		$userId		= $userInfo->use_id;
		$user_facebook_id	= $request->input("fb_id");
		$user_page_id		= $request->input("page_id");

		$fb_id			= '';
		$page_id		= '';
		$access_token	= '';

		$info 		= $this->facebook_page->getById($user_page_id);
		if(!empty($info) && $info->ufp_user_id == $userId && $info->ufp_user_facebook_id == $user_facebook_id){
			$fb_id			= $info->ufp_fb_id;
			$page_id		= $info->ufp_page_id;
			$access_token	= $info->ufp_page_access_token;
		}

		// Lấy data từ facebook
		$data = $this->facebook::getPost($page_id, $access_token);

		if(isset($data['data']) && $data['data']){
			foreach ($data['data'] as $key => $value) {

				$id			= isset($value['id']) ? $value['id'] : '';
				$message	= isset($value['message']) ? $value['message'] : '';
				$story		= isset($value['story']) ? $value['story'] : '';
				$date		= isset($value['created_time']) ? $value['created_time'] : '';

				if($id != ""){
					$temp = [
							'user_facebook_id'	=> $user_facebook_id,
							'user_page_id'		=> $user_page_id,
							'user_id'			=> $userId,
							'fb_id'				=> $fb_id,
							'page_id'			=> $page_id,
							'post_id'			=> $id,
							'post_message'		=> $message,
							'post_story'		=> $story,
							'date'				=> $date,
							'update_at'			=> time(),
						];
					$result 	= $this->facebook_post->save($temp);
					var_dump($result);
				}
			}
			return response()->json(array('status'=> true, 'data' => $data), 200);
		}else{
			return response()->json(array('status'=> false, 'msg' => 'Không tìm thấy các adaccount của tài khoản này'), 200);
		}

		return response()->json(array('status'=> false));
	}
}
