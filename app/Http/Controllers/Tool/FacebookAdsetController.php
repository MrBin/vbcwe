<?php

namespace App\Http\Controllers\Tool;
use Illuminate\Http\Request;
use App\Helper\FaceBookHelper;
use App\Repositories\FacebookCampaignRepository;
use App\Repositories\FacebookAdsetRepository;
use App\Repositories\FacebookAdaccountRepository;
use App\Models\UserFacebook;
use App\Models\UserFacebookAdaccount;
class FacebookAdsetController
{
	protected $facebook;
	protected $facebook_adset;
	protected $face_campaign;
	public function __construct(FaceBookHelper $facebook, FacebookAdsetRepository $facebook_adset, FacebookCampaignRepository $face_campaign){
		$this->facebook = $facebook;
		$this->facebook_adset = $facebook_adset;
		$this->face_campaign = $face_campaign;
	}

	public function index(Request $request, $campaign_id = 0){
		$userId 	= app()->USER_INFO->use_id;

		// Lấy các tk facebook user này có
		$arrFace	= \App\Models\UserFacebook::select('usf_id as id', 'usf_fb_id as fb_id','usf_fb_name as fb_name')
												->where('usf_user_id', '=', $userId)
												->get()->toArray();

		$dataFilter = [['ufa_user_id', '=', $userId]];
		if($campaign_id > 0){
			$dataFilter[] 	= ['ufa_user_campaign_id', '=', $campaign_id];
		}
		$adsets		= $this->facebook_adset->get($dataFilter, ['facebook', 'adaccount', 'campaign']);

		return view('module.tool.facebook_adset.index')->with(['arrFace' => $arrFace, 'adsets' => $adsets]);
	}

	public function get_adset(Request $request){
		$userInfo			= app()->USER_INFO;
		$userId				= $userInfo->use_id;
		$user_campaign_id	= $request->input("user_campaign_id");

		$user_adaccount_id 	= 0;
		$user_facebook_id 	= 0;
		$adaccount_id	= '';
		$campaign_id 	= '';
		$access_token	= '';

		$info 		= $this->face_campaign->getById($user_campaign_id);

		if(!empty($info) && $info->uac_user_id == $userId){
			$user_facebook_id	= $info->uac_user_facebook_id;
			$user_adaccount_id	= $info->uac_user_adaccount_id;
			$adaccount_id		= $info->uac_adaccount_id;
			$campaign_id		= $info->uac_campaign_id;
			$access_token		= $this->facebook->getAccessTokenFbById($user_facebook_id, $userId);
		}

		// Lấy data từ facebook
		$data = $this->facebook::getAdset($campaign_id, $access_token);

		if(isset($data['data']) && $data['data']){
			foreach ($data['data'] as $key => $value) {

				$id		= isset($value['id']) ? $value['id'] : '';
				$name	= isset($value['name']) ? $value['name'] : '';
				$status	= isset($value['effective_status']) ? $value['effective_status'] : '';
				$date	= isset($value['created_time']) ? $value['created_time'] : '';

				if($id != ""){
					$temp = [
							'user_facebook_id'	=> $user_facebook_id,
							'user_adaccount_id'	=> $user_adaccount_id,
							'user_campaign_id'	=> $user_campaign_id,
							'adaccount_id'		=> $adaccount_id,
							'campaign_id'		=> $campaign_id,
							'user_id'			=> $userId,
							'adset_id'			=> $id,
							'adset_name'		=> $name,
							'adset_status'		=> $status,
							'date'				=> $date,
							'update_at'			=> time(),
						];
					$result 	= $this->facebook_adset->save($temp);
				}
			}
			return response()->json(array('status'=> true, 'data' => $data), 200);
		}else{
			return response()->json(array('status'=> false, 'msg' => 'Không tìm thấy các campaign của tài khoản này'), 200);
		}

		return response()->json(array('status'=> false));
	}
}
