<?php

namespace App\Http\Controllers\Tool;
use Illuminate\Http\Request;
use App\Helper\FaceBookHelper;
use App\Repositories\FacebookAdaccountRepository;
use App\Repositories\UserFacebookRepository;
use App\Models\UserFacebook;
class FacebookAdaccountController
{
	protected $facebook;
	protected $user_facebook;
	protected $facebook_adaccount;
	public function __construct(FaceBookHelper $facebook, FacebookAdaccountRepository $facebook_adaccount, UserFacebookRepository $user_facebook){
		$this->facebook				= $facebook;
		$this->facebook_adaccount	= $facebook_adaccount;
		$this->user_facebook		= $user_facebook;
	}

	public function index(Request $request){
		$userId 	= app()->USER_INFO->use_id;
		// Lấy các tk facebook user này có
		$arrFace	= \App\Models\UserFacebook::select('usf_id as id', 'usf_fb_id as fb_id','usf_fb_name as fb_name')->where('usf_user_id', '=', $userId)->get()->toArray();
		$temp		= $this->facebook_adaccount->get([['ufad_user_id', '=', $userId]]);
		$adAccounts	= [];
		foreach ($temp as $key => $value) {
			if(!isset($pages[$value->ufad_fb_id])) $pages[$value->ufad_fb_id] 	= [];
			$adAccounts[$value->ufad_fb_id][] = $value;
		}

		return view('module.tool.facebook_adaccount.index')->with(['arrFace' => $arrFace, 'adAccounts' => $adAccounts]);
	}

	public function get_adaccount(Request $request){
		$userInfo		= app()->USER_INFO;
		$userId			= $userInfo->use_id;
		$userFacebookId	= $request->input("user_facebook_id");

		$data = $this->facebook::getAdaccount($userId, $userFacebookId);

		if(isset($data['status']) && $data['status']){
			return response()->json(array('status'=> true), 200);
		}else{
			$msg 	= (isset($data['msg']) && $data['msg']) ? $data['msg'] : 'Get Adaccount facebook error!';
			return response()->json(array('status'=> false, 'msg' => $msg), 200);
		}

		return response()->json(array('status'=> false));
	}
}
