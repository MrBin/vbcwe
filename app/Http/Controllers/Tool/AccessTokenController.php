<?php

namespace App\Http\Controllers\Tool;
use Illuminate\Http\Request;
use App\Helper\FaceBookHelper;
use App\Repositories\UserFacebookRepository;
//use Redis;
class AccessTokenController
{
	protected $facebook;
	public function __construct(FaceBookHelper $facebook){
		$this->facebook = $facebook;
	}

	public function index(Request $request){
		$url_redirect 	= urlencode(route('tool.index'));
		$url_redirect	= urldecode($request->input('url_redirect'));

		return view('module.tool.access_token.index')->with(['url_redirect' => $url_redirect]);
	}

	public function info(Request $request){
		$userInfo 		= app()->USER_INFO;
		$userId			= $request->input("user_id");
		$accessToken	= $request->input("access_token");

		$data = $this->facebook::getInfoUser($userId, $accessToken);
		if(isset($data['id']) && $data['id']){

			// Lấy tiếp long access token
			$temp = $this->facebook::getLongAccessToken($accessToken);
			if(isset($temp['access_token'])) $accessToken = $temp['access_token'];
			// Lưu vào data base
			$data = [
					'user_id'		=> $userInfo->use_id,
					'access_token'	=> $accessToken,
					'token_expire' 	=> time() + 5184000,
					'fb_id'			=> $data['id'],
					'fb_name'		=> $data['name'],
					'created_time'	=> time(),
					'update_at'		=> time(),
					'active'		=> 1,
					];

			$access_token_repo 	= new UserFacebookRepository();
			$result 	= $access_token_repo->save($data);

			if(isset($result['status']) && $result['status'] == true){
				return response()->json(array('status'=> true, 'data' => $data), 200);
			}else{
				return response()->json(array('status'=> false, 'msg' => 'Xảy ra lỗi khi cập nhật mã truy cập facebook. Vui lòng thử lại'), 200);
			}
		}else{
			return response()->json(array('status'=> false, 'msg' => 'Không tìm thấy thông tin tài khoản facebook của bạn. Vui lòng thử lại'), 200);
		}

		return response()->json(array('status'=> false));
	}

	public function getLongAccessToken(Request $request){
		$userId 	='';
	}
}
