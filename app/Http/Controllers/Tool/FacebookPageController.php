<?php

namespace App\Http\Controllers\Tool;
use Illuminate\Http\Request;
use App\Helper\FaceBookHelper;
use App\Repositories\FacebookPageRepository;
use App\Repositories\UserFacebookRepository;
use App\Models\UserFacebook;
class FacebookPageController
{
	protected $facebook;
	protected $facebook_page;
	protected $user_facebook;
	public function __construct(FaceBookHelper $facebook, FacebookPageRepository $facebook_page, UserFacebookRepository $user_facebook){
		$this->facebook = $facebook;
		$this->facebook_page = $facebook_page;
		$this->user_facebook		= $user_facebook;
	}

	public function index(Request $request){

		$userId 	= app()->USER_INFO->use_id;
		// Lấy các tk facebook user này có
		$arrFace	= \App\Models\UserFacebook::select('usf_id as id', 'usf_fb_id as fb_id','usf_fb_name as fb_name')->where('usf_user_id', '=', $userId)->get()->toArray();

		$dataSearch = [['ufp_user_id', '=', $userId]];

		$temp	= $this->facebook_page->get($dataSearch);
		$pages	= [];
		foreach ($temp as $key => $value) {
			if(!isset($pages[$value->ufp_fb_id])) $pages[$value->ufp_fb_id] 	= [];
			$pages[$value->ufp_fb_id][] = $value;
		}


		return view('module.tool.facebook_page.index')->with(['arrFace' => $arrFace, 'pages' => $pages]);
	}

	public function get_page(Request $request){
		$userInfo		= app()->USER_INFO;
		$userId			= $userInfo->use_id;
		$userFacebookId	= $request->input("user_facebook_id");

		$data	= $this->facebook::getPages($userId, $userFacebookId);

		if(isset($data['status']) && $data['status']){
			return response()->json(array('status'=> true), 200);
		}else{
			$msg 	= (isset($data['msg']) && $data['msg']) ? $data['msg'] : 'Get Pages facebook error!';
			return response()->json(array('status'=> false, 'msg' => $msg), 200);
		}

		return response()->json(array('status'=> false));
	}
}
