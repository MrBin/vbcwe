<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper\Upload;
class UploadController extends Controller
{
	protected $upload;

	public function __construct(Upload $upload){
		$this->upload 	= $upload;
	}

	public function upload(Request $request){
		$type 	= $request->input('uploadType');
		$file 	= $request->file('Filedata');

		$result 	= $this->upload::store($file, $type);

		return response()->json($result, 200);
	}

	public function get(Request $request){
		echo "geted";
	}
}
