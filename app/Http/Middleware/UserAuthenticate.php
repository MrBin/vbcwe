<?php

namespace App\Http\Middleware;

use Closure;
use Route;
use App\Helper\Users;
class UserAuthenticate
{
	protected $users;

	public function __construct(Users $users){
		$this->users 	= $users;
	}
	 /**
	  * Handle an incoming request.
	  *
	  * @param  \Illuminate\Http\Request  $request
	  * @param  \Closure  $next
	  * @return mixed
	  */
	 public function handle($request, Closure $next)
	 {

		if(!$this->users::isLogged()){
			return redirect()->route('authen.login');
		}else{
			// Lấy thông tin
			$info = $this->users::getInfoLogged();

			if($info){
				// Lưu thông tin
				app()->singleton('USER_INFO', function() use ($info){
		            return $info;
		       	});

            	/*// Lấy menu
            	$menuUser 	= $this->users::getMenuLogged();
            	// Lưu thông tin
                app()->singleton('USER_MENU', function() use ($menuUser){
                    return $menuUser;
                });*/

			}else{
				$nameRouteCurrent = Route::currentRouteName();
					return redirect()->route('authen.login');

				return redirect()->route('access_denied');
			}
		}

		return $next($request);
	 }
}