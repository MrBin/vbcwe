<?php

namespace App\Http\Middleware;

use App\Helper\UserTranslate as UserTranslate;
use Closure;
use Illuminate\Support\Facades\Config;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locales = Config::get('app.locales');
        $locale = UserTranslate::getLangCurrent();

        if ($locale && array_key_exists($locale, $locales)) {
            app()->setLocale($locales[$locale]);
        }
        return $next($request);
    }
}
