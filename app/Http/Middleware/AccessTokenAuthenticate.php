<?php

namespace App\Http\Middleware;

use Closure;
use Route;
use App\Repositories\UserFacebookRepository;
class AccessTokenAuthenticate
{
	protected $access_token;

	public function __construct(UserFacebookRepository $access_token){
		$this->access_token 	= $access_token;
	}
	 /**
	  * Handle an incoming request.
	  *
	  * @param  \Illuminate\Http\Request  $request
	  * @param  \Closure  $next
	  * @return mixed
	  */
	 public function handle($request, Closure $next)
	 {
	 	$userInfo 	= app()->USER_INFO;
	 	// Lấy accessToken
	 	$accessToken 	= $this->access_token->getByUserId($userInfo->use_id);
	 	if(!$accessToken){
	 		$url_current	= url()->current();
			$url_redirect 	= route('tool.facebook.accesstoken'). "?url_redirect=" . urlencode($url_current);
			return redirect($url_redirect);
		}


		return $next($request);
	 }
}