<?php

namespace App\Http\Middleware;

use Closure;
use Route;
use App\Helper\Users;
class ToolAuthenticate
{
	protected $user;

	public function __construct(Users $user){
		$this->user 	= $user;
	}
	 /**
	  * Handle an incoming request.
	  *
	  * @param  \Illuminate\Http\Request  $request
	  * @param  \Closure  $next
	  * @return mixed
	  */
	 public function handle($request, Closure $next)
	 {

		if(!$this->user::isLogged()){
			$url_current	= url()->current();
			$url_redirect 	= route('authen.login'). "?url_redirect=" . urlencode($url_current);
			return redirect($url_redirect);
		}else{
			// Lấy thông tin
			$info = $this->user::getInfoLogged();

			if($info){
				// Lưu thông tin
				app()->singleton('USER_INFO', function() use ($info){
		            return $info;
		       	});

			}else{
				return redirect()->route('access_denied');
			}
		}

		return $next($request);
	 }
}