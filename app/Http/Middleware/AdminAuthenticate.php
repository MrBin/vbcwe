<?php

namespace App\Http\Middleware;

use Closure;
use Route;
use App\Helper\AdminUser;
class AdminAuthenticate
{
	protected $admin_user;

	public function __construct(AdminUser $admin_user){
		$this->admin_user 	= $admin_user;
	}
	 /**
	  * Handle an incoming request.
	  *
	  * @param  \Illuminate\Http\Request  $request
	  * @param  \Closure  $next
	  * @return mixed
	  */
	 public function handle($request, Closure $next)
	 {

		if(!$this->admin_user::isLogged()){
			return redirect()->route('auth_admin.login');
		}else{
			// Lấy thông tin
			$info = $this->admin_user::getInfoLogged();

			if($info){
				// Lưu thông tin
				app()->singleton('ADMIN_INFO', function() use ($info){
		            return $info;
		       	});

				// Gọi api lấy tất cả các quyền
            	$staffRole 		=  $this->admin_user::getPermissionLogged();

            	// Lưu thông tin quyền vào đây để tranh phải gọi api nhiều lần
                app()->singleton('ADMIN_ROLE', function() use ($staffRole){
                    return $staffRole;
                });

            	// Lấy menu
            	$menuAdmin 	= $this->admin_user::getMenuLogged();
            	// Lưu thông tin
                app()->singleton('ADMIN_MENU', function() use ($menuAdmin){
                    return $menuAdmin;
                });

				// Check quyền vào module
				$nameRouteCurrent = Route::currentRouteName();
				//dd($nameRouteCurrent);
				$routeNotCheckPermission = $this->admin_user::routeNotCheckPermission();
				if(!in_array($nameRouteCurrent, $routeNotCheckPermission)) {
					$temp	= explode(".", $nameRouteCurrent);
					$module	= isset($temp[1]) ? $temp[1] : '';
					$per	= isset($temp[2]) ? $temp[2] : '';

					if(!$this->admin_user::checkPermission($module, $per)){
						return redirect()->route('access_denied');
					}
				}
			}else{
				$nameRouteCurrent = Route::currentRouteName();
				if($nameRouteCurrent == 'admin.home'){
					//return redirect()->route('auth_admin.login');
				}
				return redirect()->route('auth_admin.login');
			}
		}

		return $next($request);
	 }
}