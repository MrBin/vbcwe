<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpinionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opinion', function (Blueprint $table) {
            $table->id('opi_id');
            $table->string('opi_title')->default('');
            $table->string('opi_name')->default('');
            $table->string('opi_lang')->default('');
            $table->smallInteger('opi_status')->default(0);
            $table->smallInteger('opi_order')->default(0);
            $table->text('opi_description')->default('');
            $table->integer('opi_created_time')->default(0);
            $table->integer('opi_updated_at')->default(0);
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opinion');
    }
}
