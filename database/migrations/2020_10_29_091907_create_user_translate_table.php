<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTranslateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_translate', function (Blueprint $table) {
            $table->id('ust_id');
            $table->string('ust_keyword');
            $table->string('ust_keyword_md5');
            $table->string('ust_vi');
            $table->string('ust_en');
            $table->integer('ust_created_time');
            $table->integer('ust_updated_at');
            $table->unique('ust_keyword_md5', 'key_unique');
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_translate');
    }
}
