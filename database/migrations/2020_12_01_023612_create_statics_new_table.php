<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaticsNewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statics_new', function (Blueprint $table) {
            $table->id('sta_id');
            $table->integer('category_id')->default(0);
            $table->string('sta_title')->default('');
            $table->string('sta_title_en')->default('');
            $table->string('sta_name_rewrite')->default('');
            $table->integer('sta_order')->default(0);
            $table->smallInteger('sta_status')->default(0);
            $table->string('sta_picture')->default('');
            $table->string('sta_picture_en')->default('');
            $table->text('sta_teaser')->default('');
            $table->text('sta_teaser_en')->default('');
            $table->text('sta_description')->default('');
            $table->text('sta_description_en')->default('');
            $table->string('sta_meta_title')->default('');
			$table->string('sta_meta_title_en')->default('');
			$table->text('sta_meta_keyword')->default('');
			$table->text('sta_meta_keyword_en')->default('');
			$table->text('sta_meta_description')->default('');
			$table->text('sta_meta_description_en')->default('');

            $table->integer('sta_created_time')->default(0);
            $table->integer('sta_updated_at')->default(0);
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statics');
    }
}
