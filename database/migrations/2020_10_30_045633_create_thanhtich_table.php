<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThanhtichTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thanhtich', function (Blueprint $table) {
            $table->id('tht_id');
            $table->string('tht_index')->default('');
            $table->string('tht_title')->default('');
            $table->string('tht_title_en')->default('');
            $table->string('tht_icon')->default('');
            $table->integer('tht_order')->default(0);
            $table->smallInteger('tht_status')->default(0);
            $table->integer('tht_created_time')->default(0);
            $table->integer('tht_updated_at')->default(0);
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('thanhtich');
    }
}
