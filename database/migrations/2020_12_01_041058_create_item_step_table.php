<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemStepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_step', function (Blueprint $table) {
            $table->id('its_id');
            $table->string('its_title')->default('');
            $table->string('its_title_en')->default('');
            $table->smallInteger('its_type')->default(0);
            $table->integer('its_order')->default(0);
            $table->smallInteger('its_status')->default(0);
            $table->string('its_picture')->default('');
            $table->string('its_picture_en')->default('');
            $table->text('its_teaser')->default('');
            $table->text('its_teaser_en')->default('');
            $table->integer('its_created_time')->default(0);
            $table->integer('its_updated_at')->default(0);
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_step');
    }
}
