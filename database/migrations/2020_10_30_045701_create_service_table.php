<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service', function (Blueprint $table) {
            $table->id('ser_id');
            $table->string('ser_title')->default('');
            $table->string('ser_title_en')->default('');
            $table->string('ser_icon')->default('');
            $table->string('ser_icon_en')->default('');
            $table->integer('ser_order')->default(0);
            $table->smallInteger('ser_status')->default(0);
            $table->string('ser_picture')->default('');
            $table->string('ser_picture_en')->default('');
            $table->text('ser_teaser')->default('');
            $table->text('ser_teaser_en')->default('');
            $table->text('ser_description')->default('');
            $table->text('ser_description_en')->default('');
            $table->string('ser_meta_title')->default('');
			$table->string('ser_meta_title_en')->default('');
			$table->text('ser_meta_keyword')->default('');
			$table->text('ser_meta_keyword_en')->default('');
			$table->text('ser_meta_description')->default('');
			$table->text('ser_meta_description_en')->default('');

            $table->integer('ser_created_time')->default(0);
            $table->integer('ser_updated_at')->default(0);
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
