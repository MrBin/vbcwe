<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact', function (Blueprint $table) {
            $table->id('con_id');
            $table->string('con_title')->default('');
            $table->string('con_name')->default('');
            $table->string('con_email')->default('');
            $table->smallInteger('con_status')->default(0);
            $table->text('con_description')->default('');
            $table->integer('con_created_time')->default(0);
            $table->integer('con_updated_at')->default(0);
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact');
    }
}
