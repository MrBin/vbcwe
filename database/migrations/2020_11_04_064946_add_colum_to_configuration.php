<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumToConfiguration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configuration', function (Blueprint $table) {
        	$table->string('con_banner_thamgia_mangluoi')->default('');
            $table->string('con_banner_thamgia_mangluoi_en')->default('');
            $table->string('con_title_thamgia_mangluoi')->default('');
            $table->string('con_title_thamgia_mangluoi_en')->default('');
            $table->text('con_description_thamgia_mangluoi')->default('');
            $table->text('con_description_thamgia_mangluoi_en')->default('');
            $table->string('con_banner_kienthuc_thamkhao')->default('');
            $table->string('con_banner_kienthuc_thamkhao_en')->default('');
            $table->string('con_title_kienthuc_thamkhao')->default('');
            $table->string('con_title_kienthuc_thamkhao_en')->default('');
            $table->text('con_description_kienthuc_thamkhao')->default('');
            $table->text('con_description_kienthuc_thamkhao_en')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('configuration', function (Blueprint $table) {
            //
        });
    }
}
