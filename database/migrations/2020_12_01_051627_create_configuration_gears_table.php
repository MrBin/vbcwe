<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigurationGearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuration_gears', function (Blueprint $table) {
            $table->id('cog_id');
			$table->string('cog_text_1')->default('');
			$table->string('cog_text_1_en')->default('');
			$table->string('cog_text_2_1')->default('');
			$table->string('cog_text_2_1_en')->default('');
			$table->text('cog_text_2_2')->default('');
			$table->text('cog_text_2_2_en')->default('');
			$table->string('cog_picture_2')->default('');
			$table->string('cog_picture_2_en')->default('');
			$table->string('cog_text_3')->default('');
			$table->string('cog_text_3_en')->default('');
			$table->string('cog_text_4')->default('');
			$table->string('cog_text_4_en')->default('');
			$table->string('cog_text_5')->default('');
			$table->string('cog_text_5_en')->default('');
			$table->string('cog_text_6')->default('');
			$table->string('cog_text_6_en')->default('');
			$table->string('cog_text_7')->default('');
			$table->string('cog_text_7_en')->default('');
			$table->string('cog_picture_7')->default('');
			$table->string('cog_picture_7_en')->default('');
			$table->integer('cog_updated_at')->default(0);
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configuration_gears');
    }
}
